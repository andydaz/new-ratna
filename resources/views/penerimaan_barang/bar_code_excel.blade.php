<!DOCTYPE html>
<html>
<body>
<table id="" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <tr>
        <th>Nama Barang</th>
        <th>No Barcode</th>
        <th>Harga Jual</th>
        <th>NO PO</th>
    </tr>
    <tbody>
    @foreach($product as $key => $value)
        <tr >
            <td>{{$value->b_name.' '.$value->t_name}}</td>
            <td>{{$value->bar_code}}</td>
            <td>{{$value->price_sale}}</td>
            <td>{{$value->purchase_order_number}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>