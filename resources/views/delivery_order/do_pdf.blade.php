<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
</style>
<div>
  <h3 style="text-align: center;">TOKO MATERIAL BU MERRY</h3>
  <h4 style="text-align: center;">DELIVERY ORDER </h4>
  <hr>
  <table style ="width:100%">
    <tr>
      <td style="width: 25%">NO</td>
      <td style="width: 25%">: {{$doheader->delivery_order_number}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Tanggal DO</td>
      <td>: {{$doheader->date_delivery_order}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Kepada</td>
      <td>: {{$doheader->company_name}}</td>
      <td></td>
    </tr>
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
      <tr style="background-color: #8B8C89">
        <th class="color" style="text-align: center;">No</th>
        <th class="color" style="text-align: center;">Kode</th>
        <th class="color" style="text-align: center;">Satuan</th>
        <th class="color" style="text-align: center;">Nama Barang</th>
        <th class="color" style="text-align: center;">Quantity</th>
      </tr>
    </thead>
    <tbody >
      @foreach($dodetail as $key => $value)
      <tr >
        <td>
          <span>{{$key+1}}</span>
        </td>
        <td>
          <span>{{$value->product_code}}</span>
        </td>
        <td>
          <span>{{$value->unit_description}}</span>
        </td>
        <td>
          <span>{{$value->product_name}}</span>
        </td>
        <td>
          <span>{{$value->quantity_sent}}</span>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div>
    <div style="text-align: right; margin-top: 80px">______________</div>
    <div style="text-align: right;">Tanda Tangan</div>
  </div>
</div>
</div>