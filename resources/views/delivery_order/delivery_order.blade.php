<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <!-- <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text"><i class="material-icons">search</i>Cari Do Barang</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row margin-top">
                                <div class="input-field col l3">
                                    <input id="filterDo" type="text" class="f-input">
                                    <label>Filter Do No</label>
                                </div>
                                <div class="input-field col l3">
                                    <input id="filterSo" type="text" class="f-input datepicker">
                                    <label>Filter PO</label>
                                </div>
                                <div class="input-field col l3">
                                    <input id="filterPelanggan" type="text" class="f-input">
                                    <label>Filter Pelanggan</label>
                                </div>
                                <div class="col l12 m12 s12 margin-top">
                                    <div class="table-responsive">
                                        <table id="doTable" class="highlight table table-bordered display nowrap dataTable dtr-inline" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>No Do</th>
                                                <th>No Sales Order</th>
                                                <th>Notes</th>
                                                <th>Shipping Term</th>
                                                <th>Tanggal</th>
                                                <th>Nama</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {{--@foreach($data['do'] as $key => $value)--}}
                                                {{--<tr mode="view" value="{{$value->delivery_order_id}}">--}}
                                                    {{--<td class="nodo">{{$value->delivery_order_number}}</td>--}}
                                                    {{--<td>{{$value->so->sales_order_number}}</td>--}}
                                                    {{--<td>{{$value->note}}</td>--}}
                                                    {{--<td>{{$value->shipping_term->shipping_term_name}}</td>--}}
                                                    {{--<td>{{$value->date_delivery}}</td>--}}
                                                    {{--<td>{{$value->so->customer->first_name.' '.$value->so->customer->last_name}}</td>--}}
                                                    {{--<td>--}}
                                                        {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit tooltipped"--}}
                                                           {{--mode="edit" data-position="bottom" data-tooltip="edit" value="{{$value->delivery_order_id}}"><i--}}
                                                                    {{--class="material-icons">edit</i></a>--}}
                                                        {{--<a class="btn btn-sm btn-raised red delete-modal tooltipped" data-position="bottom" data-tooltip="delete"><i--}}
                                                                    {{--class="material-icons">delete</i></a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                            </tbody>
                                        </table>
                                        </br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul> -->
        </div>
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Do Barang</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <form id="formDo">
                                    <div class="input-field col l3">
                                        <input id="donumber" type="text" class="f-input" name="donumber" disabled>
                                        <input id="iddo" name="id" type="text" class="f-input" hidden disabled>
                                        <label>No. Pengiriman Barang</label>
                                    </div>
                                    <div class="input-field col l3">
                                        <select id="noso" name="so" class="selectpicker browser-default" data-live-search="true">
                                            <option value="0">Select SO</option>
                                            @foreach($data['so'] as $key => $value)
                                                <option value="{{$value->sales_order_id}}">{{$value->sales_order_number.' - '.$value->customer_name}}</option>
                                            @endforeach
                                        </select>
                                        <label>No Sales Order</label>
                                    </div>
                                    <div class="input-field col l3">
                                        <input id="customer" name="customer" type="text" class="f-input" disabled>
                                        <label>Nama Customer</label>
                                    </div>
                                    <div class="input-field col l3">
                                        <input id="tgldo" type="text" name="tgldo" class="f-input datepicker" data-maxdate="today">
                                        <label>Tanggal</label>
                                    </div>
                                    <div class="input-field col l3">
                                        <select id="shippingterm" name="shippingterm" class="selectpicker browser-default" data-live-search="true">
                                            <option value="0">Select Shipping Term</option>
                                            @foreach($data['shippingterm'] as $key => $value)
                                                <option value="{{$value->shipping_term_id}}">{{$value->shipping_term_name}}</option>
                                            @endforeach
                                        </select>
                                        <label>Shipping Term</label>
                                    </div>
                                    <div class="input-field col l3">
                                        <input id="notes" name="notes" type="text" class="f-input">
                                        <label>Notes</label>
                                    </div>
                                    <div class="col l12 m12 s12 margin-top">
                                        <div class="table-responsive">
                                            <table class="stoko-table no-border">
                                                <thead>
                                                <tr>
                                                    <th class="theader">Kategori Barang</th>
                                                    <th class="theader">Brand</th>
                                                    <th class="theader">Tipe Barang</th>
                                                    <th class="theader">Send</th>
                                                </tr>
                                                </thead>
                                                <tbody id="barang-data">
                                                <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                                                <tr class="barang-row" hidden>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="id-1" type="text" class="f-input id-detail" name="id[]" hidden>
                                                            <input id="id-barang-1" type="text" class="f-input id-barang" name="idbarang[]" hidden>
                                                            <input id="kategori-1" type="text" class="f-input" name="kategori[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="brand-1" type="text" class="f-input brand" name="brand[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="type-1" type="text" class="f-input type" name="type[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" id="checkbox-sent-1" class="filled-in sent" value="" />
                                                        <label id="label-sent-1" for="checkbox-sent-1"></label>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <br>
                                        <div class="main-button-group input-field right">
                                            <a id="clear" href="" class="btn-stoko btn-stoko-primary orange">Clear</a>
                                            <a id="edit-do" href="" mode="edit" class="btn-stoko btn-stoko-primary" hidden>Edit BPB</a>
                                            <a id="submit-do" href="" mode="save" class="btn-stoko btn-stoko-primary">Submit BPB</a>
                                            <a id="print-barcode" href="" target="_blank" class="btn-stoko btn-stoko-primary" hidden>Print Barcode</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-do" class="modal">
    <div class="modal-content">
        <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
        <a id="confirm-delete-do" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#noso').on('change',function(){
            var id = $(this).val();
            $.ajax({
                type:"Get",
                url:"getsofordelivery",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    if (response.length == 0) {
                        $original.clone();
                        $('.barang-row').remove();
                        $original.attr('hidden',true);
                        $original.appendTo('#barang-data');
                        $('#formDo').find("input[type=text], textarea").val("");
                        $('#no-item').removeAttr('hidden');
                        reset();
                    }else{
                        $('#customer').val(response['sodata'].customer.first_name+' '+response['sodata'].customer.last_name);
                        var $cloned = $original.clone();
                        $('.barang-row').remove();
                        for (var i=0; i<response['sodetail'].length; i++)
                        {
                            var newid = "id-"+(i+1);
                            var newIdBarang = "id-barang-"+(i+1);
                            var newKategori = "kategori-"+(i+1);
                            var newBrand = "brand-"+(i+1);
                            var newType = "type-"+(i+1);
                            var newSent = "checkbox-sent-"+(i+1);
                            var newCancel = "checkbox-cancel-"+(i+1);
                            var newLabelSent = "label-sent-"+(i+1);
                            var newLabelCancel = "label-cancel-"+(i+1);
                            var newGudang = "gudang-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id',newid);
                            $temp.find('#id-barang-1').attr('id',newIdBarang);
                            $temp.find('#kategori-1').attr('id',newKategori);
                            $temp.find('#brand-1').attr('id',newBrand);
                            $temp.find('#type-1').attr('id',newType);
                            $temp.find('#checkbox-sent-1').attr('id',newSent);
                            $temp.find('#checkbox-cancel-1').attr('id',newCancel);
                            $temp.find('#label-sent-1').attr('id',newLabelSent).attr('for',"checkbox-sent-"+(i+1));
                            $temp.find('#label-cancel-1').attr('id',newLabelCancel).attr('for',"checkbox-cancel-"+(i+1));
                            $temp.find('#gudang-1').attr('id',newGudang);
                            $temp.appendTo('#barang-data');
                            $('#id-'+(i+1)).val(response['sodetail'][i].sales_order_details_id).attr('disabled',true);
                            $('#id-barang-'+(i+1)).val(response['sodetail'][i].product.product_id).attr('disabled',true);
                            $('#kategori-'+(i+1)).val(response['sodetail'][i].product.category.description).attr('disabled',true);
                            $('#brand-'+(i+1)).val(response['sodetail'][i].product.brand.name).attr('disabled',true);
                            $('#type-'+(i+1)).val(response['sodetail'][i].product.type.name).attr('disabled',true);
                            $('#gudang-'+(i+1)).val(response['sodetail'][i].product.warehouse.warehouse_id).attr('disabled',true);
                        }
                        $('#no-item').attr('hidden',true);

                    }
                }
            });
        });

        $('#doTable').on('click', 'tr, .edit', function(event){
            event.stopImmediatePropagation();
            id = $(this).attr('value');
            var mode = $(event.currentTarget).attr('mode');
            $.ajax({
                type:"GET",
                url:"getdeliveryorder",
                data:{id:id},
                success:function(response){
                    $('#customer').val(response.company_name);
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    $('#iddo').val(response.delivery_order_id);
                    $('#donumber').val(response.delivery_order_number).attr('disabled',true);
                    $('#noso').append('<option class="remove-when-clear" value="'+response.so.sales_order_id+'" selected="selected">'+response.so.sales_order_number+'</option>').attr('disabled',true);
                    $('#tgldo').val(response.date_delivery).attr('disabled',true).attr('disabled',true);
                    $('#customer').val(response.so.customer.first_name+' '+response.so.customer.last_name).attr('disabled',true);
                    $('#notes').val(response.note).attr('disabled',true);
                    $('#shippingterm').val(response.term_delivery_order).attr('disabled',true);
                    $('#no-item').attr('hidden',true);
                    $('#barang-row').removeAttr('hidden');
                    $.each(response.details, function(i,v){
                        var newid = "id-"+(i+1);
                        var newKategori = "kategori-"+(i+1);
                        var newBrand = "brand-"+(i+1);
                        var newType = "type-"+(i+1);
                        var newSent = "checkbox-sent-"+(i+1);
                        var newCancel = "checkbox-cancel-"+(i+1);
                        var newLabelSent = "label-sent-"+(i+1);
                        var newLabelCancel = "label-cancel-"+(i+1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id',newid);
                        $temp.find('#kategori-1').attr('id',newKategori);
                        $temp.find('#brand-1').attr('id',newBrand);
                        $temp.find('#type-1').attr('id',newType);
                        $temp.find('#checkbox-sent-1').attr('id',newSent);
                        $temp.find('#label-sent-1').attr('id',newLabelSent).attr('for',"checkbox-sent-"+(i+1));
                        $temp.find('#label-cancel-1').attr('id',newLabelCancel).attr('for',"checkbox-cancel-"+(i+1));
                        $temp.appendTo('#barang-data');
                        $('#kategori-'+(i+1)).val(v.product.category.description).attr('disabled',true);
                        $('#brand-'+(i+1)).val(v.product.brand.name).attr('disabled',true);
                        $('#type-'+(i+1)).val(v.product.type.name).attr('disabled',true);
                        $('#checkbox-sent-'+(i+1)).attr('disabled',true);
                    });
                    $('.selectpicker').selectpicker('refresh');
                },complete:function(){
                    if(mode == "edit"){
                        $('#edit-do').removeAttr('hidden');
                        $('#submit-do').attr('hidden',true);
                        $('#tgldo, #notes, #shippingterm').removeAttr('disabled');
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            })
        });

        $('#submit-do, #edit-do').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('#donumber,#iddo, #nopo, .earnedqty').removeAttr('disabled');
            var kode = $('#donumber').val();
            mode = $(event.currentTarget).attr('mode');

            //validasi checkbox receive dan cancel berbarenagan
            validcheckbox = [];
            $('.barang-row').each(function(k,v){
                if($(this).find('.sent').is(':checked')) {
                    validcheckbox.push('true');
                }else{
                    validcheckbox.push('false');
                }
            });

            if(validcheckbox.indexOf('false') > -1 && mode =='save'){
                toastr.warning('Anda belum memilih semuan barang yang akan dikirim')
            }else if($('#shippingterm').val() == 0){
                toastr.warning('Anda Belum Memilih Shipping Term!');
            }else if($('#notes').val() == ''){
                toastr.warning('Anda Belum mengisi Notes!');
            }else{
                hidemainbuttongroup();

                if($(event.currentTarget).attr('mode') == 'save')
                {
                    var url = "createdeliveryorder";
                    var successmessage = 'Delivery Order '+kode+' telah berhasil dibuat!';
                }else{
                    var url = "updatedeliveryorder";
                    var successmessage = 'Delivery Order '+kode+' telah berhasil diubah!';
                }

                var id = [];
                var idbarang = [];
                var so = $('#noso').val();
                var nodo = $('#donumber').val();
                var iddo = $('#iddo').val();
                var tgldo = $('#tgldo').val();
                var notes = $('#notes').val();
                var shippingterm = $('#shippingterm').val();
                //get id po details dan gudang dari row yang di select
                $('.sent').each(function(i,v){
                    if($(this).is(':checked'))
                    {
                        id.push($(this).closest('tr').find('.id-detail').val());
                        idbarang.push($(this).closest('tr').find('.id-barang').val());
                    }
                })

                //create barang kalau yang di checked lebih dari 0 && execute ajax bila mode edit
                if(id.length > 0 || mode == 'edit')
                {
                    $.ajax({
                        type:"POST",
                        url:url,
                        data:{id:id, idbarang:idbarang, so:so, iddo:iddo, nodo:nodo, tgldo:tgldo, shippingterm:shippingterm, notes:notes},
                        success:function(response){
                            toastr.success(successmessage, {"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                        }
                    })
                }
            }
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .nav-item.active a').click()
        });

        $('body').on('click','.delete-detail',function(event){
            event.stopImmediatePropagation();
            $(this).closest('tr').remove();
        });

        $('#doTable').on('click','.delete-modal', function(event){
            event.stopImmediatePropagation();
            var nodo = $(this).closest('tr').find('.nodo').html();
            var iddo = $(this).closest('tr').attr('value');
            $('#confirm-delete-do').attr('value',iddo).attr('nomor',nodo);
            $("#delete-message").html("Yakin ingin menghapus data "+nodo+" ?")
            $('#modal-delete-do').modal('open');
        });

        $('#confirm-delete-do').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-do').modal('close');
            var id = $(this).attr('value');
            var nodo = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletedeliveryorder",
                data:{id:id},
                success:function(response){
                    toastr.success('Delivery Order dengan '+nodo+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                }
            })
        });

        function firstload(){
            $('#tgldo').dcalendarpicker({
                format: 'dd-mm-yyyy'
            });

            $('#tgldo').val(moment().format('DD-MM-YYYY'));

            $('.selectpicker').selectpicker('render');

            $.ajax({
                type:"GET",
                url:"lastdonumber",
                success:function(response){
                    $('#donumber').val(response);
                }
            })

            $('.number').each(function(){
                $(this).html(accounting.formatMoney($(this).html(),'Rp ',2,',','.'));
            });

            // doTable = $('#doTable').DataTable({ // This is for home page
            //     searching: true,
            //     responsive: true,
            //     'aaSorting':[],
            //     'sDom':'tip',
            //     "bPaginate":true,
            //     "bFilter": false,
            //     "sPaginationType": "full_numbers",
            //     "iDisplayLength": 10,
            //     "language": {
            //         "infoEmpty": "No records to display",
            //         "zeroRecords": "No records to display",
            //         "emptyTable": "No data available in table",
            //     },
            // });

            // doTable = $('#doTable').DataTable({ // This is for home page
            //     searching: true,
            //     processing: true,
            //     serverSide: true,
            //     "aaSorting": [],
            //     'sDom': 'tip',
            //     "pageLength": 5,
            //     ajax: {
            //         url : 'getdeliveryordertable',
            //         data : function (d){
            //             d.number = $('#filterDo').val();
            //             d.so = $('#filterSo').val();
            //             d.pelanggan = $('#filterPelanggan').val();
            //         }
            //     },
            //     rowId : 'delivery_order_id',
            //     columns: [
            //         { data: 'delivery_order_number', name: 'delivery_order_number', class:'nodo'},
            //         { data: 'so.sales_order_number', name: 'sales_order_number'},
            //         { data: 'note', name: 'note'},
            //         { data: 'shipping_term.shipping_term_name', name: 'shipping_term_name'},
            //         { data: 'date_delivery', name: 'date_delivery'},
            //         { data: 'so.customer_name', name: 'so.customer_name'},
            //         { data: 'action', name: 'action'},
            //     ],
            // });

            // $('#filterDo').on('keyup', function () { // This is for news page
            //     doTable.draw();
            // });
            // $('#filterSo').on('keyup', function () { // This is for news page
            //     doTable.draw();
            // });
            // $('#filterPelanggan').on('keyup', function () { // This is for news page
            //     doTable.draw();
            // });
        }

        function reset()
        {
            $('#nopo').val(0);
            $('#supplier').val("");
            $('#notes').val("");
            $('#tgldo').val(moment().format('DD-MM-YYYY'));
        }

        function hidemainbuttongroup(){
            $('.main-button-group a').attr('hidden',true);
        }
    });
</script>
