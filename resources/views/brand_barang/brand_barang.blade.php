<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l4 m4 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Brand Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <form id="formBrand">
                <div class="row margin-top">
                  <div class="col l12">
                    <div class="input-field col l12 m12 s12">
                      <input id="id" type="text" class="f-input" name="id" hidden>
                      <input id="brand" name="brand" type="text" class="f-input">
                      <label>Brand Barang</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <select id="category" name="category" type="text" class="browser-default selectpicker" data-live-search="true">
                        @foreach($category as $key => $value)
                          <option value="{{$value->product_category_id}}">{{$value->description}}</option>
                        @endforeach
                      </select>
                      <label>Kategori Barang</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="code" name="code" type="text" class="f-input">
                      <label>Kode</label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col l12 m12 s12 main-button-group">
                    <button id="btn-submit" type="submit" class="btn btn-raised btn-sm light-blue darken-2" mode="save">simpan</button>
                    <a href="#" class="btn btn-sm btn-raised grey lighten-4 grey-text text-darken-4 reset">cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l8 m8 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Brand</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l6">
                  <input id="filterBrand" type="text" class="f-input">
                  <label>Search</label>
                </div>
                <div class="col l12 m12 s12">
                  <table id="brandTable" class="table table-bordered display nowrap dataTable dtr-inline">
                    <thead>
                    <tr>
                      <th>Brand</th>
                      <th>Kategori</th>
                      <th>Kode</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {{--@foreach($brand as $key => $value)--}}
                    {{--<tr value="{{$value->product_brand_id}}">--}}
                    {{--<td>{{$value->name}}</td>--}}
                    {{--<td>{{$value->category->description}}</td>--}}
                    {{--<td>{{$value->code}}</td>--}}
                    {{--<td>--}}
                    {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>--}}
                    {{--<a href="#delete_data" class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                    {{--</td>--}}
                    {{--</tr>--}}
                    {{--@endforeach--}}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Modal Delete Satuan -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data berikut?</h5>
    <div class="row">
      <div class="col l12">
        <table id="modal-delete-data">

        </table>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text delete">Hapus</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </form>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        // brandTable = $('#brandTable').DataTable({ // This is for home page
        //   searching: true,
        //   responsive: true,
        //   'sDom': 'ti',
        //   'pagingType': 'full_numbers_no_ellipses',
        //   "language": {
        //     "infoEmpty": "No records to display",
        //     "zeroRecords": "No records to display",
        //     "emptyTable": "No data available in table",
        //   },
        // });

        brandTable = $('#brandTable').DataTable({ // This is for home page
            searching: true,
            processing: true,
            serverSide: true,
            "aaSorting": [],
            'sDom': 'tip',
            "pageLength": 5,
            ajax: {
                url : 'getbrandtable',
                data : function (d){
                    d.brand = $('#filterBrand').val();
                }
            },
            rowId : 'product_brand_id',
            columns: [
                { data: 'name', name: 'name'},
                { data: 'category.description', name: 'c_description'},
                { data: 'code', name: 'code'},
                { data: 'action', name: 'action'},
            ],
        });

        $('.selectpicker').selectpicker('render');

        $('#filterBrand').on('keyup', function () { // This is for news page
            brandTable.draw();
        });

        $('#formBrand').submit(function(event){
            event.preventDefault();
            var mode = $(this).find('#btn-submit').attr('mode');
            var empty = required(['brand' ,'code']);
            if(empty != '0')
            {
                toastr.warning(empty+' tidak boleh kosong!');
            }else if($('#code').val().length > 3) {
                toastr.warning('Kode Tidak Boleh Lebih Dari 3 Karakter');
            }else{
                hidemainbuttongroup();
                if (mode == 'save')
                {
                    var id = $(this).find('#id').val();
                    $.ajax({
                        type:"POST",
                        url:"createBrandBarang",
                        data: $("#formBrand").serialize(),
                        success:function(response){
                            toastr.success('Brand Barang Telah Berhasil Dibuat!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                        }
                    });
                }else if(mode == 'edit'){
                    var id = $(this).find('#id').val();
                    $.ajax({
                        type:"POST",
                        url:"updateBrandBarang",
                        data: $("#formBrand").serialize()+"&id="+id,
                        success:function(response){
                            toastr.success('Brand Barang Telah Berhasil Diubah!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                        }
                    });
                }
            }
        });

        $('body').on('click','#formBrand .reset', function(event){
            event.stopImmediatePropagation();
            $(this).closest('#formKategori').find("input[type=text], textarea").val("");
            $('#btn-submit').attr('mode', 'save');
        });

        $('body').on('click','#brandTable .edit', function(event){
            event.stopImmediatePropagation();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                type:"GET",
                url:"getBrandData",
                data: {id:id},
                success:function(response){
                    $('#id').val(response.product_brand_id);
                    $('#brand').val(response.name);
                    $('#category').val(response.category.product_category_id);
                    $('#code').val(response.code);
                    $('.selectpicker').selectpicker('refresh');
                }
            });
            $('#btn-submit').attr('mode', 'edit');
        });

        $("#brandTable").on('click',".delete-modal", function(event){
            event.preventDefault();
            var id = $(this).closest('tr').attr('value');

            $.ajax({
                type:"GET",
                url:"modalBrandData",
                async:false,
                data:{
                    id:id,
                },
                success:function(response){
                    $('#modal-delete-data').html(response);
                }
            });
            $('#delete_data').modal('open');
        });

        $(".delete").on('click',function(){
            var id = $(this).closest('.modal').find('#id-delete').attr('value');
            console.log(id);
            $.ajax({
                type:"Get",
                url:"deleteBrandBarang",
                data:{id:id},
                success:function(response){
                    toastr.success('Berhasil Menghapus Brand!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                }
            });
        });

        function hidemainbuttongroup(){
            $('.main-button-group').attr('hidden',true);
        }
    })
</script>
