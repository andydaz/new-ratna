<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>History Refund</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br/>
                <div class="input-field col l3">
                  <label>Filter Retur</label>
                  <input id="filterReturNumber" type="text" class="f-input" placeholder="Filter Retur" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Pelanggan</label>
                  <input id="filterPelanggan" type="text" class="f-input" placeholder="Filter Pelanggan" />
                </div>
                <div class="input-field col l3">
                  <label>Filter SO</label>
                  <input id="filterSoNumber" type="text" class="f-input" placeholder="Filter SO" />
                </div>
              </div>
              <div class="row">
                <div class="col l12 m12 s12">
                  <div class="table-responsive">
                    <table id="refundTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th class="theader">No Retur</th>
                          <th class="theader">Pelanggan</th>
                          <th class="theader">Modified By</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($refund as $key => $value)
                        <tr value="{{$value->sales_retur_id}}">
                          <td>{{$value->company_name}}</td>
                          <td>{{$value->date_retur}}</td>
                          <td>{{$value->total_price}}</td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    firstload();

    function firstload()
    {
      refundTable = $('#refundTable').DataTable({ // This is for home page
      searching: true, 
      responsive: true,
      'sDom':'tip',
      "bPaginate":true,
      "bFilter": false,
      "sPaginationType": "full_numbers",
      "iDisplayLength": 10,
      "language": {
        "infoEmpty": "No records to display",
        "zeroRecords": "No records to display",
        "emptyTable": "No data available in table",
      },
    });

    $('#filterReturNumber').on('keyup', function () { // This is for news page
      returTable.column(0).search(this.value).draw();
    });
    $('#filterPelanggan').on('keyup', function () { // This is for news page
      returTable.column(1).search(this.value).draw();
    });
    $('#filterSoNumber').on('keyup', function () { // This is for news page
      returTable.column(2).search(this.value).draw();
    });
    }
  });
</script>
