@if(count($history_credit) > 0)
	@foreach($history_credit as $key => $value)
		<tr soId={{$value->so->sales_order_id}}>
		  <td>{{$value->so->date_sales_order}}</td>
		  <td>{{$value->so->payment_type->payment_description}}</td>
		  <td>{{$value->product->category->description}}</td>
		  <td>{{$value->product->brand->name}}</td>
		  <td>{{$value->product->type->name}}</td>
		  <td>{{$value->product->bar_code}}</td>
		  <td>{{$value->invoice_sales_number}}</td>
		  <td>{{$value->term_number}}</td>
		  <td>{{$value->due_date}}</td>
		  <td>{{$value->total_amount}}</td>
		  <td>{{$value->payment ? $value->payment->date_sales_payment : "-"}}</td>
		  <td>{{$value->aging}}</td>
		  <td><a class="show-p-notes-modal"><i class="material-icons">note_add</i></a></td>
		</tr>
	@endforeach
@else
	<tr>
		<td colspan="12">No Data</td>
	</tr>
@endif	
