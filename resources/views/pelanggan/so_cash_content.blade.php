@if(count($history_cash) > 0)
	@foreach($history_cash as $key => $value)
		<tr>
		  <td>{{$value->so->date_sales_order}}</td>
		  <td>{{$value->so->payment_type->payment_description}}</td>
		  <td>{{$value->product->bar_code}}</td>
		  <td>{{$value->product->category->description}}</td>
		  <td>{{$value->product->brand->name}}</td>
		  <td>{{$value->product->type->name}}</td>
		  <td>{{$value->price}}</td>
		  <td>{{$value->discount_percentage}}</td>
		  <td>{{$value->discount_nominal}}</td>
		  <td>{{$value->sub_total}}</td>
		</tr>
	@endforeach
@else
	<tr>
		<td colspan="9">No Data</td>
	</tr>
@endif	
