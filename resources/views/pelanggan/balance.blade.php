<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Saldo Pelanggan</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <div class="col l12 m12 s12 margin-top">
                  <div class="col l12 m12 s12">
                    <div class="input-field col l3 m12 s12 no-padding">
                      <select id="customer" name="customer" class="f-select">
                        <option value='0'>Select Customer</option>
                        @foreach($data['customer'] as $key => $value)
                        <option value="{{$value->customer_id}}">{{$value->company_name}}</option>
                        @endforeach
                      </select>
                      <label style="left:0px">Customer</label>
                    </div>
                    <div class="input-field col l3">
                      <label>Total Balance</label>
                      <input id="balance" type="text" class="f-input" name="balance" value="0" disabled>
                    </div>
                  </div>
                  <div class="col l12 m12 s12">
                    <div class="table-responsive">
                      <table id="StockOverviewTable" style="width: 100%" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                        <thead>
                          <tr>
                            <th class="theader">Tgl</th>
                            <th class="theader">Description</th>
                            <th class="theader">Debit</th>
                            <th class="theader">Kredit</th>
                          </tr>
                        </thead>
                        <tbody id="detail-table">

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.3/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    $('#customer').on('click', function(event){
      event.stopImmediatePropagation();
      var id = $(this).val();
      if (id == 0) {
        return;
      }
      $.ajax({
        method : "GET",
        url: 'detail-balance',
        data: {id:id},
        success: function(response){
          $('#detail-table').html(response.view);
          /*$('#balance').val(response.totalbalance);*/
          $('#balance').val(accounting.formatMoney(response.totalbalance,'Rp ',2,',','.'));
        },
        complete: function(){
          var totalcredit = 0;
          var totaldebit = 0;
          $('.credit').each(function(){
            if ($(this).html() =='-') {
              totalcredit += 0;  
            }else{
              totalcredit += parseInt($(this).html());
            }
          })
          $('.debit').each(function(){
            if ($(this).html() =='-') {
              totaldebit += 0;  
            }else{
              totaldebit += parseInt($(this).html());
            }
          })
          // $(this).val(accounting.formatMoney(totaldebit - totalcredit,'Rp ',2,',','.'))
          $('#column-total-balance').html(totaldebit - totalcredit);
          formatnumber();
        }  
      });           
    });

    function formatnumber(){
      $('.number').each(function(){
        if($(this).html() != '-')
        {
            $(this).html(accounting.formatMoney($(this).html(),'Rp ',2,',','.'));
        }
      });
    }
  });
</script>