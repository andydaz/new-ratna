<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l4 m4 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Tipe Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <form id="formTipe">
                <div class="row margin-top">
                  <div class="col l12">
                    <div class="input-field col l12 m12 s12">
                      <input id="id" type="text" class="f-input" name="id" hidden>
                      <input id="type" name="type" type="text" class="f-input">
                      <label>Tipe Barang</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <select id="brand" name="brand" type="text" class="browser-default selectpicker" data-live-search="true">
                        @foreach($brand as $key => $value)
                          <option value="{{$value->product_brand_id}}">{{$value->name}}</option>
                        @endforeach
                      </select>
                      <label>Brand Barang</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="category-id" type="text" class="f-input" name="category-id" hidden>
                      <input id="category" name="category" type="text" class="f-input" disabled>
                      <label>Kategori Barang</label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col l12 m12 s12 main-button-group">
                    <button id="btn-submit" type="submit" class="btn btn-raised btn-sm light-blue darken-2" mode="save">simpan</button>
                    <a href="#" class="btn btn-sm btn-raised grey lighten-4 grey-text text-darken-4 reset">cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l8 m8 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Tipe Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l6">
                  <input id="filterTipe" type="text" class="f-input">
                  <label>Search</label>
                </div>
                <div class="col l12 m12 s12">
                  <table id="tipeTable" class="table table-bordered display nowrap dataTable dtr-inline">
                    <thead>
                    <tr>
                      <th>Type</th>
                      <th>Brand</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {{--@foreach($tipe as $key => $value)--}}
                    {{--<tr value="{{$value->product_type_id}}">--}}
                    {{--<td>{{$value->name}}</td>--}}
                    {{--<td>{{$value->brand->name}}</td>--}}
                    {{--<td>--}}
                    {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>--}}
                    {{--<a href="#delete_data" class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                    {{--</td>--}}
                    {{--</tr>--}}
                    {{--@endforeach--}}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Modal Delete Satuan -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data berikut?</h5>
    <div class="row">
      <div class="col l12">
        <table id="modal-delete-data">

        </table>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text delete">Hapus</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </form>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        // tipeTable = $('#tipeTable').DataTable({ // This is for home page
        //   searching: true,
        //   responsive: true,
        //   'sDom': 'ti',
        //   'pagingType': 'full_numbers_no_ellipses',
        //   "language": {
        //     "infoEmpty": "No records to display",
        //     "zeroRecords": "No records to display",
        //     "emptyTable": "No data available in table",
        //   },
        // });

        tipeTable = $('#tipeTable').DataTable({ // This is for home page
            searching: true,
            processing: true,
            serverSide: true,
            "aaSorting": [],
            'sDom': 'tip',
            "pageLength": 5,
            ajax: {
                url : 'gettipetable',
                data : function (d){
                    d.tipe = $('#filterTipe').val();
                }
            },
            rowId : 'product_type_id',
            columns: [
                { data: 'name', name: 'name'},
                { data: 'brand.name', name: 'b_name'},
                { data: 'action', name: 'action'},
            ],
        });

        $('.selectpicker').selectpicker('render');

        $('#brand').on('change', function(event){
            var id = $(this).val();
            $.ajax({
                type:"GET",
                url:"getKategoriOnBrand",
                async:false,
                data:{
                    id:id,
                },
                success:function(response){
                    $('#category-id').val(response.product_category_id);
                    $('#category').val(response.description);
                }
            })
        })

        $('#filterTipe').on('keyup', function () { // This is for news page
            tipeTable.draw();
        });

        $('#formTipe').submit(function(event){
            event.preventDefault();
            var mode = $(this).find('#btn-submit').attr('mode');
            var empty = required(['type', 'code']);
            if(empty != '0')
            {
                toastr.warning(empty+' tidak boleh kosong!');
            }else{
                hidemainbuttongroup();
                if (mode == 'save')
                {
                    var id = $(this).find('#id').val();
                    $.ajax({
                        type:"POST",
                        url:"createTipeBarang",
                        data: $("#formTipe").serialize(),
                        success:function(response){
                            toastr.success('Tipe Barang Telah Berhasil Dibuat!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                        }
                    });
                }else if(mode == 'edit'){
                    var id = $(this).find('#id').val();
                    $.ajax({
                        type:"POST",
                        url:"updateTipeBarang",
                        data: $("#formTipe").serialize()+"&id="+id,
                        success:function(response){
                            toastr.success('Tipe Barang Telah Berhasil Diubah!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                        }
                    });
                }
            }
        });

        $('body').on('click','#formTipe .reset', function(event){
            event.stopImmediatePropagation();
            $(this).closest('#formTipe').find("input[type=text], textarea").val("");
            $('#btn-submit').attr('mode', 'save');
        });

        $('body').on('click','#tipeTable .edit', function(event){
            event.stopImmediatePropagation();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                type:"GET",
                url:"getTipeData",
                data: {id:id},
                success:function(response){
                    $('#id').val(response.product_type_id);
                    $('#type').val(response.name);
                    $('#brand').val(response.brand.product_brand_id);
                    $('#category-id').val(response.brand.category.product_category_id);
                    $('#category').val(response.brand.category.description);
                    $('.selectpicker').selectpicker('refresh');
                }
            });
            $('#btn-submit').attr('mode', 'edit');
        });

        $("#tipeTable").on('click','.delete-modal',function(event){
            event.preventDefault();
            var id = $(this).closest('tr').attr('value');

            $.ajax({
                type:"GET",
                url:"modalTipeData",
                async:false,
                data:{
                    id:id,
                },
                success:function(response){
                    $('#modal-delete-data').html(response);
                }
            });
            $('#delete_data').modal('open');
        });

        $(".delete").on('click',function(){
            var id = $(this).closest('.modal').find('#id-delete').attr('value');
            console.log(id);
            $.ajax({
                type:"Get",
                url:"deleteTipeBarang",
                data:{id:id},
                success:function(response){
                    toastr.success('Berhasil Menghapus Tipe Barang!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                }
            });
        });

        function hidemainbuttongroup(){
            $('.main-button-group').attr('hidden',true);
        }
    })
</script>
