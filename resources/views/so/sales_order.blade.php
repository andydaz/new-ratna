<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet"
      href="css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text "><i class="material-icons">search</i>Cari
            Sales Order
          </div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Filter Periode (Awal Periode)</label>
                  <input type="text" class="f-input datepicker" placeholder="Awal Periode"/>
                </div>
                <div class="input-field col l3">
                  <label>Filter Periode (Akhir Periode)</label>
                  <input type="text" class="f-input datepicker" placeholder="Akhir Periode"/>
                </div>
                <div class="input-field col l6">
                  <a href="#" class="btn btn-raised blue white-text">preview</a>
                </div>
              </div>
              <div class="row">
                <div class="input-field col l3">
                  <label>Filter SO</label>
                  <input id="filterSoNumber" type="text" class="f-input" placeholder="Filter SO"/>
                </div>
                <div class="input-field col l3">
                  <label>Filter Pelanggan</label>
                  <input id="filterPelanggan" type="text" class="f-input"
                         placeholder="Filter Pelanggan"/>
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="soTable"
                           class="table highlight table-bordered display nowrap dataTable dtr-inline"
                           style="width:100%">
                      <thead>
                      <tr>
                        <th class="theader">No SO</th>
                        <th class="theader">Tanggal SO</th>
                        <th class="theader">Customer</th>
                        <th class="theader">Jenis Pembayaran</th>
                        <th class="theader">Total</th>
                        <th class="theader">Created By</th>
                        <th class="theader">Action</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{--@foreach($so as $key => $value)--}}
                      {{--<tr value="{{$value->sales_order_id}}">--}}
                      {{--<td>{{$value->sales_order_number}}</td>--}}
                      {{--<td>{{$value->date_sales_order}}</td>--}}
                      {{--<td>{{$value->customer->first_name.' '.$value->customer->last_name}}</td>--}}
                      {{--<td>{{$value->payment_type->payment_description}}</td>--}}
                      {{--<td>{{number_format($value->grand_total_idr)}}</td>--}}
                      {{--<td>{{$value->staff->full_name}}</td>--}}
                      {{--<td>--}}
                      {{--@if(Session('roles')->role_id == 1 && $value->is_draft == 1)--}}
                      {{--<a class="btn btn-sm btn-raised draft" mode="draft" value="{{$value->sales_order_id}}"><i--}}
                      {{--class="material-icons">search</i>--}}
                      {{--</a>--}}
                      {{--@endif--}}
                      {{--@if(Session('roles')->role_id == 1)--}}
                      {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit tooltipped"--}}
                      {{--mode="edit" data-position="bottom" data-tooltip="edit" value="{{$value->sales_order_id}}"><i--}}
                      {{--class="material-icons">edit</i></a>--}}
                      {{--<a class="btn btn-sm btn-raised red delete-modal tooltipped" data-position="bottom" data-tooltip="delete"><i--}}
                      {{--class="material-icons">delete</i></a>--}}
                      {{--@endif--}}
                      {{--</td>--}}
                      {{--</tr>--}}
                      {{--@endforeach--}}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir
            Sales Order
          </div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <form id="formSo">
                  <div class="input-field col l3">
                    <label>No Sales Order</label>
                    <input id="nosalesorder" type="text" class="f-input" name="nosalesorder"
                           disabled>
                    <input id="idsalesorder" type="text" class="f-input" name="idso" disabled
                           hidden>
                  </div>
                  <div class="row">
                    <div class="input-field col l3">
                      <label>Pelanggan</label>
                      <select id="pelanggan" class="selectpicker browser-default"
                              data-live-search="true" name="pelanggan"
                              data-size="5">
                        <option value="0">Select Pelanggan</option>
                        @foreach($customer as $key => $value)
                          <option value="{{$value->customer_id}}">{{$value->first_name.' '.$value->last_name.' / '.$value->book_number}}</option>
                        @endforeach
                      </select>
                      <!-- <input type="text" class="f-input datepicker" placeholder="Prima Integrasi Solusindo" disabled> -->
                    </div>
                    <div class="input-field col l3">
                      <label>Customer Level</label>
                      <input id="level" type="text" class="f-input" name="level"
                             placeholder="Level Pelanggan" disabled>
                    </div>
                    <div class="input-field col l3">
                      <label>Saldo</label>
                      <input id="balance" type="text" class="f-input" name="notes"
                             placeholder="Saldo" disabled>
                    </div>
                    <div class="input-field col l3">
                      <label>Pembayaran</label>
                      <select id="pembayaran" class="selectpicker browser-default"
                              data-live-search="true" name="pembayaran">
                        <option value="0">Pilih Jenis Pembayaran</option>
                        @foreach($payment_term as $key => $value)
                          <option value="{{$value->payment_type_id}}">{{$value->payment_description}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="input-field col l3">
                      <label>Terms</label>
                      <input id="terms" type="number" max="8" class="f-input" placeholder="Terms"
                             name="terms" disabled>
                    </div>
                    <div class="input-field col l3">
                      <label>Tgl SO</label>
                      <input id="tglso" type="text" class="f-input datepicker" name="tglso"
                             placeholder="Tanggal SO">
                    </div>
                    <div class="input-field col l3">
                      <label>Notes</label>
                      <input id="notes" type="text" class="f-input datepicker" name="notes"
                             placeholder="Tanggal Kirim">
                    </div>
                    <div class="input-field col l3">
                      <label>Sales</label>
                      <select id="sales" class="selectpicker browser-default"
                              data-live-search="true" data-size="5" name="sales">
                        <option value="0">Pilih Sales</option>
                        @foreach($staff as $key => $value)
                          <option value="{{$value->account_id}}">{{$value->full_name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="input-field col l3">
                      <label>Nama Pelanggan</label>
                      <input id="customername" type="text" name="customername" class="f-input" placeholder="Nama Pelanggan">
                    </div>
                    <div class="input-field col l3">
                      <label>No HP</label>
                      <input id="customerphone" type="text" name="customerphone" class="f-input" placeholder="No Hp">
                    </div>
                    <div class="input-field col l3">
                      <label>No Buku</label>
                      <input id="booknumber" type="text" name="booknumber" class="f-input" placeholder="No Hp" disabled>
                    </div>
                  </div>
                  <div class="input-field col l3">
                    <input id="barcode" type="text" name="barcode" class="f-input">
                    <label>Barcode</label>
                  </div>
                  <div class="input-field col l9">
                    <button id="tambah-barang" type="button" class="btn-stoko btn-stoko-primary"
                            style="margin : 8px 8px; float:left">Tambah Barang
                    </button>
                    <button id="show-diskon" type="button" class="btn-stoko btn-stoko-primary"
                            style="margin : 8px 8px; float:left">Tampilkan Diskon 2
                    </button>
                    <button id="hide-diskon" type="button" class="btn-stoko btn-stoko-primary"
                            style="margin: 8px 8px; float:left; display: none">Sembunyikan Diskon 2
                    </button>
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader">Barcode</th>
                          <th class="theader">Kategori Barang</th>
                          <th class="theader">Brand</th>
                          <th class="theader">Tipe Barang</th>
                          <th class="theader">Harga Jual</th>
                          <th class="theader" style="width:100px">Diskon Percent 1</th>
                          <th class="theader diskon2header" style="width:100px" hidden>Diskon
                            Percent 2
                          </th>
                          <th class="theader">Diskon Nominal</th>
                          <th class="theader">Subtotal</th>
                          <th id="cicilanheader" class="theader" hidden>Cicilan</th>
                          <th class="theader">Action</th>
                        </tr>
                        </thead>
                        <tbody id="barang-data">
                        <tr class="barang-row">
                          <td>
                            <div class="input-field">
                              <input type="text" id="barcode-1" class="f-input barcode"
                                     name="barcode[]" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input type="text" id="idbarang-1" class="f-input idbarang"
                                     name="idbarang[]" hidden disabled>
                              <input type="text" id="barcode-1" class="f-input barcode"
                                     name="barcode" hidden disabled>
                              <input type="text" id="harga-beli-1"
                                     class="f-input hargabeli" name="hargabeli" hidden
                                     disabled>
                              <input type="text" id="kategori-1" class="f-input kategori"
                                     name="kategori[]" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input type="text" id="brand-1" class="f-input brand"
                                     name="brand[]" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input type="text" id="type-1" class="f-input type"
                                     name="type[]" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="harga-jual-1" type="text"
                                     class="f-input hargajual" name="hargajual[]"
                                     disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="percent-first-1" type="text"
                                     class="f-input percentfirst" name="percentfirst[]"
                                     value="0" style="display: inline-block; width:60%">
                              <input class="f-input percent-sym"
                                     style="display: inline-block; width:35%;" value="%"
                                     name="rate[]" disabled>
                            </div>
                          </td>
                          <td class="diskon2-data" hidden>
                            <div class="input-field">
                              <input id="percent-second-1" type="text"
                                     class="f-input percentsecond" name="percentsecond[]"
                                     value="0" style="display: inline-block; width:60%">
                              <input class="f-input percent-sym"
                                     style="display: inline-block; width:35%;" value="%"
                                     name="rate[]" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="diskon-1" type="text" class="f-input diskon"
                                     name="diskon[]" value="0">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="subtotal-1" type="text" class="f-input subtotal"
                                     name="subtotal[]" disabled value="0">
                            </div>
                          </td>
                          <td class="cicilan-data" hidden>
                            <div class="input-field">
                              <input id="cicilan-1" type="text" class="f-input cicilan"
                                     name="cicilan[]" disabled value="0">
                            </div>
                          </td>
                          <td>
                            <a class="btn btn-sm white grey-text delete-detail"><i class="material-icons">delete</i></a>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                      <br>
                    </div>
                    <div class="row">
                      <div class="col l6">
                        <div class="input-field" class="">
                          <input id="total" style="display:inline-block; width: 90%;" type="text" class="f-input" disabled>
                          <a style="display:inline-block; width: 9%;"
                             class="btn btn-sm btn-raised green darken-2 copy-value right tooltipped" data-position="bottom" data-tooltip="copy"><i
                                    class="material-icons">content_copy</i></a>
                          <input id="total-hidden" type="text" class="f-input" name="total"
                                 hidden>
                          <label>Total</label>
                        </div>
                        <div id="installment-div" class="input-field" hidden>
                          <input id="installment" type="text" class="f-input" style="display:inline-block; width: 90%;" disabled>
                          <a style="display:inline-block; width: 9%;"
                             class="btn btn-sm btn-raised green darken-2 copy-value right tooltipped" data-position="bottom" data-tooltip="copy"><i
                                    class="material-icons">content_copy</i></a>
                          <input id="installment-hidden" type="text" class="f-input"
                                 name="installment" hidden>
                          <label>Total Cicilan Pertama</label>
                        </div>
                        <div id="payable-div" class="input-field" hidden>
                          <input id="payable" type="text" class="f-input" disabled>
                          <label>Sisa Hutang Setelah Cicilan 1</label>
                        </div>
                        <div>
                          <div class="main-button-group input-field right">
                            <a id="clear" href="" class="btn-stoko orange">Clear</a>
                            <a id="submit-so" href="" class="btn-stoko btn-stoko-primary"
                               mode="save">Submit So</a>
                            <a id="edit-so" href="" class="btn-stoko btn-stoko-primary"
                               mode="edit" hidden>Edit So</a>
                            <a id="approve-draft" href="" class="btn-stoko btn-stoko-primary"
                               mode="approve" hidden>Approve</a>
                            <a id="reject-draft" href="" class="btn-stoko red darken-1"
                               mode="reject" hidden>Reject</a>
                            <a id="cetak-so" href="" target="_blank"
                               class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak
                              So</a>
                          </div>
                        </div>
                      </div>
                      <div id="div-pembayaran" class="col l6">
                        <div id="append-metode-pembayaran">
                          <div class="metode-pembayaran-div col l12">
                            <div class="input-field col l4">
                              <label>Metode Pembayaran</label>
                              <select id="metode-1"
                                      class="selectpicker browser-default metode"
                                      data-live-search="true" name="metode[]"
                                      style="width: 100%">
                                @foreach($payment_method as $key => $value)
                                  <option value="{{$value->payment_method_id}}">{{$value->bank->bank_name.' '.$value->payment_method_type->description}}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="input-field col l8">
                              <label>Jumlah Bayar</label>
                              <input style="display:inline-block; width: 80%" id="bayar-1"
                                     type="text" class="f-input bayar" placeholder=""
                                     name="bayar[]">
                              <a id="tambah-metode-pembayaran"
                                 style="display:inline-block; width: 15%;"
                                 class="btn btn-sm btn-raised green darken-2 add right tooltipped" data-position="bottom" data-tooltip="add row"><i
                                        class="material-icons">add</i></a>
                              <a style="display:none; width: 15%;"
                                 class="btn btn-sm btn-raised red darken-2 remove right tooltipped" data-position="bottom" data-tooltip="delete row"><i
                                        class="material-icons">remove</i></a>
                            </div>
                          </div>
                        </div>
                        <div class="col l12">
                          <div class="input-field col l6">
                            <label>Additional Charge</label>
                            <input id="additionalCharge" name="addcharge" type="text" class="f-input">
                          </div>
                          <div class="input-field col l6">
                            <label>Additional Charge Note</label>
                            <input id="additionalNote" name="addnote" type="text" class="f-input">
                          </div>
                        </div>
                        <div class="col l12">
                          <div class="input-field col l12">
                            <label>Total Bayar</label>
                            <input type="text" name="totalbayar" id="totalbayar"
                                   class="f-input" disabled>
                          </div>
                          <div id="div-remaining" class="input-field col l12">
                            <label>Sisa Bayar</label>
                            <input type="text" name="remaining" id="remaining"
                                   class="f-input" disabled>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="modal-tambah-barang" class="modal">
  <form id="form-list-barang">
    <div class="modal-content">
      <h4>Tambah Barang</h4>
      <div class="row" id="append-barang">
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!"
         class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius submit-barang">OK</a>
      <a href="#!"
         class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
    </div>
  </form>
</div>

<div id="modal-draft" class="modal">
  <div class="modal-content">
    <h4>Konfirmasi</h4>
    <div class="row" id="append-barang">
      <p>Total Sales (yang belum full terbayar) ( <span id="total-sales-modal"></span> ) dan sales saat ini telah
        melebihi Limit Kredit ( <span id="limit-customer-modal"></span> ) </p>
      <p>Silahkan Klik OK untuk Membuat Draft SO agar dapt di approve oleh master</p>
    </div>
  </div>
  <div class="modal-footer">
    <a id="submit-draft" href="#"
       class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius submit-barang">OK</a>
    <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
  </div>
</div>

<!-- Modal Rubah Purchase Request -->
<div id="rubah_purchase_request" class="modal">
  <div class="modal-content">
    <h4>Rubah Purchase Request</h4>
    <div class="row">
      <div class="input-field col l6">
        <select class="f-select browser-default">
          <option value="" disabled selected>Pilih Barang</option>
          <option value="1">Barang 1</option>
          <option value="2">Barang 2</option>
          <option value="3">Barang 3</option>
          <option value="4">Barang 4</option>
          <option value="5">Barang 5</option>
          <option value="6">Barang 6</option>
        </select>
        <label>Pilih Barang</label>
      </div>
      <div class="input-field col l6">
        <select class="f-select browser-default">
          <option value="" disabled selected>Pilih Pelanggan</option>
          <option value="1">Pelanggan 1</option>
          <option value="2">Pelanggan 2</option>
          <option value="3">Pelanggan 3</option>
          <option value="4">Pelanggan 4</option>
          <option value="5">Pelanggan 5</option>
          <option value="6">Pelanggan 6</option>
        </select>
        <label>Pilih Pelanggan</label>
      </div>
      <div class="input-field col l6">
        <input type="text" class="f-input"/>
        <label>Qty</label>
      </div>
      <div class="input-field col l6">
        <input type="text" class="f-input" disabled/>
        <label>Harga</label>
      </div>
      <div class="input-field col l6">
        <input type="text" class="f-input datepicker"/>
        <label>Expected Delivery Date</label>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a href="#!" onclick="Materialize.toast('Berhasil merubah purchase request', 4000)"
       class="modal-action modal-close waves-effect btn-stoko teal white-text">simpan</a>
  </div>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-so" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-so" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var currentLimit = 0;
        var currentTotalSales = 0;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#pelanggan').on('change', function (event) {
            event.stopImmediatePropagation();
            $.ajax({
                type: "GET",
                url: "getPelangganInfo",
                data: {id: $(this).val()},
                success: function (response) {
                    $('#balance').val(response.customer.total_balance);
                    $('#level').val(response.customer.laststate.level.customer_level_name);
                    $('#limit-customer-modal').html(response.customer.laststate.level.limit_credit);
                    $('#total-sales-modal').html(response.totalunsettled);
                    $('#booknumber').val(response.customer.book_number);
                    if(response.customer.customer_id != 1){
                        $('#customername').val(response.customer.first_name+' '+response.customer.last_name).attr('disabled',true);
                        $('#customerphone').val(response.customer.phone).attr('disabled',true);
                    }else{
                        $('#customername').val('').removeAttr('disabled');
                        $('#customerphone').val('').removeAttr('disabled');
                    }
                    currentLimit = response.customer.laststate.level.limit_credit;
                    currentRemainingCredit = response.totalremaining;
                }
            });
        });

        $('#tambah-barang').on('click', function (event) {
            event.stopImmediatePropagation();
            var barcode = $('#barcode').val();
            var existingbarcode = [];
            var count = 0;
            $('.barcode').each(function () {
                if($(this).val() != '')
                {
                    existingbarcode.push($(this).val());
                    count++;
                }
            });

            if($('#pembayaran').val() == 2 && count >= 1)
            {
                toastr.warning('Untuk Pembayaran cicilan hanya dibatasi 1 item!');
            }
            //check apakah ada barcode yang sudah dimasukkan agar tidak double input
            else if (existingbarcode.indexOf(barcode) == -1) {
                $.ajax({
                    type: "GET",
                    url: "getBarangUseBarcode",
                    data: {barcode: barcode},
                    success: function (response) {
                        if(response.status == 1) {
                            var $row = $(".barang-row").length;
                            var $initNumber = $(".barang-row").length;
                            $initNumber++;
                            var $newBarcode = "barcode-" + $initNumber;
                            var $newId = "idbarang-" + $initNumber;
                            var $newBarcode = "barcode-" + $initNumber;
                            var $newKategori = "kategori-" + $initNumber;
                            var $newBrand = "brand-" + $initNumber;
                            var $newType = "type-" + $initNumber;
                            var $newHargaJual = "harga-jual-" + $initNumber;
                            var $newHargaBeli = "harga-beli-" + $initNumber;
                            var $newPercentFirst = "percent-first-" + $initNumber;
                            var $newPercentSecond = "percent-second-" + $initNumber;
                            var $newDiskon = "diskon-" + $initNumber;
                            var $newSubtotal = "subtotal-" + $initNumber;
                            var $newCicilan = "cicilan-" + $initNumber;

                            var $orginal = $('.barang-row:first');
                            var $cloned = $orginal.clone().addClass('cloned');

                            //check apakah belum ada yang diinput sebelumnya (dilihat dari text kategory yang kosong)
                            if ($cloned.find("#kategori-1").val() != "" && $initNumber <= 10) {
                                $cloned.find("#barcode-1").attr('id', $newId).val(response.bar_code);
                                $cloned.find("#idbarang-1").attr('id', $newId).val(response.product_id);
                                $cloned.find("#harga-beli-1").attr('id', $newHargaBeli).val(response.price_buy);
                                $cloned.find("#barcode-1").attr('id', $newBarcode).val(response.bar_code);
                                $cloned.find("#kategori-1").attr('id', $newKategori).val(response.category.description);
                                $cloned.find("#brand-1").attr('id', $newBrand).val(response.brand.name);
                                $cloned.find("#type-1").attr('id', $newType).val(response.type.name);
                                $cloned.find("#harga-jual-1").attr('id', $newHargaJual).val(accounting.formatMoney(response.price_sale,'',0,',','.'));
                                $cloned.find("#percent-first-1").attr('id', $newPercentFirst);
                                $cloned.find("#percent-second-1").attr('id', $newPercentSecond);
                                $cloned.find("#diskon-1").attr('id', $newDiskon);
                                $cloned.find("#subtotal-1").attr('id', $newSubtotal);
                                $cloned.find("#cicilan-1").attr('id', $newCicilan);
                                //if you use bootstrap select you need this code so you can change the cloned select value
                                $cloned.find('.bootstrap-select').replaceWith(function () {
                                    return $('select', this);
                                });
                                $cloned.find('select').selectpicker();
                                //end of comment
                                $cloned.appendTo('#barang-data');
                                $('.btn-remove-main-product:last').css('visibility', 'visible');
                            } else {
                                $("#idbarang-1").val(response.product_id);
                                $("#harga-beli-1").val(response.price_buy);
                                $("#barcode-1").val(response.bar_code);
                                $("#kategori-1").val(response.category.description);
                                $("#brand-1").val(response.brand.name);
                                $("#type-1").val(response.type.name);
                                $("#harga-jual-1").val(accounting.formatMoney(response.price_sale,'',0,',','.'));
                            }
                            calculatetotal();
                        }else if(response.status == 3){
                            toastr.warning('Barang Ini Sedang Di Blokir');
                        }else if(response.status == 5){
                            toastr.warning('Barang Ini Sedang Di Booking');
                        }
                    }
                });
            } else {
                toastr.warning('Barang Telah Di Input');
            }

            $('#barcode').val('');
            barcodeFocus();
        });

        $('#tambah-metode-pembayaran').on('click', function (event) {
            event.stopImmediatePropagation();
            var $orginal = $('.metode-pembayaran-div:first');
            var $cloned = $orginal.clone().addClass('cloned');
            $cloned.find('.bayar').val("");
            $cloned.find('.add').remove();
            $cloned.find('.remove').css("display", "inline-block");
            $cloned.find('.bootstrap-select').replaceWith(function () {
                return $('select', this);
            });
            $cloned.find('select').selectpicker();
            $cloned.appendTo('#append-metode-pembayaran');
            $('.tooltipped').tooltip({delay: 50});
        });

        $('body').on('click', '.remove', function (event) {
            event.stopImmediatePropagation();
            $(this).closest('.metode-pembayaran-div').remove();
            calculateTotalBayar();
        });

        $('#approve-draft, #reject-draft').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var mode = $(event.currentTarget).attr('mode');
            var kode = $('#nosalesorder').val();
            if(mode == 'approve'){
                var url = "approvedraft";
                var successmessage = "Draft Sales Order " + kode + ' telah disetujui';
            }else if(mode == 'reject'){
                var url = "rejectdraft";
                var successmessage = "Draft Sales Order " + kode + ' telah ditolak';
            }
            var id = $('#idsalesorder').val();
            $.ajax({
                type: "POST",
                url: url,
                data: {id:id},
                success: function (response) {
                    toastr.success(successmessage, {
                        "onShow": setTimeout(function () {
                            $('.side-nav .nav-item.active a').click();
                        }, 2600)
                    });
                }
            });
        });

        $('#submit-so, #edit-so').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var mode = $(event.currentTarget).attr('mode');
            var kode = $('#nosalesorder').val();

            if (mode == 'save') {
                var url = "createsalesorder";
                var successmessage = 'Sales Order ' + kode + ' telah berhasil dibuat!';
            }else{
                var url = "updatesalesorder";
                var successmessage = 'Sales Order ' + kode + ' telah berhasil diubah!';
            }

            var totalbayar = 0;
            $('.bayar').each(function () {
                totalbayar += parseFloat(accounting.unformat($(this).val()));
            });
            var total = parseFloat($('#total-hidden').val());
            var installment = parseFloat($('#installment-hidden').val());

            //validasi apakah menggunakan saldo, kalau ada di cek apakah dia melebihi saldo pelanggan. kalau lebih valid saldo diubah false
            validsaldo = true;
            $('.metode-pembayaran-div').each(function () {
                if ($(this).find('select.metode').val() == 2 && parseInt($(this).find('.bayar').val()) > parseInt($('#balance').val())) {
                    validsaldo = false;
                }
            });

            if ($('.barang-row:first').find('.kategori').val() == '') {
                toastr.warning('Anda Belum Memilih Barang!');
            } else if ($('#pelanggan').val() == 0) {
                toastr.warning('Anda Belum Memilih Pelanggan!');
            } else if ($('#pembayaran').val() == 1 && totalbayar != total) {
                toastr.warning('Jumlah Bayar harus sama dengan Total untuk jenis pembayaran Cash!');
            } else if ($('#pembayaran').val() == 2 && totalbayar < installment) {
                toastr.warning('Jumlah Bayar harus  lebih / sama dengan Total Cicilan Pertama untuk jenis pembayaran Credit!');
            } else if (($('#pembayaran').val() == 2 || $('#pembayaran').val() == 4) && $('#pelanggan').val() == 1 ) {
                toastr.warning('Pembelian selain Cash dan Online hanya bisa dengan customer yang sudah terdaftar');
            } else if (validsaldo == false) {
                toastr.warning('Metode bayar saldo melebihi jumlah saldo pelanggan');
            }else if ($('#pembayaran').val() == 0) {
                toastr.warning('Anda Belum Memilih Jenis Pembayaran!');
            }else if ($('#sales').val() == 0) {
                toastr.warning('Anda Belum Memilih Sales!');
            }else if ( ($('#pembayaran').val() == 2 || $('#pembayaran').val() == 4 )&& parseInt(currentRemainingCredit) + parseInt(total) >= currentLimit) {
                $('#modal-draft').modal('open');
            }else {
                submit(url, successmessage);
            }
        });

        $('#submit-draft').on('click', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var url = "createdraftsalesorder";
            var kode = $('#nosalesorder').val();
            var successmessage =  'Draft Sales Order ' + kode + ' telah Dibuat!';
            submit(url , successmessage);
            $('#modal-draft').modal('close');
        })

        $('#formSo').on('keyup', '.percentfirst, .percentsecond', function (event) {
            event.stopImmediatePropagation();
            calculateDiskon($(this));
            calculatetotal();
        });

        $('#formSo').on('keyup', '.diskon', function (event) {
            event.stopImmediatePropagation();
            var nominal = $(this).val();
            var hargajual = accounting.unformat($(this).closest('tr').find('.hargajual').val());
            $(this).closest('tr').find('.percentsecond').val(0);
            $(this).closest('tr').find('.percentfirst').val(nominal / hargajual * 100);
            console.log(accounting.formatMoney(hargajual - nominal,'',0,',','.'));
            calculatetotal();
        });

        $('#formSo').on('keyup', '.bayar', function (event) {
            event.stopImmediatePropagation();
            $(this).val(accounting.unformat($(this).val()));
            $(this).val(accounting.formatMoney($(this).val(),'',0,',','.'));
            calculateTotalBayar();
        });

        $('#additionalCharge').on('keyup', function(event){
            $(this).val(accounting.unformat($(this).val()));
            $(this).val(accounting.formatMoney($(this).val(),'',0,',','.'));
            calculateTotalBayar();
        })

        $('#formSo').on('change', '#pembayaran', function (event) {
            event.stopImmediatePropagation();
            clearItem();
            if ($(this).val() == 1 || $(this).val() == 3) { // Cash && Online
                $('#terms').attr('disabled', true);
                $('#terms').val(1);
                barcodeFocus();
                hideCicilan();
                showDivPembayaran();
            } else if ($(this).val() == 2) { // Cicil
                $('#terms').removeAttr('disabled').val(5);
                barcodeFocus();
                showCicilan();
                showDivPembayaran();
            }else if($(this).val() == 4){
                $('#terms').removeAttr('disabled').val(1);
                hideDivPembayaran();
                barcodeFocus();
                showCicilan();
                disableCustomerInfo();
            }
        });

        $('body').on('click',"#soTable tr, #soTable .edit, #soTable .draft",function(event){
            event.stopImmediatePropagation();
            var id = $(this).attr('value');
            var mode = $(event.currentTarget).attr('mode');
            var draft = false;
            // alert(mode);
            $.ajax({
                type:"GET",
                url:"getsalesorder",
                data:{id:id},
                success:function(response){
                    console.log(response);
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    if(response.details.length > 0)
                    {
                        $('#total').val(accounting.formatMoney(response.grand_total_idr,'Rp ',2,',','.'));
                        $('#total-hidden').val(response.grand_total_idr);
                        $('#terms').val(response.term_payment).attr('disabled',true);
                        $('#idsalesorder').val(response.sales_order_id).attr('disabled',true);
                        $('#pelanggan').val(response.customer_id).attr('disabled',true);
                        $('#pembayaran').val(response.payment_type_id).attr('disabled',true);
                        $('#tglso').val(response.date_sales_order).attr('disabled',true);
                        $('#sales').val(response.sales_id).attr('disabled',true);
                        $('#notes').val(response.note).attr('disabled',true);
                        $('#customername').val(response.customer_name).attr('disabled',true);
                        $('#customerphone').val(response.customer_phone).attr('disabled',true);
                        for(var i=0; i<response.details.length; i++){
                            var $newBarcode = "barcode-" + (i+1);
                            var $newBarangId = "idbarang-" + (i+1);
                            var $newKategori = "kategori-" + (i+1);
                            var $newBrand = "brand-" + (i+1);
                            var $newType = "type-" + (i+1);
                            var $newHargaJual = "harga-jual-" + (i+1);
                            var $newHargaBeli = "harga-beli-" + (i+1);
                            var $newPercentFirst = "percent-first-" + (i+1);
                            var $newPercentSecond = "percent-second-" + (i+1);
                            var $newDiskon = "diskon-" + (i+1);
                            var $newSubtotal = "subtotal-" + (i+1);
                            var $newCicilan = "cicilan-" + (i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#barcode-1').attr('id',$newBarcode);
                            $temp.find('#idbarang-1').attr('id',$newBarangId);
                            $temp.find('#kategori-1').attr('id',$newKategori);
                            $temp.find('#brand-1').attr('id',$newBrand);
                            $temp.find('#type-1').attr('id',$newType);
                            $temp.find('#harga-jual-1').attr('id',$newHargaJual);
                            $temp.find('#harga-beli-1').attr('id',$newHargaBeli);
                            $temp.find('#percent-first-1').attr('id',$newPercentFirst);
                            $temp.find('#percent-second-1').attr('id',$newPercentSecond);
                            $temp.find('#diskon-1').attr('id',$newDiskon);
                            $temp.find('#subtotal-1').attr('id',$newSubtotal);
                            $temp.find('#cicilan-1').attr('id',$newCicilan);
                            $temp.find('.delete-detail').attr('disabled',true);
                            $temp.appendTo('#barang-data');
                            $('#idbarang-'+(i+1)).val(response.details[i].product.product_id);
                            $('#barcode-'+(i+1)).val(response.details[i].product.bar_code);
                            $('#kategori-'+(i+1)).val(response.details[i].product.category.description);
                            $('#brand-'+(i+1)).val(response.details[i].product.brand.name);
                            $('#type-'+(i+1)).val(response.details[i].product.type.name);
                            $('#harga-jual-'+(i+1)).val(response.details[i].product.price_sale).attr('disabled',true);
                            $('#harga-beli-'+(i+1)).val(response.details[i].product.price_buy).attr('disabled',true);
                            $('#percent-first-'+(i+1)).val(response.details[i].discount_percentage).attr('disabled',true);
                            $('#percent-second-'+(i+1)).val(response.details[i].discount_percentage_2).attr('disabled',true);
                            $('#diskon-'+(i+1)).val(response.details[i].discount_nominal).attr('disabled',true);
                            $('#subtotal-'+(i+1)).val(response.details[i].sub_total).attr('disabled',true);
                            $('#cicilan-'+(i+1)).val(response.details[i].sub_total /response.term_payment ).attr('disabled',true);
                            $('#no-item').attr('hidden',true);
                        }
                        $('#tambah-barang').attr('disabled',true).css('cursor','not-allowed');
                        $('.selectpicker').selectpicker('refresh');
                        $('#pelanggan').trigger('change');

                        if(response.payment_type_id == 2 || response.payment_type_id == 4){
                            showCicilan();
                            calculatetotal();
                        }else{
                            $('.cicilan').val(0);
                            hideCicilan();
                            calculatetotal();
                        }

                        if(response.first_payment){
                            showDivPembayaran();
                            var $paymentdiv = $('.metode-pembayaran-div:first');
                            $('.metode-pembayaran-div').remove();
                            for(var i = 0; i< response.first_payment.details.length; i++){
                                var $cloneddiv = $paymentdiv.clone();
                                var $newBayar = 'bayar-'+(i+1);
                                var $newMetode = 'metode-'+(i+1);

                                $cloneddiv.find('#bayar-1').attr('id',$newBayar).attr('disabled',true);
                                $cloneddiv.find('#metode-1').attr('id',$newMetode).attr('disabled',true);
                                $('#append-metode-pembayaran').append($cloneddiv);
                                $('#metode-'+(i+1)).val(response.first_payment.details[i].payment_method_id);
                                $('#bayar-'+(i+1)).val(response.first_payment.details[i].paid);
                                $cloneddiv.find('.bootstrap-select').replaceWith(function() { return $('select', this); });
                                $cloneddiv.find('select').selectpicker();
                            }
                            $('#additionalCharge').val(accounting.formatMoney(response.first_payment.additional_charge,'',0,',','.')).attr('disabled',true);
                            $('#additionalNote').val(response.first_payment.additional_note).attr('disabled',true);
                            calculateTotalBayar();
                        }else{
                            hideDivPembayaran();
                        }
                        hideDivRemaining();

                        if(response.is_draft)
                        {
                            draft = true;
                        }else{
                            draft = false;
                        }
                    }else {
                        $original.clone();
                        $original.attr('hidden', true)
                        $original.appendTo('#barang-data');
                        $('#formSo').find("input[type=text], textarea").val("");
                    }
                    // $('.selectpicker').selectpicker('refresh');
                },
                complete:function(){
                    $('#submit-so').attr('hidden',true);
                    if(mode=="draft"){
                        $('#approve-draft').removeAttr('hidden');
                        $('#reject-draft').removeAttr('hidden');
                    }else{
                        $('#approve-draft').attr('hidden',true);
                        $('#reject-draft').attr('hidden',true);
                    }
                    if(mode == "edit"){
                        $('#edit-so').removeAttr('hidden');
                        $('#cetak-so').removeAttr('hidden').attr('href',"downloadsalesorder/"+id);
                        $('.percentfirst, .percentsecond, .diskon, .metode, .bayar, #notes, #tglso, #additionalCharge, #additionalNote').removeAttr('disabled');
                        $('.selectpicker').selectpicker('refresh');
                        showDivRemaining();
                    }

                    if (draft == false){
                        $('#cetak-so').removeAttr('hidden').attr('href',"downloadsalesorder/"+id);
                    }else{
                        $('#cetak-so').attr('hidden',true);
                    }


                }
            });
        });

        $('#clear').on('click', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .nav-item.active a').click()
        });

        $('#soTable').on('click', '.delete-modal', function (event) {
            event.stopImmediatePropagation();
            var noso = $(this).closest('tr').find('.noso').html();
            var idso = $(this).closest('tr').attr('value');
            $('#confirm-delete-so').attr('value', idso).attr('nomor', noso);
            $("#delete-message").html("Yakin ingin menghapus data " + noso + " ?")
            $('#modal-delete-so').modal('open');
        });

        $('#confirm-delete-so').on('click', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-so').modal('close');
            var id = $(this).attr('value');
            var noso = $(this).attr('nomor');
            $.ajax({
                type: "POST",
                url: "deletesalesorder",
                data: {id: id},
                success: function (response) {
                    toastr.success('Sales Order ' + noso + ' telah berhasil Dihapus!', {
                        "onShow": setTimeout(function () {
                            $('.side-nav .nav-item.active a').click();
                        }, 2600)
                    });
                }
            })
        });

        $('#terms').on('change', function (event) {
            event.stopImmediatePropagation();
            calculatetotal();
        });

        $('#show-diskon').on('click', function (event) {
            showDiskon2();
        });

        $('#hide-diskon').on('click', function (event) {
            hideDiskon2();
        });

        $('#formSo').on('click','.delete-detail',function(event){
            event.stopImmediatePropagation();
            if($('.barang-row').length > 1)
            {
                $(this).closest('tr').remove();
            }else{
                $(this).closest('tr').find("input[type='text']:not(.percentfirst, .percentsecond, .diskon)").val("");
                // $(this).closest('tr').find('.id').val("").removeAttr('disabled');
            }
        });

        $('.copy-value').on('click',function(event){
            var nominal = accounting.unformat($(this).closest('.input-field').find('input[type="text"]:first').val(),',');
            $('.bayar:first').val(nominal).trigger('keyup');
        });

        //function
        function firstload() {

            $('#modalBarangTable').attr('datatable', false);

            $('#terms').val(0);

            $('.selectpicker').selectpicker('render');

            $('#tglso').dcalendarpicker({
                format: 'dd-mm-yyyy'
            });
            $('#tglso').val(moment().format('DD-MM-YYYY'));

            /*$('.total-col').html(accounting.formatMoney($(this).html(),'Rp',2,',','.'));*/
            /*$('#nosalesorder').val("SO/"+moment().format('DDMMYY')+)*/
            $.ajax({
                type: "GET",
                url: "lastsalesordernumber",
                success: function (response) {
                    $('#nosalesorder').val(response);
                }
            });

            // soTable = $('#soTable').DataTable({ // This is for home page
            //     searching: true,
            //     responsive: true,
            //     'sDom': 'tip',
            //     "bPaginate": true,
            //     "bFilter": false,
            //     "sPaginationType": "full_numbers",
            //     "iDisplayLength": 10,
            //     "aaSorting": [],
            //     "language": {
            //         "infoEmpty": "No records to display",
            //         "zeroRecords": "No records to display",
            //         "emptyTable": "No data available in table",
            //     },
            // });

            soTable = $('#soTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                'sDom': 'tip',
                "pageLength": 5,
                ajax: {
                    url : 'getsalesordertable',
                    data : function (d){
                        d.number = $('#filterSoNumber').val();
                        d.pelanggan = $('#filterPelanggan').val();
                    }
                },
                rowId : 'sales_order_id',
                columns: [
                    { data: 'sales_order_number', name: 'sales_order_number', class:'noso'},
                    { data: 'date_sales_order', name: 'date_sales_order'},
                    { data: 'customer_name', name: 'customer_name'},
                    { data: 'payment_type.payment_description', name: 'payment_description'},
                    { data: 'grand_total_idr', name: 'grand_total_idr'},
                    { data: 'staff.full_name', name: 'staff_name'},
                    { data: 'action', name: 'action'},
                ],
            });

            $('#filterSoNumber').on('keyup', function () { // This is for news page
                soTable.draw();
            });
            $('#filterPelanggan').on('keyup', function () { // This is for news page
                soTable.draw();
            });
        }

        function submit(url, message) {
            hidemainbuttongroup();
            $('.hargajual, .diskon, .subtotal, .idbarang, .cicilan, #idsalesorder , #nosalesorder, #total, #tglso, .subtotal, #terms, #pembayaran, .weight, #grandtotal, #level, #customername, #customerphone').removeAttr('disabled');
            $('#additionalCharge').val(accounting.unformat($('#additionalCharge').val()));
            $('.subtotal, .bayar, .cicilan, .hargajual').each(function(){
                $(this).val(accounting.unformat($(this).val()));
            });
            toastr.warning('Saving...',{
                timeOut: 0,
                extendedTimeOut: 0
            });
            $.ajax({
                type: "POST",
                url: url,
                data: $('#formSo').serialize(),
                success: function (response) {
                    window.open('downloadsalesorder/'+response, '_blank');
                    toastr.clear();
                    toastr.success(message, {
                        "onShow": setTimeout(function () {
                            $('.side-nav .nav-item.active a').click();
                        }, 2600)
                    });
                }
            });
        }

        //function untuk hitung subtotal dari setiap row dan grand total nya

        function calculateDiskon(elm) {
            var hargajual = accounting.unformat(elm.closest('tr').find('.hargajual').val());
            var percentfirst = elm.closest('tr').find('.percentfirst').val();
            var percentsecond = elm.closest('tr').find('.percentsecond').val();

            var diskonfirst = percentfirst / 100 * hargajual;
            var hargafirst = hargajual - diskonfirst;
            var diskonsecond = percentsecond / 100 * hargafirst
            var diskonfinal = diskonfirst + diskonsecond;

            elm.closest('tr').find('.diskon').val(diskonfinal);
            elm.closest('tr').find('.subtotal').val(accounting.formatMoney(hargajual - diskonfinal,'',0,',','.'));
        }

        function calculatetotal() {
            var total = 0;
            $('.barang-row').each(function (i, v) {
                var hargajual = parseFloat(accounting.unformat($(this).find('.hargajual').val()));
                var diskon = parseFloat($(this).find('.diskon').val());
                $(this).find('.subtotal').val(accounting.formatMoney(hargajual - diskon,'',0,',','.'));
                var subtotal = hargajual - diskon;
                total += subtotal;

                var hargabeli = $(this).find('.hargabeli').val();
                var namabarang = $(this).find('.kategori').val() + ' ' + $(this).find('.brand').val() + ' ' + $(this).find('.type').val();
                if (subtotal < hargabeli) {
                    toastr.remove();
                    toastr.warning('harga ' + namabarang + ' dibawah harga modal' ,'', {"timeOut": "0","extendedTimeOut": "0"});
                }else{
                    toastr.clear();
                }

                var terms = $('#terms').val();
                var cicilan = subtotal / terms;
                $(this).find('.cicilan').val(accounting.formatMoney(cicilan,'',0,',','.'));
            });

            $('#total').val(accounting.formatMoney(total, 'Rp ', 2, '.', ','));
            $('#total-hidden').val(total);

            if ($('#pembayaran').val() == 2 || $('#pembayaran').val() == 4) {
                var terms = $('#terms').val();
                $('#installment-div').removeAttr('hidden');
                $('#installment').val(accounting.formatMoney((total / terms), 'Rp ', 2, '.', ','));
                $('#installment-hidden').val(total / terms);

                $('#payable-div').removeAttr('hidden');
                $('#payable').val(accounting.formatMoney(total - (total / terms), 'Rp ', 2, '.', ','));
            }else{
                $('#installment').val('');
                $('#installment-hidden').val('');
                $('#installment-div').attr('hidden',true);

                $('#payable').val('');
                $('#payable-div').attr('hidden',true);
            }
        }

        function barcodeFocus() {
            setTimeout(function () {
                $('#barcode').focus()
            }, 1000);
        }

        function clearItem() {
            var $orginal = $('.barang-row:first');
            var $cloned = $orginal.clone().addClass('cloned');
            $cloned.find("input").not('.percent-sym').val('');
            $cloned.find(".percentfirst, .percentsecond, .diskon, .subtotal").val(0);
            $('.barang-row').remove();
            $cloned.appendTo('#barang-data');
        }

        function disablePembayaran() {
            $('#append-metode-pembayaran select, #append-metode-pembayaran input, #append-metode-pembayaran a').attr('disabled', true);
            $('.selectpicker').selectpicker('refresh');
        }

        function enablePembayaran() {
            $('#append-metode-pembayaran select, #append-metode-pembayaran input, #append-metode-pembayaran a').removeAttr('disabled');
            $('.selectpicker').selectpicker('refresh');
        }

        function hideCicilan() {
            $('#cicilanheader, .cicilan-data').attr('hidden', true);
        }

        function showCicilan() {
            $('#cicilanheader, .cicilan-data').removeAttr('hidden');
        }

        function showDiskon2() {
            $('#show-diskon').css('display', 'none');
            $('#hide-diskon').css('display', 'inline-block');
            $('.diskon2header, .diskon2-data').removeAttr('hidden');
        }

        function enableCustomerInfo() {
            $('#customername').removeAttr('disabled');
            $('#customerphone').removeAttr('disabled');
        }

        function disableCustomerInfo() {
            $('#customername').attr('disabled',true);
            $('#customerphone').attr('disabled',true);
        }

        function hideDiskon2() {
            $('#hide-diskon').css('display', 'none');
            $('#show-diskon').css('display', 'inline-block');
            $('.diskon2-data .percentsecond').val(0);
            $('.diskon2header, .diskon2-data').attr('hidden', true);
            $('.barang-row').each(function (i,v){
                calculateDiskon($(this).find('.percentfirst'));
            });
        }

        function hidetambahbaris(){
            $('#tambah-barang').attr('hidden',true);
        }

        function showtambahbaris(){
            $('#tambah-barang').removeAttr('hidden');
        }

        function showDivPembayaran(){
            $('#div-pembayaran').removeAttr('hidden');
        }
        function hideDivPembayaran(){
            $('#div-pembayaran').attr('hidden',true);
        }
        function hideDivRemaining(){
            $('#div-remaining').attr('hidden',true);
        }
        function showDivRemaining(){
            $('#div-remaining').removeAttr('hidden');
        }

        function calculateTotalBayar() {
            if($('#pembayaran').val() == 1 || $('#pembayaran').val() == 3)
            {
                var totalso = parseFloat($('#total-hidden').val());
            }else if($('#pembayaran').val() == 2 ){
                var totalso = parseFloat($('#installment-hidden').val());
            }
            var total = 0;
            $('.bayar').each(function () {
                total += parseFloat(accounting.unformat($(this).val()));
            });

            var addcharge = parseFloat(accounting.unformat($('#additionalCharge').val()));
            $('#totalbayar').val(accounting.formatMoney(total + addcharge, 'Rp ', 2, '.', ','));
            $('#remaining').val(accounting.formatMoney(totalso - total, 'Rp ', 2, '.', ','));
        }

        function hidemainbuttongroup(){
            $('.main-button-group a').attr('hidden',true);
        }
    });
</script>