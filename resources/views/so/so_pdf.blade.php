<style type="text/css">
    table.table {
        border-collapse: collapse;
    }
    /*table.table, table.table th, table.table td{*/
        /*border: 1px solid black;*/
    /*}*/
    p , span{
        font-size: 12px;
        font-weight: 700;
    }
    body{
        margin:0px;
    }
    html { margin: 1cm 1cm 0cm 1cm}
</style>
<body>
<div style="height: 1cm; box-sizing: border-box; text-align: center" >
  {{$so->payment_type->payment_description}}
</div>
<div style="">
    <table style ="width:100%">
        <tr>
            <td style="width: 2.5cm"><span></span></td>
            <td style="width: 11cm"><span>{{$so->sales_order_number}}</span></td>
            <td style="width: 2.2cm"><span></span></td>
            <td><span>{{$so->date_sales_order}}</span></td>
        </tr>
        <tr>
            <td ><span></span></td>
            <td ><span>{{$so->customer_name}}</span></td>
            <td ><span></span></td>
            <td ><span>IDR (Rupiah)</span></td>
        </tr>
        <tr>
            <td ><span></span></td>
            <td ><span>{{$so->customer->phone}}</span></td>
            <td ><span></span></td>
            <td ><span>{{$so->first_payment->details->count() == 1 ? $so->first_payment->details[0]->method->bank->bank_name.' '.$so->first_payment->details[0]->method->payment_method_type->description : 'Mixed Payment'}}</span></td>
        </tr>
        <tr>
            <td style="width: 2.5cm"><span></span></td>
            <td colspan="3"><span>{{$so->customer->address}}</span></td>
        </tr>
    </table>
    <div>
        <p>Note : {{$so->note}}</p>
    </div>
    <table style="width:100%; clear: both; margin-top:10px">
        <thead>
        <tr>
            <th class="color" style="text-align: center;"><span>No</span></th>
            <th class="color" style="text-align: center;"><span>Kode</span></th>
            <th class="color" style="text-align: center;"><span>Nama</span></th>
            <th class="color" style="text-align: center;"><span>Jumlah</span></th>
            <th class="color" style="text-align: center;"><span>Unit</span></th>
            <th class="color" style="text-align: center;"><span>Harga</span></th>
            <th class="color" style="text-align: center;"><span>Diskon</span></th>
            <th class="color" style="text-align: center;"><span>Sub Total</span></th>
        </tr>
        </thead>
        <tbody >
        @foreach($so->details as $key => $value)
            <tr style="text-align: center">
                <td>
                    <span>{{($key + 1)}}</span>
                </td>
                <td>
                    <span>{{$value->product->bar_code}}</span>
                </td>
                <td>
                    <span>{{$value->product->category->description.' '.$value->product->brand->name.' '.$value->product->type->name}}</span>
                </td>
                <td>
                    <span>1</span>
                </td>
                <td>
                    <span>Pcs</span>
                </td>
                <td>
                    <span>{{number_format($value->price)}}</span>
                </td>
                <td>
                    <span>{{$value->discount_percentage}}</span>
                </td>
                <td>
                    <span>{{number_format($value->sub_total)}}</span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div style="position: fixed; top:70mm; left:120mm" >
  <p style="margin: 1.5mm; padding: 0">Additional Charge {{ $so->first_payment->additional_note? "( ".$so->first_payment->additional_note." )" :''}} : {{number_format($so->first_payment->additional_charge)}}</p>
</div>
<div style="position: fixed; top:78mm; left:15mm" >
  <p style="margin: 1.5mm; padding: 0">Terbilang : {{$terbilang}} Rupiah</p>
</div>
<div style="position: fixed; top:78mm; left:155mm" >
    <p style="margin: 1.5mm; padding: 0">{{number_format($so->grand_total_idr)}}</p>
    <p style="margin: 1.5mm; padding: 0">{{number_format($so->first_payment->details->sum('paid'))}}</p>
    <p style="margin: 1.5mm; padding: 0">{{number_format($so->grand_total_idr - $so->first_payment->details->sum('paid'))}}</p>
</div>
<div style="position: fixed; top:94mm; left:120mm">
  <p style="margin: 3.0mm 1.5mm 1.5mm 1.5mm; padding: 0">Tambah Saldo: {{number_format($so->first_payment->overpaid)}}</p>
</div>
</body>
