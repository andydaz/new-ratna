<style>
    #logo{
        display: flex;
        min-height:320px;
        display: flex;
    }
    #logo img{
        margin: auto;
        max-width:240px;
        max-height:70px;
    }
</style>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="row">
    <div class="row" style="margin: 0px">
        <div id="logo" class="col l6 m6 s12">
            <img src="{{asset('images/logo.png')}}" alt="">
        </div>
        <div id="news" class="col l6 m6 s12">

        </div>
    </div>

    <div class="row" style="margin: 0px">
        <div id="stock" class="col l6 m6 s12">

        </div>

        <div id="expense" class="col l6 m6 s12">

        </div>
    </div>

    <div id="salesachievement" class="col l12 m12 s12" @if(Session('roles')->name != 'master') hidden @endif>

    </div>

    <div id="purchaseachievement" class="col l12 m12 s12">

    </div>

    <div id="invoicedue" class="col l12 m12 s12">

    </div>

    <div id="accountreceiveable" class="col l12 m12 s12">

    </div>

    <div id="piutang-so">

    </div>

    <div id="piutang-po">

    </div>

    <div id="target">

    </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        loadinvoicedue();
        loadstock();
        loadnews();
        loadexpense();
        loadsalesachievement();
        loadpurchaseachievement();
        loadaccountreceiveable();

        // loadpiutangso();
        // loadpiutangpo();
        homeTable_2 = $('#homeTable_2').DataTable({ // This is for home page
            searching: true,
            responsive: true,
            "scrollY": "400px",
            'sDom': 'ti',
            'pagingType': 'full_numbers_no_ellipses',
            "language": {
                "infoEmpty": "No records to display",
                "zeroRecords": "No records to display",
                "emptyTable": "No data available in table",
            },
        });

        $(document).on('click', '.po-paginator .pagination a', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                type:"Get",
                url:url,
                success:function(response){
                    $('#piutang-po').html(response);
                }
            });
        });

        $(document).on('click', '.so-paginator .pagination a', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                type:"Get",
                url:url,
                success:function(response){
                    $('#piutang-so').html(response);
                }
            });
        });
    });

    function loadnews(){
        $.ajax({
            type:'GET',
            url: "home/news",
            datatype:"html",
            data :{},
            success:
                function(response){
                    $('#news').html(response);
                }
        });
    }

    function loadstock(){
        $.ajax({
            type:'GET',
            url:'home/stock',
            datatype:"html",
            data:{},
            success:function(response){
                $('#stock').html(response);
            },
        });
    }

    function loadinvoicedue(){
        $.ajax({
            type:'GET',
            url:'home/salesinvoicedue',
            datatype:"html",
            data:{},
            success:function(response){
                $('#invoicedue').html(response);
            }
        });
    }

    function loadexpense(){
        $.ajax({
            type:'GET',
            url:'home/expense',
            datatype:"html",
            data:{},
            success:function(response){
                $('#expense').html(response);
            }
        });
    }

    function loadsalesachievement(){
        $.ajax({
            type:'GET',
            url:'home/salesachievement',
            datatype:"html",
            data:{},
            success:function(response){
                $('#salesachievement').html(response);
            }
        });
    }

    function loadpurchaseachievement(){
        $.ajax({
            type:'GET',
            url:'home/purchaseachievement',
            datatype:"html",
            data:{},
            success:function(response){
                $('#purchaseachievement').html(response);
            }
        });
    }

    function loadaccountreceiveable(){
        $.ajax({
            type:'GET',
            url:'home/accountreceiveable',
            datatype:"html",
            data:{},
            success:function(response){
                $('#accountreceiveable').html(response);
            }
        });
    }

    function closecolapsible(){

    }
</script>





