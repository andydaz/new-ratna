<div id="nt-example2-container">
  <ul id="nt-example2">
    <?php 
    $i=1;
    $data="";$tgl = "";
    $count = 0;
    foreach($hasil as $row)
    {
      $count = count($hasil);
      $dt = date('d M', strtotime($row->date));
      if($i==1){
        $data = nl2br($row->description);
        $tgl = $dt;
      }
      echo "<li class='light-blue.darken-2' data-infos='".nl2br($row->description);
      echo "'><span class='hour' style='display : none'> $dt </span>".$row->title."</li>";
      /*echo "<span class='hour'> $dt </span>- ".$row->Biaya."</li>";*/
      $i++;
    }
    ?>
  </ul>
  <div id="nt-example2-infos-container">
    <div id="nt-example2-infos-triangle"></div>
    <div id="nt-example2-infos" class="row">
      <div class="span4 centered">
        <div class="infos-hour">
          {!!$tgl!!}
        </div>
      </div>
      <div class="span8">
        <div class="infos-text">
          {!!$data!!}
        </div>
      </div>
      <div id="btn-container">
        <i class="fa fa-arrow-left" id="nt-example2-prev"></i>
        <i class="fa fa-arrow-right" id="nt-example2-next"></i>
      </div>
    </div>
  </div>  
</div>
<script>
  $(function (){
    var nt_example2 = $('#nt-example2').newsTicker({
      row_height: 60,
      max_rows: 1,
      speed: 1000,
      duration: 6000,
      prevButton: $('#nt-example2-prev'),
      nextButton: $('#nt-example2-next'),
      hasMoved: function() {
        $('#nt-example2-infos-container').fadeOut(200, function(){
          $('#nt-example2-infos .infos-hour').text($('#nt-example2 li:first span').text());
          $('#nt-example2-infos .infos-text').html($('#nt-example2 li:first').data('infos'));
          $(this).fadeIn(400);
        });
      },
      pause: function() {
        $('#nt-example2 li i').removeClass('fa-play').addClass('fa-pause');
      },
      unpause: function() {
        $('#nt-example2 li i').removeClass('fa-pause').addClass('fa-play');
      }
    });
    $('#nt-example2-infos').hover(function(){
      nt_example2.newsTicker('pause');
    }, 
    function() {
      nt_example2.newsTicker('unpause');
    });
  });
</script>