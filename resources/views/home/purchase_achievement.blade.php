<ul class="collapsible" data-collapsible="accordion">
    <li>
        <div class="collapsible-header red darken-1 white-text"><i class="material-icons">attach_money</i>Purchase Achievement</div>
        <div class="collapsible-body">
            <div class="container-fluid">
                <div class="table-responsive bordered margin-top padding-bottom">
                    <table class="highlight table table-bordered display nowrap dtr-inline">
                        <thead>
                        <tr>
                            <th rowspan="2">{{\Carbon\Carbon::now()->format('F')}}</th>
                            <th colspan="2" style="text-align: center;">PO</th>
                        </tr>
                        <tr>
                            <th style="text-align: center;">Qty</th>
                            <th style="text-align: center;">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($productcat as $key => $value)
                            <tr>
                                <td>{{$value->description}}</td>
                                <td style="text-align: center;">{{$detailpurchase->where('product_category_id',$value->product_category_id)->count()}}</td>
                                <td style="text-align: center;">{{number_format($detailpurchase->where('product_category_id',$value->product_category_id)->sum('net_price_buy'))}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>Total</td>
                            <td style="text-align: center;">{{$detailpurchase->count()}}</td>
                            <td style="text-align: center;">{{number_format($detailpurchase->sum('net_price_buy'))}}</td>
                        </tr>
                        <tr>
                            <td>Supplier Paid</td>
                            <td colspan="2" style="text-align: center;">{{number_format($purchasepayment->sum('total_paid') + $purchasepayment->sum('deposit_reduction'))}}</td>
                        </tr>
                        <tr>
                            <td>Total Outstanding</td>
                            <td colspan="2" style="text-align: center;">{{number_format($outstanding)}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </li>
</ul>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $('body').on('click', '.mail', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            var url = "sendmail/"+$(this).closest('tr').attr('value');
            $.ajax({
                url : url,
                method:"GET",
                success:function (response) {
                    toastr.success('Email Telah Dikirim');
                }
            })
        });
    })
</script>
