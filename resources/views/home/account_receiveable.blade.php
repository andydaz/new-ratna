<ul class="collapsible" data-collapsible="accordion">
    <li>
        <div class="collapsible-header red darken-1 white-text"><i class="material-icons">attach_money</i>Aging AR</div>
        <div class="collapsible-body">
            <div class="container-fluid">
                <div class="table-responsive bordered margin-top padding-bottom">
                    <table id="homeTable_3" class="table table-bordered display nowrap dataTable dtr-inline">
                        <thead>
                        <tr>
                            <th>Scale</th>
                            <th>#Customer</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                          $countcustomer = 0;
                          $counttotal = 0;
                        @endphp
                        <tr>
                            <td>{{'>1 Year'}}</td>
                            <td>
                              @php
                                $count = $invoiceunpaid->where('due_date','<',\Carbon\Carbon::now()->subDays(360))
                                  ->groupBy('customer_id')
                                  ->count();
                              $countcustomer = $countcustomer + $count;
                              @endphp
                                {{$count}}
                            </td>
                            <td>
                              @php
                              $nominal1 = $invoiceunpaid->where('due_date','<',\Carbon\Carbon::now()->subDays(360))
                                  ->sum('total_amount');
                              $counttotal = $counttotal + $nominal1;
                              @endphp
                                {{number_format($nominal1)}}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'>6 Month'}}</td>
                            <td>
                              @php
                                $count2 = $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(360)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(180)->toDateString())
                                ->groupBy('customer_id')
                                ->count();
                              $countcustomer = $countcustomer + $count2;
                              @endphp
                                {{$count2}}
                            </td>
                            <td>
                              @php
                                $nominal2 = $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(360)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(180)->toDateString())
                                ->sum('total_amount');
                                $counttotal = $counttotal + $nominal2;
                              @endphp
                                {{number_format($nominal2)}}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'3-6 Month'}}</td>
                            <td>
                              @php
                                $count3 = $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(180)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(90)->toDateString())
                                ->groupBy('customer_id')
                                ->count();
                              $countcustomer = $countcustomer + $count3;
                              @endphp
                                {{$count3}}
                            </td>
                            <td>
                              @php
                                $nominal3 = $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(180)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(90)->toDateString())
                                ->sum('total_amount');
                                $counttotal = $counttotal + $nominal3;
                              @endphp
                                {!! number_format($nominal3) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'2-3 Month'}}</td>
                            <td>
                              @php
                                $count4 = $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(90)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(60)->toDateString())
                                ->groupBy('customer_id')
                                ->count();
                              $countcustomer = $countcustomer + $count4;
                              @endphp
                                {{$count4}}
                            </td>
                            <td>
                              @php
                                $nominal4 = $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(90)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(60)->toDateString())
                                ->sum('total_amount');
                                $counttotal = $counttotal + $nominal4;
                              @endphp
                                {!! number_format($nominal4) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'1 Month'}}</td>
                            <td>
                              @php
                                $count5 = $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(60)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(30)->toDateString())
                                ->groupBy('customer_id')
                                ->count();
                              $countcustomer = $countcustomer + $count5;
                              @endphp
                                {{$count5}}
                            </td>
                            <td>
                              @php
                                $nominal5 = $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(60)->toDateString())
                                ->where('due_date','<',\Carbon\Carbon::now()->subDays(30)->toDateString())
                                ->sum('total_amount');
                                $counttotal = $counttotal + $nominal5;
                              @endphp
                                {!! number_format($nominal5) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'0 Month'}}</td>
                            <td>
                              @php
                                $count6 = $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(30)->toDateString())
                                ->groupBy('customer_id')
                                ->count();
                              $countcustomer = $countcustomer + $count6;
                              @endphp
                                {!! $count6 !!}
                            </td>
                            <td>
                              @php
                                $nominal6 = $invoiceunpaid->where('due_date','>=',\Carbon\Carbon::now()->subDays(30)->toDateString())
                                ->sum('total_amount');
                                $counttotal = $counttotal + $nominal6;
                              @endphp
                                {!! number_format($nominal6) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>{{'Total'}}</td>
                            <td>{{number_format($countcustomer)}}</td>
                            <td>{{number_format($counttotal)}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </li>
</ul>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
    })
</script>
