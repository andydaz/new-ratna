<ul class="collapsible" data-collapsible="accordion">
    <li>
        <div class="collapsible-header red darken-1 white-text"><i class="material-icons">attach_money</i>Stock Overview</div>
        <div class="collapsible-body">
            <div class="container-fluid">
                <div class="table-responsive bordered margin-top padding-bottom">
                    <table id="homeTableStock" class="table table-bordered display nowrap dataTable dtr-inline">
                        <thead>
                        <tr>
                            <th>Kategori</th>
                            <th>Total Qty</th>
                            @foreach($years as $key => $value)
                                <td>{{$value->years}}</td>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $totalqty = 0;
                            foreach ($years as $key => $value)
                            {
                                ${"total$value->years"} = 0;
                            }
                        @endphp
                        @foreach($category as $key => $value)
                            <tr>
                                <td>{{$value->description}}</td>
                                <td>
                                    {{number_format($stock->where('product_category_id',$value->product_category_id)->count())}}
                                    @php
                                        $totalqty += $stock->where('product_category_id',$value->product_category_id)->count();
                                    @endphp
                                </td>
                                @foreach($years as $key2 => $value2)
                                <td>
                                    {{number_format($stock->where('product_year', $value2->years)->where('product_category_id',$value->product_category_id)->count())}}
                                    @php
                                        ${"total$value2->years"} +=  $stock->where('product_year', $value2->years)->where('product_category_id',$value->product_category_id)->count();
                                    @endphp
                                </td>
                                @endforeach
                            </tr>
                        @endforeach
                        <tr>
                            <td>Total</td>
                            <td>{{number_format($totalqty)}}</td>
                            @foreach($years as $key => $value)
                                <td>{{ number_format(${"total$value->years"}) }}</td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </li>
</ul>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        // homeTableStock = $('#homeTableStock').DataTable({ // This is for home page
        //     searching: true,
        //     responsive: true,
        //     bSortClasses: false,
        //     'sDom': 'ti',
        //     'pagingType': 'full_numbers_no_ellipses',
        //     "language": {
        //         "infoEmpty": "No records to display",
        //         "zeroRecords": "No records to display",
        //         "emptyTable": "No data available in table",
        //     },
        // });
    })
</script>
