<ul class="collapsible" data-collapsible="accordion">
  <li>
    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">attach_money</i>Piutang Penjualan</div>
    <div class="collapsible-body">
      <div class="container-fluid">
        <div class="table-responsive margin-top padding-bottom">
          <table id="homeTable_2" class="table table-bordered display nowrap dataTable dtr-inline">
           <thead>
            <tr>
              <th>Jatuh Tempo</th>
              <th>Tanggal SO</th>
              <th>Pelanggan</th>
              <th>Total SO</th>
              <th>Sisa Piutang</th>
            </tr>
          </thead>
          <tbody>
            @foreach($piutang_so as $key => $value)
            <tr>
              <td>{{$value->jatuh_tempo}}</td>
              <td>{{$value->date_sales_order}}</td>
              <td>{{$value->company_name}}</td>
              <td class="number">{{$value->grand_total_idr}}</td>
              <td class="number">{{$value->grand_total_idr - $value->total_paid}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <div class="so-paginator">
        @if(!empty($piutang_so))
          {{--{{$piutang_so->links()}}--}} 
        @endif
        </div>
      </div>
    </div>
  </li>
</ul>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $('.number').each(function(){
          $(this).html(accounting.formatMoney($(this).html(),'Rp ',2,',','.'));
      });
  })
</script>