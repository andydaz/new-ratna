<ul class="collapsible" data-collapsible="accordion">
  <li>
    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">attach_money</i>Utang Pembelian</div>
    <div class="collapsible-body">
      <div class="container-fluid">
        <div class="table-responsive bordered margin-top padding-bottom">
          <table id="homeTable_3" class="table table-bordered display nowrap dataTable dtr-inline">
            <thead>
              <tr>
                <th>Jatuh Tempo</th>
                <th>PO</th>
                <th>Supplier</th>
                <th>Utang</th>
              </tr>
            </thead>
            <tbody>
              @foreach($piutang_po as $row)
              @php
              @endphp
              <tr>
                <td class="jatuh-tempo">{{$row->jatuh_tempo}}</td>
                <td>{{$row->purchase_order_number}}</td>
                <td>{{$row->company_name}}</td>
                <td style='text-align:right;' val='{{$row->total_amount}}'>{{number_format($row->total_amount,2,",",".")}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </li>
</ul>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });
  
    homeTable_3 = $('#homeTable_3').DataTable({ // This is for home page
      searching: true,
      responsive: true,
      bSortClasses: false,
      'sDom': 'ti',
      'pagingType': 'full_numbers_no_ellipses',
      "language": {
        "infoEmpty": "No records to display",
        "zeroRecords": "No records to display",
        "emptyTable": "No data available in table",
      },
    });

    // $('.jatuh-tempo').each(function(key,value){
    //   var jatuhtempo = moment($(this).html(), 'YYYY-MM-DD').format('YYYY, MM, DD');
    //   var now = moment().format('YYYY, MM, DD');

    //   console.log(jatuhtempo);
    //   console.log(now);

    //   var a = moment(jatuhtempo);
    //   var b = moment(now);
    //   var diff = a.diff(b, 'days');
      
    //   if(diff < 7)
    //   {
    //     $(this).closest('tr').css({'background-color':'#E53935', 'color':'white'});
    //   }else{
    //     $(this).closest('tr').css({'background-color':'#E53935', 'color':'white'});
    //   }
    // })
  })
</script>
