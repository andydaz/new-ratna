<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text"><i class="material-icons">search</i>Cari Pembayaran Supplier</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row margin-top">
                                <div class="input-field col l3">
                                    <input id="filterPayment" type="text" class="f-input">
                                    <label>Filter Payment No.</label>
                                </div>
                                <div class="input-field col l3">
                                    <input id="filterSupplier" type="text" class="f-input">
                                    <label>Filter Supplier</label>
                                </div>
                                <div class="col l12 m12 s12 margin-top">
                                    <div class="table-responsive">
                                        <table id="purchasePaymentTable" class="highlight table table-bordered display nowrap dataTable dtr-inline" style="width: 100%">
                                            <thead>
                                            <tr>
                                                <th>No Pembayaran</th>
                                                <th>Supplier</th>
                                                <th>Total</th>
                                                <th>Payment Method</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {{--@foreach($data['payment'] as $key => $value)--}}
                                                {{--<tr value="{{$value->payment_purchase_id}}">--}}
                                                    {{--<td class="nopayment">{{$value->payment_purchase_number}}</td>--}}
                                                    {{--<td>{{$value->company_name}}</td>--}}
                                                    {{--<td>{{number_format($value->total_paid + $value->deposit_reduction)}}</td>--}}
                                                    {{--<td>{{$value->bank_name.' '.$value->description}}</td>--}}
                                                    {{--<td>--}}
                                                        {{-- <a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->invoice_purchase_id}}"><i class="material-icons">edit</i></a> --}}
                                                        {{--<a class="btn btn-sm btn-raised red delete-modal tooltipped" data-position="bottom" data-tooltip="delete"><i class="material-icons">delete</i></a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Pembayaran Supplier</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <form id="formPayment">
                                    <br/>
                                    <div class="input-field col l4">
                                        <label>No Pembayaran</label>
                                        <input id="nopayment" type="text" name="nopayment" class="f-input restrict" placeholder="No Pembayaran">
                                        <input id="idpayment" type="text" name="id" class="f-input" placeholder="No Pembayaran" hidden>
                                    </div>
                                    <div class="input-field col l4">
                                        <label>No Invoice</label>
                                        <select id="noinvoice" class="browser-default selectpicker" name="noinvoice" data-live-search="true">
                                            <option value="0">Select Invoice</option>
                                            @foreach($data['invoice'] as $key => $value)
                                                <option value='{{$value->invoice_purchase_id}}'>{{$value->company_name.' / '.$value->invoice_purchase_number}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input-field col l4">
                                        <label>Supplier</label>
                                        <input id="supplier" type="text" name="supplier" class="f-input" placeholder="Supplier" disabled>
                                    </div>
                                    <div class="col l12 m12 s12 margin-top">
                                        <div class="table-responsive">
                                            <table class="stoko-table no-border">
                                                <thead>
                                                <tr>
                                                    <th class="theader">Grand Total</th>
                                                    <th class="theader">Diskon Pembayaran</th>
                                                    <th class="theader">Deposito Supplier</th>
                                                    <th class="theader">Total Terbayar</th>
                                                    <th class="theader">Nominal Pembayaran</th>
                                                    <th class="theader">Tipe Pembayaran</th>
                                                    <th class="theader">Payment Method</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                                                <tr id="payment-row" hidden>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="total" name="total" type="text" class="f-input" disabled>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="diskon" type="text" class="f-input" disabled>
                                                            <input id="diskon-hidden" name="diskon" type="text" class="f-input" hidden>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="deposito" type="text" class="f-input" disabled>
                                                            <input id="deposito-hidden" name="deposito" type="text" class="f-input" hidden>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="total-paid" type="text" class="f-input" disabled>
                                                            <input id="total-paid-hidden" name="total-paid" type="text" class="f-input" hidden>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="pembayaran" type="text" class="f-input" disabled>
                                                            <input id="pembayaran-hidden" name="pembayaran" type="text" class="f-input" hidden>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="paymenttype" type="text" class="f-input" disabled>
                                                            <input id="ptid" type="text" name="paymenttype" class="f-input" hidden>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <select id="paymentmethod" name="paymentmethod" class="f-select">
                                                                @foreach($data['paymentmethod'] as $key => $value)
                                                                    <option value="{{$value->payment_method_id}}">{{$value->bank->bank_name.' '.$value->payment_method_type->description}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col l12 s12 m12 main-button-group margin-top">
                                        <a href="#" class="btn-stoko teal white-text submit">simpan</a>
                                        <!-- <a id="" href="#" class="btn-stoko red white-text">hapus</a> -->
                                        <a id="clear" href="#" class="btn-stoko orange">batal</a>
                                        <a id="cetak-payment" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Payment</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Details PO</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <form id="formPo">
                                    <div class="col l12 m12 s12">
                                        <div class="table-responsive" style="overflow: visible;">
                                            <br>
                                            <table class="stoko-table no-border">
                                                <thead>
                                                <tr>
                                                    <th class="theader">Kategori Barang</th>
                                                    <th class="theader">Brand</th>
                                                    <th class="theader">Tipe Barang</th>
                                                    <th class="theader">Harga Beli</th>
                                                    <th class="theader">Diskon</th>
                                                    <th class="theader">Harga Jual</th>
                                                    <th class="theader">Rate Keuntungan</th>
                                                </tr>
                                                </thead>
                                                <tbody id="barang-data">
                                                <tr id="no-item-po"><td colspan="7"><span> No Item Selected</span></td></tr>
                                                <tr class="barang-row" hidden>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="id-1" type="text" class="f-input id-detail" name="idbarang[]" hidden>
                                                            <input id="kategori-1" type="text" class="f-input" name="kategori[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="brand-1" type="text" class="f-input brand" name="brand[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="type-1" type="text" class="f-input type" name="type[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="harga-beli-1" type="text" class="f-input hargabeli" name="hargabeli[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="diskon-1" type="text" class="f-input diskon" name="diskon[]" value="0">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="harga-jual-1" type="text" class="f-input hargajual" name="hargajual[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="rate-1" type="text" class="f-input rate" name="rate[]" disabled>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <br>
                                            <div id="discount-transaction-div" class="input-field" hidden>
                                                <label>Diskon Transaksi</label>
                                                <input id="diskon-transaksi" type="text" class="f-input diskon-transaksi" name="diskontransaksi" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="modal-delete-payment" class="modal">
    <div class="modal-content">
        <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
        <a id="confirm-delete-payment" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });
        firstload();

        $('#noinvoice').on('change',function(event){
            event.stopImmediatePropagation();
            var id = $(this).val();
            $.ajax({
                type:"GET",
                url:"getpurchaseinvoiceforpayment",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    if(response.length == 0){
                        $('#formPayment').find("input[type=text], textarea").not('.restrict').val("");
                        $('#payment-row').attr('hidden',true);
                        $('#discount-transaction-div').attr('hidden',true);
                        $('#no-item').removeAttr('hidden');
                        $('#no-item-po').removeAttr('hidden');

                        $original.clone();
                        $('.barang-row').remove();
                        $original.attr('hidden',true);
                        $original.appendTo('#barang-data');
                    }else{
                        var $cloned = $original.clone();
                        $('.barang-row').remove();

                        $('#no-item').attr('hidden',true);
                        $('#no-item-po').attr('hidden',true);
                        $('#payment-row').removeAttr('hidden');
                        $('#supplier').val(response.invoice.company_name);
                        $('#paymenttype').val(response.invoice.payment_description);
                        $('#ptid').val(response.invoice.payment_type_id);
                        $('#total').val(accounting.formatMoney(parseInt(response.invoice.total_amount) + parseInt(response.invoice.total_discount),'Rp ',2,',','.'));
                        $('#diskon').val(accounting.formatMoney(response.invoice.total_discount,'Rp ',2,',','.'));
                        $('#diskon-hidden').val(response.invoice.total_discount);
                        $('#deposito').val(accounting.formatMoney(response.invoice.deposit,'Rp ',2,',','.'));
                        $('#deposito-hidden').val(response.invoice.deposit);
                        $('#total-paid').val(accounting.formatMoney(response.payment,'Rp ',2,',','.'));
                        $('#total-paid-hidden').val(response.payment);
                        $('#pembayaran').val(accounting.formatMoney(response.invoice.total_amount - response.invoice.deposit - response.payment, 'Rp ',2,',','.'));
                        $('#pembayaran-hidden').val(response.invoice.total_amount - response.invoice.deposit - response.payment);

                        if(response.invoice.payment_type_id == 2){
                            $('#pembayaran').removeAttr('disabled');
                        }

                        $.each(response.podetail, function(k,v){
                            var newid = "id-"+(k+1);
                            var newKategori = "kategori-"+(k+1);
                            var newBrand = "brand-"+(k+1);
                            var newType = "type-"+(k+1);
                            var newHargaBeli = "harga-beli-"+(k+1);
                            var newHargaJual = "harga-jual-"+(k+1);
                            var newDiskon = "diskon-"+(k+1);
                            var newRate = "rate-"+(k+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id',newid);
                            $temp.find('#kategori-1').attr('id',newKategori);
                            $temp.find('#brand-1').attr('id',newBrand);
                            $temp.find('#type-1').attr('id',newType);
                            $temp.find('#harga-jual-1').attr('id',newHargaJual);
                            $temp.find('#harga-beli-1').attr('id',newHargaBeli);
                            $temp.find('#diskon-1').attr('id',newDiskon);
                            $temp.find('#rate-1').attr('id',newRate);
                            $temp.appendTo('#barang-data');
                            $('#id-'+(k+1)).val(v.purchase_order_details_id).attr('disabled',true);
                            $('#kategori-'+(k+1)).val(v.c_name).attr('disabled',true);
                            $('#brand-'+(k+1)).val(v.b_name).attr('disabled',true);
                            $('#type-'+(k+1)).val(v.t_name).attr('disabled',true);
                            $('#harga-beli-'+(k+1)).val(accounting.formatMoney(v.price_buy,'',0,',','.')).attr('disabled',true);
                            $('#harga-jual-'+(k+1)).val(accounting.formatMoney(v.price_sale,'',0,',','.')).attr('disabled',true);
                            $('#rate-'+(k+1)).val(v.profit_rate).attr('disabled',true);
                            $('#diskon-'+(k+1)).val(v.discount_nominal).attr('disabled',true);
                        });

                        $('#diskon-transaksi').val(response.invoice.discount_transaction).attr('disabled',true);
                        $('#discount-transaction-div').removeAttr('hidden');
                    }
                }
            });
        })

        $('#purchasePaymentTable').on('click', 'tr', function(){
            var id = $(this).attr('value');
            $.ajax({
                type:"GET",
                url:"getpurchasepayment",
                data: {id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    $('#noinvoice').append('<option class="remove-when-clear" value="'+response.payment.invoice_purchase_id+'" selected="selected">'+response.payment.invoice_purchase_number+'</option>').attr('disabled',true);
                    $('#nopayment').val(response.payment.payment_purchase_number).attr('disabled',true);
                    $('#supplier').val(response.payment.company_name);
                    $('#total').val(accounting.formatMoney(response.payment.grand_total_idr,'Rp ',2,'.','.'));
                    $('#pembayaran').val(accounting.formatMoney(response.payment.total_paid,'Rp ',2,',','.'));
                    $('#diskon').val(accounting.formatMoney(response.payment.discount_nominal,'Rp ',2,',','.'));
                    $('#deposito').val(accounting.formatMoney(response.payment.deposit_reduction,'Rp ',2,',','.'));
                    $('#paymenttype').val(response.payment.payment_description);
                    $('#paymentmethod').val(response.payment.payment_method_id).attr('disabled',true);
                    $('#ptid').val(response.payment.payment_type_id);
                    $('.selectpicker').selectpicker('refresh');
                    $('#no-item').attr('hidden',true);
                    $('#payment-row').removeAttr('hidden');

                    $.each(response.podetail, function(k,v){
                        var newid = "id-"+(k+1);
                        var newKategori = "kategori-"+(k+1);
                        var newBrand = "brand-"+(k+1);
                        var newType = "type-"+(k+1);
                        var newHargaBeli = "harga-beli-"+(k+1);
                        var newHargaJual = "harga-jual-"+(k+1);
                        var newDiskon = "diskon-"+(k+1);
                        var newRate = "rate-"+(k+1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id',newid);
                        $temp.find('#kategori-1').attr('id',newKategori);
                        $temp.find('#brand-1').attr('id',newBrand);
                        $temp.find('#type-1').attr('id',newType);
                        $temp.find('#harga-jual-1').attr('id',newHargaJual);
                        $temp.find('#harga-beli-1').attr('id',newHargaBeli);
                        $temp.find('#diskon-1').attr('id',newDiskon);
                        $temp.find('#rate-1').attr('id',newRate);
                        $temp.appendTo('#barang-data');
                        $('#id-'+(k+1)).val(v.purchase_order_details_id).attr('disabled',true);
                        $('#kategori-'+(k+1)).val(v.c_name).attr('disabled',true);
                        $('#brand-'+(k+1)).val(v.b_name).attr('disabled',true);
                        $('#type-'+(k+1)).val(v.t_name).attr('disabled',true);
                        $('#harga-beli-'+(k+1)).val(accounting.formatMoney(v.price_buy,'',0,',','.')).attr('disabled',true);
                        $('#harga-jual-'+(k+1)).val(accounting.formatMoney(v.price_sale,'',0,',','.')).attr('disabled',true);
                        $('#rate-'+(k+1)).val(v.profit_rate).attr('disabled',true);
                        $('#diskon-'+(k+1)).val(v.discount_nominal).attr('disabled',true);
                    });
                    $('#no-item-po').attr('hidden',true);
                    $('#cetak-payment').removeAttr('hidden').attr('href',"downloadpurchasepayment/"+id);
                    $('#diskon-transaksi').val(response.invoice.discount_transaction).attr('disabled',true);
                    $('#discount-transaction-div').removeAttr('hidden');
                }
            });
        });

        $('#pembayaran').on('keyup',function(){
            $('#pembayaran-hidden').val($(this).val());
        });

        $('#pembayaran').on('focusout',function(){
            $(this).val(accounting.formatMoney($(this).val() ,'Rp ',2,',','.'));
        })

        $('#pembayaran').on('focusin',function(){
            $(this).val('');
            $('#pembayaran-hidden').val('');
        })

        $(".submit").on('click',function(){
            $('#customer, #total, #pembayaran').removeAttr('disabled');
            /*$('#pembayaran').val(accounting.unformat($(this).val()));*/
            if($('#noinvoice').val()== 0){
                toastr.warning('Anda Belum Memilih Invoice!');
            }else{
                hidemainbuttongroup();
                var kode = $('#nopayment').val()
                $.ajax({
                    type:"POST",
                    url:"createpurchasepayment",
                    data: $('#formPayment').serialize(),
                    success:function(response){
                        // $('.side-nav .nav-item.active a').click();
                        toastr.success('Purchase Payment '+kode+' has been Created!', {"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                    }
                });
            }
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .nav-item.active a').click()
        });

        $('#purchasePaymentTable').on('click', '.delete-modal', function(event){
            event.stopImmediatePropagation();
            var nopayment = $(this).closest('tr').find('.nopayment').html();
            var idpayment = $(this).closest('tr').attr('value');
            $('#confirm-delete-payment').attr('value',idpayment).attr('nomor',nopayment);
            $("#delete-message").html("Yakin ingin menghapus data "+nopayment+" ?")
            $('#modal-delete-payment').modal('open');
        });

        $('#confirm-delete-payment').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-payment').modal('close');
            var id = $(this).attr('value');
            var nopayment = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletepurchasepayment",
                data:{id:id},
                success:function(response){
                    toastr.success('Purchase Payment '+nopayment+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                }
            })
        });

        //function
        function firstload(){

            $('.selectpicker').selectpicker('render');

            $.ajax({
                type:"GET",
                url:"lastpurchasepaymentnumber",
                success:function(response){
                    $('#nopayment').val(response);
                }
            });

            purchasePaymentTable = $('#purchasePaymentTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                'sDom': 'tip',
                "pageLength": 5,
                ajax: {
                    url : 'getpurchasepaymenttable',
                    data : function (d){
                        d.number = $('#filterPayment').val();
                        d.supplier = $('#filterSupplier').val();
                    }
                },
                rowId : 'payment_purchase_id',
                columns: [
                    { data: 'payment_purchase_number', name: 'payment_purchase_number', class:'nopayment'},
                    { data: 'company_name', name: 'company_name'},
                    { data: 'total_paid', name: 'total_paid'},
                    { data: 'payment_method', name: 'payment_method'},
                    { data: 'action', name: 'action'},
                ],
            });

            $('#filterPayment').on('keyup', function () { // This is for news page
                purchasePaymentTable.column(0).search(this.value).draw();
            });
            $('#filterSupplier').on('keyup', function () { // This is for news page
                purchasePaymentTable.column(1).search(this.value).draw();
            });
        }

        function hidemainbuttongroup(){
            $('.main-button-group a').attr('hidden',true);
        }
    });
</script>