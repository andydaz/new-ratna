<style type="text/css">
    table.table {
        border-collapse: collapse;
    }
    table.table th{
        border-top: 1px solid black;
        border-bottom: 1px solid black;
    }
    p , span{
        font-size: 12px;
    }
    body{
        margin:0px;
    }
    html { margin: 2cm 1cm 0cm 1cm}

</style>
<body>
<div style="">
    <div style="position: fixed; top: 0px">
        <span style="float: right">Bekasi, {{\Carbon\Carbon::now()->format('d F Y')}}</span>
    </div>
    <div>
        <img src="{{asset('images/ratna.png')}}" alt="no images">
    </div>
    <div>
        <p>Kepada Yth: {{$customer->first_name.' '.$customer->last_name}}</p>
    </div>
    <div>
        <p>Berikut ini kami sampaikan daftar Piutang yang belum anda selesaikan sampai dengan tanggal {{\Carbon\Carbon::now()->format('d F Y')}} :</p>
    </div>
    <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
        <thead>
        <tr>
            <th class="color" style="text-align: center;"><span>Tanggal Pembelian</span></th>
            <th class="color" style="text-align: center;"><span>Nama Barang</span></th>
            <th class="color" style="text-align: center;"><span>Cicilan Ke</span></th>
            <th class="color" style="text-align: center;"><span>Jatuh Tempo</span></th>
            <th class="color" style="text-align: center;"><span>Aging</span></th>
            <th class="color" style="text-align: center;"><span>Nilai Piutang</span></th>
        </tr>
        </thead>
        <tbody >
            @foreach($invoicedue as $key => $value)
                <tr style="text-align: center">
                    <td>{{$value->so->date_sales_order}}</td>
                    <td>{{$value->product->category->description.' '.$value->product->brand->name.' '.$value->product->type->name}}</td>
                    <td>{{$value->term_number}}</td>
                    <td>{{$value->due_date}}</td>
                    <td>
                        {{
	                        Carbon\Carbon::createFromFormat('Y-m-d', $value->due_date)->diffInDays(\Carbon\Carbon::now(), false) > 0 ?
                            Carbon\Carbon::createFromFormat('Y-m-d', $value->due_date)->diffInDays(\Carbon\Carbon::now(), false)
                            : '0'
                        }}
                    </td>
                    <td>{{number_format($value->total_amount)}}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="5" style="text-align: right">Total</td>
                <td style="text-align: center">{{number_format($invoicedue->sum('total_amount'))}}</td>
            </tr>
        </tbody>
    </table>

    <div style="border: solid 1px black; margin-top: 20px; background-color: powderblue;">
        <span>Terbilang : {{$terbilang}} rupiah</span>
    </div>

    <div style="margin-top: 20px; ">
        <p>Jika terdapat kesalahan pencatatan dari data yang kami berikan, mohon segera dikonfirmasikan
            Atas perhatian dan kerjasamanya kami mengucapkan terimakasih </p>
    </div>

    <div style="margin-top: 20px; ">
        <p>Regards,</p>
        <p>Ratna Boutique Team</p>
    </div>

    <div style="margin-top: 20px; ">
        <p><i>Surat ini dapat dikembalikan apabila tidak setuju dengan penagihan yang dilakukan dengan alasan : </i></p>
    </div>
    <div style="height: 20mm; border: solid 1px black">

    </div>
</div>
</body>
