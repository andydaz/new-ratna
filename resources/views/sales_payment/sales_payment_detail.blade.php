<table class="stoko-table no-border">
    <thead>
    <tr>
        <th class="theader">No Invoice</th>
        <th class="theader">Cicilan</th>
        <th class="theader">Jatuh Tempo</th>
        <th class="theader">Nilai Invoice</th>
        <th class="theader">Aksi</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($creditdata))
        @foreach($creditdata as $key1 => $value1)
            <tr class="grey darken-3 white-text">
                <td colspan="5">{{$value1->category->description.' '.$value1->brand->name.' '.$value1->type->name.' / '.$value1->bar_code}}</td>
            </tr>
            @foreach($value1->invoice as $key2 => $value2)
                <tr>
                    <td>
                        <div class="input-field">
                            <input id="{{'noinvoice-'.$key2}}" name="noinvoice[]" type="text" class="f-input noinvoice" value="{{$value2->invoice_sales_number}}" disabled>
                        </div>
                    </td>
                    <td>
                        <div class="input-field">
                            <input id="{{'terms-'.$key2}}" name="terms[]" type="text" class="f-input terms" value="{{$value2->term_number}}" disabled>
                        </div>
                    </td>
                    <td>
                        <div class="input-field">
                            <input id="{{'due-'.$key2}}" name="due[]" type="text" class="f-input due" value="{{$value2->due_date}}" disabled>
                        </div>
                    </td>
                    <td>
                        <div class="input-field">
                            <input id="{{'amount-'.$key2}}" name="amount[]" type="text" class="f-input amount" value="{{number_format($value2->total_amount)}}" disabled>
                        </div>
                    </td>
                    <td>
                        <div class="input-field">
                            @if($value2->payment)
                                {{$value2->payment->date_sales_payment}}
                            @else
                                <input type="checkbox" id="{{'Checkbox'.$key1.$key2}}" class="filled-in checkbox" name="checkinvoice[]" value="{{$value2->invoice_sales_id}}"/>
                                <label for="{{'Checkbox'.$key1.$key2}}"></label>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
        @endforeach
    @elseif(isset($paymentdata))
        <tr class="grey darken-3 white-text">
            <td colspan="5">{{$paymentdata->product->category->description.' '.$paymentdata->product->brand->name.' '.$paymentdata->product->type->name.' / '.$paymentdata->product->bar_code}}</td>
        </tr>
        <tr>
            <td>
                <div class="input-field">
                    <input id="{{'noinvoice-1'}}" name="noinvoice[]" type="text" class="f-input noinvoice" value="{{$paymentdata->invoice_sales_number}}" disabled>
                </div>
            </td>
            <td>
                <div class="input-field">
                    <input id="{{'terms-1'}}" name="terms[]" type="text" class="f-input terms" value="{{$paymentdata->term_number}}" disabled>
                </div>
            </td>
            <td>
                <div class="input-field">
                    <input id="{{'due-1'}}" name="due[]" type="text" class="f-input due" value="{{$paymentdata->due_date}}" disabled>
                </div>
            </td>
            <td>
                <div class="input-field">
                    <input id="{{'amount-1'}}" name="amount[]" type="text" class="f-input amount" value="{{number_format($paymentdata->total_amount)}}" disabled>
                </div>
            </td>
            <td>
                <div class="input-field">
                    @if($paymentdata->payment)
                        {{$paymentdata->payment->date_sales_payment}}
                    @endif
                </div>
            </td>
        </tr>
    @else(!isset($creditdata))
        <tr id="no-item"><td colspan="5"><span> No Item Selected</span></td></tr>
    @endif
    </tbody>
</table>