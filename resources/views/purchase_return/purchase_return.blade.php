<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text" ><i class="material-icons">search</i>Cari Purchase Return</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br/>
                                <div class="input-field col l3">
                                    <label>Filter Retur</label>
                                    <input id="filterReturNumber" type="text" class="f-input" placeholder="Filter Retur" />
                                </div>
                                <div class="input-field col l3">
                                    <label>Filter supplier</label>
                                    <input id="filterSupplier" type="text" class="f-input" placeholder="Filter supplier" />
                                </div>
                                <div class="input-field col l3">
                                    <label>Filter PO</label>
                                    <input id="filterPoNumber" type="text" class="f-input" placeholder="Filter SO" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col l12 m12 s12">
                                    <div class="table-responsive">
                                        <table id="returTable" class="highlight table table-bordered display nowrap dataTable dtr-inline" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th class="theader">No Retur</th>
                                                <th class="theader">supplier</th>
                                                <th class="theader">No PO</th>
                                                <th class="theader">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {{--@foreach($data['retur'] as $key => $value)--}}
                                                {{--<tr mode="view" value="{{$value->purchase_retur_id}}">--}}
                                                    {{--<td class="noretur">{{$value->retur_number}}</td>--}}
                                                    {{--<td>{{$value->company_name}}</td>--}}
                                                    {{--<td>{{$value->purchase_order_number}}</td>--}}
                                                    {{--<td>--}}
                                                        {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit tooltipped" mode="edit" data-position="bottom" data-tooltip="edit" value="{{$value->purchase_retur_id}}"><i class="material-icons">edit</i></a>--}}
                                                        {{--<a class="btn btn-sm btn-raised red delete-modal tooltipped" data-position="bottom" data-tooltip="delete"><i class="material-icons">delete</i></a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir Penerimaan Barang</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br/>
                                <form id="formRetur">
                                    <div class="input-field col l3">
                                        <label>No Retur PO</label>
                                        <input id="noretur" type="text" name="noretur" class="f-input" disabled>
                                        <input id="idretur" type="text" name="idretur" class="f-input" hidden disabled>
                                    </div>
                                    <div class="input-field col l3">
                                        <label>No Good Receive</label>
                                        <select id="nogr" name="nogr" class="browser-default selectpicker" data-live-search="true">
                                            <option value="0">Pilih No Good Receive</option>
                                            @foreach($data['goodreceive'] as $key => $value)
                                                <option value="{{$value->good_receive_id}}">{{$value->good_receive_code.' - '.$value->purchase_order_number}}</option>
                                        @endforeach
                                        <!-- <option value="" selected>170001</option>
                        <option value="">170002</option> -->
                                        </select>
                                    </div>
                                    <div class="input-field col l3">
                                        <input id="notes" name="notes" type="text" class="f-input" disabled>
                                        <label>Notes</label>
                                    </div>
                                    <div class="input-field col l3">
                                        <label>supplier</label>
                                        <input id="supplier" type="text" name="supplier" class="f-input" placeholder="Prima PT" disabled>
                                    </div>
                                    <div class="input-field col l3">
                                        <label>Tanggal Retur</label>
                                        <input id="tglretur" type="text" name="tglretur" class="f-input datepicker" placeholder="Tanggal Retur">
                                    </div>
                                    <div class="input-field col l3">
                                        <label>Tipe Retur</label>
                                        <select id="returtype" class="browser-default selectpicker" data-live-search="true" name="returtype">
                                            <option value="0">Select Retur Type</option>
                                            @foreach($data['returtype'] as $key => $value)
                                                <option value="{{$value->retur_type_id}}">{{$value->retur_type_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col l12 m12 s12 margin-top">
                                        <div class="table-responsive">
                                            <table class="stoko-table no-border">
                                                <thead>
                                                <tr>
                                                    <th class="theader">Kategori Barang</th>
                                                    <th class="theader">Brand</th>
                                                    <th class="theader">Tipe Barang</th>
                                                    <th class="theader">Harga Beli</th>
                                                    <th class="theader">Harga Jual</th>
                                                    <th class="theader">Gudang</th>
                                                    <th class="theader">Retur</th>
                                                </tr>
                                                </thead>
                                                <tbody id="barang-data">
                                                <tr id="no-item"><td colspan="7"><span> No Item Selected</span></td></tr>
                                                <tr class="barang-row" hidden>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="id-1" type="text" class="f-input id-detail" name="id" hidden>
                                                            <input id="kategori-1" type="text" class="f-input" name="kategori[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="brand-1" type="text" class="f-input brand" name="brand[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="type-1" type="text" class="f-input type" name="type[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="harga-beli-1" type="text" class="f-input hargabeli" name="hargabeli[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="harga-jual-1" type="text" class="f-input hargajual" name="hargajual[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <select id="gudang-1" class="f-select gudang" name="gudang[]">
                                                                @foreach($data['warehouse'] as $key => $value)
                                                                    <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" id="checkbox-retur-1" class="filled-in retur" value="" />
                                                        <label id="label-retur-1" for="checkbox-retur-1"></label>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            </br>
                                            <div class="input-field">
                                                <label>Nilai Retur</label>
                                                <input id="totalvalue" type="text" name="totalvalue" class="f-input datepicker" placeholder="Nilai Retur" value="0" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col l12 s12 m12 main-button-group margin-top">
                                        <a href="#" id="submit-retur" mode="save" class="btn-stoko teal white-text">simpan</a>
                                        <a href="#" id="edit-retur" mode="edit" class="btn-stoko teal white-text" hidden>simpan</a>
                                        <a href="#" class="btn-stoko">batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Modal Delete Delivery Order -->
<div id="modal-delete-retur" class="modal">
    <div class="modal-content">
        <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
        <a id="confirm-delete-retur" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
</div>

<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#nogr').on('change',function(){
            var id = $(this).val();
            $.ajax({
                type:"Get",
                url:"getgoodreceiveforreturn",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    if(response.goodreceive.length == 0)
                    {
                        $original.clone();
                        $original.attr('hidden',true)
                        $original.appendTo('#barang-data');
                        $('#no-item').removeAttr('hidden');
                    }else{
                        $('#no-item').attr('hidden',true);
                        $('#supplier').val(response['goodreceive'][0].company_name);
                        $('#notes').val(response['goodreceive'][0].po_notes);
                        $.each(response['goodreceive'], function(i,v){
                            var newid = "id-"+(i+1);
                            var newKategori = "kategori-"+(i+1);
                            var newBrand = "brand-"+(i+1);
                            var newType = "type-"+(i+1);
                            var newGudang = "gudang-"+(i+1);
                            var newRetur = "checkbox-retur-"+(i+1);
                            var newHargaBeli = "harga-beli-"+(i+1);
                            var newHargaJual = "harga-jual-"+(i+1);
                            var newDiskon = "diskon-"+(i+1);
                            var newRate = "rate-"+(i+1);
                            var newLabelRetur = "label-retur-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id',newid);
                            $temp.find('#kategori-1').attr('id',newKategori);
                            $temp.find('#brand-1').attr('id',newBrand);
                            $temp.find('#type-1').attr('id',newType);
                            $temp.find('#gudang-1').attr('id',newGudang);
                            $temp.find('#harga-jual-1').attr('id',newHargaJual);
                            $temp.find('#harga-beli-1').attr('id',newHargaBeli);
                            $temp.find('#checkbox-retur-1').attr('id',newRetur);
                            $temp.find('#label-retur-1').attr('id',newLabelRetur).attr('for',"checkbox-retur-"+(i+1));
                            $temp.appendTo('#barang-data');
                            $('#id-'+(i+1)).val(v.good_receive_details_id).attr('disabled',true);
                            $('#kategori-'+(i+1)).val(v.c_name).attr('disabled',true);
                            $('#brand-'+(i+1)).val(v.b_name).attr('disabled',true);
                            $('#type-'+(i+1)).val(v.t_name).attr('disabled',true);
                            $('#gudang-'+(i+1)).val(v.warehouse_id).attr('disabled',true);
                            $('#harga-beli-'+(i+1)).val(accounting.formatMoney(v.price_buy,'',0,',','.')).attr('disabled',true);
                            $('#harga-jual-'+(i+1)).val(accounting.formatMoney(v.price_sale,'',0,',',',')).attr('disabled',true);
                        });

                        $('#returtype').html("");
                        $.each(response['returtype'],function(i,v){
                            $('#returtype').append("<option value='"+v.retur_type_id+"''>"+v.retur_type_name+"</option>").removeAttr('disabled');
                        });
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
        });

        $('body').on('click','.retur', function(event){
            event.stopImmediatePropagation();
            calculatetotalretur();
        })

        $('body').on('keyup','.qty', function(){
        });

        $('#returTable tr, .edit').on('click', function(event){
            var id = $(this).attr('value');
            $.ajax({
                type: "GET",
                url: "getpurchasereturn",
                data: {id: id},
                success: function (response) {
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    console.log(response);
                    $('#nogr').append('<option class="remove-when-clear" value="'+response.purchase_retur_id+'" selected="selected">'+response.retur_number+'</option>').attr('disabled',true);
                    $('#tglretur').val(response.date_retur).attr('disabled',true);
                    $('#returtype').val(response.retur_type_id).attr('disabled',true);
                    $('#supplier').val(response.goodreceive.po.supplier.company_name).attr('disabled',true);
                    $.each(response.details, function(i,v) {
                        var newid = "id-" + (i + 1);
                        var newKategori = "kategori-" + (i + 1);
                        var newBrand = "brand-" + (i + 1);
                        var newType = "type-" + (i + 1);
                        var newGudang = "gudang-" + (i + 1);
                        var newRetur = "checkbox-retur-" + (i + 1);
                        var newHargaBeli = "harga-beli-" + (i + 1);
                        var newHargaJual = "harga-jual-" + (i + 1);
                        var newLabelRetur = "label-retur-" + (i + 1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id', newid);
                        $temp.find('#kategori-1').attr('id', newKategori);
                        $temp.find('#brand-1').attr('id', newBrand);
                        $temp.find('#type-1').attr('id', newType);
                        $temp.find('#gudang-1').attr('id', newGudang);
                        $temp.find('#harga-jual-1').attr('id', newHargaJual);
                        $temp.find('#harga-beli-1').attr('id', newHargaBeli);
                        $temp.find('#checkbox-retur-1').attr('id', newRetur);
                        $temp.find('#label-retur-1').attr('id', newLabelRetur).attr('for', "checkbox-retur-" + (i + 1));
                        $temp.appendTo('#barang-data');
                        $('#kategori-' + (i + 1)).val(v.detailpo.category.description).attr('disabled', true);
                        $('#brand-' + (i + 1)).val(v.detailpo.brand.name).attr('disabled', true);
                        $('#type-' + (i + 1)).val(v.detailpo.type.name).attr('disabled', true);
                        $('#gudang-' + (i + 1)).val(v.detailgr.warehouse_id).attr('disabled', true);
                        $('#harga-beli-' + (i + 1)).val(v.detailpo.price_buy).attr('disabled', true);
                        $('#harga-jual-' + (i + 1)).val(v.detailpo.price_sale).attr('disabled', true);
                        $('#checkbox-retur-' + (i+1)).attr('checked',true).attr('disabled',true);
                    });
                    $('#no-item').attr('hidden', true);
                    $('.selectpicker').selectpicker('refresh');
                    calculatetotalretur();
                }
            })
        });

        $('#formRetur #submit-retur, #formRetur #edit-retur').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var id = [];
            var nogr = $('#nogr').val();
            var noretur = $('#noretur').val();
            var tglretur = $('#tglretur').val();
            var returtype = $('#returtype').val();
            var returvalue = $('#totalvalue').val();
            $('.retur').each(function(i,v){
                if($(this).is(':checked'))
                {
                    id.push($(this).closest('tr').find('.id-detail').val());
                }
            });

            if(id.length == 0)
            {
                toastr.warning('Anda belum memilih Retur');
            }else{
                hidemainbuttongroup();
                $.ajax({
                    type:"POST",
                    url:'createpurchasereturn',
                    data:{id:id, nogr:nogr, noretur:noretur, returtype:returtype, tglretur:tglretur, returvalue:returvalue},
                    success:function(response){
                        toastr.success("Retur "+noretur+" Berhasil Dibuat",{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                    }
                })
            }

        });

        $('#returTable').on('click','.delete-modal', function(event){
            event.stopImmediatePropagation();
            var noretur = $(this).closest('tr').find('.noretur').html();
            var idretur = $(this).closest('tr').attr('value');
            $('#confirm-delete-retur').attr('value',idretur).attr('nomor',noretur);
            $("#delete-message").html("Yakin ingin menghapus data "+noretur+" ?")
            $('#modal-delete-retur').modal('open');
        });

        $('#confirm-delete-retur').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-retur').modal('close');
            var id = $(this).attr('value');
            var noretur = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletepurchasereturn",
                data:{id:id},
                success:function(response){
                    toastr.success('Purchase Retur '+noretur+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                }
            })
        });

        //function
        function firstload()
        {
            $.ajax({
                type:"GET",
                url:"lastpurchasereturnnumber",
                success:function(response){
                    $('#noretur').val(response);
                }
            })

            $('.selectpicker').selectpicker('render');

            $('#tglretur').dcalendarpicker({
                format: 'dd-mm-yyyy'
            });
            $('#tglretur').val(moment().format('DD-MM-YYYY'));

            // returTable = $('#returTable').DataTable({ // This is for home page
            //     searching: true,
            //     responsive: true,
            //     'aaSorting':[],
            //     'sDom':'tip',
            //     "bPaginate":true,
            //     "bFilter": false,
            //     "sPaginationType": "full_numbers",
            //     "iDisplayLength": 10,
            //     "language": {
            //         "infoEmpty": "No records to display",
            //         "zeroRecords": "No records to display",
            //         "emptyTable": "No data available in table",
            //     },
            // });

            returTable = $('#returTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                'sDom': 'tip',
                "pageLength": 5,
                ajax: {
                    url : 'getpurchasereturntable',
                    data : function (d){
                        d.number = $('#filterReturNumber').val();
                        d.po = $('#filterPoNumber').val();
                        d.supplier = $('#filterSupplier').val();
                    }
                },
                rowId : 'purchase_retur_id',
                columns: [
                    { data: 'retur_number', name: 'retur_number', class:'noretur'},
                    { data: 'company_name', name: 'company_name'},
                    { data: 'purchase_order_number', name: 'purchase_order_number'},
                    { data: 'action', name: 'action'},
                ],
            });

            $('#filterReturNumber').on('keyup', function () { // This is for news page
                returTable.draw();
            });
            $('#filterSupplier').on('keyup', function () { // This is for news page
                returTable.draw();
            });
            $('#filterPoNumber').on('keyup', function () { // This is for news page
                returTable.draw();
            });

        }

        function calculatetotalretur(){
            var total = 0;
            $('.retur').each(function(i,v){
                if($(this).is(':checked')){
                    hargabeli = accounting.unformat($(this).closest('tr').find('.hargabeli').val());
                    total = total + parseInt(hargabeli);
                }
            });
            $('#totalvalue').val(total);
            // $('.barang-row').each(function(key, value){
            //   totalqty = 0;
            //   $(this).find('.qty').each(function(key,value){
            //     totalqty += parseInt($(this).val());
            //   })
            //   total += totalqty * $(this).find('.price').val();
            // });
        }

        function hidemainbuttongroup(){
            $('.main-button-group a').attr('hidden',true);
        }
    });
</script>
