<table id="followupTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <thead>
    <tr>
        <th>Tanggal Sales Order</th>
        <th>Nama Pelanggan</th>
        <th>Barang</th>
        <th>Hutang Jatuh Tempo</th>
        <th>Status Aging</th>
        <th>WA</th>
        <th>Notes</th>
    </tr>
    </thead>
    <tbody>
    @if(count($data) > 0)
      @foreach($data as $key => $value)
      <tr 
        soId="{{$value->so->sales_order_id}}"
        invoiceId="{{$value->invoice_sales_id}}"
        customerId="{{$value->so->customer->customer_id}}"
        latestAging="{{$value->aging}}"
      >
          <td>{{$value->so->date_sales_order}}</td>
          <td>{{$value->so->customer_name}}</td>
          <td>{{$value->product->brand->category->description.' '.$value->product->brand->name}}</td>
          <td>{{$value->total_due ? number_format($value->total_due) : 0}}</td>
          <td>{{$value->aging}}</td>
          <td><a class="show-wa-modal"><img class="wa-icon" src="images/wa.png"/></a></td>
          <td><a class="show-notes-modal"><i class="material-icons">note_add</i></a></td>
      </tr>
      @endforeach
    @else
      <tr>
        <td colspan="7">No Data</td>
      </tr>
    @endif
    </tbody>
</table>
<script type="text/javascript">
    followupTable = $('#followupTable').DataTable({ // This is for home page
              searching: true,
              responsive: true,
              'sDom':'tip',
              "aaSorting": [],
              "bPaginate":true,
              "bFilter": false,
              "sPaginationType": "full_numbers",
              "iDisplayLength": 10,
              language: {
                  "sProcessing":   "Sedang proses...",
                  "sLengthMenu":   "Tampilan _MENU_ entri",
                  "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                  "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                  "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
                  "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                  "sInfoPostFix":  "",
                  "sSearch":       "Cari:",
                  "sUrl":          "",
                  "oPaginate": {
                      "sFirst":    "Awal",
                      "sPrevious": "Balik",
                      "sNext":     "Lanjut",
                      "sLast":     "Akhir"
                  }
              },
          });
</script>