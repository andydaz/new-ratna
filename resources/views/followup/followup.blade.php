<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<style>
  .wa-icon {
    width:20px;
    height:20px;
  }
  #notes, #edit-notes {
    height: 6rem;
  }
</style>
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Follow Up</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row margin-top">
                              <div class="input-field col l3">
                                <label>Type</label>
                                <select id="filterType" name="type" class="browser-default selectpicker">
                                    <option value="all">No Filter</option>
                                    <option value="aging">Aging</option>
                                    <option value="date">So Date</option>
                                </select>
                              </div>
                              <div class="input-field col l3">
                                <div id="filterAgingContainer" hidden>
                                  <label>Aging Status</label>
                                  <select id="filterAging" name="aging" class="browser-default selectpicker">
                                      <option value="1 Bulan">1 Bulan</option>
                                      <option value="1-2 Bulan">1-2 Bulan</option>
                                      <option value="2-3 Bulan">2-3 Bulan</option>
                                      <option value="3-6 Bulan">3-6 Bulan</option>
                                      <option value="6-12 Bulan">6-12 Bulan</option>
                                      <option value=">1 Tahun">>1tahun</option>
                                  </select>
                                </div>
                                <div id="filterSoDateContainer" hidden>
                                  <label>Select So Date</label>
                                  <input id="tglso" type="text" class="f-input" placeholder="Tanggal SO">
                                </div>
                              </div>
                              <div class="input-field col l3">
                                <label></label>
                                <a id="preview" href="#" class="btn btn-raised blue white-text">preview</a>
                                <a id="export-follow-up" target="_blank" href="#" class="btn btn-raised blue white-text" disabled>Export</a>
                              </div>
                              <div class="col l12 m12 s12 margin-top">
                                <div id="report-table" class="table-responsive">
                                  <table id="followupTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                                    <thead>
                                    <tr>
                                        <th>Tanggal Sales Order</th>
                                        <th>Nama Pelanggan</th>
                                        <th>Barang</th>
                                        <th>Hutang Jatuh Tempo</th>
                                        <th>Status Aging</th>
                                        <th>WA</th>
                                        <th>Notes</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                          <td colspan="8">No Data</td>
                                      </tr>
                                      </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="wa-modal" class="modal">
  <div class="modal-content">
    <div class="row">
      <div class="col l12">
        <table id="modal-delete-data">
          <thead>
              <tr>
                <th>Pilih No Whatsapp</th>        
              </tr>
          </thead>
          <tbody id="wa-modal-content">
              
          </tbody>
        </table>
        <div>
          <label>62</label>
          <input id="wa-manual" type="text" name="manual">
          <a id="submit-wa" href="#" target="_blank" class="btn btn-raised blue white-text">go to wa</a>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </form>
  </div>
</div>
<div id="notes-modal" class="modal">
  <div class="modal-content">
    <div class="row">
      <div class="col l12">
        <div>
          <label>Notes</label>
          <textarea id="notes"></textarea>
          <button id="submit-notes-button">Submit</button>
        </div>
        <table class="highlight table table-bordered">
          <thead>
              <tr>
                <th>Nama Product</th>
                <th>Tanggal Notes</th>
                <th>Notes</th>
                <th>Status Aging Latest Update</th>
                <th>Action</th>
              </tr>
          </thead>
          <tbody id="notes-modal-content">
              
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">OK</a>
    </form>
  </div>
</div>
<div id="edit-notes-modal" class="modal">
  <div class="modal-content">
    <div class="row">
      <div class="col l12">
        <div>
          <label>Notes</label>
          <textarea id="edit-notes"></textarea>
          <button id="edit-notes-button">Edit</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </form>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        $("#filterType").on('change',function(event){
          var value = $(this).val()
          if(value == "aging"){
            $('#preview').removeAttr('disabled');
            $('#filterSoDateContainer').attr('hidden',true);
            $('#filterAgingContainer').removeAttr('hidden');
          }else if(value == "date"){
            $('#preview').removeAttr('disabled');
            $('#filterAgingContainer').attr('hidden',true);
            $('#filterSoDateContainer').removeAttr('hidden');
          }else{
            $('#preview').trigger('click');
            $('#preview').attr('disabled', true);
            $('#filterSoDateContainer').attr('hidden',true);
            $('#filterAgingContainer').attr('hidden',true);
          }
        });

        $("#wa-manual").on('change',function(event){
          var value = $(this).val();
          $('#submit-wa').attr("href", "https://wa.me/62"+value);
        });

        $("body").on('click', ".show-wa-modal", function(event){
          event.preventDefault();
          var id = $(this).closest('tr').attr('customerId');

          $.ajax({
            type:"GET",
            url:"showWaContent",
            async:false,
            data:{
              id:id,
            },
            success:function(response){
              $('#wa-modal-content').html(response);
            }
          });
          $('#wa-modal').modal('open');
        });

        $("body").on('click', ".edit-notes", function(event){
          var id = $(this).closest('tr').attr('followupId');
          $.ajax({
            type:"GET",
            url:"getFollowUpDetail/"+id,
            async:false,
            data:{},
            success:function(response){
              $("#edit-notes-button").attr("followupId", id);
              $('#edit-notes').val(response.notes.replace(/<br \/>/g, ""));
            }
          });
          $("#edit-notes-modal").modal('open');
        });

        $("body").on('click', '.show-notes-modal', function(event){
          event.preventDefault();
          var soId = $(this).closest('tr').attr('soId');
          var invoiceId = $(this).closest('tr').attr('invoiceId');
          var latestAging = $(this).closest('tr').attr('latestAging');
          $('#notes-modal').attr("soId", soId);
          $('#notes-modal').attr("invoiceId", invoiceId);
          $('#notes-modal').attr("latestAging", latestAging);
          $.ajax({
            type:"GET",
            url:"getFollowUpNotes",
            async:false,
            data:{
              id:soId,
            },
            success:function(response){
              $('#notes-modal-content').html(response);
            }
          });
          $('#notes-modal').modal('open');
        });

        $("#submit-notes-button").on('click', function(event){
          event.preventDefault();
          event.stopImmediatePropagation();
          $.ajax({
            type:"POST",
            url:"createFollowUpNotes",
            async:false,
            data:{
              type: $("#filterType").val(),
              notes: $('#notes').val(),
              soId: $('#notes-modal').attr('soId'),
              invoiceId: $('#notes-modal').attr('invoiceId'),
              latestAging: $('#notes-modal').attr('latestAging')
            },
            success:function(response){
              $('#notes').val('');
              $("tr[soId="+$('#notes-modal').attr('soId')+"]").find(".show-notes-modal").trigger('click');
            }
          });
        });

        $("#edit-notes-button").on('click', function(event){
          event.preventDefault();
          event.stopImmediatePropagation();
          var id = $(this).attr("followupId");
          $.ajax({
            type:"POST",
            url:"updateFollowUpNotes/"+id,
            async:false,
            data:{
              notes: $('#edit-notes').val(),
            },
            success:function(response){
              $('#edit-notes').val('');
              $('#edit-notes-modal').modal('close');
              $("tr[soId="+$('#notes-modal').attr('soId')+"]").find(".show-notes-modal").trigger('click');
            }
          });
        });

        $('#preview').on('click', function(event){
            event.stopImmediatePropagation();
            var type = $('#filterType').val();
            var date = $('#tglso').val();
            var aging = $('#filterAging').val();
            $.ajax({
                method:"get",
                url:"getFollowUpData",
                data:{
                  type:type, 
                  aging:aging,
                  date:date
                },
                success:function(response){
                    $('#report-table').html(response);
                    // changetitle();
                },complete:function (){
                    if(type=="aging"){
                      $('#export-follow-up').attr('href','downloadFollowUpByAging?aging='+aging).removeAttr('disabled');
                    }else{
                      $('#export-follow-up').attr('href','downloadFollowUpByDate?date='+date).removeAttr('disabled');
                    }
                }
            })
        });

        firstload();

        //function
        function firstload(){
          $('#preview').attr('disabled', true);
          $('#tglso').dcalendarpicker({
            format: 'dd-mm-yyyy'
          });

          var date = moment().format('DD-MM-YYYY');
          // var month = date.month()+1;
          // var year = date.year();
          // var endofmonth = moment().endOf('month').format('DD');

          $('#tglso').val(date);

          $('.selectpicker').selectpicker('render');
        }
    });
</script>