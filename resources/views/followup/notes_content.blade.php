@if(count($notes) > 0)
	@foreach($notes as $key => $value)
		<tr followupId="{{$value->id}}" style="{{$value->type == 'aging'? "background-color: #FF4242": "background-color: #F2FF49"}}">
		  <td>{{$value->invoice->product->category->description.' '.$value->invoice->product->brand->name}}</td>
		  <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->toDateString()}}</td>
		  <td>{!!$value->notes!!}</td>
		  <td>{{$value->latest_aging}}</td>
		  <td><a class="btn btn-sm edit-notes"><i class="material-icons">edit</i></a></td>
		</tr>
	@endforeach
@else
	<tr>
	 	<td colspan="5">No Data</td>
	</tr>
@endif
