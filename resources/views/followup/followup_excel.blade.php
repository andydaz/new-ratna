@php 
    $max = 0;
    foreach ($data as $key => $value) {
        $count = $value->so->followup->count();
        if ($count > $max){
            $max = $count;
        }
    }
@endphp
<table id="followupTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <thead>
    <tr>
        <th>Tanggal Sales Order</th>
        <th>Nama Pelanggan</th>
        <th>Barang</th>
        <th>Hutang Jatuh Tempo</th>
        <th>Status Aging</th>
        @for($i = 1; $i <= $max; $i++)
          <th>Notes {{$i}}</th  >
        @endfor
    </tr>
    </thead>
    <tbody>
    @if(count($data) > 0)
      @foreach($data as $key => $value)
      <tr 
        soId="{{$value->so->sales_order_id}}"
        invoiceId="{{$value->invoice_sales_id}}"
        customerId="{{$value->so->customer->customer_id}}"
        latestAging="{{$value->aging}}"
      >
          <td>{{$value->so->date_sales_order}}</td>
          <td>{{$value->so->customer_name}}</td>
          <td>{{$value->product->brand->category->description.' '.$value->product->brand->name}}</td>
          <td>{{$value->total_due ? number_format($value->total_due) : 0}}</td>
          <td>{{$value->aging}}</td>
          @foreach($value->so->followup as $key => $value)
            <td>{!!$value->notes!!}</td>
          @endforeach
      </tr>
      @endforeach
    @else
      <tr>
        <td colspan="7">No Data</td>
      </tr>
    @endif
    </tbody>
</table>