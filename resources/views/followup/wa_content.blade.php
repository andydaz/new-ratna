@php
  $phone1 = $customer->phone;
  if($phone1.startsWith("0")){
    $phone1 = "62".substr($phone1, 1);
  }
  $phone2 = $customer->phone2;
  if($phone2.startsWith("0")){
    $phone2 = "62".substr($phone2, 1);
  }
@endphp
<tr class="phone">
  <td>
    <a href="{{"https://wa.me/".$phone1}}" target="_blank">{{$phone1}}</a>
  </td>
</tr>
<tr class="phone">
  <td>
    <a href="{{"https://wa.me/".$phone2}}" target="_blank">{{$phone2}}</a>
  </td>
</tr>