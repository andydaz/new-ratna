<style type="text/css">
  .table.none tr, .table.none td, .table.none th, .table.none thead{
    border: none;
    vertical-align: middle;
    font-weight: 600;
    font-size: 16px;
  }

  .left-td{
    width: 120px
  }

  .right-td{
    width: 150px
  }
</style>
<div class="row">
  <div class="col l12 margin-top">
    <table class="table none">
      @php
       $total_piutang = $total->sum('total_receiveable');
       $unpaid = $total->sum('unpaid_amount');
       $paid = $total->sum('paid_amount');
       $remaining = $total_piutang - $paid;
      @endphp
      <thead>
        <tr>
          <th colspan="5">Total Receiveable By Year Outstanding</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="left-td">Piutang</td>
          <td>{{number_format($total_piutang)}}</td>
          <td></td>
          <td class="right-td">% Achievement</td>
          <td>{{$total_piutang == 0 ? 0 : number_format($paid / $total_piutang * 100)}} %</td>
        </tr>
        <tr>
          <td class="left-td">Jatuh Tempo</td>
          <td>{{number_format($unpaid)}}</td>
          <td></td>
          <td class="right-td">% On Time A/R</td>
          <td>
            {{$total_piutang == 0 ? 0 : number_format($paid / ($paid + $total_piutang) * 100)}} %
          </td>
        </tr>
        <tr>
          <td class="left-td">Paid</td>
          <td>{{number_format($paid)}}</td>
          <td></td>
          <td class="right-td">Sisa Piutang</td>
          <td>{{number_format($remaining)}}</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<div class="row">
  @php $monthNumber = 1 @endphp
  @foreach($data as $key => $value)
    <div class="col l3 margin-top">
      @php
        $total = $value->sum('total_receiveable');
        $unpaid = $value->sum('unpaid_amount');
        $paid = $value->sum('paid_amount');
        $remaining = $total-$paid;
        $achievement = $total == 0 ? 0 : $paid / $total * 100;
        $ontime = $total == 0 ? 0 : $paid / ($paid + $unpaid) * 100;
        $achievementStyle = "";
        $ontimeStyle = "";
        if($total > 0 && $achievement < 50){
          $achievementStyle = "background-color: #D1495B";
        }else if($total > 0 &&  $achievement < 80){
          $achievementStyle = "background-color: #FFBC42";
        }else if($total > 0 && $achievement < 100){
          $achievementStyle = "background-color: #52B2CF";
        }else if($total > 0 &&  $achievement == 100){
          $achievementStyle = "background-color: #16F4D0";
        }

        if($total > 0 && $ontime < 50){
          $ontimeStyle = "background-color: #D1495B";
        }else if($total > 0 &&  $ontime < 80){
          $ontimeStyle = "background-color: #FFBC42";
        }else if($total > 0 && $ontime < 100){
          $ontimeStyle = "background-color: #52B2CF";
        }else if($total > 0 &&  $ontime == 100){
          $ontimeStyle = "background-color: #16F4D0";
        }
      @endphp
      <table class="table table-bordered display nowrap dataTable dtr-inline">
        <thead>
          <tr>
            <th colspan="2" style="position: relative; text-align: center">
              <span>{{ucfirst($key).' '.$year}}</span>
              <a href="{{"outstanding-download/${monthNumber}/${year}"}}" target="_blank" class="outstanding-download" value="{{$key}}" style="position: absolute; right: 5px; cursor: pointer;"><i class="material-icons">file_download</i></a>
          </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Piutang</td>
            <td>{{number_format($total)}}</td>
          </tr>
          <tr>
            <td>Jatuh Tempo</td>
            <td>{{number_format($unpaid)}}</td>
          </tr>
          <tr>
            <td>Paid</td>
            <td>{{number_format($paid)}}</td>
          </tr>
          <tr>
            <td>Sisa Piutang</td>
            <td>{{number_format($remaining)}}</td>
          </tr>
          <tr style="{{$achievementStyle}}">
            <td>% Achievement</td>
            <td>{{$total == 0 ? 0 : number_format($paid / $total * 100)}} %</td>
          </tr>
          <tr style="{{$ontimeStyle}}">
            <td>% On Time A/R</td>
            <td>{{$total == 0 ? 0 : number_format($paid / ($paid + $unpaid) * 100)}} %</td>
          </tr>
        </tbody>
      </table>
    </div>
    @php $monthNumber++ @endphp
  @endforeach
</div>