<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<style type="text/css">
  .outstanding-legend-section{
    height: 45px;
    display: flex;
    align-items: center;
  }

  .legend-green, .legend-blue, .legend-yellow, .legend-red{
    display: inline-block;
    width: 10px;
    height: 10px; 
  }

  .legend-green{
    background-color: #16F4D0
  }

  .legend-blue{
    background-color: #52B2CF
  }

  .legend-yellow{
    background-color: #FFBC42
  }

  .legend-red{
    background-color: #D1495B
  }

  .outstanding-legend{
    margin: 0 10px;
  }
</style>
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Outstanding Receiveable</div>
                    <div class="collapsible-body">
                      <div class="container-fluid">
                        <div class="row mb-0">
                          <div class="input-field col l3">
                            <label>Filter Year</label>
                            <select id="filterOutstandingYear" name="year" class="browser-default selectpicker">
                              
                            </select>
                          </div>
                          <div class="input-field col l9 outstanding-legend-section">
                            <span>Legend :</span>
                            <div class="outstanding-legend">
                              <div class="legend-green"></div>
                              <span>100%</span>
                            </div>
                            <div class="outstanding-legend">
                              <div class="legend-blue"></div>
                              <span>>80%</span>
                            </div>
                            <div class="outstanding-legend">
                              <div class="legend-yellow"></div>
                              <span>>50%</span>
                            </div>
                            <div class="outstanding-legend">
                              <div class="legend-red"></div>
                              <span>0-50%</span>
                            </div>
                          </div>
                        </div>
                        <div id="outstanding-content" class="pv-xs">
                          @include('outstanding_receiveable.outstanding_receiveable_content')
                        </div>
                      </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="wa-modal" class="modal">
  <div class="modal-content">
    <div class="row">
      <div class="col l12">
        <table id="modal-delete-data">
          <thead>
              <tr>
                <th>Pilih No Whatsapp</th>        
              </tr>
          </thead>
          <tbody id="wa-modal-content">
              
          </tbody>
        </table>
        <div>
          <label>62</label>
          <input id="wa-manual" type="text" name="manual">
          <a id="submit-wa" href="#" target="_blank" class="btn btn-raised blue white-text">go to wa</a>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </form>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        // $("#outstanding-content").on('click', '.outstanding-download', function(event){
        //   var value = $(this).attr('value');
        //   var year = $('#filterOutstandingYear').val();
        //   var month = moment().month(value).format("M");
        //   $.ajax({
        //         method:"get",
        //         url:"outstanding-download",
        //         data:{
        //           year:year,
        //           month: month 
        //         },
        //         success:function(response){
        //             // $('#outstanding-content').html(response);
        //             // changetitle();
        //         },complete:function (){
        //             // $('#export').attr('href','downloadreportpo/'+start+'/'+end).removeAttr('disabled');
        //         }
        //     })
        // });

        firstload();

        $('#filterOutstandingYear').on('change', function(event){
            event.stopImmediatePropagation();
            $('#outstanding-content').html("<div class='loader'></div>");
            var year = $('#filterOutstandingYear').val();
            $.ajax({
                method:"get",
                url:"outstanding-receiveable",
                data:{
                  year:year, 
                },
                success:function(response){
                    $('#outstanding-content').html(response);
                    // changetitle();
                },complete:function (){
                    // $('#export').attr('href','downloadreportpo/'+start+'/'+end).removeAttr('disabled');
                }
            })
        });

        //function
        function firstload(){
          min = 2018;
          max = new Date().getFullYear();
          select = document.getElementById('filterOutstandingYear');

          for (var i = min; i<=max; i++){
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            if(i == max){              
              opt.selected = true
            }
            select.appendChild(opt);
          }
          $('.selectpicker').selectpicker('render');
        }
    });
</script>