@php 
    $max = 0;
    foreach ($report as $key => $value) {
        if ($value->last_paid_term > $max){
            $max = $value->last_paid_term;
        }
    }
@endphp
<table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <tr>
    <td>Sales Order Number</td>
    <td>Kode Cutomer</td>
    <td>Name</td>
    <td>Kategori Barang</td>
    <td>Deskripsi Barang</td>
    <td>Harga Jual Setelah Diskon</td>
    <td>Jumlah Cicilan</td>
    <td>Cicilan Per Bulan</td>
    <td>Total Bayar</td>
    <td>Total Jatuh Tempo</td>
    <td>Purchase Date</td>
    <td>Day Purchase</td>
    <td>Last Payment</td>
    <td>Month Aging</td>
    @for($i = 1; $i <= $max; $i++)
        <td>Cicilan {{$i}}</td>
    @endfor
    </tr>
    <tbody>
    @foreach($report as $key => $value)
        <tr>
            <td>{{$value->so->sales_order_number}}</td>
            <td>{{$value->so->customer->customer_id}}</td>
            <td>{{$value->so->customer_name}}</td>
            <td>{{$value->product->category->description}}</td>
            <td>{{$value->product->brand->name.' '.$value->product->type->name}}</td>
            <td>{{$value->total_receiveable}}</td>
            <td>{{$value->so->term_payment}}</td>
            <td>{{$value->total_amount}}</td>
            <td>{{$value->paid_amount}}</td>
            <td>{{$value->unpaid_amount}}</td>
            <td>{{$value->so->date_sales_order}}</td>
            <td>{{Carbon\Carbon::createFromFormat('Y-m-d', $value->so->date_sales_order)->day}}</td>
            <td>{{$value->lastpaid}}</td>
            <td>{{$value->upcoming_due ? Carbon\Carbon::createFromFormat('Y-m-d', $value->upcoming_due)->diffInMonths(\Carbon\Carbon::now(), false): "-1"}}</td>
            @for($i = 1; $i <= $value->last_paid_term; $i++)
                <td>{{"✓"}}</td>
            @endfor
            @for($i = $value->last_paid_term; $i <= $max; $i++)
                <td></td>
            @endfor
        </tr>
    @endforeach
    </tbody>
</table>