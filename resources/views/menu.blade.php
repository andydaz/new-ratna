<ul id="slide-out" class="side-nav fixed">
  <li>
    <div class="userView">
      <div class="background">
        <img src="images/office.jpg">
      </div>
      <a href="#!user">
        <img class="circle" @if(Session('user')->profile_picture) src="{{'uploads/avatars/'.Session('user')->profile_picture}}" @else src="uploads/avatars/profile.jpg" @endif>
      </a>
      <a><h6 class="white-text">{{Session('user')->full_name}} - {{Session('roles')->name}}</h6></a>
      <a href="#!email" data-activates="email-drop" class="dropdown-button"><span class="white-text email">{{Session('user')->email}}<i class="right material-icons">arrow_drop_down</i></span></a>
    </div>
    <ul id="email-drop" class="dropdown-content">
      <li><a href="logout">Logout</a></li>
    </ul>
  </li>
  <ul class="collapsible" data-collapsible="accordion">
    <li>
      <ul class="collapsible-header ">
        <li><span class="subheader">Application List</span></li>
      </ul>
      <ul class="collapsible-body">
        <li class="nav-item active"><a url="homes"><i class="material-icons">home</i>Home</a></li>
        <li class="nav-item "><a href="user_profile"><i class="material-icons">person_pin</i>User Profile</a></li>
        <li><a href="change_password"><i class="material-icons">person_pin</i>Change Password</a></li>
        @if(Session('roles')->name == 'master')
          <li><a href="manage_user"><i class="material-icons">accessibility</i>User Manage</a></li>
        @endif
        <li><a href="logout" class="nav-item"><i class="material-icons">exit_to_app</i>Logout</a></li>
      </ul>
    </li>
  </ul>
  <li class="divider"></li>
  <ul class="collapsible" data-collapsible="accordion">
    <li>
      <ul class="collapsible-header">
        <li><span class="subheader">Master</span></li>
      </ul>
      <ul class="collapsible-body">
        <li class="nav-item"><a url="pelanggan" ><i class="material-icons">people</i>Pelanggan</a></li>
        <li class="nav-item"><a url="supplier" ><i class="material-icons">add_shopping_cart</i>Supplier</a></li>
        @if(Session('roles')->name == 'master')
          <li class="nav-item"><a url="news"><i class="material-icons">trending_up</i>News</a></li>
          <li class="nav-item"><a url="bank"><i class="material-icons">attach_money</i>Bank</a></li>
          <li class="nav-item"><a url="gudang"><i class="material-icons">store</i>Warehouse</a></li>
        @endif
        @if(Session('roles')->name == 'master' || Session('roles')->name == 'Admin Purchasing')
          <li class="nav-item"><a url="kategoribarang"><i class="material-icons">donut_large</i>Kategori Barang</a></li>
          <li class="nav-item"><a url="brandbarang"><i class="material-icons">donut_large</i>Brand Barang</a></li>
          <li class="nav-item"><a url="tipebarang"><i class="material-icons">donut_large</i>Tipe Barang</a></li>
        @endif
        @if(Session('roles')->name == 'master')
          <li class="nav-item"><a url="levelpelanggan"><i class="material-icons">people</i>Level Pelanggan</a></li>
        @endif
        <li class="nav-item"><a url="expensecategory"><i class="material-icons">people</i>Kategori Expense</a></li>
        @if(Session('roles')->name == 'master')
          <li class="nav-item"><a url="shippingterm"><i class="material-icons">people</i>Shipping Term</a></li>
        @endif
      </ul>
    </li>
  </ul>
  <li class="divider"></li>
  @if(Session('roles')->name == 'master' || Session('roles')->name == 'Admin Purchasing')
    <ul class="collapsible" data-collapsible="accordion">
      <li>
        <ul class="collapsible-header">
          <li><span class="subheader">Pembelian</span></li>
        </ul>
        <ul class="collapsible-body">
          <li class="nav-item"><a url="purchaseorder"><i class="material-icons">attach_money</i>Pemesanan / PO</a></li>
          <li class="nav-item"><a url="hargajual"><i class="material-icons">attach_money</i>Input Harga Jual</a></li>
          <li class="nav-item"><a url="penerimaanbarang"><i class="material-icons">arrow_back</i>Penerimaan Barang</a></li>
          <li class="nav-item"><a url="purchaseinvoice"><i class="material-icons">assignment</i>Purchase Invoice</a></li>
          <li class="nav-item"><a url="purchasepayment"><i class="material-icons">assignment</i>Purchase Payment</a></li>
          <li class="nav-item"><a url="purchasereturn"><i class="material-icons">arrow_back</i>Purchase Return</a></li>
          <li class="nav-item"><a url="depositopayment"><i class="material-icons">arrow_back</i>Deposito Clearance</a></li>
        </ul>
      </li>
    </ul>
    <li class="divider"></li>
  @endif
  @if(Session('roles')->name == 'master' || Session('roles')->name == 'Admin Sales')
    <ul class="collapsible" data-collapsible="accordion">
      <li>
        <ul class="collapsible-header">
          <li><span class="subheader">Penjualan</span></li>
        </ul>
        <ul class="collapsible-body">
          <li class="nav-item"><a url="salesorder"><i class="material-icons">account_box</i>Sales Order</a></li>
          <li class="nav-item"><a url="deliveryorder"><i class="material-icons">account_box</i>Delivery Order</a></li>
          <li class="nav-item"><a url="salespayment"><i class="material-icons">account_box</i>Pembayaran Cicilan</a></li>
          <li class="nav-item"><a url="salesreturn"><i class="material-icons">arrow_back</i>Return Sales</a></li>
          <li class="nav-item"><a url="balancemanagement"><i class="material-icons">account_box</i>Saldo Pelanggan</a></li>
          <li class="nav-item"><a url="existingreceiveable"><i class="material-icons">short_text</i><p>Existing Receiveable</p></a></li>
        </ul>
      </li>
    </ul>
    <li class="divider"></li>
  @endif
  <ul class="collapsible" data-collapsible="accordion">
    <li>
      <ul class="collapsible-header">
        <li><span class="subheader">Stock</span></li>
      </ul>
      <ul class="collapsible-body">
        <li class="nav-item"><a url="stockoverview"><i class="material-icons">import_contacts</i>Stock Overview</a></li>
        @if(Session('roles')->name == 'master')
          <li class="nav-item"><a url="stockcorrection"><i class="material-icons">import_contacts</i>Stock Correction</a></li>
          <li class="nav-item"><a url="historystockcorrection"><i class="material-icons">import_contacts</i>History Correction</a></li>
        @endif
      </ul>
    </li>
  </ul>
  <li class="divider"></li>
  <ul class="collapsible" data-collapsible="accordion">
    <li>
      <ul class="collapsible-header">
        <li><span class="subheader">Report</span></li>
      </ul>
      <ul class="collapsible-body">
        <li class="nav-item"><a url="reportpo"><i class="material-icons">book</i>Report PO</a></li>
        <li class="nav-item"><a url="reportso"><i class="material-icons">book</i>Report SO</a></li>
        <li class="nav-item"><a url="reportpiutang"><i class="material-icons">book</i>Report Piutang</a></li>
        <li class="nav-item"><a url="reportpembayaranpiutang"><i class="material-icons">book</i><p>Report Pembayaran Piutang</p></a></li>
        <li class="nav-item"><a url="reportexpense"><i class="material-icons">book</i><p>Report Biaya Harian</p></a></li>
        @if(Session('roles')->name == 'master')
          <li class="nav-item"><a url="reportcashbalance"><i class="material-icons">book</i><p>Report Cash Balance</p></a></li>
          <li class="nav-item"><a url="reportprofitloss"><i class="material-icons">book</i><p>Report Profit Loss</p></a></li>
          <li class="nav-item"><a url="reportexistingreceiveable"><i class="material-icons">book</i><p>Report Existing Transaction</p></a></li>
        @endif
      </ul>
    </li>
  </ul>
  <li class="divider"></li>

  <ul class="collapsible" data-collapsible="accordion">
    <li>
      <ul class="collapsible-header">
        <li><span class="subheader">Misc</span></li>
      </ul>
      <ul class="collapsible-body">
        <li class="nav-item"><a url="expense"><i class="material-icons">short_text</i><p>Expense</p></a></li>
        <li class="nav-item"><a url="followup"><i class="material-icons">short_text</i><p>Follow Up</p></a></li>
        <li class="nav-item"><a url="outstanding-receiveable"><i class="material-icons">short_text</i><p>Outstanding Receiveable</p></a></li>
      </ul>
    </li>
  </ul>
</ul>