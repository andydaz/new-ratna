<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Filter Report Pembayaran Piutang</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="input-field col l3">
                                    <label>Tanggal Awal</label>
                                    <input id="tglmulai" type="text" class="f-input" placeholder="Tanggal Awal">
                                </div>
                                <div class="input-field col l3">
                                    <label>Tanggal Akhir</label>
                                    <input id="tglakhir" type="text" class="f-input" placeholder="Tanggal Akhir">
                                </div>
                                <div class="input-field col l3">
                                    <label>Customer</label>
                                    <select id="customer" name="customer" class="browser-default selectpicker" data-live-search="true">
                                        <option value="0">Select Customer</option>
                                        @foreach($customer as $key => $value)
                                            <option value="{{$value->customer_id}}">{{$value->first_name.' '.$value->last_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-field col l3">
                                    <label></label>
                                    <a id="preview" href="#" class="btn btn-raised blue white-text">preview</a>
                                    <a id="export" target="_blank" href="" class="btn btn-raised blue white-text" disabled>Export</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Report Pembayaran Piutang</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="col l12 m12 s12">
                                </div>
                                <br>
                                <div class="col l12 m12 s12">
                                    <h4 id="report-title"></h4>
                                    <div class="table-responsive">
                                        {{-- @include('report.report_pembayaran_piutang_detail') --}}
                                        <table id="pembayaran-piutang-table" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                                          <thead>
                                              <th>No Invoice</th>
                                              <th>Customer</th>
                                              <th>Cicilan Ke</th>
                                              <th>Kode Barcode</th>
                                              <th>Pembayaran Masuk</th>
                                          </thead>
                                          <tbody>

                                          </tbody>
                                      </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        $('#preview').on('click', function(event){
            event.stopImmediatePropagation();
            var start = $('#tglmulai').val();
            var end = $('#tglakhir').val();
            var customer = $('#customer').val();
            pembayaranPiutangTable.draw();
            $('#export').attr('href','downloadreportpembayaranpiutang/'+start+'/'+end+'/'+customer)
                        .removeAttr('disabled');
        });

        firstload();

        function firstload(){
            $('#tglmulai, #tglakhir').dcalendarpicker({
                format: 'dd-mm-yyyy'
            });

            var date = moment();
            var month = date.month()+1;
            var year = date.year();
            var endofmonth = moment().endOf('month').format('DD');

            var start = '01-'+month+'-'+year;
            var end = endofmonth+'-'+month+'-'+year

            $('#tglmulai').val(start);
            $('#tglakhir').val(end);

            $('.selectpicker').selectpicker('render');

            pembayaranPiutangTable = $('#pembayaran-piutang-table').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                'sDom': 'tip',
                "pageLength": 50,
                ajax: {
                    url : 'getpembayaranpiutangtable',
                    data : function (d){
                        d.startdate = $('#tglmulai').val();
                        d.enddate = $('#tglakhir').val();
                        d.customer = $('#customer').val();
                    }
                },
                rowId: 'invoice_sales_id',
                columns: [
                    { data: 'invoice_sales_number', name: 'invoice_sales_number'},
                    { data: 'so.customer_name', name: 'customer_name'},
                    { data: 'term_number', name: 'term_number'},
                    { data: 'product.bar_code', name: 'bar_code'},
                    { data: 'total_amount', name: 'total_amount'},
                ],
            });

            changetitle();
        }
        
        function changetitle(){
            $('#report-title').html("Report Pemabayaran Piutang Periode "+$('#tglmulai').val()+" - "+$('#tglakhir').val());
        }
    });

</script>