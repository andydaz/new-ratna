<!DOCTYPE html>
<html>
<body>
<table id="reportSoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <tr>
        <td>No</td>
        <td>Tanggal</td>
        <td>Month</td>
        <td>Nota</td>
        <td>Kode Customer</td>
        <td>Nama</td>
        <td>Kode Barcode</td>
        <td>Klasifikasi Barang</td>
        <td>Deskripsi Barang</td>
        <td>Cicilan Ke</td>
        <td>Harga Jual</td>
        <td>Jumlah Tenor</td>
        <td>Sisa Hutang</td>
        <td>Cicilan Per Bulan</td>
        <td>Denda</td>
        <td>Pembayaran Masuk</td>
        <td>Metode Bayar</td>
        <td>Note</td>
        <td>Payment Number</td>
        @foreach($metode as $key => $value)
            <th>{{$value->bank->bank_name.' '.$value->payment_method_type->description}}</th>
        @endforeach
    </tr>
    <tbody>
    @foreach($report as $key => $value)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$value->payment->date_sales_payment}}</td>
            <td>{{\Carbon\Carbon::parse($value->payment->date_sales_payment)->month}}</td>
            <td>{{$value->invoice_sales_number}}</td>
            <td>{{$value->so->customer_id}}</td>
            <td>{{$value->so->customer_name}}</td>
            <td>{{$value->product->bar_code}}</td>
            <td>{{$value->product->category->description}}</td>
            <td>{{$value->product->brand->name.' '.$value->product->type->name}}</td>
            <td>{{$value->term_number}}</td>
            <td>{{$value->product->detailso->sub_total}}</td>
            <td>{{$value->so->term_payment}}</td>
            <td>{{$value->product->detailso->sub_total - ($value->term_number * $value->total_amount)}}</td>
            <td>{{$value->total_amount}}</td>
            <td></td>
            <td>{{$value->total_amount}}</td>
            <td>{{$value->payment->details->count() > 1
            ? 'Mixed Payment'
            : $value->payment->details[0]->method->bank->bank_name.' '.$value->payment->details[0]->method->payment_method_type->description}}</td>
            <td>{{$value->payment->note}}</td>
            <td>{{$value->payment->payment_sales_number}}</td>
        @foreach($metode as $key2 => $value2)
                <td>
                    @if($value->payment->details->where('payment_method_id', $value2->payment_method_id)->count() > 0)
                        {{$value->payment->details->where('payment_method_id', $value2->payment_method_id)->first()->paid}}
                    @else
                        -
                    @endif
                </td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>