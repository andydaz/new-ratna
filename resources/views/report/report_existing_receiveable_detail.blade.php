<table id="reportTotalPoTable" class="highlight table table-bordered display nowrap dtr-inline margin-top">
  <tbody>
    <tr>
      <td colspan="4">Total Existing Receiveable</td>
      <td id="total-existing">{{number_format($customer->sum('existing_receiveable'))}}</td>
    </tr>
  @if(sizeOf($transaction) > 0)
    <tr>
      <td colspan="4">Total Bayar</td>
      <td id="total-existing-paid">{{number_format($transaction->sum('payment'))}}</td>
    </tr>
  @endif
  </tbody>
</table>
<table id="reportExistingReceiveableTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
  <th>No Transaksi</th>
  <th>Nama Customer</th>
  <th>Nama Product</th>
  <th>Tanggal</th>
  <th>Payment</th>
  </thead>
  <tbody>
  @foreach($transaction as $key => $value)
    <tr id="{{$value->existing_receiveable_transaction_id}}" class="transaction-row">
      <td class="nopo">{{$value->transaction_number}}</td>
      <td>{{$value->customer->first_name.' '.$value->customer->last_name}}</td>
      <td>{{$value->product_name}}</td>
      <td>{{$value->transaction_date}}</td>
      <td class="payment">{{number_format($value->payment)}}</td>
    </tr>
  @endforeach
  </tbody>
</table>
<script type="text/javascript">
    reportExistingReceiveableTable = $('#reportExistingReceiveableTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'tip',
        'aaSorting':[],
        "language": {
            "infoEmpty": "No records to display",
            "zeroRecords": "No records to display",
            "emptyTable": "No data available in table",
        },
    });
</script>