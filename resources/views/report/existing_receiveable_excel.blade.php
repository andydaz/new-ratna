<!DOCTYPE html>
<html>
<head>
    <title>Report PO</title>
</head>
<body>
    <table id="reportExistingReceiveableTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
      <tr>
          <th>No Transaksi</th>
          <th>Nama Customer</th>
          <th>Nama Product</th>
          <th>Tanggal</th>
          <th>Payment</th>
      </tr>
      <tbody>
          @foreach($report as $key => $value)
          <tr id="{{$value->existing_receiveable_transaction_id}}" class="transaction-row">
              <td class="nopo">{{$value->transaction_number}}</td>
              <td>{{$value->customer->first_name.' '.$value->customer->last_name}}</td>
              <td>{{$value->product_name}}</td>
              <td>{{$value->transaction_date}}</td>
              <td class="payment">{{$value->payment}}</td>
          </tr>
          @endforeach
      </tbody>
      <tfoot>
          @if(sizeOf($report) > 0)
          <tr>
              <td colspan="4">Total Bayar</td>
              <td id="total-po">{{$report->sum('payment')}}</td>
          </tr>
          @endif
      </tfoot>
  </table>
</body>
</html>