<table id="homeTableStock" class="table table-bordered display nowrap dataTable dtr-inline">
  <thead>
  <tr>
    <th>Category</th>
    <th>Brand</th>
    <th>0-3 Month</th>
    <th>3-6 Month</th>
    <th>6-12 Month</th>
    <th>12-24 Month</th>
    <th>>24 Month</th>
    <th>Total Qty</th>
  </tr>
  </thead>
  <tbody>
  @php
    $totalqty = 0;
  @endphp
  @foreach($data as $key => $value)
    <tr>
      <td>{{$value['category']}}</td>
      <td>{{$value['name']}}</td>
      <td>{{$value['3months']}}</td>
      <td>{{$value['3-6months']}}</td>
      <td>{{$value['6-12months']}}</td>
      <td>{{$value['12-24months']}}</td>
      <td>{{$value['24months']}}</td>
      <td>{{
      $value['3months'] + $value['3-6months']
      +$value['6-12months']+$value['12-24months']+$value['24months']
      }}</td>
    </tr>
  @endforeach
  </tbody>
</table>