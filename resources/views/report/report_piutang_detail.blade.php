<table id="reportPiutangTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
  <th>No SO</th>
  <th>Customer</th>
  <th>Total Terbayar</th>
  <th>Saldo Terhutang</th>
  <th>Last Jatuh Tempo</th>
  </thead>
  <tbody>
  @foreach($report as $key => $value)
    <tr id="{{$value->sales_order_id}}" class="so-row">
      <td class="noso">{{$value->so->sales_order_number}}</td>
      <td>{{$value->so->customer_name}}</td>
      <td>{{number_format($value->total_paid)}}</td>
      <td>{{number_format($value->product->price_sale - $value->total_paid)}}</td>
      <td>{{$value->last_due_date}}</td>
    </tr>
  @endforeach
  </tbody>
</table>
<script type="text/javascript">
    reportPiutangTable = $('#reportPiutangTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'tip',
        'aaSorting':[],
        "language": {
            "infoEmpty": "No records to display",
            "zeroRecords": "No records to display",
            "emptyTable": "No data available in table",
        },
    });
</script>