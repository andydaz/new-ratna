<table id="reportTotalSoTable" class="highlight table table-bordered display nowrap dtr-inline margin-top">
  <tbody>
  @if(sizeOf($so) > 0)
    <tr>
      @php
        $totalso = $so->sum('grand_total_idr');
      @endphp
      <td colspan="5">Total Nilai SO</td>
      <td id="total-so">{{number_format($totalso)}}</td>
    </tr>
    <tr>
      <td colspan="5">Total Payment</td>
      <td id="total-payment">
        @php
          $totalpaid = 0;
          foreach($so as $key => $value){
              $totalpaid += $value->payment->sum('total_paid');
          }
        @endphp
        {{number_format($totalpaid)}}
      </td>
    </tr>
    <tr>
      <td colspan="5">Total Piutang</td>
      <td id="total-receiveable">{{number_format($totalso - $totalpaid)}}</td>
    </tr>
  @endif
  </tbody>
</table>

<table id="reportSoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
  <th>No SO</th>
  <th>Deskripsi</th>
  <th>Customer</th>
  <th>Tanggal</th>
  <th>Total</th>
  <th>Payment</th>
  </thead>
  <tbody>
  @foreach($so as $key => $value)
    <tr id="{{$value->sales_order_id}}" class="so-row">
      <td class="nopo">{{$value->sales_order_number}}</td>
      <td>{{$value->note}}</td>
      <td>{{$value->customer->first_name.' '.$value->customer->last_name}}</td>
      <td>{{$value->date_sales_order}}</td>
      <td class="total">{{number_format($value->grand_total_idr)}}</td>
      @php
        $total = 0;
        foreach($value->payment as $key => $value)
        {
            $total += $value->total_paid;
        }
      @endphp
      <td class="payment">{{number_format($total)}}</td>
    </tr>
  @endforeach
  </tbody>
</table>
<script type="text/javascript">
    reportPoTable = $('#reportSoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'tip',
        'aaSorting':[],
        "language": {
            "infoEmpty": "No records to display",
            "zeroRecords": "No records to display",
            "emptyTable": "No data available in table",
        },
    });
</script>