<table id="reportStockTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
  <tr>
    <th>Barcode</th>
    <th>Kategori</th>
    <th>Brand</th>
    <th>Tipe</th>
    <th>Harga Beli</th>
    <th>Harga Jual</th>
    <th>Tanggal Penerimaan</th>
    <th>Supplier</th>
  </tr>
  </thead>
  <tbody>
    @foreach($stock as $key => $value)
        <tr>
            <td>{{$value->bar_code}}</td>
            <td>{{$value->category->description}}</td>
            <td>{{$value->brand->name}} </td>
            <td>{{$value->type->name}}</td>
            <td>{{$value->price_buy}}</td>
            <td>{{$value->price_sale}}</td>
            <td>{{$value->detailgr ? $value->detailgr->goodreceive->good_receive_date : '-'}}</td>
            <td>{{$value->detailgr ? $value->detailgr->goodreceive->po->supplier->company_name : '-'}}</td>
        </tr>
    @endforeach
  </tbody>
</table>