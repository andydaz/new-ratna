<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Filter Report Purchase Order</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="input-field col l3">
                                    <label>Tanggal Awal</label>
                                    <input id="tglmulai" type="text" class="f-input" placeholder="Tanggal Awal">
                                </div>
                                <div class="input-field col l3">
                                    <label>Tanggal Akhir</label>
                                    <input id="tglakhir" type="text" class="f-input" placeholder="Tanggal Akhir">
                                </div>
                                <div class="input-field col l3">
                                    <label>Supplier</label>
                                    <select id="supplier" name="supplier" class="browser-default selectpicker" data-live-search="true">
                                        <option value="0">Select Supplier</option>
                                        @foreach($supplier as $key => $value)
                                            <option value="{{$value->supplier_id}}">{{$value->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-field col l3">
                                    <label></label>
                                    <a id="preview" href="#" class="btn btn-raised blue white-text">preview</a>
                                    <a id="export" target="_blank" href="#" class="btn btn-raised blue white-text" disabled>Export</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Report PO</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="col l12 m12 s12">
                                </div>
                                <br>
                                <div class="col l12 m12 s12">
                                    <h4 id="report-title"></h4>
                                    <div id="report-table" class="table-responsive">
                                        @include('report.report_po_detail')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div id="modal-detail-po" class="modal">
    <div class="modal-content">
        <h4 id="modal-title"></h4>
        <div class="row">
            <table class="table table-bordered display nowrap dataTable dtr-inline">
                <thead>
                <th>Kode Barang</th>
                <th>Nama Barang</th>
                <th>Quantity</th>
                </thead>
                <tbody id="modal-data">

                </tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius">OK</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        $('#preview').on('click', function(event){
            event.stopImmediatePropagation();
            var start = $('#tglmulai').val();
            var end = $('#tglakhir').val();
            var supplier = $('#supplier').val();
            $.ajax({
                method:"get",
                url:"reportpo",
                data:{start:start, end:end, supplier:supplier},
                success:function(response){
                    $('#report-table').html(response);
                    changetitle();
                },complete:function (){
                    $('#export').attr('href','downloadreportpo/'+start+'/'+end).removeAttr('disabled');
                }
            })
        });

        $('.highlight tbody tr').on('click',function(event){
            var id = $(this).attr('id');
            var nopo = $(this).find('.nopo').html();
            $.ajax({
                method:"get",
                url:"detailreportpo",
                data:{id:id},
                success:function(response){
                    $('#modal-title').html('Detail PO '+ nopo);
                    $('#modal-data').html(response);
                    $('#modal-detail-po').modal('open');
                }
            })
        });

        firstload();

        function firstload(){
            $('#tglmulai, #tglakhir').dcalendarpicker({
                format: 'dd-mm-yyyy'
            });

            var date = moment();
            var month = date.month()+1;
            var year = date.year();
            var endofmonth = moment().endOf('month').format('DD');

            $('#tglmulai').val('01-'+month+'-'+year);
            $('#tglakhir').val(endofmonth+'-'+month+'-'+year);

            $('.selectpicker').selectpicker('render');

            changetitle();
        }

        function changetitle(){
            $('#report-title').html("Report PO Periode "+$('#tglmulai').val()+" - "+$('#tglakhir').val());
        }

        function calculatetotal(){
            var total = 0
            var payment = 0
            var diskon = 0

            if($('.po-row').length > 0)
            {
                $('.total').each(function(){
                    if($(this).html() =='-') {
                        total += 0;
                    }else{
                        total += parseInt($(this).html());
                    }
                });

                $('.payment').each(function(){
                    if($(this).html() =='-') {
                        payment += 0;
                    }else{
                        payment += parseInt($(this).html());
                    }
                });

                $('#total-po').html(total);
                $('#total-diskon').html(total - diskon);
                $('#total-payment').html(payment);
                $('#total-payable').html(total-diskon-payment);
            }
        }
    });

</script>