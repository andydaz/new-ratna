<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Filter Report Piutang</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="input-field col l3">
                                    <label>Customer</label>
                                    <select id="customer" name="customer" class="browser-default selectpicker" data-live-search="true">
                                        <option value="0">Select Customer</option>
                                        @foreach($customer as $key => $value)
                                            <option value="{{$value->customer_id}}">{{$value->first_name.' '.$value->last_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-field col l3">
                                    <label></label>
                                    <a id="preview" href="#" class="btn btn-raised blue white-text">preview</a>
                                    <a id="export" target="_blank" href="" class="btn btn-raised blue white-text" disabled>Export</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Report Piutang</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="col l12 m12 s12">
                                </div>
                                <br>
                                <div class="col l12 m12 s12">
                                    <div id="report-table" class="table-responsive">
                                        @include('report.report_piutang_detail')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        $('#preview').on('click', function(event){
            event.stopImmediatePropagation();
            var customer = $('#customer').val();
            $.ajax({
                method:"get",
                url:"reportpiutang",
                data:{customer:customer},
                success:function(response){
                    $('#report-table').html(response);
                },
                complete:function (){
                    $('#export').attr('href','downloadreportpiutang/'+customer).removeAttr('disabled');
                }
            })
        });


        firstload();

        function firstload(){
            $('.selectpicker').selectpicker('render');
        }
    });

</script>