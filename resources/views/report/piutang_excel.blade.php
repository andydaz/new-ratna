<!DOCTYPE html>
<html>
<body>
<table id="reportSoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <tr>
        <td>Date</td>
        <td>{{Carbon\Carbon::now()->format('d-m-Y')}}</td>
    </tr>
    <tr></tr>
    <tr>
        <td>Kode Customer</td>
        <td>Name</td>
        <td>Kode Barcode</td>
        <td>Klasifikasi Barang</td>
        <td>Deskripsi Barang</td>
        <td>Harga Jual Setelah Diskon</td>
        <td>Jumlah Cicilan</td>
        <td>Cicilan Per Bulan</td>
        <td>Jumlah Cicilan Terbayar</td>
        <td>Total Bayar</td>
        <td>Saldo Terhutang</td>
        <td>Hutang Jatuh Tempo</td>
        <td>Purchase Date</td>
        <td>Last Jatuh Tempo</td>
        <td>Last Payment</td>
        <td>Aging</td>
        <td>Month Aging</td>
        <td>Status</td>
    </tr>
    <tbody>
    @foreach($report as $key => $value)
        <tr>
            <td>{{$value->so->customer_id}}</td>
            <td>{{$value->so->customer_name}}</td>
            <td>{{$value->product->bar_code}}</td>
            <td>{{$value->product->category->description}}</td>
            <td>{{$value->product->brand->name.' '.$value->product->type->name}}</td>
            <td>{{$value->so->grand_total_idr}}</td>
            <td>{{$value->so->term_payment}}</td>
            <td>{{$value->total_amount}}</td>
            <td>{{$value->total_paid / $value->total_amount}}</td>
            <td>{{$value->total_paid}}</td>
            <td>{{$value->product->price_sale - $value->total_paid}}</td>
            <td>{{$value->total_due}}</td>
            <td>{{$value->so->date_sales_order}}</td>
            <td>{{$value->last_due_date}}</td>
            <td>{{$value->last_payment_date}}</td>
            <td>{{Carbon\Carbon::createFromFormat('Y-m-d', $value->last_due_date)->diffInDays(\Carbon\Carbon::now(), false)}}</td>
            <td>{{Carbon\Carbon::createFromFormat('Y-m-d', $value->last_due_date)->diffInMonths(\Carbon\Carbon::now(), false)}}</td>
            <td>{{$value->aging}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>