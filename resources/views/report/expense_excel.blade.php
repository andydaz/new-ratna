<!DOCTYPE html>
<html>
<body>
<table id="reportSoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <tr>
        <td>No</td>
        <td>Tanggal</td>
        <td>Bulan</td>
        <td>Deskripsi</td>
        <td>Klasifikasi Biaya</td>
        <td>Jumlah</td>
        <td>Nama Pengguna</td>
        <td>Sumber Dana</td>
    </tr>
    <tbody>
    @foreach($report as $key => $value)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$value->date_expense}}</td>
            <td>{{\Carbon\Carbon::parse($value->date_expense)->month}}</td>
            <td>{{$value->description}}</td>
            <td>{{$value->expense_category->expense_description}}</td>
            <td>{{$value->nominal}}</td>
            <td>{{$value->pengguna}}</td>
            <td>{{$value->payment_method->bank->bank_name.' '.$value->payment_method->payment_method_type->description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>