<!DOCTYPE html>
<html>
<body>
<table id="reportSoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <tr>
        <th>No</th>
        <th>Tanggal</th>
        <th>Tanggal Dibuat</th>
        <th>Month</th>
        <th>No Sales Order</th>
        <th>Deskripsi</th>
        <th>Kode Customer</th>
        <th>Nama</th>
        <th>Kode Barcode</th>
        <th>Jenis Klasifikasi</th>
        <th>Deskripsi Barang</th>
        <th>Tipe Pembayaran</th>
        <th>Jumlah Cicilan</th>
        <th>Harga Modal</th>
        <th>Harga Jual</th>
        <th>Diskon 1</th>
        <th>Diskon 2</th>
        <th>Harga Setelah Diskon</th>
        <th>Laba Kotor</th>
        <th>Cicilan Per Bulan</th>
        <th>Total Nilai SO</th>
        <th>Total Outstanding</th>
        <th>Pembayaran Bulan Ini</th>
        <th>Akumulasi Pembayaran Masuk</th>
        @foreach($metode as $key => $value)
            <th>{{$value->bank->bank_name.' '.$value->payment_method_type->description}}</th>
        @endforeach
        <th>Sales</th>
    </tr>
    <tbody>
    @foreach($report as $key => $value)
        @foreach($value->details as $key2 => $value2)
            @if($key2 == 0)
                <tr id="{{$value->sales_order_id}}" class="so-row">
                    <td>{{$key+1}}</td>
                    <td>{{$value->date_sales_order}}</td>
                    <td>{{$value->created_at->toDateString()}}</td>
                    <td>{{\Carbon\Carbon::parse($value->date_sales_order)->month}}</td>
                    <td>{{$value->sales_order_number}}</td>
                    <td>{{$value->note}}</td>
                    <td>{{$value->customer_id}}</td>
                    <td>{{$value->customer_name}}</td>
                    <td>{{$value2->product->bar_code}}</td>
                    <td>{{$value2->product->category->description}}</td>
                    <td>{{$value2->product->brand->name.' '.$value2->product->type->name}}</td>
                    <td>{{$value->payment_type->payment_description}}</td>
                    <td>{{$value->term_payment}}</td>
                    <td>{{$value2->product->price_buy}}</td>
                    <td>{{$value2->price}}</td>
                    <td>{{$value2->discount_percentage}}</td>
                    <td>{{$value2->discount_percentage_2}}</td>
                    <td>{{$value2->sub_total}}</td>
                    <td>{{($value2->sub_total - $value2->product->price_buy)}}</td>
                    <td>
                        {{-- Kalau Cicil baru tampilin jumlah cicilan per bulan --}}
                        @if($value->payment_type_id == 2)
                            {{($value->grand_total_idr / $value->term_payment)}}
                        @else
                            {{0}}
                        @endif
                    </td>
                    <td>{{$value->grand_total_idr}}</td>
                    @php
                        $payment_this_month = ($value->payment->where('date_sales_payment','>=', $start)->where('date_sales_payment','<=', $end)->sum('total_paid'))
                    @endphp
                    <td>{{$value->grand_total_idr - $payment_this_month}}</td>
                    <td>{{ $payment_this_month }}</td>
                    <td>{{($value->payment->sum('total_paid'))}}</td>
                    @foreach($metode as $key3 => $value3)
                        <td>
                        @php $total = 0; @endphp
                        @foreach($value->payment as $key4 => $value4)
                            @if($value4->details->where('payment_method_id',$value3->payment_method_id)->count() > 0)
                                @php $total += $value4->details->where('payment_method_id',$value3->payment_method_id)->first()->paid @endphp
                                @endif
                                @endforeach
                        {{$total}}
                        </td>
                    @endforeach
                    <td>{{($value->staff->full_name)}}</td>
                </tr>
            @else
                <tr class="so-row">
                    <td></td>
                    <td>{{$value->date_sales_order}}</td>
                    <td>{{$value->created_at->toDateString()}}</td>
                    <td>{{\Carbon\Carbon::parse($value->date_sales_order)->month}}</td>
                    <td>{{$value->sales_order_number}}</td>
                    <td>{{$value->note}}</td>
                    <td>{{$value->customer_id}}</td>
                    <td>{{$value->customer_name}}</td>
                    <td>{{$value2->product->bar_code}}</td>
                    <td>{{$value2->product->category->description}}</td>
                    <td>{{$value2->product->brand->name.' '.$value2->product->type->name}}</td>
                    <td>{{$value->payment_type->payment_description}}</td>
                    <td>{{$value->term_payment}}</td>
                    <td>{{$value2->product->price_buy}}</td>
                    <td>{{$value2->price}}</td>
                    <td>{{$value2->discount_percentage}}</td>
                    <td>{{$value2->discount_percentage_2}}</td>
                    <td>{{$value2->sub_total}}</td>
                    <td>{{($value2->sub_total - $value2->product->price_buy)}}</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    @foreach($metode as $key3 => $value3)
                        <td>
                          0
                        </td>
                    @endforeach
                    <td>{{($value->staff->full_name)}}</td>
                </tr>
            @endif
        @endforeach
    @endforeach
    </tbody>
</table>
</body>
</html>