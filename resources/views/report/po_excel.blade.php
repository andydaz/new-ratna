<!DOCTYPE html>
<html>
    <head>
        <title>Report PO</title>
    </head>
    <body>
        <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
            <tr>
                <td>No PO</td>
                <td>Deskripsi</td>
                <td>Tanggal</td>
                <td>Supplier</td>
                <td>Status</td>
                <td>Total</td>
                <td>Payment</td>
            </tr>
            <tbody>
    @foreach($report as $key => $value)
        <tr id="{{$value->purchase_order_id}}" class="po-row">
            <td class="nopo">{{$value->purchase_order_number}}</td>
            <td>{{$value->notes}}</td>
            <td>{{$value->date_purchase_order}}</td>
            <td>{{$value->supplier->company_name}}</td>
            <td>{{$value->po_status}}</td>
            <td class="total">{{$value->grand_total_idr}}</td>
            <td class="payment">{{$value->totalpaid ? $value->totalpaid + $value->deposit_reduction: '-' }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    @if(sizeOf($report) > 0)
        <tr>
            <td colspan="5">Total Nilai PO</td>
            <td id="total-po">{{$report->sum('grand_total_idr')}}</td>
        </tr>
        <tr>
            <td colspan="5">Total Pembayaran</td>
            <td id="total-payment">{{$report->sum('totalpaid') + $report->sum('deposit_reduction')}}</td>
        </tr>
        <tr>
            <td colspan="5"> Total Utang Tersisa</td>
            <td id="total-payable">{{$report->sum('grand_total_idr') - ( $report->sum('totalpaid') + $report->sum('deposit_reduction') )}}</td>
        </tr>
    @endif
    </tfoot>
</table>
</body>
</html>