<!DOCTYPE html>
<html>
<body>
<table id="reportCashBalance" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <tr>
        <td>Account</td>
        <td>Debit</td>
        <td>Credit</td>
        <td>Saldo</td>
    </tr>
    <tbody>
    @foreach($metode as $key => $value)
        <tr>
            @php
                $debit = 0;
                foreach($paymentsales as $key2 => $value2)
                {
                    if($value2->details->where('payment_method_id',$value->payment_method_id)->count() > 0)
                    {
                        $debit += $value2->details->where('payment_method_id',$value->payment_method_id)->sum('paid');
                    }
                }
                $debit += $depositoclearance->where('payment_method_id',$value->payment_method_id)->sum('payment_amount');
                $credit = $paymentpurchase->where('payment_method_id',$value->payment_method_id)->sum('total_paid');
                + $expense->where('payment_method_id',$value->payment_method_id)->sum('nominal');
            @endphp
            <td>{{$value->bank->bank_name.' '.$value->payment_method_type->description}}</td>
            <td>{{$debit}}</td>
            <td>{{$credit}}</td>
            <td>{{$debit - $credit}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>