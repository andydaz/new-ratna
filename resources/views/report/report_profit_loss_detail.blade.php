<table id="reportProfitLossTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <tr>
        <td width="50">Laporan Laba Rugi</td>
        <td>Debit</td>
        <td>Kredit</td>
    </tr>
    <tbody>
    <tr>
        <td colspan="3" style="background-color:#aeea00">
            Sales
        </td>
    </tr>
    <tr>
        @php
            $totalsales = $data['sales']->sum('grand_total_idr') - $data['salesretur']->sum('total_price');
        @endphp
        <td>Total Sales</td>
        <td>{{number_format($totalsales)}}</td>
        <td></td>
    </tr>
    </tr>
    @foreach($data['paymenttype'] as $key => $value)
        <tr>
            <td align="right">{{$value->payment_description}}</td>
            <td>{{number_format($data['sales']->where('payment_type_id',$value->payment_type_id)->sum('grand_total_idr'))}}</td>
            <td></td>
        </tr>
    @endforeach
    <tr>
        <td align="right">Sales Retur</td>
        <td></td>
        <td>{{number_format($data['salesretur']->sum('total_price'))}}</td>
    </tr>
    <tr></tr>
    <tr>
        <td colspan="3" style="background-color:#aeea00">
            COGS
        </td>
    </tr>
    <tr>
        @php
            $totalbeginventory = $data['beginventory']->sum('price_buy');
        @endphp
        <td>Persediaan Awal</td>
        <td></td>
        <td>{{number_format($totalbeginventory)}}</td>
    </tr>
    @foreach($data['category'] as $key => $value)
        <tr>
            <td align="right">{{$value->description}}</td>
            <td></td>
            <td>{{number_format($data['beginventory']->where('product_category_id',$value->product_category_id)->sum('price_buy'))}}</td>
        </tr>
    @endforeach
    <tr></tr>
    <tr>
        @php
            $totalpurchase = $data['purchase']->sum('grand_total_idr') - $data['purchaseretur']->sum('total_price');
        @endphp
        <td>Pembelian</td>
        <td></td>
        <td>{{number_format($totalpurchase)}}</td>
    </tr>
    @foreach($data['category'] as $key => $value)
        <tr>
            <td align="right">{{$value->description}}</td>
            <td></td>
            @php
                $total = 0;
                foreach ($data['purchase'] as $key2 => $value2){
                    if($value2->details->where('product_category_id', $value->product_category_id)->count() > 0){
                        $total += $value2->details->where('product_category_id', $value->product_category_id)->sum('sub_total');
                    }
                }
            @endphp
            <td>{{number_format($total)}}</td>
        </tr>
    @endforeach
    <tr>
        <td>Diskon Pembelian</td>
        <td>{{number_format($data['purchase']->sum('discount_transaction_nominal'))}}</td>
        <td></td>
    </tr>
    <tr>
        <td>Retur Pembelian</td>
        <td>{{number_format($data['purchaseretur']->sum('total_price'))}}</td>
        <td></td>
    </tr>
    <tr></tr>
    <tr>
        <td>Total Barang Tersedia</td>
        <td></td>
        <td>{{number_format($totalbeginventory + $totalpurchase)}}</td>
    </tr>
    <tr></tr>
    <tr>
        @php
            $totalendinventory = $data['endinventory']->sum('price_buy');
        @endphp
        <td>Persediaan Akhir</td>
        <td></td>
        <td>{{number_format($totalendinventory)}}</td>
    </tr>
    @foreach($data['category'] as $key => $value)
        <tr>
            <td align="right">{{$value->description}}</td>
            <td></td>
            <td>{{number_format($data['endinventory']->where('product_category_id',$value->product_category_id)->sum('price_buy'))}}</td>
        </tr>
    @endforeach
    <tr></tr>
    <tr>
        <td>Harga Pokok Penjualan</td>
        <td></td>
        <td>{{number_format(($totalbeginventory + $totalpurchase) - $totalendinventory)}}</td>
    </tr>
    <tr></tr>
    <tr>
        <td>Gross Profit</td>
        @php $profit = $totalsales - (($totalbeginventory + $totalpurchase) - $totalendinventory); @endphp
        <td>{{number_format($totalsales - (($totalbeginventory + $totalpurchase) - $totalendinventory))}}</td>
        <td></td>
    </tr>
    <tr>
        <td>% GP</td>
        <td>
            @if($totalsales > 0)
                {{number_format((float)($totalsales - (($totalbeginventory + $totalpurchase) - $totalendinventory)) / $totalsales * 100, 2, '.', '')}}
            @else
                {{0}}
            @endif
        </td>
        <td></td>
    </tr>
    <tr></tr>
    <tr>
        <td colspan="3" style="background-color:#aeea00">
            Expense
        </td>
    </tr>
    <tr>
        @php
            $totalexpense = $data['expense']->sum('nominal');
        @endphp
        <td>Total Expense</td>
        <td></td>
        <td>{{number_format($totalexpense)}}</td>
    </tr>
    @foreach($data['expensecategory'] as $key => $value)
        <tr>
            <td align="right">{{$value->expense_description}}</td>
            <td></td>
            <td>{{number_format($data['expense']->where('expense_category_id',$value->expense_category_id)->sum('nominal'))}}</td>
        </tr>
    @endforeach
    <tr></tr>
    <tr style="background-color: #0f9d58">
        @php
            $netprofit = $profit - $totalexpense;
        @endphp
        @if($netprofit > 0)
            <td>Net Profit Before Tax</td>
            <td>{{number_format($netprofit)}}</td>
            <td></td>
        @else
            <td>Net Loss Before Tax</td>
            <td></td>
            <td>{{number_format(abs($netprofit))}}</td>
        @endif
    </tr>
    <tr>
        @if($netprofit > 0)
            <td>% Net Profit</td>
            <td>
                @if($totalsales > 0)
                    {{$netprofit / $totalsales * 100}}
                @else
                    {{0}}
                @endif
                </td>
            <td></td>
        @else
            <td>% Net Loss</td>
            <td></td>
            <td>
                @if($totalsales > 0)
                    {{number_format((float) abs($netprofit) / $totalsales * 100, 2, '.', '')}}
                @else
                    {{0}}
                @endif
            </td>
        @endif
    </tr>
    </tbody>
</table>