<table id="modalBarangTable">
	<thead>
		<th>Nama</th>
		<th>Qty</th>  
		@if($src !== 'po')
		<th>Qty</th>
		@endif
		<th>check</th>
	</thead>
	<tbody id="modal-barang-data">
		@foreach($barang as $key => $value)
		<tr>
			<td>{{$value->product_code}}</td>
			<td>{{$value->product_name}}</td>
			@if($src !== 'po')
			<td>{{$value->total_qty}}</td>
			@endif
			<td>
				@php
				$check = 0;
				@endphp
				@for($i=0; $i<sizeof($reqbarang); $i++)
				@if($value->product_id == $reqbarang[$i])
				@php $check++; @endphp
				@endif
				@endfor
				@if($check > 0)
				<input type="checkbox" id="{{'myCheckbox'.$key}}" class="filled-in" value="{{$value->product_id}}" name="barang[]" checked/>
				<label for="{{'myCheckbox'.$key}}"></label>
				@else
				<input type="checkbox" id="{{'myCheckbox'.$key}}" class="filled-in" value="{{$value->product_id}}" name="barang[]" />
				<label for="{{'myCheckbox'.$key}}"></label>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<script type="text/javascript">
	$(document).ready(function(){
		modalBarangTable = $('#modalBarangTable').DataTable({
            'sDom':'lftir',
            "destroy": true
          });
	});
</script>
