<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Stock Correction</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="col l12 m12 s12">
                  <div class="card blue darken-1">
                    <div class="card-content white-text">
                      <p>Langkah - Langkah Melakukan  Stock Crrection</p>
                      <br>
                      <p>1. Export List Produk yang ada saat ini dengan menekan tombol Export.</p>
                      <p>2. Buka File Excel yang didownload</p>
                      <p>3. Cocokkan setiap baris pada Excel dengan produk yang ada saat ini</p>
                      <p>4. Apabila barang tertentu akan dihapus, maka ubah "biarkan" pada kolom tindakan menjadi "hapus"</p>
                      <p>5. Setelah selesai, cukup import kembali file excel dan submit</p>
                    </div>
                  </div>
                </div>
                <form id="formSc">
                  <div class="input-field col l12">
                    <a id="export-stock" href="{{url('stockcorrection/export')}}" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4">Export Product List</a>
                  </div>
                  <div class="input-field col l6 margin-top">
                    <input id="file" type="file" name="file" class="f-input">
                    <label>Import Excel</label>
                  </div>
                  <div class="input-field col l6 margin-top">
                    <button id="submit" class="btn-stoko btn-stoko-primary blue darken-1">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    $.ajax({
      url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
      dataType: "script",
    });

    $('#formSc #submit').on('click', function(event){
      event.stopImmediatePropagation()
      event.preventDefault();
      
      var data = new FormData();
      jQuery.each(jQuery('#file')[0].files, function(i, file) {
        data.append('file', file);
      });
      
      $.ajax({
        url: 'stockcorrection/import',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        success: function(data){
          console.log(data);
          toastr.success("Success",{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
        }
      });
    });

  });
</script>