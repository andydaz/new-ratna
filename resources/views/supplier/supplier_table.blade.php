<table id="supplierTable" class="table table-bordered display nowrap dataTable dtr-inline">
	<thead>
		<tr>
			<th>Supplier</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody id="supplierData">
		@foreach($supplier as $row => $value)
		<tr value="{{$value->supplier_id}}">
			<td>{{$value->company_name}}</td>
			<td>
				<a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
				<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
{{$supplier->links()}}
<script type="text/javascript">
	 supplierTable = $('#supplierTable').DataTable({ // This is for home page
      searching: true,
      responsive: true,
      'sDom': 'ti',
      'pagingType': 'full_numbers',
      "language": {
        "infoEmpty": "No records to display",
        "zeroRecords": "No records to display",
        "emptyTable": "No data available in table",
      },
    });
</script>