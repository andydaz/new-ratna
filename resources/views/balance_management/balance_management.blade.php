<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap-select.min.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text"><i class="material-icons">search</i>Cari Tranasaksi Saldo</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row margin-top">
                                <div class="input-field col l3">
                                    <input id="filterPaymentNumber" type="text" class="f-input">
                                    <label>Filter Payment No.</label>
                                </div>
                                <div class="input-field col l3">
                                    <input type="text" id="filterPelanggan" class="f-input">
                                    <label>Filter Supplier</label>
                                </div>
                                <div class="col l12 m12 s12 margin-top">
                                    <div class="table-responsive">
                                        <table id="depositoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                                            <thead>
                                            <tr>
                                                <th>No Pembayaran</th>
                                                <th>Pelanggan</th>
                                                <th>Total</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($data['transaction'] as $key => $value)
                                                <tr value="{{$value->balance_transaction_id}}">
                                                    <td class="notransaction">{{$value->balance_transaction_number}}</td>
                                                    <td>{{$value->customer->first_name.' '.$value->customer->last_name}}</td>
                                                    <td>{{$value->amount}}</td>
                                                    <td>
                                                        @if(Session('roles')->role_id == 1)
                                                            <a class="btn btn-sm btn-raised red delete-modal tooltipped" data-position="bottom" data-tooltip="delete"><i
                                                                        class="material-icons">delete</i></a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Transaksi Saldo</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <form id="formPayment">
                                    <br/>
                                    <div class="input-field col l3">
                                        <label>No Pembayaran</label>
                                        <input id="nopayment" type="text" name="nopayment" class="f-input" placeholder="No Pembayaran">
                                    </div>
                                    <div class="input-field col l3">
                                        <label>Customer</label>
                                        <select id="customer" class="browser-default selectpicker"  data-live-search="true" name="customer">
                                            <option value="0">Select Customer</option>
                                            @foreach($data['customer'] as $key => $value)
                                                <option value="{{$value->customer_id}}">{{$value->first_name.' '.$value->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input-field col l3">
                                        <label>Tgl Transaksi</label>
                                        <input id="tgltransaction" type="text" name="tgltransaction" class="f-input" placeholder="">
                                    </div>
                                    <div class="col l12 m12 s12 margin-top">
                                        <div class="table-responsive">
                                            <table class="stoko-table no-border">
                                                <thead>
                                                <tr>
                                                    <th class="theader">Total Balance</th>
                                                    <th class="theader">Pencairan Dana</th>
                                                    <th class="theader">Payment Method</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr id="no-item"><td colspan="2"><span> No Item Selected</span></td></tr>
                                                <tr id="payment-row" hidden>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="balance" name="balance" type="text" class="f-input" disabled>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="pembayaran" type="text" class="f-input">
                                                            <input id="pembayaran-hidden" name="pembayaran" type="text" class="f-input" hidden>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <select id="paymentmethod" name="paymentmethod" class="f-select">
                                                                @foreach($data['paymentmethod'] as $key => $value)
                                                                    <option value="{{$value->payment_method_id}}">{{$value->bank->bank_name.' '.$value->payment_method_type->description}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col l12 s12 m12 main-button-group margin-top">
                                        <a href="#" class="btn-stoko teal white-text submit">simpan</a>
                                        <a id="clear" href="#" class="btn-stoko orange">batal</a>
                                        <a href="#" class="btn-stoko">print</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Modal Delete Delivery Order -->
<div id="modal-delete-transaction" class="modal">
    <div class="modal-content">
        <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
        <a id="confirm-delete-transaction" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#salesPaymentTable tr').on('click', function(){
            // var id = $(this).attr('value');
            // $.ajax({
            //   type:"GET",
            //   url:"getsalespayment",
            //   data: {id:id},
            //   success:function(response){
            //     $('#nopayment').val(response.payment_sales_number).attr('disabled',true);
            //     $('#customer').val(response.customer_id).attr('disabled',true);
            //     $('#total').val(accounting.formatMoney(response.total_balance,'Rp ',2,'.','.'));
            //     $('#pembayaran').val(accounting.formatMoney(response.total_paid,'Rp ',2,',','.')).attr('disabled',true);
            //     $('#payment-row').removeAttr('hidden');
            //     $('#no-item').attr('hidden',true);
            //   }
            // });
        });

        $(".submit").on('click',function(){
            if($('#payment-row').attr('hidden') == "hidden")
            {
                toastr.warning('Anda Belum Memilih Customer');
            }else{
                hidemainbuttongroup();
                $('#customer, #balance, #pembayaran').removeAttr('disabled');
                var kode = $('#nopayment').val();
                $.ajax({
                    type:"POST",
                    url:"createbalancetransaction",
                    data: $('#formPayment').serialize(),
                    success:function(response){
                        toastr.success('Balance Transaction '+kode+' has been Created!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                    }
                });
            }
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .nav-item.active a').click()
        });

        $('#pembayaran').on('focusout', function(){
            $(this).val(accounting.formatMoney($(this).val(),'Rp ',2,',','.'))
        });
        $('#pembayaran').on('keyup', function(){
            $('#pembayaran-hidden').val($(this).val());
        })
        $('#pembayaran').on('focusin', function(){
            $(this).val("");
        });

        $('#transactionmethod').on('change',function(event){
            event.stopImmediatePropagation();
            if($(this).val() == 1)
            {
                $('#payment-method-div').removeAttr('hidden');
            }else{
                $('#payment-method-div').attr('hidden',true);
            }
        });

        $('#customer').on('change', function(event){
            event.stopImmediatePropagation();
            $.ajax({
                type:"GET",
                url:"getcustomerbalance",
                data:{id:$(this).val()},
                success:function(response){
                    if(response == ""){
                        $('#payment-row').attr('hidden',true);
                        $('#no-item').removeAttr('hidden');
                    }else{
                        $('#balance').val(response);
                        $('#total').val(accounting.formatMoney(response,'Rp ',2,',','.'));
                        $('#payment-row').removeAttr('hidden');
                        $('#no-item').attr('hidden',true);
                    }
                }
            });
        });

        $('.delete-modal').on('click', function (event) {
            event.stopImmediatePropagation();
            var notransaction = $(this).closest('tr').find('.notransaction').html();
            var idtransaction = $(this).closest('tr').attr('value');
            $('#confirm-delete-transaction').attr('value', idtransaction).attr('nomor', notransaction);
            $("#delete-message").html("Yakin ingin menghapus data " + notransaction + " ?")
            $('#modal-delete-transaction').modal('open');
        });

        $('#confirm-delete-transaction').on('click', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-transaction').modal('close');
            var id = $(this).attr('value');
            var notransaction = $(this).attr('nomor');
            $.ajax({
                type: "POST",
                url: "deletebalancetransaction",
                data: {id: id},
                success: function (response) {
                    toastr.success('Transaksi ' + notransaction + ' telah berhasil Dihapus!', {
                        "onShow": setTimeout(function () {
                            $('.side-nav .nav-item.active a').click();
                        }, 2600)
                    });
                }
            })
        });

        //function
        function firstload(){

            $('#tgltransaction').dcalendarpicker({
                format: 'dd-mm-yyyy'
            });

            $('#tgltransaction').val(moment().format('DD-MM-YYYY'));

            $('.selectpicker').selectpicker('render');

            $.ajax({
                type:"GET",
                url:"lastbalancetransactionnumber",
                success:function(response){
                    $('#nopayment').val(response);
                }
            });

            depositoTable = $('#depositoTable').DataTable({ // This is for home page
                searching: true,
                responsive: true,
                'aaSorting':[],
                'sDom':'tip',
                "bPaginate":true,
                "bFilter": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 5,
                "language": {
                    "infoEmpty": "No records to display",
                    "zeroRecords": "No records to display",
                    "emptyTable": "No data available in table",
                },
            });

            $('#filterPaymentNumber').on('keyup', function () { // This is for news page
                salesPaymentTable.column(0).search(this.value).draw();
            });
            $('#filterPelanggan').on('keyup', function () { // This is for news page
                salesPaymentTable.column(1).search(this.value).draw();
            });
        }

        function hidemainbuttongroup(){
            $('.main-button-group a').attr('hidden',true);
        }

    });
</script>