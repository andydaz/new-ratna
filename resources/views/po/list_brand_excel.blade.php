<!DOCTYPE html>
<html>
<head>
</head>
<body>
<table id="reportCashBalance" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <tr>
    <td>Kode</td>
    <td>Brand</td>
  </tr>
  <tbody>
  @foreach($brand as $key => $value)
    <tr>
      <td>{{$value->product_brand_id}}</td>
      <td>{{$value->name}}</td>
    </tr>
  @endforeach
  </tbody>
</table>
</body>
</html>