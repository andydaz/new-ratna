<!DOCTYPE html>
<html>
<head>
</head>
<body>
<table id="reportCashBalance">
  <tr>
    <td>Gabungan ID</td>
    <td>Kategori ID</td>
    <td>Brand ID</td>
    <td>Kategori</td>
    <td>Brand</td>
    <td>Tipe</td>
  </tr>
  <tbody>
  @foreach($type as $key => $value)
    <tr>
      <td>{{$value->brand->category->product_category_id.', '.$value->brand->product_brand_id.', '.$value->product_type_id}}</td>
      <td>{{$value->brand->category->product_category_id}}</td>
      <td>{{$value->brand->product_brand_id}}</td>
      <td>{{$value->brand->category->description}}</td>
      <td>{{$value->brand->name}}</td>
      <td>{{$value->name}}</td>
  @endforeach
  </tbody>
</table>
</body>
</html>