<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet"
      href="css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text"><i class="material-icons">search</i>
            Purchase Order List
          </div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Filter PO</label>
                  <input id="filterPo" type="text" class="f-input" placeholder="Filter PO">
                </div>
                <div class="input-field col l3">
                  <label>Filter tanggal</label>
                  <input id="filterTanggal" type="text" class="f-input" placeholder="Filter Tanggal">
                </div>
                <div class="input-field col l3">
                  <label>Filter Supplier</label>
                  <input id="filterSupplier" type="text" class="f-input"
                         placeholder="Filter Supplier">
                </div>
                <div class="col l12 m12 s12">
                  <div class="table-responsive">
                    <table id="poTable"
                           class="table highlight table-bordered display nowrap dataTable dtr-inline" style="width: 100%">
                      <thead>
                      <tr>
                        <th class="theader">No PO</th>
                        <th class="theader">Tanggal PO</th>
                        <th class="theader">Deskripsi</th>
                        <th class="theader">Supplier</th>
                        <th class="theader">Jenis Pembayaran</th>
                        <th class="theader">Total</th>
                        <th class="theader">Created By</th>
                        <th class="theader">Status</th>
                        <th class="theader">Action</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{--@foreach($data['request'] as $key => $value)--}}
                      {{--<tr value="{{$value->purchase_order_id}}" mode="view">--}}
                      {{--<td class="nopo">{{$value->purchase_order_number}}</td>--}}
                      {{--<td>{{$value->notes}}</td>--}}
                      {{--<td>{{$value->date_purchase_order}}</td>--}}
                      {{--<td>{{$value->company_name}}</td>--}}
                      {{--<td>{{$value->payment_description}}</td>--}}
                      {{--<td class='number'>{{$value->grand_total_idr}}</td>--}}
                      {{--<td>{{$value->full_name}}</td>--}}
                      {{--<td>{{$value->state->description}}</td>--}}
                      {{--<td>--}}
                      {{--@if(Session('roles')->role_id == 1 && $value->state->purchase_order_state_id == 4 )--}}
                      {{--<a class="btn btn-sm btn-raised green release tooltipped" data-position="bottom" data-tooltip="release"><i--}}
                      {{--class="material-icons">lock_open</i></a>--}}
                      {{--<a class="btn btn-sm btn-raised red reject tooltipped" data-position="bottom" data-tooltip="block"><i--}}
                      {{--class="material-icons">block</i></a>--}}
                      {{--@endif--}}
                      {{--@if(Session('roles')->role_id == 2 && $value->state->purchase_order_state_id == 3)--}}
                      {{--<a class="btn btn-sm btn-raised light-blue darken-2 revise"--}}
                      {{--mode="revise" value="{{$value->purchase_order_id}}"><i--}}
                      {{--class="material-icons">edit</i></a>--}}
                      {{--@endif--}}
                      {{--</td>--}}
                      {{--</tr>--}}
                      {{--@endforeach--}}
                      {{--@foreach($data['release'] as $key => $value)--}}
                      {{--<tr value="{{$value->purchase_order_id}}" mode="view">--}}
                      {{--<td class="nopo">{{$value->purchase_order_number}}</td>--}}
                      {{--<td>{{$value->date_purchase_order}}</td>--}}
                      {{--<td>{{$value->notes}}</td>--}}
                      {{--<td>{{$value->company_name}}</td>--}}
                      {{--<td>{{$value->payment_description}}</td>--}}
                      {{--<td class='number'>{{$value->grand_total_idr}}</td>--}}
                      {{--<td>{{$value->full_name}}</td>--}}
                      {{--<td>{{$value->state->description}}</td>--}}
                      {{--<td>--}}
                      {{--@if(Session('roles')->role_id == 1 && $value->state->purchase_order_state_id == 1  && sizeof($value->good_receive) == 0)--}}
                      {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit tooltipped"--}}
                      {{--mode="edit" data-position="bottom" data-tooltip="edit" value="{{$value->purchase_order_id}}"><i--}}
                      {{--class="material-icons">edit</i></a>--}}
                      {{--<a class="btn btn-sm btn-raised red delete-modal tooltipped" data-position="bottom" data-tooltip="delete"><i--}}
                      {{--class="material-icons">delete</i></a>--}}
                      {{--@endif--}}
                      {{--</td>--}}
                      {{--</tr>--}}
                      {{--@endforeach--}}
                      </tbody>
                    </table>
                    </br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    {{-- Admmin bisa lihat po yang dibuat oleh staff dan memberikan approval atas po tersebut--}}
    {{-- <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Staff Created Purchase Order</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Filter PO</label>
                  <input id="filterRequestPo" type="text" class="f-input" placeholder="Filter PO">
                </div>
                <div class="input-field col l3">
                  <label>Filter Supplier</label>
                  <input id="filterrequestSupplier" type="text" class="f-input" placeholder="Filter Supplier">
                </div>
                <div class="col l12 m12 s12">
                  <div class="table-responsive">
                    <table id="poRequestTable" class="table highlight table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th class="theader">No PO</th>
                          <th class="theader">Tanggal PO</th>
                          <th class="theader">Supplier</th>
                          <th class="theader">Jenis Pembayaran</th>
                          <th class="theader">Total</th>
                          <th class="theader">Created By</th>
                          @if(Session('roles')->role_id == 1)
                          <th class="theader">Action</th>
                          @else
                          <th class="theader">Status</th>
                          @endif
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data['request'] as $key => $value)
                        <tr value="{{$value->purchase_order_id}}" mode="view">
                          <td class="nopo">{{$value->purchase_order_number}}</td>
                          <td>{{$value->date_purchase_order}}</td>
                          <td>{{$value->company_name}}</td>
                          <td>{{$value->payment_description}}</td>
                          <td class='number'>{{$value->grand_total_idr}}</td>
                          <td>{{$value->full_name}}</td>
                          @if(Session('roles')->role_id == 1)
                          <td>
                            <a class="btn btn-sm btn-raised green release"><i class="material-icons">lock_open</i></a>
                          </td>
                          @else
                          <td>Waiting For Approval</td>
                          @endif
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    </br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div> --}}

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Create Purchase Order
          </div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <form id="formPo">
                  <div class="row">
                    <div class="input-field col l3 m12 s12">
                      <label>Import</label>
                      <input id="import-po" name="id" type="file" class="f-input">
                      <button id="import-po-btn" type="button" class='btn btn-stoko blue'>Import</button>
                    </div>
                    <div class="input-field col l6 m12 s12">
                      <label>Download Excel dengan List Product Ter Update</label>
                      <a href="downloadexcelforimportpo" target="_blank"><button class='btn btn-stoko orange' type="button" style="margin: 8px 0">Download</button></a>
                    </div>
                  </div>
                  <div class="input-field col l3 m12 s12">
                    <label>No PO</label>
                    <input id="idpurchaseorder" name="id" type="text" class="f-input" hidden
                           disabled>
                    <input id="nopurchaseorder" name="nopo" type="text" class="f-input"
                           placeholder="No PO" disabled>
                  </div>
                  <div class="input-field col l3 m12 s12">
                    <label>Supplier</label>
                    <select id="supplier" name="supplier" class="browser-default selectpicker"
                            data-live-search="true">
                      <option value="0">Select Supplier</option>
                      @foreach($data['supplier'] as $key => $value)
                        <option value="{{$value->supplier_id}}">{{$value->company_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3 m12 s12">
                    <label>Pembayaran</label>
                    <select id="pembayaran" class="selectpicker browser-default"
                            data-live-search="true" name="pembayaran">
                      @foreach($data['payment_term'] as $key => $value)
                        <option value="{{$value->payment_type_id}}">{{$value->payment_description}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3 m12 s12">
                    <label>Jatuh Tempo</label>
                    <input id="terms" name="terms" type="text" class="f-input" placeholder="Terms">
                  </div>
                  <div class="input-field col l3 m12 s12">
                    <label>Tgl PO</label>
                    <input id="tglpo" name="tglpo" type="text" class="f-input datepicker"
                           placeholder="Tgl PO" data-maxdate="today">
                  </div>
                  <div class="input-field col l3 m12 s12">
                    <label>Deskripsi</label>
                    <input id="notes" name="notes" type="text" class="f-input" placeholder="Notes">
                  </div>
                  <div class="col l12 m12 s12">
                    <button id="tambah-barang" type="button"
                            class="btn-stoko btn-stoko-primary modal-trigger"
                            style="float:left; margin:0px 8px 8px 0px;">Tambah Baris
                    </button>
                    <button id="show-diskon" type="button" class="btn-stoko btn-stoko-primary"
                            style="margin:0px 8px 8px 0px; float:left">Tampilkan Diskon 2
                    </button>
                    <button id="hide-diskon" type="button" class="btn-stoko btn-stoko-primary"
                            style="margin:0px 8px 8px 0px; float:left; display:none">Sembunyikan Diskon 2
                    </button>
                    <div class="table-responsive" style="overflow: visible;">
                      <br>
                      <table class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader" style="width:25px">No</th>
                          <th class="theader" style="width:150px">Kategori Barang</th>
                          <th class="theader" style="width:200px">Brand</th>
                          <th class="theader" style="width:200px">Tipe Barang</th>
                          <th class="theader">Harga Beli</th>
                          <th class="theader" style="width:100px">Diskon Percent</th>
                          <th class="theader diskon2header" style="width:100px" hidden>
                            Diskon Percent 2
                          </th>
                          <th class="theader">Diskon Nominal</th>
                          <th class="theader">Subtotal</th>
                          <th class="theader">Action</th>
                        </tr>
                        </thead>
                        <tbody id="barang-data">
                        <tr class="barang-row" row-number="1">
                          <td class="row-number">
                            1
                          </td>
                          <td>
                            <div class="input-field">
                              <select id="kategori-1"
                                      class="browser-default selectpicker kategori"
                                      name="kategori[]">
                                <option value="">Pilih Kategori</option>
                                @foreach($data['category'] as $key => $value)
                                  <option value="{{$value->product_category_id}}">{{$value->description}}</option>
                                @endforeach
                              </select>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <select id="brand-1"
                                      class="browser-default selectpicker brand"
                                      data-live-search="true"
                                      name="brand[]">

                              </select>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <select id="type-1"
                                      class="browser-default selectpicker type"
                                      data-live-search="true" name="type[]"
                                      data-none-results-text="Add {0}">
                              </select>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="harga-beli-1" type="text"
                                     class="f-input hargabeli">
                              <input id="harga-beli-hidden-1" type="text"
                                     class="f-input hargabelihidden" name="hargabeli[]" hidden>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="percent-first-1" type="text"
                                     class="f-input percentfirst" name="percentfirst[]"
                                     value="0" style="display: inline-block; width:60%">
                              <input class="f-input percent-sym"
                                     style="display: inline-block; width:35%;" value="%"
                                     name="rate[]" disabled>
                            </div>
                          </td>
                          <td class="diskon2-data" hidden>
                            <div class="input-field">
                              <input id="percent-second-1" type="text"
                                     class="f-input percentsecond" name="percentsecond[]"
                                     value="0" style="display: inline-block; width:60%">
                              <input class="f-input percent-sym"
                                     style="display: inline-block; width:35%;" value="%"
                                     name="rate[]" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="diskon-1" type="text" class="f-input diskon"
                                     name="diskon[]" value="0">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="subtotal-1" type="text" class="f-input subtotal"
                                     name="subtotal[]" disabled value="0">
                            </div>
                          </td>
                          <td>
                            <a class="btn btn-sm white grey-text delete-detail tooltipped" data-position="bottom" data-tooltip="delete row"><i class="material-icons">delete</i></a>
                          </td>
                          {{--<td>--}}
                          {{--<div class="input-field">--}}
                          {{--<input id="harga-jual-1" type="text"--}}
                          {{--class="f-input hargajual" name="hargajual[]">--}}
                          {{--</div>--}}
                          {{--</td>--}}
                          {{--<td>--}}
                          {{--<div class="input-field">--}}
                          {{--<input id="rate-1" type="text" class="f-input rate"--}}
                          {{--style="display: inline-block; width:60%"--}}
                          {{--name="rate[]" disabled>--}}
                          {{--<input class="f-input"--}}
                          {{--style="display: inline-block; width:35%;" value="%"--}}
                          {{--name="rate[]" disabled>--}}
                          {{--</div>--}}
                          {{--</td>--}}
                        </tr>
                        </tbody>
                      </table>
                      <br>
                      <div class="input-field col l6">
                        <label>Diskon Transaksi Nominal</label>
                        <input id="diskon-transaksi-nominal" type="text"
                               class="f-input diskon-transaksi-nominal"
                               name="diskontransaksinominal" value="0">
                      </div>
                      <div class="input-field col l6">
                        <label>Diskon Transaksi Percent</label>
                        <input id="diskon-transaksi-percent" type="text"
                               class="f-input diskon-transaksi-percent"
                               name="diskontransaksipercent" value="0">
                      </div>
                      <div class="input-field col l6">
                        <label for="">Grand Total Transaksi</label>
                        <input type="text" name="totaltransaksi" id="total-transaksi" class="f-input" value="0">
                      </div>
                    </div>
                  </div>

                  <div class="col l4 main-button-group margin-top">
                    <a id="submit-po" href="#!" mode="save"
                       class="waves-effect btn-stoko teal white-text">simpan</a>
                    <a id="edit-po" href="#!" mode="edit"
                       class="waves-effect btn-stoko teal white-text" hidden>edit</a>
                    <a id="revise-po" href="#!" mode="revise"
                       class="waves-effect btn-stoko teal white-text" hidden>Revise</a>
                    <a id="clear" class="waves-effect btn-stoko orange white-text">Batal</a>
                    <a id="cetak-po" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak PO</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="modal-tambah-discount" class="modal">
  <form id="form-discount">
    <div id="append-disc" class="modal-content">
      <h4>Tambah Discount</h4>
      <div class="row">
        <div class="col l3 m12 s12">
          <p class="disc-type" style="font-size: 18px; margin-top: 20px">Discount Type</p>
        </div>
        <div class="input-field  col l2">
          <input name="type" type="radio" id="typeNominal" value="1"/>
          <label for="typeNominal">Nominal</label>
        </div>
        <div class="input-field  col l2">
          <input name="type" type="radio" id="typePercent" value="2"/>
          <label for="typePercent">Percent</label>
        </div>
      </div>
      <div class="row disc-row">
        <tr>
          <div class="col l3 m12 s12">
            <p class="disc-no" style="font-size: 18px; margin-top: 20px">Discount 1</p>
          </div>
          <div class="input-field col l3 m12 s12">
            <label>Discount</label>
            <input id="discount-1" name="discount[]" type="text" class="f-input discount"
                   placeholder="discount">
          </div>
          <div class="input-field col l3 m12 s12">
            <label>Discount Period</label>
            <input id="period-1" name="period[]" type="text" class="f-input period" placeholder="Period">
          </div>
          <div class="input-field col l3 m12 s12">
            <label></label>
            <a href="#!" class="add-disc btn-flat green white-text no-border-radius">Add</a>
            <a href="#!" class="remove-disc btn-flat red white-text no-border-radius"
               style="display: none;">Remove</a>
          </div>
        </tr>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="waves-effect btn-flat red white-text no-border-radius submit-disc">OK</a>
      <a href="#!"
         class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
    </div>
  </form>
</div>
<div id="modal-delete-po" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-po" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>
<div id="modal-release-po" class="modal">
  <div class="modal-content">
    <h5 id="release-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-release-po" class="modal-action modal-close waves-effect btn-stoko green white-text">Release</a>
  </div>
</div>
<div id="modal-reject-po" class="modal">
  <div class="modal-content">
    <h5 id="reject-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-reject-po" class="modal-action modal-close waves-effect btn-stoko red white-text">Reject</a>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        var $orginal = $('.barang-row:first');
        //get the barang row
        // var $original = $('.barang-row:first');

        $('.bootstrap-select>.dropdown-toggle').on('click', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            $(this).closest('.bootstrap-select').addClass("open");
            $(this).attr('aria-expanded', true);
        })

        $(document).on('click', '.type li.no-results', function (event) {
            event.stopImmediatePropagation();
            var elementid = $(this).closest('.input-field').find('select').attr('id');
            var new_option = $(this).text().split('"')[1];
            var brand = $(this).closest('tr').find('select.brand').val();
            // $("#"+elementid).append('<option value="new-'+new_option+'">'+ new_option +'</option>').selectpicker('refresh');
            //You can also call AJAX here to add the value in DB
            $.ajax({
                method: "post",
                url: "createTipeBarangFromPo",
                data: {name: new_option, brand: brand},
                success: function (response) {
                    $("#" + elementid).html('');
                    for (var i = 0; i < response.length; i++) {
                        $("#" + elementid).append('<option value="' + response[i].product_type_id + '">' + response[i].name + '</option>');
                    }
                    $("#" + elementid).selectpicker('refresh');
                }
            })
        });

        $('#poTable').on('click','tr,  .edit, .revise', function (event) {
            event.stopImmediatePropagation();
            console.log($(event.currentTarget).attr('mode'));
            var mode = $(event.currentTarget).attr('mode');
            var id = $(this).attr('value');
            //remove all barang row
            $('.barang-row').remove();
            $.ajax({
                type: "GET",
                url: "getpurchaseorder",
                data: {id: id},
                success: function (response) {
                    console.log(response);
                    $('#idpurchaseorder').val(response.po[0].purchase_order_id).attr('disabled', true);
                    $('#nopurchaseorder').val(response.po[0].purchase_order_number).attr('disabled', true);
                    $('#tglpo').val(moment(response.po[0].date_purchase_order, 'YYYY-MM-DD').format('DD-MM-YYYY')).attr('disabled', true);
                    $('#supplier').val(response.po[0].supplier_id).attr('disabled', true);
                    $('#pembayaran').val(response.po[0].payment_type_id).attr('disabled', true);
                    $('#terms').val(response.po[0].payment_term).attr('disabled', true);
                    $('#diskon-transaksi-nominal').val(accounting.formatMoney(response.po[0].discount_transaction_nominal,'',0,',','.')).attr('disabled', true);
                    $('#diskon-transaksi-percent').val(response.po[0].discount_transaction_percentage).attr('disabled', true);
                    $('#total-transaksi').val(response.po[0].grand_total_idr).attr('disabled', true);
                    $('#notes').val(response.po[0].notes).attr('disabled', true);
                    $('#tambah-barang').attr('disabled', true).css('cursor', 'not-allowed');
                    $.each(response.po, function (i, v) {
                        var newKategori = "kategori-" + (i + 1);
                        var newBrand = "brand-" + (i + 1);
                        var newType = "type-" + (i + 1);
                        var newHargaJual = "harga-jual-" + (i + 1);
                        var newDiskon = "diskon-" + (i + 1);
                        var newPercentFirst = "percent-first-" + (i + 1);
                        var newPercentSecond = "percent-second-" + (i + 1);
                        var newHargaBeli = "harga-beli-" + (i + 1);
                        var newHargaBeliHidden = "harga-beli-hidden" + (i + 1);
                        var newSubtotal = "subtotal-" + (i + 1);

                        var $cloned = $orginal.clone().addClass('cloned');
                        $cloned.find("#kategori-1").attr('id', newKategori);
                        $cloned.find("#brand-1").attr('id', newBrand);
                        $cloned.find("#type-1").attr('id', newType);
                        $cloned.find("#harga-jual-1").attr('id', newHargaJual);
                        $cloned.find("#diskon-1").attr('id', newDiskon);
                        $cloned.find("#percent-first-1").attr('id', newPercentFirst);
                        $cloned.find("#percent-second-1").attr('id', newPercentSecond);
                        $cloned.find("#harga-beli-1").attr('id', newHargaBeli);
                        $cloned.find("#harga-beli-hidden-1").attr('id', newHargaBeliHidden);
                        $cloned.find("#subtotal-1").attr('id', newSubtotal);
                        $cloned.appendTo('#barang-data');
                        //if you use bootstrap select you need this code so you can change the cloned select value
                        $cloned.find('.bootstrap-select').replaceWith(function () {
                            return $('select', this);
                        });
                        $cloned.find('select').selectpicker();

                        $('#kategori-' + (i + 1)).val(v.product_category_id).attr('disabled', true);
                        $('#brand-' + (i + 1)).html('').append('<option value="' + v.product_brand_id + ' selected">' + v.b_name + '</option>').attr('disabled', true);
                        $('#type-' + (i + 1)).html('').append('<option value="' + v.product_type_id + ' selected">' + v.t_name + '</option>').attr('disabled', true);
                        $('#harga-jual-' + (i + 1)).val(v.price_sale).attr('disabled', true);
                        $('#harga-beli-' + (i + 1)).val(accounting.formatMoney(v.price_buy,'',0,',','.')).attr('disabled', true);
                        $('#harga-beli-hidden' + (i + 1)).val(v.price_buy);
                        $('#diskon-' + (i + 1)).val(accounting.formatMoney(v.discount_nominal,'',0,',','.')).attr('disabled', true);
                        $('#percent-first-' + (i + 1)).val(v.discount_percentage).attr('disabled', true);
                        $('#percent-second-' + (i + 1)).val(v.discount_percentage_2).attr('disabled', true)
                        $('#subtotal-' + (i + 1)).val(accounting.formatMoney(v.sub_total,'',0,',','.')).attr('disabled', true)

                    });
                    $('#no-item').attr('hidden', true);
                    $('.selectpicker').selectpicker('refresh');
                    showDiskon2();
                },
                complete: function () {
                    calculatetotalpo();
                    $('#submit-po').attr('hidden', true);
                    $('#cetak-po').removeAttr('hidden').attr('href', "downloadpurchaseorder/" + id);
                    if (mode == "edit") {
                        $('#supplier, #pembayaran, #tglpo, #tambah-barang, #notes, #tambah-discount, .hargabeli, .diskon, .percentfirst, .percentsecond, #diskon-transaksi-nominal, #diskon-transaksi-percent').removeAttr('disabled');
                        $('#pembayaran').trigger('change');
                        $('#edit-po').removeAttr('hidden');
                        $('#submit-po').attr('hidden', true);
                        $('.selectpicker').selectpicker('refresh');
                    } else if (mode == "revise") {
                        $('#supplier, #pembayaran, #tglpo, #tambah-barang, #notes, #tambah-discount, .hargabeli, .diskon, .percentfirst, .percentsecond, #diskon-transaksi-nominal, #diskon-transaksi-percent').removeAttr('disabled');
                        $('#submit-po').attr('hidden', true);
                        $('#revise-po').removeAttr('hidden');
                    }else{
                        $('.delete-detail').attr('disabled',true);
                        disablediskonbtn();
                    }
                }
            });
        })

        $('#poTable').on('click','.release', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var idpo = $(this).closest('tr').attr('value');
            var nopo = $(this).closest('tr').find('.nopo').html();

            $('#confirm-release-po').attr('value', idpo).attr('nomor', nopo);
            $("#release-message").html("Yakin ingin release " + nopo + " ?");
            $('#modal-release-po').modal('open');
        });

        $('#confirm-release-po').on('click', function (event){
            var id = $(this).attr('value');
            releasepo(id);
        });

        $('#poTable').on('click','.reject', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var idpo = $(this).closest('tr').attr('value');
            var nopo = $(this).closest('tr').find('.nopo').html();

            $('#confirm-reject-po').attr('value', idpo).attr('nomor', nopo);
            $("#reject-message").html("Yakin ingin reject " + nopo + " ?");
            $('#modal-reject-po').modal('open');
        });

        $('#confirm-reject-po').on('click', function (event){
            var id = $(this).attr('value');
            rejectpo(id);
        });

        $('#submit-po, #edit-po, #revise-po').click(function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var kode = $('#nopurchaseorder').val();
            var validdiskon = [];
            var validhargabeli = [];
            var url = "";

            if ($(event.currentTarget).attr('mode') == 'save') {
                var url = "createpurchaseorder";
                var successmessage = 'Purchase Order ' + kode + ' telah berhasil dibuat!';
            } else if ($(event.currentTarget).attr('mode') == 'revise') {
                var url = "revisepurchaseorder";
                var successmessage = 'Purchase Order ' + kode + ' telah berhasil direvisi!';
            } else {
                var url = "updatepurchaseorder";
                var successmessage = 'Purchase Order ' + kode + ' telah berhasil diubah!';
            }

            $('.barang-row').each(function () {
                validdiskon.push(ceknumber($(this).find('.diskon').val()));
                validhargabeli.push(ceknumber($(this).find('.hargabelihidden').val()));
            });

            var arraytipe = $('select.type').get().map(function(el) { return el.value });
            var arraybrand = $('select.brand').get().map(function(el) { return el.value });

            if ($('.barang-row:first').attr('hidden') == 'hidden' && $('.barang-row:first').attr('deleted') != 'true') {
                toastr.warning('Anda Belum Memilih Barang!');
            } else if(arraytipe.indexOf('') != -1 || arraybrand.indexOf('') != -1){
                toastr.warning('Isi brand atau tipe Barang dengan benar!');
            } else if ($('#supplier').val() == 0) {
                toastr.warning('Anda Belum Memilih Supplier!');
            } else if (validhargabeli.indexOf(false) > -1) {
                toastr.warning('Masukkan Harga Beli dengan Benar!');
            } else if ($('#pembayaran').val() == 2 && ceknumber($('#terms').val()) == false) {
                toastr.warning('Jatuh Tempo harus angka');
            }else if ($('#notes').val() == '') {
                toastr.warning('Notes Harus di isi');
            }else { // validasi sukses
                hidemainbuttongroup();
                $('.kategori, .brand, .type, .hargabeli, #idpurchaseorder, #pembayaran, #nopurchaseorder, #total-transaksi, #terms, #diskon-transaksi, .subtotal').removeAttr('disabled');
                unformatNumber();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $(this).closest('#formPo').serialize(),
                    success: function (response) {
                        toastr.success(successmessage, {
                            "onShow": setTimeout(function () {
                                $('.side-nav .nav-item.active a').click();
                            }, 2600)
                        });
                    }
                })
            }
        });

        $('body').on('change', '#pembayaran', function (event) {
            event.stopImmediatePropagation();
            console.log('test');
            if ($(this).val() == 1) {
                $('#terms').attr('disabled', true);
                $('#terms').val(1);
            } else {
                $('#terms').removeAttr('disabled');
            }
        })

        $('body').on('change', '.kategori', function (event) {
            event.stopImmediatePropagation();
            var id = $(this).val();
            var brand = $(this).closest('.barang-row').find('select.brand');
            $.ajax({
                method: "get",
                url: "getBrandOnKategori",
                data: {id: id},
                success: function (response) {
                    console.log(response);
                    brand.html('');
                    brand.append('<option value="">Pilih Brand</option>')
                    for (var i = 0; i < response.length; i++) {
                        brand.append('<option value="' + response[i].product_brand_id + '">' + response[i].name + '</option>')
                    }
                    brand.selectpicker('refresh');
                }
            })
        });

        $('body').on('change', '.brand', function (event) {
            event.stopImmediatePropagation();
            var id = $(this).val();
            var type = $(this).closest('.barang-row').find('select.type');
            $.ajax({
                method: "get",
                url: "getTipeOnBrand",
                data: {id: id},
                success: function (response) {
                    console.log(response);
                    type.html('');
                    type.append('<option value="">Pilih Tipe</option>')
                    for (var i = 0; i < response.length; i++) {
                        type.append('<option value="' + response[i].product_type_id + '">' + response[i].name + '</option>')
                    }
                    type.selectpicker('refresh');
                }
            })
        });


        $('#clear').on('click', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .nav-item.active a').click();
        });

        $('#tambah-barang').on('click', function (event) {
            event.stopImmediatePropagation();
            var $initNumber = $(".barang-row").length;
            $initNumber++;
            var $newKategori = "kategori-" + $initNumber;
            var $newBrand = "brand-" + $initNumber;
            var $newType = "type-" + $initNumber;
            var $newHargaJual = "harga-jual-" + $initNumber;
            var $newDiskon = "diskon-" + $initNumber;
            var $newPercent = "percent-" + $initNumber;
            var $newHargaBeli = "harga-beli-" + $initNumber;
            var $newRate = "rate-" + $initNumber;

            if ($initNumber <= 10) {
                var $orginal = $('.barang-row:first');
                var $cloned = $orginal.clone().addClass('cloned');
                $cloned.attr('row-number', $initNumber);
                $cloned.find('.row-number').html($initNumber);
                $cloned.find("#kategori-1").attr('id', $newKategori);
                $cloned.find("#brand-1").attr('id', $newBrand);
                $cloned.find("#type-1").attr('id', $newType);
                $cloned.find("#harga-jual-1").attr('id', $newHargaJual);
                $cloned.find("#diskon-1").attr('id', $newDiskon);
                $cloned.find("#percent-1").attr('id', $newPercent);
                $cloned.find("#harga-beli-1").attr('id', $newHargaBeli);
                $cloned.appendTo('#barang-data');
                //if you use bootstrap select you need this code so you can change the cloned select value
                $cloned.find('.bootstrap-select').replaceWith(function () {
                    return $('select', this);
                });
                $cloned.find('select').selectpicker();
                //end of comment
                $('.btn-remove-main-product:last').css('visibility', 'visible');
            }
        });

        $('body').on('click', '.delete-detail', function (event) {
            event.stopImmediatePropagation();
            if ($('.barang-row').length > 1) {
                $(this).closest('tr').remove();
            } else {
                $(this).closest('tr').find('.nama').val("").removeAttr('disabled');
                $(this).closest('tr').find('.id').val("").removeAttr('disabled');
            }
        });

        $('#poTable').on('click', '.delete-modal', function (event) {
            event.stopImmediatePropagation();
            var nopo = $(this).closest('tr').find('.nopo').html();
            var idpo = $(this).closest('tr').attr('value');
            $('#confirm-delete-po').attr('value', idpo).attr('nomor', nopo);
            $("#delete-message").html("Yakin ingin menghapus data " + nopo + " ?")
            $('#modal-delete-po').modal('open');
        });

        $('#confirm-delete-po').on('click', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-po').modal('close');
            var id = $(this).attr('value');
            var nopo = $(this).attr('nomor');
            $.ajax({
                type: "POST",
                url: "deletepurchaseorder",
                data: {id: id},
                success: function (response) {
                    toastr.success('Purchase Order ' + nopo + ' telah berhasil Dihapus!', {
                        "onShow": setTimeout(function () {
                            $('.side-nav .nav-item.active a').click();
                        }, 2600)
                    });
                }
            })
        });

        // $('.percent').on('keyup', function (event) {
        //     percent = $(this).val();
        //     hargabeli = $(this).closest('tr').find('.hargabeli').val();
        //     nominaldiskon = percent / 100 * hargabeli;
        //     $(this).closest('tr').find('.diskon').val(nominaldiskon);
        //     $('#formPo .hargajual').trigger('keyup');
        // });

        $('#formPo').on('keyup', '.percentfirst, .percentsecond', function (event) {
            event.stopImmediatePropagation();
            calculateDiskon($(this));
            $('.diskon-transaksi-nominal').trigger('keyup');
            calculatetotalpo();
        });

        $('#formPo').on('keyup', '.hargabeli', function (event) {
            event.stopImmediatePropagation();
            $(this).closest('tr').find('.hargabelihidden').val(accounting.unformat($(this).val()));
            calculateDiskon($(this));
            calculatetotalpo();
        });

        $('#formPo').on('focusout', '.hargabeli', function (event) {
            event.stopImmediatePropagation();
            $(this).val(accounting.formatMoney($(this).val(),'',0,',','.'));
        });

        $('#formPo').on('focusin', '.hargabeli', function (event) {
            event.stopImmediatePropagation();
            $(this).val("");
            $(this).closest('tr').find('.hargabelihidden').val(0);
        });

        $('#formPo').on('keyup', '.diskon', function (event) {
            event.stopImmediatePropagation();
            $(this).val(accounting.formatMoney($(this).val(),'',0,',','.'));
            var nominal = accounting.unformat($(this).val());
            var hargabeli = $(this).closest('tr').find('.hargabelihidden').val();
            $(this).closest('tr').find('.percentsecond').val(0);
            $(this).closest('tr').find('.percentfirst').val(nominal / hargabeli * 100);
            $(this).closest('tr').find('.subtotal').val(accounting.formatMoney(hargabeli - nominal,'',0,',','.'));
            calculatetotalpo();
        });

        $('.diskon-transaksi-percent').on('keyup', function (event) {
            percent = $(this).val();
            totalhargabeli = 0;

            $('.subtotal').each(function () {
                totalhargabeli += parseFloat(accounting.unformat($(this).val()));
            });

            nominaldiskon = percent / 100 * totalhargabeli;
            $('#diskon-transaksi-nominal').val(accounting.formatMoney(parseFloat(nominaldiskon),'',0,',','.'));
            calculatetotalpo();
        });

        $('.diskon-transaksi-nominal').on('keyup', function (event) {
            nominal = $(this).val();
            total = 0;

            $('.subtotal').each(function () {
                total += parseFloat($(this).val());
            });

            percentdiskon = nominal / total * 100;
            $('#diskon-transaksi-percent').val(parseFloat(percentdiskon).toFixed(2));
            calculatetotalpo();
        });

        $('#show-diskon').on('click', function (event) {
            showDiskon2();
        });

        $('#hide-diskon').on('click', function (event) {
            hideDiskon2();

        });

        $('#formPo').on('click','.delete-detail',function(event){
            event.stopImmediatePropagation();
            if($('.barang-row').length > 1)
            {
                $(this).closest('tr').remove();
            }else{
                $(this).closest('tr').find("input[type='text']:not(.percentfirst, .percentsecond, .diskon)").val("");
                $(this).closest('tr').find('.kategori, .brand, .type').val(0);
                $('.selectpicker').selectpicker('refresh');
            }
        });

        $('.delete-modal').on('click', function(event){
            event.stopImmediatePropagation();
            var nopo = $(this).closest('tr').find('.nopo').html();
            var idpo = $(this).closest('tr').attr('value');
            $('#confirm-delete-po').attr('value',idpo).attr('nomor',nopo);
            $("#delete-message").html("Yakin ingin menghapus data "+nopo+" ?");
            $('#modal-delete-po').modal('open');
        });

        $('#confirm-delete-po').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-po').modal('close');
            var id = $(this).attr('value');
            var nopo = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletepurchaseorder",
                data:{id:id},
                success:function(response){
                    toastr.success('Purchase Order dengan '+nopo+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                }
            })
        });

        $('#import-po-btn').click(function(event){
            event.preventDefault();
            var formData = new FormData();
            jQuery.each(jQuery('#import-po')[0].files, function(i, v) {
                formData.append('importpo',v);
            });

            $.ajax({
                type:"POST",
                url:"importPo",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success:function(response){
                    if(response.error_row)
                    {
                        toastr.warning('Terdapat Kesalahan Input pada baris '+response.error_row, '', {"showDuration": "800","timeOut": "5000"});
                    }
                    else
                        {
                        toastr.success('Berhasil');
                        $('.barang-row.cloned').remove();
                        $.each(response.data, function(i,v){
                            if(i != 0)
                            {
                                var $original = $('.barang-row:first');
                                var row = parseInt($('.barang-row:last').attr('row-number'))+1;
                                var $cloned = $original.clone().addClass('cloned');
                                $cloned.attr('row-number', row);
                                $cloned.find('.row-number').html(row);
                                $cloned.find("#kategori-1").attr('id', 'kategori-'+row);
                                $cloned.find("#brand-1").attr('id', 'brand-'+row);
                                $cloned.find("#type-1").attr('id', 'type-'+row);
                                $cloned.find("#harga-jual-1").attr('id', 'harga-jual-'+row);
                                $cloned.find("#diskon-1").attr('id', 'diskon-'+row);
                                $cloned.find("#percent-first-1").attr('id', 'percent-first-'+row);
                                $cloned.find("#percent-second-1").attr('id', 'percent-second-'+row);
                                $cloned.find("#harga-beli-1").attr('id', 'harga-beli-'+row);
                                $cloned.find("#harga-beli-hidden-1").attr('id', 'harga-beli-hidden-'+row);
                                $cloned.find("#subtotal-1").attr('id', 'subtotal-'+row);
                                $cloned.appendTo('#barang-data');
                                //if you use bootstrap select you need this code so you can change the cloned select value
                                $cloned.find('.bootstrap-select').replaceWith(function () {
                                    return $('select', this);
                                });
                                $cloned.find('select').selectpicker();

                                $('#kategori-' + row).val(v.category.product_category_id).attr('disabled', true);
                                $('#brand-' + row).html('').append('<option value="' + v.brand.product_brand_id + '" selected>' + v.brand.name+ '</option>').attr('disabled', true);
                                $('#type-' + row).html('').append('<option value="' + v.type.product_type_id + '" selected>' + v.type.name + '</option>').attr('disabled', true);
                                $('#percent-first-' + row).val(v.diskon == null ? 0 : v.diskon);
                                $('#harga-beli-hidden-' + row).val(v.price_buy);
                                $('#harga-beli-' + row).val(accounting.formatMoney(v.price_buy,'',0,',','.'));
                            }else{
                                var row = 1;
                                $('#kategori-' + row).val(v.category.product_category_id).attr('disabled', true);
                                $('#brand-' + row).html('').append('<option value="' + v.brand.product_brand_id + '" selected>' + v.brand.name+ '</option>').attr('disabled', true);
                                $('#type-' + row).html('').append('<option value="' + v.type.product_type_id + '" selected">' + v.type.name + '</option>').attr('disabled', true);
                                $('#percent-first-' + row).val(v.diskon == null ? 0 : v.diskon);
                                $('#harga-beli-hidden-' + row).val(v.price_buy);
                                $('#harga-beli-' + row).val(accounting.formatMoney(v.price_buy,'',0,',','.'));
                            }
                        });
                    }
                },
                complete:function(){
                    $('.selectpicker').selectpicker('refresh');
                    calculateAfterImport();
                }
            });
        });

        function firstload() {

            $('#modalBarangTable').attr('datatable', false);

            $('#terms').val(0).attr('disabled', true);

            $('.selectpicker').selectpicker('render');

            $('#pembayaran').trigger('change');

            $('#tglpo').dcalendarpicker({
                format: 'dd-mm-yyyy'
            });
            $('#tglpo').val(moment().format('DD-MM-YYYY'));

            $('.number').each(function () {
                console.log($(this).html());
                $(this).html(accounting.formatMoney($(this).html(), '', 2, ',', '.'));
            });

            $.ajax({
                type: "GET",
                url: "lastpurchaseordernumber",
                success: function (response) {
                    $('#nopurchaseorder').val(response);
                }
            });

            poTable = $('#poTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                'sDom': 'tip',
                "pageLength": 5,
                ajax: {
                    url : 'getpurchaseordertable',
                    data : function (d){
                        d.ponumber = $('#filterPo').val();
                        d.supplier = $('#filterSupplier').val();
                        d.date = $('#filterTanggal').val();
                    }
                },
                rowId : 'purchase_order_id',
                columns: [
                    { data: 'purchase_order_number', name: 'purchase_order_number', class:'nopo'},
                    { data: 'date_purchase_order', name: 'date_purchase_order'},
                    { data: 'notes', name: 'notes'},
                    { data: 'company_name', name: 'company_name'},
                    { data: 'payment_description', name: 'payment_description'},
                    { data: 'grand_total_idr', name: 'grand_total_idr'},
                    { data: 'full_name', name: 'full_name'},
                    { data: 'state.description', name: 'state'},
                    { data: 'action', name: 'action'},
                ],
            });

            $('#filterPo').on('keyup', function () { // This is for news page
                poTable.draw();
            });
            $('#filterSupplier').on('keyup', function () { // This is for news page
                poTable.draw();;
            });
            $('#filterTanggal').on('keyup', function () { // This is for news page
                poTable.draw();;
            });
        }

        function showDiskon2() {
            $('#show-diskon').css('display', 'none');
            $('#hide-diskon').css('display', 'inline-block');
            $('.diskon2header, .diskon2-data').removeAttr('hidden');
        }

        function hideDiskon2() {
            $('#hide-diskon').css('display', 'none');
            $('#show-diskon').css('display', 'inline-block');
            $('.diskon2-data .percentsecond').val(0);
            $('.diskon2header, .diskon2-data').attr('hidden', true);

            $('.barang-row').each(function (i,v){
                calculateDiskon($(this).find('.percentfirst'));
            });
        }

        function disablediskonbtn(){
            $('#hide-diskon').css('display', 'none');
            $('#show-diskon').css('display', 'none');
        }

        function enablediskonbtn(){
            $('#hide-diskon').css('display', 'inline-block');
            $('#show-diskon').css('display', 'inline-block');
        }

        function calculateAfterImport(){
            var total = 0;
            $('.barang-row').each(function(i,v){
                calculateDiskon($(this).find('td'));
                var subtotal = $(this).find('.subtotal').val();
                var unformatsubtotal = accounting.unformat(subtotal);
                total += parseInt(unformatsubtotal);
            });

            var diskon_transaksi = accounting.unformat($('#diskon-transaksi-nominal').val(),'',0,',','.');
            $('#total-transaksi').val(accounting.formatMoney(parseInt(total) - parseInt(diskon_transaksi), '',0, '.', ',')).attr('disabled',true);
        }

        function calculateDiskon(elm) {
            var hargabeli = elm.closest('tr').find('.hargabelihidden').val();
            var percentfirst = elm.closest('tr').find('.percentfirst').val();
            var percentsecond = elm.closest('tr').find('.percentsecond').val();

            var diskonfirst = percentfirst / 100 * hargabeli;
            var hargafirst = hargabeli - diskonfirst;
            var diskonsecond = percentsecond / 100 * hargafirst;
            var diskonfinal = diskonfirst + diskonsecond;

            elm.closest('tr').find('.diskon').val(accounting.formatMoney(diskonfinal,'',0,',','.'));
            elm.closest('tr').find('.subtotal').val(accounting.formatMoney(hargabeli - diskonfinal,'',0,',','.'));
        }

        function calculatetotalpo() {
            var total = 0;
            $('.barang-row').each(function (i, v) {
                var subtotal = $(this).find('.subtotal').val();
                var unformatsubtotal = accounting.unformat(subtotal);
                total += parseInt(unformatsubtotal);
            });

            var diskon_transaksi = accounting.unformat($('#diskon-transaksi-nominal').val(),'',0,',','.');
            $('#total-transaksi').val(accounting.formatMoney(parseInt(total) - parseInt(diskon_transaksi), '',0, '.', ','));
        }

        function releasepo($id){
            $.ajax({
                type: "POST",
                url: 'releasepurchaseorder',
                data: {id: $id},
                success: function (response) {
                    toastr.success("PO Berhasil di Release", {
                        "onShow": setTimeout(function () {
                            $('.side-nav .nav-item.active a').click();
                        }, 2600)
                    });
                }
            })
        }

        function rejectpo($id){
            $.ajax({
                type: "POST",
                url: 'rejectpurchaseorder',
                data: {id: $id},
                success: function (response) {
                    toastr.success("PO Telah di Reject", {
                        "onShow": setTimeout(function () {
                            $('.side-nav .nav-item.active a').click();
                        }, 2600)
                    });
                }
            })
        }

        function hidemainbuttongroup(){
            $('.main-button-group a').attr('hidden',true);
        }

        function unformatNumber(){
            $('.diskon').each(function(i,v){
                $(this).val(accounting.unformat($(this).val()));
            })
            $('.subtotal').each(function(i,v){
                $(this).val(accounting.unformat($(this).val()));
            })
            $('#diskon-transaksi-nominal').val(accounting.unformat($('#diskon-transaksi-nominal').val()));
        }
    });
</script>