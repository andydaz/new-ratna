<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta charset="utf-8">
    <title>Sistem Toko</title>
    <link rel="stylesheet" href="css/materialize.css">
    <link rel="stylesheet" href="css/datepicker.min.css">
    <link rel="stylesheet" href="css/master.css">
    <link rel="stylesheet" href="css/toastr.css">
    <link rel="stylesheet" href="css/master.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<form id="form-category">
    <div>
        <input type="file" name="category" id="category">
        <button type="button" id="submit-category">Submit Category</button>
    </div>
</form>
<form id="form-brand">
    <div>
        <input type="file" name="brand" id="brand">
        <button type="submit" id="submit-brand">Submit Brand</button>
    </div>
</form>
<form id="form-type">
    <div>
        <input type="file" name="type" id="type">
        <button type="submit" id="submit-type">Submit Type</button>
    </div>
</form>
<form id="form-product">
    <div>
        <input type="file" name="product" id="product">
        <button type="submit" id="submit-product">Submit Product</button>
    </div>
</form>
<form id="form-customer">
    <div>
        <input type="file" name="customer" id="customer">
        <button type="submit" id="submit-customer">Submit Customer</button>
    </div>
</form>
<form id="form-update">
    <div>
        <input type="file" name="update" id="update">
        <button type="submit" id="update-customer">Update Customer</button>
    </div>
</form>
<script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/toastr.js"></script>
<script src="js/datepicker.min.js"></script>
<script src="js/datepicker.en.js"></script>
<script src="js/materialize.min.js"></script>
<script src="js/moment.js"></script>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#submit-category').click(function(event){
            event.preventDefault();
            var formData = new FormData();
            jQuery.each(jQuery('#category')[0].files, function(i, v) {
                formData.append('category',v);
            });

            $.ajax({
                type:"POST",
                url:"importCategory",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success:function(response){
                    toastr.success('Category Telah Berhasil Di Import!');
                }
            });
        })
        $('#submit-brand').click(function(event){
            event.preventDefault();
            var formData = new FormData();
            jQuery.each(jQuery('#brand')[0].files, function(i, v) {
                formData.append('brand',v);
            });

            $.ajax({
                type:"POST",
                url:"importBrand",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success:function(response){
                    toastr.success('Brand Telah Berhasil Di Import!');
                }
            });
        });
        $('#submit-type').click(function(event){
            event.preventDefault();
            var formData = new FormData();
            jQuery.each(jQuery('#type')[0].files, function(i, v) {
                formData.append('type',v);
            });

            $.ajax({
                type:"POST",
                url:"importType",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success:function(response){
                    toastr.success('Type Telah Berhasil Di Import!');
                }
            });
        });

        $('#submit-product').click(function(event){
            event.preventDefault();
            var formData = new FormData();
            jQuery.each(jQuery('#product')[0].files, function(i, v) {
                formData.append('product',v);
            });

            $.ajax({
                type:"POST",
                url:"importProduct",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success:function(response){
                    toastr.success('Product Telah Berhasil Di Import!');
                }
            });
        });

        $('#submit-customer').click(function(event){
            event.preventDefault();
            var formData = new FormData();
            jQuery.each(jQuery('#customer')[0].files, function(i, v) {
                formData.append('customer',v);
            });

            $.ajax({
                type:"POST",
                url:"importCustomer",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success:function(response){
                    toastr.success('Customer Telah Berhasil Di Import!');
                }
            });
        });

        $('#update-customer').click(function(event){
            event.preventDefault();
            var formData = new FormData();
            jQuery.each(jQuery('#update')[0].files, function(i, v) {
                formData.append('update',v);
            });

            $.ajax({
                type:"POST",
                url:"updateCustomer",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success:function(response){
                    toastr.success('Customer Telah Berhasil Di Update!');
                }
            });
        });
    })
</script>
</body>
</html>

