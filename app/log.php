<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class log extends Model
{
    //
    protected $table = 'log';
    protected $primaryKey='log_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}