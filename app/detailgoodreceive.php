<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailgoodreceive extends Model
{
    //
    protected $table = 'good_receive_details';
    protected $primaryKey='good_receive_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

		public function goodreceive(){
			return $this->belongsTo('App\goodreceive','good_receive_id','good_receive_id');
		}
}