<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class expensecategory extends Model
{
    //
    protected $table = 'expense_category';
    protected $primaryKey='expense_category_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}