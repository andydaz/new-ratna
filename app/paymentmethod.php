<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paymentmethod extends Model
{
    //
    protected $table = 'payment_method';
    protected $primaryKey='payment_method_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function payment_method_type(){
    	return $this->hasOne('App\paymentmethodtype','payment_method_type_id','payment_method_type_id');
    }
    
    public function bank(){
    	return $this->hasOne('App\bank','bank_id','bank_id');
    }
}
