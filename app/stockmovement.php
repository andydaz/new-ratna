<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stockmovement extends Model
{
    //
    protected $table = 'stock_movement';
    protected $primaryKey='stock_movement_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}