<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class depositopayment extends Model
{
    //
    protected $table = 'deposito_payment';
    protected $primaryKey='deposito_payment_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function payment_method(){
    	return $this->hasOne('app\paymentmethod','payment_method_id','payment_method_id');
		}
}