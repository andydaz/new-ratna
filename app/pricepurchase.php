<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pricepurchase extends Model
{
    //
    protected $table = 'price_purchase';
    protected $primaryKey='price_purchase_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}