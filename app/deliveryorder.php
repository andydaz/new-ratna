<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class deliveryorder extends Model
{
    //
    protected $table = 'delivery_order';
    protected $primaryKey='delivery_order_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function details(){
        return $this->hasMany('App\detaildeliveryorder', 'delivery_order_id', 'delivery_order_id')
        ->leftjoin('sales_order_details','delivery_order_details.sales_order_details_id','=','sales_order_details.sales_order_details_id');
    }

    public function details_not_retur(){
        return $this->details()->where('is_retur',0);
    }

		public function details_retur(){
			return $this->details()->where('is_retur',1);
		}

    public function so(){
        return $this->hasOne('App\salesorder', 'sales_order_id', 'sales_order_id');
    }

    public function send(){
        return $this->hasMany('App\detaildeliveryorder', 'delivery_order_id', 'delivery_order_id')
                    ->where('is_send',1);
    }

    public function shipping_term(){
    	return $this->hasOne('App\shippingterm','shipping_term_id','term_delivery_order');
		}
}