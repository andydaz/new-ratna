<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchasepayment extends Model
{
    //
    protected $table = 'payment_purchase';
    protected $primaryKey='payment_purchase_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function payment_method(){
    	return $this->hasOne('App\paymentmethod','payment_method_id','payment_method_id');
		}
}