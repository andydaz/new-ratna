<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class target extends Model
{
    //
    protected $table = 'target';
    protected $primaryKey='target_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}