<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchaseorder extends Model
{
    //
    protected $table = 'purchase_order';
    protected $primaryKey='purchase_order_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

		public function details(){
			return $this->hasMany('App\detailpurchaseorder','purchase_order_id','purchase_order_id')
				->where('status',1);
		}

		public function detailswopricesale(){
			return $this->details()->where('price_sale',0);
		}

    public function state(){
    	return $this->hasOne('App\purchaseorderstate', 'purchase_order_state_id', 'purchase_order_state_id')
        ->where('status',1);
    }

    public function release(){
    	return $this->hasOne('App\purchaseorderstate', 'purchase_order_state_id', 'purchase_order_state_id')
    	->where('purchase_order_state_id',1)
        ->where('status',1);
    }
    public function unreleased(){
    	return $this->hasOne('App\purchaseorderstate', 'purchase_order_state_id', 'purchase_order_state_id')
    	->where('purchase_order_state_id','<>',1)
        ->where('status',1);
    }

    public function unset_price_sale(){
      return $this->details()
        ->where('price_sale',0);
    }

    public function supplier(){
			return $this->hasOne('App\supplier','supplier_id','supplier_id');
		}

		public function paymenttype(){
			return $this->hasOne('App\paymenttype','payment_type_id','payment_type_id');
		}

		public function invoice(){
			return $this->hasOne('App\purchaseinvoice','purchase_order_id','purchase_order_id');
		}

		public function good_receive(){
			return $this->hasMany('App\goodreceive','purchase_order_id','purchase_order_id');
		}
}