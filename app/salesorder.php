<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;


class salesorder extends Model
{
	//
	protected $table = 'sales_order';
	protected $primaryKey='sales_order_id';
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'last_update';

	public function details(){
		return $this->hasMany('App\detailsalesorder','sales_order_id','sales_order_id')
		->where('status',1);
	}
	public function customer(){
		return $this->hasOne('App\customer', 'customer_id', 'customer_id')
			->where('status',1);
	}
	public function staff(){
		return $this->hasOne('App\account', 'account_id', 'sales_id');
	}
	public function payment_type(){
		return $this->hasOne('App\paymenttype', 'payment_type_id', 'payment_type_id')
			->where('status',1);
	}
	public function payment(){
		return $this->hasMany('App\salespayment','sales_order_id','sales_order_id')
			->where('status',1);
	}
	public function first_payment(){
		return $this->hasOne('App\salespayment','sales_order_id','sales_order_id')
		->where('is_first',1)
		->where('status',1);
	}
	public function credit_sales(){
		return $this->hasOne('App\paymenttype', 'payment_type_id', 'payment_type_id')
			->whereIn('payment_type_id',[2,4])
			->where('status',1);
	}
	public function open_invoice(){
		return $this->hasMany('App\salesinvoice', 'sales_order_id', 'sales_order_id')
			->where('payment_sales_id',null)
			->where('status',1);
	}
	public function delivery(){
		return $this->hasMany('App\deliveryorder', 'sales_order_id', 'sales_order_id')
			->where('status', 1);
	}
	public function followup(){
		return $this->hasMany('App\followup', 'sales_order_id', 'sales_order_id')
			->where('status', 1);
	}
	public function non_delivered(){
		return $this->hasMany('App\detailsalesorder','sales_order_id', 'sales_order_id')
			->whereNotExists(function ($query) {
				$query->select(DB::raw(1))
					->from('delivery_order_details')
					->whereRaw('delivery_order_details.sales_order_details_id = sales_order_details.sales_order_details_id')
					->where('status',1);
			})
			->where('status',1);
	}

	public function details_delivery(){
		return $this->hasManyThrough(
				'App\detaildeliveryorder',
				'App\deliveryorder',
				'sales_order_id',
				'delivery_order_id',
				'sales_order_id',
				'delivery_order_id'
			);
	}

	public function scopeLast3Month($query)
	{
		$today = Carbon::parse(Carbon::now())->format('Y-m-d');
		$last3 = Carbon::parse(Carbon::now())->subMonth(3)->format('Y-m-d');
		return $query->whereBetween('date_sales_order',[$last3, $today]);
	}

	public function scopeLast2Month($query)
	{
		$today = Carbon::parse(Carbon::now())->format('Y-m-d');
		$last2 = Carbon::parse(Carbon::now())->subMonth(2)->format('Y-m-d');
		return $query->whereBetween('date_sales_order',[$last2, $today]);
	}

	public function scopeLast1Month($query)
	{
		$today = Carbon::parse(Carbon::now())->format('Y-m-d');
		$last1 = Carbon::parse(Carbon::now())->subMonth(1)->format('Y-m-d');
		return $query->whereBetween('date_sales_order',[$last1, $today]);
	}
}