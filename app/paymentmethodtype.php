<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paymentmethodtype extends Model
{
    //
    protected $table = 'payment_method_type';
    protected $primaryKey='payment_method_type_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}