<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class warehouse extends Model
{
    //
    protected $table = 'warehouse';
    protected $primaryKey='warehouse_id'; 
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}