<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailsalesinvoice extends Model
{
    //
    protected $table = 'invoice_sales_details';
    protected $primaryKey='invoice_sales_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}