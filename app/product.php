<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class product extends Model
{
	//
	protected $table = 'product';
	protected $primaryKey='product_id';
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'last_update';

	public function category(){
		return $this->hasOne('App\productcategory','product_category_id','product_category_id')
			->where('status',1);
	}
	public function brand(){
		return $this->hasOne('App\brandbarang','product_brand_id','product_brand_id')
			->where('status',1);
	}
	public function type(){
		return $this->hasOne('App\tipebarang','product_type_id','product_type_id')
			->where('status',1);
	}
	public function invoice(){
		return $this->hasMany('App\salesinvoice','product_id','product_id')
			->where('status',1);
	}
	public function warehouse(){
		return $this->hasOne('App\warehouse','warehouse_id','warehouse_id')
			->where('status',1);
	}
	public function detailso(){
		return $this->belongsTo('App\detailsalesorder','product_id','product_id');
	}

	public function detailgr(){
		return $this->belongsTo('App\detailgoodreceive','good_receive_details_id','good_receive_details_id');
	}

	public function scopeWhereDate($query){
		return $query->whereBetween('created_at', [Carbon::now()->toDateTimeString(), Carbon::now()->subMonth(10)->toDateTimeString()]);
	}

}