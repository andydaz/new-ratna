<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shippingterm extends Model
{
    //
    protected $table = 'shipping_term';
    protected $primaryKey='shipping_term_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}