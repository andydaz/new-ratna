<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailpurchasereturn extends Model
{
    //
    protected $table = 'purchase_retur_details';
    protected $primaryKey='purchase_retur_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function product(){
    	return $this->hasOne('App\product','product_id','product_id');
		}

	public function detailpo(){
		return $this->hasOne('App\detailpurchaseorder','purchase_order_details_id','purchase_order_details_id');
	}

	public function detailgr(){
    	return $this->hasOne('App\detailgoodreceive','good_receive_details_id','good_receive_details_id');
	}
}