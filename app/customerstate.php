<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customerstate extends Model
{
    //
    protected $table = 'customer_state';
    protected $primaryKey='customer_state_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function level(){
    	return $this->hasOne('App\customerlevel','customer_level_id','customer_level_id');
		}
}