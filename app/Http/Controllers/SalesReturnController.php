<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\salesorder;
use App\salesreturn;
use App\detailsalesreturn;
use App\deliveryorder;
use App\product;
use App\detaildeliveryorder;
use App\detailsalesorder;
use App\salesinvoice;
use App\returtype;
use App\warehouse;
use App\customer;
use App\Http\Controllers\DeliveryOrderController;
use DB;
use DataTable;

class SalesReturnController extends Controller
{
	public function createReturn(Request $request)
	{

		$return = new salesreturn;
		$return->delivery_order_id = $request->nodo;
		$return->account_id = Session::get('user')->account_id;
		$return->status_id = 1;
		$return->retur_number = $request->noretur;
		$return->total_price = $request->returvalue;
		$return->retur_type_id = $request->returtype;
		$return->date_retur = date('Y-m-d', strtotime($request->tglretur));
		$return->status = 1;
		$return->save();

		for ($i=0; $i < sizeof($request->id); $i++) {
			$sodetail = detaildeliveryorder::find($request->id[$i])->sales_order_details_id;
			$detailreturn = new detailsalesreturn;
			$detailreturn->sales_retur_id = $return->sales_retur_id;
			$detailreturn->delivery_order_details = $request->id[$i];
			$detailreturn->product_id = $request->idbarang[$i];
			$detailreturn->status = 1;
			$detailreturn->save();

			product::where('product_id',$request->idbarang[$i])->update([
				'status'=>1,
			]);

			detaildeliveryorder::where('delivery_order_details',$request->id[$i])->update([
				'is_retur'=>1,
			]);

			//ubah status invoice agar tidak ditampilkan lagi saat melakukan pembayaran
			$so = detailsalesorder::find($sodetail)->sales_order_id;
			salesinvoice::where('sales_order_id',$so)->where('product_id',$request->idbarang[$i])->update([
				'status' => 2,
			]);
		}



		if ($request->returtype == 1){
			$customer = deliveryorder::leftjoin('sales_order','delivery_order.sales_order_id','=','sales_order.sales_order_id')
				->leftjoin('customer','sales_order.customer_id','=','customer.customer_id')
				->where('delivery_order_id', $request->nodo)
				->first();

			customer::where('customer_id',$customer->customer_id)->update([
				'total_balance' => DB::raw('total_balance+'.$request->returvalue),
			]);
		}
	}



	public function deleteReturn(Request $request){
		$retur = salesreturn::find($request->id);

		$detailretur = detailsalesreturn::where('sales_retur_id',$retur->sales_retur_id)
			->where('status',1)
			->get();

		foreach ($detailretur as $key => $value){
			$detaildelivery = detaildeliveryorder::find($value->delivery_order_details);

			product::where('product_id',$detaildelivery->product_id)->update([
				'status' => 2,
			]);

			detaildeliveryorder::where('delivery_order_details',$detaildelivery->delivery_order_details)->update([
				'is_retur'=>0,
			]);

			$so = detailsalesorder::find($detaildelivery->sales_order_details_id)->sales_order_id;
			salesinvoice::where('sales_order_id',$so)->where('product_id',$detaildelivery->product_id)->update([
				'status' => 1,
			]);
		}

		if ($retur->retur_type_id == 1){
			$customer = salesreturn::leftjoin('delivery_order','delivery_order.delivery_order_id','=','sales_retur.delivery_order_id')
				->leftjoin('sales_order','delivery_order.sales_order_id','=','sales_order.sales_order_id')
				->leftjoin('customer','sales_order.customer_id','=','customer.customer_id')
				->where('sales_retur_id', $retur->sales_retur_id)
				->first();

			customer::where('customer_id',$customer->customer_id)->update([
				'total_balance' => DB::raw('total_balance-'.$retur->total_price),
			]);
		}

		$retur->status =2;
		$retur->update();
	}

	public function getDeliveryOrderData(Request $request)
	{
		if($request->id == 0)
		{
			return;
		}
		$data['delivery'] = deliveryorder::has('details_not_retur')
			->with('details_not_retur.product.category')
			->with('details_not_retur.product.brand')
			->with('details_not_retur.product.type')
			->with('so.customer')
			->where('delivery_order_id',$request->id)
			->first();

		$data['returtype'] = returtype::where('retur_for',1)
			->where('retur_type_id', 1)
			->get();
		return $data;
	}

	public function getLastReturNumber(Request $request)
	{
		$lastreturntoday = salesreturn::where('retur_number','like','SR/'.date('dmy').'/%')->orderby('sales_retur_id','desc')->first();
	 	if(empty($lastreturntoday))
		{
			$newreturnnumber = "SR/".date('dmy')."/1";
			return $newreturnnumber;
		}
		else{
			$tmpreturn = explode('/',$lastreturntoday->retur_number);
			$lastnumber = $tmpreturn[2];
			$newreturnnumber = "SR/".date('dmy')."/".($lastnumber+1);
			return $newreturnnumber;
		}
	}

	public function getSalesReturn(Request $request)
	{
		$retur = salesreturn::where('status',1)
			->with('details.detaildelivery.product.category')
			->with('details.detaildelivery.product.brand')
			->with('details.detaildelivery.product.type')
			->with('delivery.so.customer')
			->where('sales_retur.sales_retur_id',$request->id)
			->first();

		return $retur;
	}

	public function getSalesReturnTable(Request $request)
	{
		$return = salesreturn::with('delivery.so.customer')
			->where('status',1)
			->orderBy('sales_retur_id','desc');

		if($request->number && $request->number != "")
		{
			$return = $return->where('sales_retur.retur_number', 'like', '%'.$request->number.'%');
		}
		if($request->so && $request->so != "")
		{
			$return = $return->whereHas('delivery.so', function($query) use ($request){
				$query->where('sales_order.sales_order_number', 'like', '%'.$request->so.'%');
			});
		}
		if($request->pelanggan && $request->pelanggan != "")
		{
			$return = $return->whereHas('delivery.so', function($query) use ($request){
				$query->where('sales_order.customer_name', 'like', '%'.$request->pelanggan.'%');
			});
		}
		$return = $return->get();

		return DataTable::of($return)
			->setRowAttr([
				'value' => function($return){
					return $return->sales_retur_id;
				},
			])
			->addColumn('action', function ($return){
				if(Session('roles')->name == 'master') {
					return
						'<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
				}
			})
			->smart(false)
			->make(true);
	}

	public function showRefundHistory(Request $request){
		$refund = salesreturn::leftjoin('sales_order','sales_order.sales_order_id','=','sales_retur.sales_retur_id')->leftjoin('customer','sales_order.customer_id','=','customer.customer_id')->where('retur_type_id',1)->get();
		return view('sales_return.sales_refund',compact('refund'));
	}

	public function showSalesReturnPage()
	{
		$lastmonthdate = date('Y-m-d', strtotime('-1 month', strtotime(date('Y-m-d'))));
		$data['delivery'] = deliveryorder::has('send')
			->with('so.customer')
			->has('details_not_retur')
			->wherebetween('date_delivery',[$lastmonthdate, date('Y-m-d')])
			->get();

		$data['warehouse'] = warehouse::where('status','<>',2)->get();
		$data['returtype'] = returtype::where('retur_for',1)->where('retur_type_id', 1)->get();
//		$data['retur'] = salesreturn::where('status',1)
//			->orderBy('sales_retur_id','desc')
//			->get();
		return view('sales_return.sales_return', compact('data'));
	}

}