<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\deliveryorder;
use App\detaildeliveryorder;
use App\salesorder;
use App\shippingterm;
use App\detailsalesorder;
use App\product;
use App\productwarehouse;
use App\log;
use App\warehouse;
use DB;
use DOMPDF;
use DataTable;
/*use App\deliveryorder;*/

class DeliveryOrderController extends Controller
{
	public function showDeliveryOrderPage()
	{
		$non_deliverd_ids = detailsalesorder::whereDoesntHave('delivery_order_details')->groupBy("sales_order_id")->pluck('sales_order_id');

		$data['so'] = salesorder::with('customer')
			->whereIn('sales_order_id', $non_deliverd_ids)
			->where('status','<>',2)
			->get();

		$data['do'] = deliveryorder::with('so.customer')
			->with('shipping_term')
			->where('delivery_order.status',"<>",2)
			->orderBy('delivery_order_id','desc')
			->get();

		$data['warehouse'] = warehouse::all();
		$data['shippingterm'] = shippingterm::where('status',1)->get();
		return view('delivery_order.delivery_order', compact('data'));
	}

	public function getDeliveryOrderTable(Request $request)
	{
		$do = deliveryorder::with('so.customer')
			->with('shipping_term')
			->where('delivery_order.status',"<>",2)
			->orderBy('delivery_order_id','desc');

		if($request->number && $request->number != "")
		{
			$do = $do->where('delivery_order.delivery_order_number', 'like', '%'.$request->number.'%');
		}
		if($request->so && $request->so != "")
		{
			$do = $do->whereHas('so', function($query) use ($request){
				$query->where('sales_order.sales_order_number', 'like', '%'.$request->so.'%');
			});
		}
		if($request->pelanggan && $request->pelanggan != "")
		{
			$do = $do->whereHas('so.customer', function($query) use ($request){
				$query->where('customer.first_name', 'like', '%'.$request->pelanggan.'%')
					->orWhere('customer.last_name', 'like', '%'.$request->pelanggan.'%');
			});
		}

		return DataTable::of($do)
			->setRowAttr([
				'value' => function($do){
					return $do->delivery_order_id;
				},
			])
			->addColumn('action', function ($do){
				if(Session('roles')->name == 'master') {
					return
						'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$do->sales_order_id.'"><i class="material-icons">edit</i></a>
							<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
				}
			})
			->smart(false)
			->make(true);
	}

	public function getDeliveryOrder(Request $request)
	{
		$do = deliveryorder::with('details.product.category')
			->with('details.product.brand')
			->with('details.product.type')
			->with('so.customer')
			->where('delivery_order_id',$request->id)
			->first();
		return $do;
	}

	public function getLastDeliveryNumber()
	{
		$lastdotoday = deliveryorder::where('delivery_order_number','like','DO/'.date('dmy').'/%')->orderby('delivery_order_id','desc')->first();
		if(empty($lastdotoday))
		{
			$newdonumber = "DO/".date('dmy')."/1";
			return $newdonumber;
		}
		else{
			$tmpdo = explode('/',$lastdotoday->delivery_order_number);
			$lastnumber = $tmpdo[2];
			$newdonumber = "DO/".date('dmy')."/".($lastnumber+1);
			return $newdonumber;
		}
	}

	public function getStockInWarehouse(Request $request)
	{
		$stock = productwarehouse::whereIn('product_id',$request->idproduct)
			->where('warehouse_id',$request->idgudang)
			->get();
		return $stock;
	}

	public function getSalesOrderData(Request $request)
	{
		if($request->id == 0){
			return;
		}
		$id = $request->id;

		$sodata = salesorder::with('customer')
			->where('sales_order.sales_order_id',$id)
			->where('sales_order.status','<>',2)
			->first();

		$sodetail = detailsalesorder::with('product.category')
			->with('product.brand')
			->with('product.type')
			->with('product.warehouse')
			->where('sales_order_id',$sodata->sales_order_id)
			->where('sales_order_details.status','<>',2)
			->select('sales_order_details.*')
			->get();

		return compact('sodata', 'sodetail');
	}

	public function createDeliveryOrder(Request $request)
	{
		$deliveryorder = new deliveryorder;
		$deliveryorder->sales_order_id = $request->so;
		$deliveryorder->delivery_order_number = $request->nodo;
		$deliveryorder->date_delivery  = date('Y-m-d',strtotime($request->tgldo));
		$deliveryorder->term_delivery_order = $request->shippingterm;
		$deliveryorder->note = $request->notes;
		$deliveryorder->status = 1;
		$deliveryorder->save();

		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			$detaildeliveryorder = new detaildeliveryorder;
			$detaildeliveryorder->delivery_order_id = $deliveryorder->delivery_order_id;
			$detaildeliveryorder->sales_order_details_id = $request->id[$i];
			$detaildeliveryorder->product_id = $request->idbarang[$i];
			$detaildeliveryorder->is_send = 1;
			$detaildeliveryorder->status = 1;
			$detaildeliveryorder->save();
		}

		$this->createlog('menambahkan','Delivery Order',$deliveryorder->delivery_order_number);
	}

	public function updateDeliveryOrder(Request $request)
	{
		$deliveryorder = deliveryorder::find($request->iddo);
		$deliveryorder->date_delivery = date('Y-m-d',strtotime($request->tgldo));
		$deliveryorder->note = $request->notes;
		$deliveryorder->term_delivery_order = $request->shippingterm;
		$deliveryorder->update();

		$this->createlog('mengubah','Delivery Order',$deliveryorder->delivery_order_number);
	}

	public function deleteDeliveryOrder(Request $request)
	{
		$detaildeliveryorder = detaildeliveryorder::where('delivery_order_id',$request->id)
			->where('status','<>',2)
			->get();

		$detaildeliveryorder = detaildeliveryorder::where('delivery_order_id',$request->id)
			->update([
				'status'=>2
			]);

		deliveryorder::where('delivery_order_id',$request->id)->update(["status"=>2]);

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "mengubah";
		$log->object = "Delivery Order";
		$log->object_details = $request->id;
		$log->status = 1;
		$log->save();
	}

	public function downloadDeliveryOrder(Request $request, $id)
	{
		$doheader = deliveryorder::join('sales_order','sales_order.sales_order_id','=','delivery_order.sales_order_id')
			->leftjoin('customer','customer.customer_id','=','sales_order.customer_id')
			->where('delivery_order.delivery_order_id',$id)
			->first();

		$dodetail = deliveryorder::join('delivery_order_details','delivery_order_details.delivery_order_id','=','delivery_order.delivery_order_id')
			->join('product','delivery_order_details.product_id','=','product.product_id')
			->leftjoin('unit','unit.unit_id','=','product.unit_id')
			->select('delivery_order_details.*','product.product_code','product.product_name','unit.unit_description')
			->where('delivery_order_details.delivery_order_id', $id)
			->get();

		$pdf = DOMPDF::loadView('delivery_order.do_pdf', compact('doheader','dodetail'));
		return $pdf->stream('DO.pdf');
	}

	public function createlog($action, $object, $number){
		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = $action;
		$log->object = $object;
		$log->object_details = $number;
		$log->status = 1;
		$log->save();
	}

	// public function generateDOForAllRemainingSO(){
	// 	$so = salesorder::with('details')
	// 			->whereDoesntHave('delivery')
	// 			->where('status', 1)
	// 			->whereIn('payment_type_id', [1, 2])
	// 			->limit(500)
	// 			->get();

	// 	// return $so->map(function ($so){
	// 	// 	return $so->sales_order_number." - ".$so->customer_name;
	// 	// });

	// 	foreach ($so as $key => $so) {
	// 		$nodo = $this->getLastDeliveryNumber();
	// 		$deliveryorder = new deliveryorder;
	// 		$deliveryorder->sales_order_id = $so->sales_order_id;
	// 		$deliveryorder->delivery_order_number = $nodo;
	// 		$deliveryorder->date_delivery  = date('Y-m-d');
	// 		$deliveryorder->term_delivery_order = 1;
	// 		$deliveryorder->note = "input do by sistem";
	// 		$deliveryorder->status = 1;
	// 		$deliveryorder->save();

	// 		$details = $so->details;
	// 		foreach ($details as $key => $detail) {
	// 			$detaildeliveryorder = new detaildeliveryorder;
	// 			$detaildeliveryorder->delivery_order_id = $deliveryorder->delivery_order_id;
	// 			$detaildeliveryorder->sales_order_details_id = $detail->sales_order_details_id;
	// 			$detaildeliveryorder->product_id = $detail->product_id;
	// 			$detaildeliveryorder->is_send = 1;
	// 			$detaildeliveryorder->status = 1;
	// 			$detaildeliveryorder->save();
	// 		}

	// 		$this->createlog('menambahkan','Delivery Order',$deliveryorder->delivery_order_number);
	// 	}
	// }
}