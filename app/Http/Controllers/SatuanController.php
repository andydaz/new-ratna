<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\satuan;

class SatuanController extends Controller
{
	public function showSatuanPage()
	{
		$satuan = satuan::paginate(10);
		return view('satuan.satuan', compact('satuan'));
	}
	public function createSatuan(Request $request)
	{
		$satuan = new satuan;
		$satuan->unit_code = $request->kode;
		$satuan->unit_description = $request->satuan;
		$satuan->status = 1;
		$satuan->save();
	}
	public function getSatuanData(Request $request){
		$id = $request->id;
		$satuan = satuan::where('unit_id',$id)->select('*')->first();
		return $satuan;
	}

	public function modalSatuanData(Request $request){
		$id= $request->id;
		$data= satuan::where('unit_id',$id)->first();
		return view('satuan.modal_satuan', compact('data'));
	}

	public function deleteSatuan(Request $request)
	{
		$id = $request->id;
		satuan::where('unit_id',$id)->delete();
	}

	public function updateSatuan(Request $request)
	{
		$id = $request->id;
		satuan::where('unit_id', $id)->update(
			[
			'unit_code' => $request->kode,
			'unit_description' => $request->satuan,
			]
			);
	}
}