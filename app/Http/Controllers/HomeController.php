<?php

namespace App\Http\Controllers;

use App\detailpurchaseorder;
use App\detailsalesorder;
use App\expense;
use App\expensecategory;
use App\paymenttype;
use App\product;
use App\productcategory;
use App\purchasepayment;
use App\salesinvoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\salesorder;
use App\purchaseorder;
use App\account;
use App\news;
use DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     return view('home');
    // }

    public function showMainPage()
    {   
        $user = Auth::user();
        Session::put('user', $user);
        $role = account::find($user->account_id)->roles->first();
        Session::put('roles',$role);
        return view('main');
    }

    public function showHomePage()
    {
        return view('home.home');
    }

    public function loadhomenews(){
        $hasil = news::all();
        return view('home.news',['hasil'=>$hasil]);
    }

    public function loadHomePiutangPo()
    {
        // select purchase_order.*, supplier.company_name, payment_purchase.total_paid, (CASE WHEN payment_purchase.total_paid is NULL THEN purchase_order.grand_total_idr - 0 ELSE purchase_order.grand_total_idr - (payment_purchase.total_paid + payment_purchase.discount_nominal) END) as remaining_payable from `purchase_order` left join `supplier` on `purchase_order`.`supplier_id` = `supplier`.`supplier_id` left join `invoice_purchase` on `invoice_purchase`.`purchase_order_id` = `purchase_order`.`purchase_order_id` left join `payment_purchase` on `payment_purchase`.`invoice_purchase_id` = `invoice_purchase`.`invoice_purchase_id` HAVING remaining_payable > 0

        $piutang_po = purchaseorder::leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
                    ->join('invoice_purchase','invoice_purchase.purchase_order_id','=','purchase_order.purchase_order_id')
                    ->selectRaw("purchase_order.*,invoice_purchase.*, date_add(invoice_purchase.date_purchase_invoice, Interval 14 Day) as jatuh_tempo, supplier.*")
                    ->get();

        return view('home.piutang_po',['piutang_po'=>$piutang_po]);

    }

    public function loadSalesInvoiceDue(request $request){
    	$today = Carbon::now();
//    	$finaldate = $today->addDays(1)->format('Y-m-d');

    	$invoicedue = salesinvoice::where('status',1)
				->has('product')
				->doesntHave('payment')
				->whereDate('due_date', '<', $today->format('Y-m-d'))
				->get();

    	return view('home.sales_invoice_due', compact('invoicedue'));
		}

		public function loadStockOverview(request $request){
    	$stock = product::where('status',1)
				->selectRaw('*, YEAR(created_at) product_year')
				->get();

    	$years = product::where('status',1)
				->selectRaw('Distinct YEAR(created_at) years')
				->get();

//    	dump($years);

    	$category = productcategory::where('status',1)
				->get();

    	return view('home.stock_overview', compact('stock','category', 'years'));
		}

		public function loadExpense(request $request){

			$start = Carbon::now()->startOfMonth();
			$end = Carbon::now()->endOfMonth();

			$expensecat = expensecategory::where('status',1)->get();
			$expense = expense::whereBetween('date_expense',[$start, $end])->get();
			$totalexpense = $expense->sum('nominal');
			return view('home.expense', compact('expense','totalexpense','expensecat'));
		}

		public function loadSalesAchievement(request $request)
		{
			$start = Carbon::now()->startOfMonth();
			$end = Carbon::now()->endOfMonth();

			$detailsales = detailsalesorder::leftjoin('sales_order','sales_order.sales_order_id','=','sales_order_details.sales_order_id')
				->leftjoin('product','product.product_id','=','sales_order_details.product_id')
				->whereBetween('sales_order.date_sales_order',[$start, $end])
				->where('sales_order_details.status',1)
				->where('sales_order.status',1)
				->select('sales_order.payment_type_id','product.product_category_id','product.price_buy','sales_order_details.*')
				->get();

			$paymenttype = paymenttype::where('payment_type_id','<>',4)
				->get();

			$productcat = productcategory::where('status',1)
				->get();

			return view('home.sales_achievement', compact('detailsales','paymenttype','productcat'));
		}

		public function loadPurchaseAchievement(request $request)
		{
			$start = Carbon::now()->startOfMonth();
			$end = Carbon::now()->endOfMonth();

			$detailpurchase = detailpurchaseorder::leftjoin('purchase_order','purchase_order.purchase_order_id','=','purchase_order_details.purchase_order_id')
				->leftjoin('product_category','product_category.product_category_id','=','purchase_order_details.product_category_id')
				->whereBetween('purchase_order.date_purchase_order',[$start,$end])
				->where('purchase_order_details.status',1)
				->where('purchase_order.status',1)
				->get();

			// cari purchase order yang belum terbayar lunas
			$purchaseorder = purchaseorder::where('status',1)
				->whereExists(function ($query) {
					$query->select(DB::raw(1))
						->from('payment_purchase')
						->leftjoin('invoice_purchase','invoice_purchase.invoice_purchase_id','=','payment_purchase.invoice_purchase_id')
						->where('payment_purchase.status',1)
						->where('invoice_purchase.status',1)
						->whereRaw('purchase_order.purchase_order_id = invoice_purchase.purchase_order_id')
						->havingRaw('SUM(total_paid + deposit_reduction) < purchase_order.grand_total_idr');
				})
				->with('invoice.payment')
				->get();

			$outstanding = 0;
			foreach ($purchaseorder as $key => $value)
			{
				$grandtotal = $value->grand_total_idr;

				$totalpayment = 0;

				foreach ( $value->invoice->payment as $key2 => $value2)
				{
					$totalpayment += $value2->total_paid + $value2->deposit_reduction;
				}

				$outstanding += $grandtotal - $totalpayment;
			}

			$purchasepayment = purchasepayment::where('status',1)
				->whereBetween('date_payment_purchase',[$start,$end])
				->get();

			$productcat = productcategory::where('status',1)
				->get();

			return view('home.purchase_achievement', compact('detailpurchase','outstanding', 'purchasepayment','productcat'));
		}

		public function loadAccountReceiveable(){
			$invoiceunpaid = salesinvoice::whereNull('payment_sales_id')
				->leftjoin('sales_order','invoice_sales.sales_order_id','=','sales_order.sales_order_id')
				->select('invoice_sales.*','sales_order.customer_id')
				->get();

			return view('home.account_receiveable',compact('invoiceunpaid'));
		}
}
