<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Indonesia;
use App\product;
use App\productwarehouse;

class ExtendController extends Controller
{
	public function listCities(Request $request){
		$provinsi = $request->provinsi;
		$selectedcity = $request->city;
		$cities = DB::table('indonesia_cities as city')->join('indonesia_provinces as provinsi','city.province_id','=','provinsi.id')->select('city.id','city.name')->where('provinsi.id',$provinsi)->get();
		return view('city', compact('cities','selectedcity'));
	}
	public function listBarang(Request $request){
		$reqbarang = $request->barang;
		$src = $request->src;
		$barang = product::leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')->selectRaw('product.*, sum(quantity) as total_qty')->groupby('product_id')->get();
		return view('modal.modal-barang', compact('barang','reqbarang','src'));
	}

	public function getBarangModal(Request $request){
		$barang = $request->barang;
		$id = [];
		$nama = [];
		$harga = [];
		$weight = [];
		if(!empty($barang))
		{
			foreach ($barang as $key => $value) {
				$tempbarang = product::where('product_id',$value)->select('product_id','product_name','price_sale','weight')->first();
				array_push($id,$tempbarang->product_id);
				array_push($nama,$tempbarang->product_name);
				array_push($harga,$tempbarang->price_sale);
				array_push($weight,$tempbarang->weight);
			}
			return ['barang'=>$barang, 'id'=>$id, 'nama'=>$nama, 'harga'=>$harga, 'weight'=>$weight];
		}
		return ['barang'=>[]];
	}

}