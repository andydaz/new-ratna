<?php

namespace App\Http\Controllers;

use App\balancetransaction;
use App\brandbarang;
use App\depositopayment;
use App\expense;
use App\expensecategory;
use App\paymentmethod;
use App\paymenttype;
use App\product;
use App\productcategory;
use App\purchasepayment;
use App\purchasereturn;
use App\salesinvoice;
use App\salesorder;
use App\purchaseorder;
use App\detailpurchaseorder;
use App\salespayment;
use App\salesreturn;
use App\supplier;
use App\customer;
use App\existingreceiveabletransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Excel;
use DB;
use DataTable;

class ReportController extends Controller
{
	public function showReportPO(Request $request){
		$po = purchaseorder::with('supplier')
			->with('invoice.payment')
			->where('status',1);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$po = $po->whereBetween('purchase_order.date_purchase_order',[$start, $end]);
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
			$po = $po->whereBetween('purchase_order.date_purchase_order',[$start, $end]);
		}

		if($request->supplier != 0)
		{
			$supplier = $request->supplier;
			$po = $po->whereHas('supplier', function($query) use ($supplier){
				$query->where('supplier_id', $supplier);
			});
		}
		$po = $po->get();

		$supplier = supplier::all();

		if(($request->start && $request->end)||$request->supplier)
		{
			return view('report.report_po_detail', compact('po'));
		}
		return view('report.report_po', compact('po','supplier'));
	}

	public function getDetailPo(Request $request)
	{
		$detail = detailpurchaseorder::leftjoin('product','product.product_id','=','purchase_order_details.product_id')->where('purchase_order_id',$request->id)->select('purchase_order_details.*','product.product_name','product.product_code')->get();
		return view('report.modal-detail-po', compact('detail'));
	}

	public function showReportSo(Request $request)
	{
		$so = salesorder::with('customer')
			->with('payment')
			->where('status',1);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}

		if($request->customer)
		{
			$so = $so->where('sales_order.customer_id', $request->customer);
		}
		$so = $so->get();
		$customer = customer::where('status',1)->get();

		if(($request->start && $request->end)||$request->customer)
		{
			return view('report.report_so_detail', compact('so'));
		}
		return view('report.report_so', compact('so','customer'));
	}

	public function showReportPiutang(request $request){

		$sub1 = DB::query()->fromSub(function ($query) {
    				$query->selectRaw('sub.*, sum(total_amount) total_paid')->fromSub(function ($query2) {
    					$query2->selectRaw('invoice_sales.invoice_sales_id, invoice_sales.product_id, invoice_sales.sales_order_id, invoice_sales.total_amount,invoice_sales.payment_sales_id, term_number, payment_sales.date_sales_payment')
    							->from('invoice_sales')
    							->join('payment_sales','payment_sales.payment_sales_id','=','invoice_sales.payment_sales_id')
    							->whereNotNull('invoice_sales.payment_sales_id')
    							->where('invoice_sales.status', 1)
    							->orderBy('invoice_sales.invoice_sales_id', 'desc');
    				}, 'sub')->groupby('product_id');
				}, 'paid');


		$sub2 = DB::query()->fromSub(function ($query) {
    				$query->selectRaw('invoice_sales_id, sales_order_id, due_date')
    					->from('invoice_sales')
    					->whereNull('payment_sales_id')
    					->where('status',1)
    					->groupby('sales_order_id');
				}, 'unpaid');


		$sub3 = DB::query()->fromSub(function ($query) {
    				$query->selectRaw('sum(total_amount) as total_due, sales_order_id')
    					->from('invoice_sales')
    					->whereNull('payment_sales_id')
    					->where('due_date', '<', Carbon::now()->toDateString())
    					->where('status',1)
    					->groupby('sales_order_id');
				}, 'upcoming_due');


		// filter product yang kredit (caranya cek product yang punya invoice tapi belum terbayar / payment_sales_id nya null)
		$report = salesinvoice::with(['so.customer', 'product.brand', 'product.category', 'product.type'])
					->selectRaw('invoice_sales.*, paid.term_number as last_term, paid.total_paid, paid.date_sales_payment as last_payment_date,
						CASE
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 0 THEN "0 Bulan"
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 30 THEN "1 Bulan"
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 60 THEN "1-2 Bulan"
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 90 THEN "2-3 Bulan"
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 180 THEN "3-6 Bulan"
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 360 THEN "6-12 Bulan"
						    ELSE ">1 Tahun"
						END AS aging, upcoming_due.total_due, unpaid.due_date as last_due_date')
					->join('sales_order', 'sales_order.sales_order_id', '=', 'invoice_sales.sales_order_id')
					->leftjoinSub($sub1, 'paid', function($join){
						$join->on('paid.sales_order_id','=','invoice_sales.sales_order_id');
					})
					->joinSub($sub2, 'unpaid', function($join){
						$join->on('unpaid.sales_order_id','=','invoice_sales.sales_order_id');
					})
					->leftJoinSub($sub3, 'upcoming_due', function($join){
						$join->on('upcoming_due.sales_order_id','=','invoice_sales.sales_order_id');
					})
					->whereExists(function ($query) {
						$query->select(DB::raw(1))
							->from('invoice_sales as is_1')
							->where('status',1)
							->whereNull('payment_sales_id')
							->whereNotNull('product_id')
							->whereRaw('is_1.invoice_sales_id = invoice_sales.invoice_sales_id');
					})
					->groupby('invoice_sales.product_id');

		if($request->customer != 0)
		{
			$report = $report->where('sales_order.customer_id', $request->customer);
		}
		$report = $report->get();

		// $report = salesinvoice::with('so')
		// 	->whereExists(function ($query) {
		// 		$query->select(DB::raw(1))
		// 			->from('invoice_sales as is_1')
		// 			->where('status',1)
		// 			->whereNull('payment_sales_id')
		// 			->whereNotNull('product_id')
		// 			->whereRaw('is_1.invoice_sales_id = invoice_sales.invoice_Sales_id');
		// 	})
		// 	->groupby('product_id');

		// if($request->customer != 0)
		// {
		// 	$report = $report->leftjoin('sales_order','invoice_sales.sales_order_id','=','sales_order.sales_order_id')
		// 		->where('sales_order.customer_id', $request->customer);
		// }
		// $report = $report->get();


		// //get yang terbayar dan yang belum terbayar
		// foreach ($report as $key => $value)
		// {
		// 	$invoicepaid = salesinvoice::where('product_id',$value->product_id)
		// 		->whereNotNull('payment_sales_id')
		// 		->get();

		// 	$invoiceunpaid = salesinvoice::where('product_id',$value->product_id)
		// 		->whereNull('payment_sales_id')
		// 		->get();

		// 	//get last payment nya dengan order desc
		// 	$lastpaid = salesinvoice::find(DB::table('invoice_sales')->where('product_id',$value->product_id)
		// 		->whereNotNull('payment_sales_id')
		// 		->orderby('term_number','desc')
		// 		->first()
		// 		->invoice_sales_id
		// 	);

		// 	$value['totalpaid'] = $invoicepaid->sum('total_amount');
		// 	$value['totalreceiveable'] = $invoiceunpaid->sum('total_amount');
		// 	$value['lastduedate'] = $invoiceunpaid->min('due_date');
		// 	$value['termpaid'] = $lastpaid->term_number;
		// 	$value['lastpaymentdate'] = salespayment::find($lastpaid->payment_sales_id)->date_sales_payment;
		// }

		if($request->customer != '')
		{
			return view('report.report_piutang_detail', compact('report'));
		}

		$customer = customer::where('status',1)->get();

		return view('report.report_piutang', compact('customer','report'));
	}

	public function showReportPembayaranPiutang(request $request){
		$customer = customer::where('status',1)->get();
		return view('report.report_pembayaran_piutang', compact('customer'));
	}

	public function getReportPembayaranPiutangTable(request $request){
		isset($request->startdate) ? $start = Carbon::createFromFormat('d-m-Y',$request->startdate) : $start = Carbon::now()->startOfMonth();
		isset($request->enddate) ? $end = Carbon::createFromFormat('d-m-Y', $request->enddate) : $end = Carbon::now()->endOfMonth();

		$report = salesinvoice::select('invoice_sales.*')
			->leftjoin('payment_sales','payment_sales.payment_sales_id','=','invoice_sales.payment_sales_id')
			->leftjoin('sales_order','sales_order.sales_order_id','=','invoice_sales.sales_order_id')
			->with('so')
			->with('payment')
			->with('product')
			->whereNotNull('invoice_sales.payment_sales_id')
			->whereNotNull('product_id')
			->where('invoice_sales.status',1)
			->where('payment_sales.status',1)
			->orderBy('payment_sales.date_sales_payment')
			->whereBetween('payment_sales.date_sales_payment',[$start, $end]);

		if($request->customer != 0)
		{
			$report = $report->where('sales_order.customer_id', $request->customer);
		}

		return DataTable::of($report)
			->editColumn('total_amount', function ($invoice){
   				return number_format($invoice->total_amount);
   			})
			->smart(false)
			->make(true);
	}

	public function showReportExpense(request $request){
		$report = expense::where('status',1)
			->with('payment_method');

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$report = $report->whereBetween('date_expense',[$start, $end]);
		}
		$report = $report->get();

		if($request->start && $request->end)
		{
			return view('report.report_expense_detail', compact('report'));
		}

		return view('report.report_expense', compact('report'));
	}

	public function showReportCashBalance(request $request)
	{
		isset($request->start) ? $start = Carbon::createFromFormat('d-m-Y',$request->start) : $start = Carbon::now()->startOfMonth();
		isset($request->end) ? $end = Carbon::createFromFormat('d-m-Y', $request->end) : $end = Carbon::now()->endOfMonth();

		$paymentsales = salespayment::where('status',1)
			->with('details.method')
			->whereBetween('date_sales_payment',[$start,$end])
			->get();

		$paymentpurchase = purchasepayment::where('status',1)
			->with('payment_method')
			->whereBetween('date_payment_purchase',[$start,$end])
			->get();

		$depositoclearence = depositopayment::where('status',1)
			->with('payment_method')
			->with('payment_method')
			->whereBetween('date_deposito_payment',[$start,$end])
			->get();

		$balancetransaction = balancetransaction::where('status',1)
			->whereBetween('date_balance_transaction',[$start,$end])
			->with('payment_method')
			->get();

		$expense = expense::where('status',1)
			->whereBetween('date_expense',[$start,$end])
			->get();

		$metode = paymentmethod::where('status',1)
			->where('payment_method_id','<>',2)
			->get();

		if($request->start && $request->end)
		{
			return view('report.report_cash_balance_detail', compact('paymentsales','paymentpurchase','depositoclearence','balancetransaction','expense','metode'));
		}

		return view('report.report_cash_balance', compact('paymentsales','paymentpurchase','depositoclearence','balancetransaction','expense','metode'));
	}

	public function showReportProfitLoss(request $request)
	{
		isset($request->year) ? $year = $request->year : $year = Carbon::now()->format('Y');
		isset($request->month) ? $month =  $request->month: $month = Carbon::now()->format('n');

		$start = Carbon::createFromDate($year, $month)->startOfMonth()->toDateString();
		$end = Carbon::createFromDate($year, $month)->endOfMonth()->toDateString();

		$data['sales']= salesorder::where('status',1)
			->whereBetween('date_sales_order',[$start,$end])
			->get();

		$data['salesretur']= salesreturn::where('status',1)
			->whereBetween('date_retur',[$start,$end])
			->get();

		$data['paymenttype'] = paymenttype::where('payment_type_id','<>',4)->get();

		$data['beginventory']= product::where('created_at','<',$start)
			->where(function ($query) use ($start){
				$query->whereNull('selled_date')
					->orWhere('selled_date', '>', $start);
			})
			->get();

		$data['endinventory'] = product::where('created_at','<',$end)
			->where(function ($query) use ($end){
				$query->whereNull('selled_date')
					->orWhere('selled_date', '>', $end);
			})
			->get();

		$data['category'] = productcategory::where('status',1)->get();

		$data['purchase'] = purchaseorder::where('status',1)
			->where('purchase_order_state_id',1)
			->whereBetween('date_purchase_order',[$start,$end])
			->get();

		$data['purchaseretur'] = purchasereturn::where('status',1)
			->whereBetween('date_retur',[$start,$end])
			->get();

		$data['expense'] = expense::where('status',1)
			->whereBetween('date_expense',[$start,$end])
			->get();

		$data['expensecategory'] = expensecategory::where('status',1)
			->get();

		if($request->year && $request->month)
		{
			return view('report.report_profit_loss_detail',compact('data'));
		}

		return view('report.report_profit_loss',compact('data'));
	}

	public function showGrafikPenjualan(){
		return view('so.grafik_penjualan');
	}

	public function getGrafikData($startdate, $enddate){
		$data = salesorder::leftjoin('sales_order_details','sales_order_details.sales_order_id','=','sales_order.sales_order_id')->leftjoin('product','product.product_id','=','sales_order_details.product_id')->selectraw("product.product_name, sum(sales_order_details.sub_total) as product_sales")->whereBetween('sales_order.date_sales_order',[$startdate,$enddate])->groupby('product.product_id')->get();
		return $data;
	}

	public function showGrafikTarget(){
		return view('home.grafik_target');
	}

	public function getGrafikTarget(){
		$supplier = supplier::leftjoin('purchase_order','supplier.supplier_id','=','purchase_order.supplier_id')
			->whereHas('target', function($query){
				$query->whereRaw('NOW() between date_start and date_end');
			})
			->selectraw('supplier.*, sum(purchase_order.grand_total_idr) as pembelian')->with('target')->groupby('supplier.supplier_id')->get();
		return $supplier;
	}

	public function downloadReportSo(Request $request, $start, $end, $customer)
	{

		$report = salesorder::with(['details', 'staff'])
			->with('payment.details')
			->where('status',1);

		/*if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$report = $report->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}*/
		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$report = $report->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->addMonths()->startOfMonth()->toDateString();
			$report = $report->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}

		if($request->customer != 0)
		{
			$report = $report->where('sales_order.customer_id', $customer);
		}
		$report= $report->get();

		$metode = paymentmethod::where('status',1)
			->get();

		Excel::create('Report Sales Order', function($excel) use($report, $metode, $start, $end){
			$excel->sheet('Report Sales Order', function($sheet) use ($report, $metode, $start, $end){
				$sheet->loadView('report.so_excel',['report'=>$report, 'metode'=>$metode ,'start'=> $start,'end' => $end]);
			});
		})->download('xls');
	}

	public function downloadReportPo(Request $request, $start, $end)
	{
		// $report = purchaseorder::leftjoin('invoice_purchase','invoice_purchase.purchase_order_id','=','purchase_order.purchase_order_id')
		// 	->leftjoin('invoice_purchase_details','invoice_purchase.invoice_purchase_id','=','invoice_purchase_details.invoice_purchase_id')
		// 	->leftjoin('payment_purchase','payment_purchase.invoice_purchase_id','=','invoice_purchase.invoice_purchase_id')
		// 	->selectraw("*, (CASE WHEN payment_purchase.payment_purchase_id IS NULL Then 'Not Paid' ELSE 'Paid' END) as po_status, sum(payment_purchase.total_paid) as totalpaid")
		// 	->where('purchase_order.status',1)
		// 	->where('invoice_purchase.status',1)
		// 	->where('payment_purchase.status',1)
		// 	->groupby('purchase_order.purchase_order_id');

		// if($request->start && $request->end)
		// {
		// 	$start = date('Y-m-d', strtotime($request->start));
		// 	$end = date('Y-m-d' ,strtotime($request->end));
		// 	$report = $report->whereBetween('purchase_order.date_purchase_order',[$start, $end]);
		// }
		// $report = $report->get();

		$po = purchaseorder::with('supplier')
			->with('invoice.payment')
			->where('status',1);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$po = $po->whereBetween('purchase_order.date_purchase_order',[$start, $end]);
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
			$po = $po->whereBetween('purchase_order.date_purchase_order',[$start, $end]);
		}

		if($request->supplier != 0)
		{
			if($request->supplier != 0)
			{
				$supplier = $request->supplier;
				$po = $po->whereHas('supplier', function($query) use ($supplier){
					$query->where('supplier_id', $supplier);
				});
			}
		}
		$po = $po->get();

		Excel::create('Report Purchase Order', function($excel) use($po){
			$excel->sheet('Report Purchase Order', function($sheet) use ($po){
				$sheet->loadView('report.po_excel', array('report' => $po));
			});
		})->download('xls');
	}

	public function downloadReportPiutang(Request $request, $customer)
	{
		$sub1 = DB::query()->fromSub(function ($query) {
    				$query->selectRaw('sub.*, sum(total_amount) total_paid')->fromSub(function ($query2) {
    					$query2->selectRaw('invoice_sales.invoice_sales_id, invoice_sales.product_id, invoice_sales.sales_order_id, invoice_sales.total_amount,invoice_sales.payment_sales_id, term_number, payment_sales.date_sales_payment')
    							->from('invoice_sales')
    							->join('payment_sales','payment_sales.payment_sales_id','=','invoice_sales.payment_sales_id')
    							->whereNotNull('invoice_sales.payment_sales_id')
    							->where('invoice_sales.status', 1)
    							->orderBy('invoice_sales.invoice_sales_id', 'desc');
    				}, 'sub')->groupby('product_id');
				}, 'paid');


		$sub2 = DB::query()->fromSub(function ($query) {
    				$query->selectRaw('invoice_sales_id, sales_order_id, due_date')
    					->from('invoice_sales')
    					->whereNull('payment_sales_id')
    					->where('status',1)
    					->groupby('sales_order_id');
				}, 'unpaid');


		$sub3 = DB::query()->fromSub(function ($query) {
    				$query->selectRaw('sum(total_amount) as total_due, sales_order_id')
    					->from('invoice_sales')
    					->whereNull('payment_sales_id')
    					->where('due_date', '<=', Carbon::now()->toDateString())
    					->where('status',1)
    					->groupby('sales_order_id');
				}, 'upcoming_due');


		// filter product yang kredit (caranya cek product yang punya invoice tapi belum terbayar / payment_sales_id nya null)
		$report = salesinvoice::with(['so.customer', 'product.brand', 'product.category', 'product.type'])
					->selectRaw('invoice_sales.*, paid.term_number as last_term, paid.total_paid, paid.date_sales_payment as last_payment_date,
						CASE
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) < 0 THEN "0 Bulan"
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) < 30 THEN "1 Bulan"
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) < 60 THEN "1-2 Bulan"
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) < 90 THEN "2-3 Bulan"
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) < 180 THEN "3-6 Bulan"
						    WHEN DATEDIFF(CURDATE(), unpaid.due_date) < 360 THEN "6-12 Bulan"
						    ELSE ">1 Tahun"
						END AS aging, upcoming_due.total_due, unpaid.due_date as last_due_date')
					->join('sales_order', 'sales_order.sales_order_id', '=', 'invoice_sales.sales_order_id')
					->leftjoinSub($sub1, 'paid', function($join){
						$join->on('paid.sales_order_id','=','invoice_sales.sales_order_id');
					})
					->joinSub($sub2, 'unpaid', function($join){
						$join->on('unpaid.sales_order_id','=','invoice_sales.sales_order_id');
					})
					->leftJoinSub($sub3, 'upcoming_due', function($join){
						$join->on('upcoming_due.sales_order_id','=','invoice_sales.sales_order_id');
					})
					->whereExists(function ($query) {
						$query->select(DB::raw(1))
							->from('invoice_sales as is_1')
							->where('status',1)
							->whereNull('payment_sales_id')
							->whereNotNull('product_id')
							->whereRaw('is_1.invoice_sales_id = invoice_sales.invoice_sales_id');
					})
					->groupby('invoice_sales.product_id');

		if($customer != 0)
		{
			$report = $report->where('sales_order.customer_id', $customer);
		}
		$report = $report->get();

		Excel::create('Report Piutang', function($excel) use($report){
			$excel->sheet('Report Piutang', function($sheet) use ($report){
				$sheet->loadView('report.piutang_excel',['report'=>$report]);
			});
		})->download('xls');
	}

	public function downloadReportPembayaranPiutang(Request $request, $start, $end, $customer)
	{
		$start = date('Y-m-d', strtotime($start));
		$end = date('Y-m-d' ,strtotime($end));

		//filter invoice yang sudah dibayar aja
		$report = salesinvoice::leftjoin('payment_sales','payment_sales.payment_sales_id','=','invoice_sales.payment_sales_id')
			->leftjoin('sales_order','sales_order.sales_order_id','=','invoice_sales.sales_order_id')
			->with('so')
			->with('payment')
			->with('product')
			->whereNotNull('invoice_sales.payment_sales_id')
			->whereNotNull('product_id')
			->where('invoice_sales.status',1)
			->where('payment_sales.status',1)
			->orderBy('payment_sales.date_sales_payment')
			->whereBetween('payment_sales.date_sales_payment',[$start, $end]);

		if($customer != 0)
		{
			$report = $report->where('sales_order.customer_id', $customer);
		}

		$report = $report->get();

		$metode = paymentmethod::where('status',1)
			->get();

		Excel::create('Report Pembayaran Piutang', function($excel) use($report, $metode){
			$excel->sheet('Report Pembayaran Piutang', function($sheet) use ($report, $metode){
				$sheet->loadView('report.pembayaran_piutang_excel',['report'=>$report, 'metode'=>$metode]);
			});
		})->download('xls');
	}

	public function downloadReportExpense(Request $request)
	{
		//filter invoice yang sudah dibayar aja
		$report = expense::where('status',1)
			->with('payment_method');

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$report = $report->whereBetween('date_expense',[$start, $end]);
		}
		$report = $report->get();

		$metode = paymentmethod::where('status',1)
			->where('payment_method_id','<>',2)
			->get();

		Excel::create('Report Biaya Harian', function($excel) use($report, $metode){
			$excel->sheet('Report Biaya Harian', function($sheet) use ($report, $metode){
				$sheet->loadView('report.expense_excel',['report'=>$report, 'metode'=>$metode]);
			});
		})->download('xls');
	}

	public function downloadReportCashBalance(Request $request, $start , $end)
	{
		$start = Carbon::createFromFormat('d-m-Y',$start)->toDateString();
		$end = Carbon::createFromFormat('d-m-Y',$end)->toDateString();
		//filter invoice yang sudah dibayar aja
		$paymentsales = salespayment::where('status',1)
			->with('details.method')
			->whereBetween('date_sales_payment',[$start,$end])
			->get();

		$paymentpurchase = purchasepayment::where('status',1)
			->with('payment_method')
			->whereBetween('date_payment_purchase',[$start,$end])
			->get();

		$depositoclearence = depositopayment::where('status',1)
			->with('payment_method')
			->whereBetween('date_deposito_payment',[$start,$end])
			->get();

		$balancetransaction = balancetransaction::where('status',1)
			->whereBetween('date_balance_transaction',[$start,$end])
			->with('payment_method')
			->get();

		$expense = expense::where('status',1)
			->whereBetween('date_expense',[$start,$end])
			->get();

		$metode = paymentmethod::where('status',1)
			->where('payment_method_id','<>',2)
			->get();

		Excel::create('Report Biaya Harian', function($excel) use($paymentsales, $paymentpurchase, $depositoclearence, $balancetransaction, $expense, $metode){
			$excel->sheet('Report Biaya Harian', function($sheet) use ($paymentsales, $paymentpurchase, $depositoclearence, $balancetransaction, $expense, $metode){
				$sheet->loadView('report.cash_balance_excel',
					[
						'paymentsales'=>$paymentsales,
						'paymentpurchase'=>$paymentpurchase,
						'depositoclearance'=>$depositoclearence,
						'balancetransaction'=>$balancetransaction,
						'expense'=>$expense,
						'metode'=>$metode
					]
				);
			});
		})->download('xls');
	}

	public function downloadReportProfitLoss(Request $request, $year, $month)
	{
		$start = Carbon::createFromDate($year, $month)->startOfMonth()->toDateString();
		$end = Carbon::createFromDate($year, $month)->endOfMonth()->toDateString();

		$sales = salesorder::where('status',1)
			->whereBetween('date_sales_order',[$start,$end])
			->get();

		$paymenttype = paymenttype::where('payment_type_id','<>',4)->get();

			$beginventory = product::where('created_at','<',$start)
			->where(function ($query) use ($start, $end){
				$query->whereNull('selled_date')
							->orwhere('selled_date', '>', $start);
				})
			->get();


		$endinventory = product::where('created_at','<',$end)
			->where(function ($query) use ($end){
				$query->whereNull('selled_date')
					->orWhere('selled_date', '>', $end);
			})
			->get();

		$category = productcategory::where('status',1)->get();

		$purchase = purchaseorder::where('status',1)
			->where('purchase_order_state_id',1)
			->whereBetween('date_purchase_order',[$start,$end])
			->get();

		$expense = expense::where('status',1)
			->whereBetween('date_expense',[$start,$end])
			->get();

		$expensecategory = expensecategory::where('status',1)
			->get();

		Excel::create('Report Profit Loss', function($excel) use($sales, $paymenttype, $beginventory, $category, $purchase, $endinventory, $expense, $expensecategory){
			$excel->sheet('Report Biaya Harian', function($sheet) use ($sales, $paymenttype, $beginventory, $category, $purchase, $endinventory, $expense, $expensecategory){
				$sheet->loadView('report.profit_loss_excel',
					[
						'sales'=>$sales,
						'paymenttype'=>$paymenttype,
						'beginventory'=>$beginventory,
						'endinventory'=>$endinventory,
						'category'=>$category,
						'purchase'=>$purchase,
						'expense'=>$expense,
						'expensecategory'=>$expensecategory,
					]
				);
			});
		})->download('xls');
	}

	public function downloadReportStock()
	{
		$stock = product::with('brand.type')
			->with('detailgr.goodreceive.po.supplier')
			->where('status',1)
			->get();

		Excel::create('Report Stock '.Carbon::now()->format('d_m_Y'), function($excel) use($stock){
			$excel->sheet('Stock', function($sheet) use ($stock){
				$sheet->loadView('report.report_stock',['stock'=>$stock]);
			});
		})->download('xls');
	}
	public function downloadReportBrandOverview()
	{
//		$stock = product::with('brand.type')
//			->with('detailgr.goodreceive.po.supplier')
//			->where('status',1)
//			->get();
//
//		foreach($stock as $key => $value){
//			$date1 = Carbon::createFromFormat('Y-m-d H:i:s',$value->created_at);
//			$value['monthDiff'] = $date1->diffInMonths(Carbon::now());
//		}

		$brand = brandbarang::with('category')
			->where('status',1)
			->get();
		$main = [];

		foreach($brand as $key =>$value)
		{
			$data['category'] = $value->category->description;
			$data['name'] = $value->name;
			$data['id'] = $value->product_brand_id;
			$data['3months'] = product::where('product_brand_id',$value->product_brand_id)
				->where('status',1)
				->whereRaw("DATE(created_at) >= '".Carbon::now()->subMonth(3)->toDateString()."'")
				->count();
			$data['3-6months'] = product::where('product_brand_id',$value->product_brand_id)
				->where('status',1)
				->whereRaw("DATE(created_at) < '".Carbon::now()->subMonth(3)->toDateString()."'")
				->whereRaw("DATE(created_at) >= '".Carbon::now()->subMonth(6)->toDateString()."'")
				->count();
			$data['6-12months'] = product::where('product_brand_id',$value->product_brand_id)
				->where('status',1)
				->whereRaw("DATE(created_at) < '".Carbon::now()->subMonth(6)->toDateString()."'")
				->whereRaw("DATE(created_at) >= '".Carbon::now()->subMonth(12)->toDateString()."'")
				->count();
			$data['12-24months'] = product::where('product_brand_id',$value->product_brand_id)
				->where('status',1)
				->whereRaw("DATE(created_at) < '".Carbon::now()->subMonth(12)->toDateString()."'")
				->whereRaw("DATE(created_at) >= '".Carbon::now()->subMonth(24)->toDateString()."'")
				->count();
			$data['24months'] = product::where('product_brand_id',$value->product_brand_id)
				->where('status',1)
				->whereRaw("DATE(created_at) < '".Carbon::now()->subMonth(24)->toDateString()."'")
				->count();
			array_push($main, $data);
		}

		Excel::create('Report Brand Overview '.Carbon::now()->format('d_m_Y'), function($excel) use($main){
			$excel->sheet('Stock', function($sheet) use ($main){
				$sheet->loadView('report.report_stock_overview',['data'=>$main]);
			});
		})->download('xls');
	}

	public function showReportExistingReceiveableTransaction(Request $request){
		$transaction = existingreceiveabletransaction::with('customer')
		->where('status',1);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$transaction = $transaction->whereBetween('transaction_date',[$start, $end]);
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
			$transaction = $transaction->whereBetween('transaction_date',[$start, $end]);
		}

		$customer;
		if($request->customer != 0)
		{
			$customer = customer::where('customer_id', $request->customer)->get();
			$transaction = $transaction->whereHas('customer', function($query) use ($request){
				$query->where('customer_id', $request->customer);
			});
		}else{
			$customer = customer::where('status',1)->get();
		}

		$transaction = $transaction->get();

		if(($request->start && $request->end)||$request->customer)
		{
			return view('report.report_existing_receiveable_detail', compact('transaction', 'customer'));
		}

		$all_customer = customer::where('status',1)->get();
		return view('report.report_existing_receiveable', compact('transaction','all_customer', 'customer'));
	}

	public function downloadReportExistingReceiveableTransaction(Request $request){
		$transaction = existingreceiveabletransaction::with('customer')
		->where('status',1);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$transaction = $transaction->whereBetween('transaction_date',[$start, $end]);
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
			$transaction = $transaction->whereBetween('transaction_date',[$start, $end]);
		}
		if($request->customer != 0)
		{
			$customer = $request->customer;
			$transaction = $transaction->whereHas('customer', function($query) use ($customer){
				$query->where('customer_id', $customer);
			});
		}

		$transaction = $transaction->get();

		Excel::create('Report Existing Receiveable Transaction', function($excel) use($transaction){
			$excel->sheet('Report Purchase Order', function($sheet) use ($transaction){
				$sheet->loadView('report.existing_receiveable_excel',['report'=>$transaction]);
			});
		})->download('xls');

	}
}