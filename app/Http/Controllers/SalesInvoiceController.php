<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\salesinvoice;
use App\detailsalesinvoice;
use App\Traits\NumberToLetterTrait;
use App\salesorder;
use App\log;
use App\customer;
use App\product;
use DOMPDF;
use DB;
use Illuminate\Support\Facades\Mail;

class SalesInvoiceController extends Controller
{
	use NumberToLetterTrait;
	public function showSalesInvoicePage()
	{
		/*select sales_order.sales_order_id, sales_order.grand_total_idr, tableb.total_paid from sales_order left join (SELECT sales_order.sales_order_id, sales_order.sales_order_number, sales_order.grand_total_idr, sum(total_amount) as total_paid FROM sales_order LEFT join invoice_sales on sales_order.sales_order_id = invoice_sales.sales_order_id LEFT JOIN invoice_sales_details on invoice_sales.invoice_sales_id = invoice_sales_details.invoice_sales_id GROUP by sales_order_id) tableb on tableb.sales_order_id = sales_order.sales_order_id*/
        
		$so = salesorder::leftjoin(DB::raw(
            '(SELECT sales_order.sales_order_id, sales_order.sales_order_number, sales_order.grand_total_idr, sum(invoice_sales.total_amount) as total_paid 
            FROM sales_order 
            LEFT join invoice_sales on sales_order.sales_order_id = invoice_sales.sales_order_id 
            where sales_order.status <> 2 and invoice_sales.status <> 2
            GROUP by sales_order_id) tableb'
        ),'sales_order.sales_order_id','=','tableb.sales_order_id')
        ->select('sales_order.sales_order_id', 'sales_order.sales_order_number', 'sales_order.grand_total_idr','tableb.total_paid')
        ->where('sales_order.status','<>',2)
        ->Where(function ($query) {
            $query->whereRaw('total_paid  < sales_order.grand_total_idr - reduction')
                ->orWhereNull('total_paid');
        })
        ->get();


        // ->whereRaw('total_paid  < sales_order.grand_total_idr - reduction')
        // ->orWhereNull('total_paid')
        $invoice = salesinvoice::leftjoin('sales_order','sales_order.sales_order_id','=','invoice_sales.sales_order_id')
            ->leftjoin('customer','customer.customer_id','=','sales_order.customer_id')
            ->where('invoice_sales.status','<>',2)
            ->get();
		return view('sales_invoice.sales_invoice', compact('so','invoice'));
	}

	public function getLastInvoiceNumber()
    {
    	$lastinvoicetoday = salesinvoice::where('invoice_sales_number','like','SIN/'.date('dmy').'/%')->orderby('invoice_sales_id','desc')->first();
    	if(empty($lastinvoicetoday))
    	{
    		$newinvoicenumber = "SIN/".date('dmy')."/1";
    		return $newinvoicenumber;
    	}
    	else{
    		$tmpinvoice = explode('/',$lastinvoicetoday->invoice_sales_number); 
    		$lastnumber = $tmpinvoice[2];
    		$newinvoicenumber = "SIN/".date('dmy')."/".($lastnumber+1);
    		return $newinvoicenumber;
    	}
    }

    public function getSalesInvoice(Request $request)
    {
        /*select invoice_sales.*, invoice_sales_details.total_amount, sales_order.grand_total_idr, tablea.total from `invoice_sales` left join `invoice_sales_details` on `invoice_sales`.`invoice_sales_id` = `invoice_sales_details`.`invoice_sales_id` LEFT JOIN sales_order on sales_order.sales_order_id = invoice_sales.sales_order_id LEFT JOIN (SELECT sales_order.sales_order_id, SUM(invoice_sales_details.total_amount) as total FROM sales_order LEFT JOIN invoice_sales on sales_order.sales_order_id = invoice_sales.sales_order_id LEFT JOIN invoice_sales_details on invoice_sales.invoice_sales_id = invoice_sales_details.invoice_sales_details_id WHERE sales_order.sales_order_id = 14)tablea on sales_order.sales_order_id = tablea.sales_order_id where invoice_sales.invoice_sales_id = 2*/
        $so = salesinvoice::where('invoice_sales_id',$request->id)->first()->sales_order_id;
        $invoice = salesinvoice::leftjoin('sales_order','sales_order.sales_order_id','=','invoice_sales.sales_order_id')
            ->leftjoin(DB::Raw(
                '(SELECT sales_order.sales_order_id, SUM(invoice_sales.total_amount) as total 
                FROM sales_order 
                LEFT JOIN invoice_sales on sales_order.sales_order_id = invoice_sales.sales_order_id 
                WHERE sales_order.sales_order_id ='.$so.')tablea'
            ),'sales_order.sales_order_id','=','tablea.sales_order_id')
            ->leftjoin('customer','customer.customer_id','=','sales_order.customer_id')
            ->leftjoin('payment_type','payment_type.payment_type_id','=','sales_order.payment_type_id')
            ->where('invoice_sales.invoice_sales_id', $request->id)
            ->select('invoice_sales.*','sales_order.grand_total_idr','sales_order.reduction','tablea.total','customer.*','sales_order.sales_order_id','sales_order.sales_order_number','payment_type.payment_description')
            ->first();
            
        return $invoice;
    }

    public function getSalesOrderData(Request $request)
    {
        if($request->id == 0)
        {
            return;
        }
    	$so = salesorder::leftjoin('customer','customer.customer_id','=','sales_order.customer_id')
            ->leftjoin('payment_type','payment_type.payment_type_id','=','sales_order.payment_type_id')
            ->leftjoin('invoice_sales','invoice_sales.sales_order_id','=','sales_order.sales_order_id')
            ->selectraw('sales_order.sales_order_number, sales_order.customer_id, sales_order.grand_total_idr , sum(invoice_sales.total_amount) as total, sales_order.customer_id, sales_order.reduction, company_name,payment_description')
            ->where('sales_order.sales_order_id',$request->id)
            ->where('invoice_sales.status','<>',2)
            ->first();
    	/*SELECT sales_order.sales_order_id, sales_order.sales_order_number, sales_order.grand_total_idr, sum(total_amount) as total_paid FROM sales_order LEFT join invoice_sales on sales_order.sales_order_id = invoice_sales.sales_order_id LEFT JOIN invoice_sales_details on invoice_sales.invoice_sales_id = invoice_sales_details.invoice_sales_id*/
    	return $so;
    }

    public function createInvoice(Request $request)
    {
        $salesinvoice = new salesinvoice;
        $salesinvoice->sales_order_id = $request->id;
        $salesinvoice->invoice_sales_number = $request->number;
        $salesinvoice->total_amount = $request->payment;
        $salesinvoice->status = 1;
        $salesinvoice->save();
        
        customer::where('customer_id',$request->idcustomer)->update([
            'total_balance' => DB::raw('total_balance-'.$request->payment), 
            ]);

        $log = new log;
        $log->actor = "andy.daz";
        $log->activity = "menambahkan";
        $log->object = "Sales Invoice";
        $log->object_details = $salesinvoice->invoice_sales_number;
        $log->status = 1;
        $log->save();
    }

    public function updateInvoice(Request $request)
    {
        $salesinvoice = salesinvoice::find($request->idinvoice);

        customer::where('customer_id',$request->idcustomer)->update([
            'total_balance' => DB::raw('total_balance+'.$salesinvoice->total_amount), 
            ]);

        $salesinvoice->total_amount = $request->payment;
        $salesinvoice->save();
        
        customer::where('customer_id',$request->idcustomer)->update([
            'total_balance' => DB::raw('total_balance-'.$salesinvoice->total_amount), 
            ]);


        $log = new log;
        $log->actor = "andy.daz";
        $log->activity = "mengubah";
        $log->object = "Sales Invoice";
        $log->object_details = $salesinvoice->invoice_sales_number;
        $log->status = 1;
        $log->save();
    }

    public function deleteInvoice(Request $request)
    {
    	$salesinvoice = salesinvoice::find($request->id);

        customer::where('customer_id',$request->idcustomer)->update([
            'total_balance' => DB::raw('total_balance+'.$salesinvoice->total_amount), 
            ]);

        salesinvoice::where('invoice_sales_id',$request->id)->update(["status"=>2]);
        

        $log = new log;
        $log->actor = "andy.daz";
        $log->activity = "menghapus";
        $log->object = "Sales Invoice";
        $log->object_details = $salesinvoice->invoice_sales_number;
        $log->status = 1;
        $log->save();
    }

    public function sendMail(Request $request, $id)
		{
			$customer = salesinvoice::with('so')
				->where('invoice_sales_id',$id)
				->first()
				->so
				->customer;

			$invoicedue = customer::with('dueinvoice')
				->where('customer_id', $customer->customer_id)
				->first()
				->dueinvoice;


			$terbilang = $this->terbilang($invoicedue->sum('total_amount'));

			$email = $customer->email;
			$pdf = DOMPDF::loadView('sales_payment.receiveable_reminder_pdf', compact('invoicedue','customer','terbilang'));

			Mail::send('emails.invoice', ['customer'=> $customer], function($message) use ($email, $pdf){
				$message->from('adiprawiro@ratnaboutique.com','No Reply');
				$message->to($email);
				$message->subject('Jatuh Tempo Reminder');
				$message->attachData($pdf->stream(),'Surat Penagihan Piutang.pdf');
			});

			salesinvoice::where('invoice_sales_id',$id)
				->update([
				'is_emailed' => 1,
			]);

			return $email;
		}

	public function testMail(Request $request, $id)
	{
		$customer = customer::where('customer_id',$id)->first();
		$email = $customer->email;
		Mail::send('emails.testmail', ['customer'=> $customer], function($message) use ($email){
			$message->from('adiprawiro@ratnaboutique.com','No Reply');
			$message->to($email);
			$message->subject('Test Email');
		});
		return $email;
	}
	
}