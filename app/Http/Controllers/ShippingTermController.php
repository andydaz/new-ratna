<?php

namespace App\Http\Controllers;

use App\shippingterm;
use Illuminate\Http\Request;

class ShippingTermController extends Controller
{
	//
	public function showShippingTermPage(){
		$shippingterm = shippingterm::where('status',1)->get();
		return view('shippingterm.shippingterm',compact('shippingterm'));
	}

	public function createShippingTerm(Request $request){
		$shippingterm = new shippingterm;
		$shippingterm->shipping_term_name = $request->name;
		$shippingterm->status = 1;
		$shippingterm->save();
	}

	public function getShippingTermData(Request $request){
		$shippingterm = shippingterm::find($request->id);
		return $shippingterm;
	}

	public function modalShippingTermData(Request $request){
		$id= $request->id;
		$data = shippingterm::where('shipping_term_id',$id)->first();
		return view('shippingterm.modal_shippingterm', compact('data'));
	}

	public function updateShippingTerm(Request $request){
		$shippingterm = shippingterm::find($request->id);
		$shippingterm->shipping_term_name = $request->name;
		$shippingterm->update();
	}

	public function deleteShippingTerm(Request $request){
		$shippingterm = shippingterm::find($request->id);
		$shippingterm->status = 2;
		$shippingterm->update();
	}
}
