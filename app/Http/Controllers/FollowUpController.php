<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\salesinvoice;
use App\customer;
use App\followup;
use DB;
use Carbon\Carbon;
use Excel;

class FollowUpController extends Controller
{
	public function showFollowUp(){
		return view('followup.followup');
	}

	public function showData(Request $request){
// 		SELECT is1.sales_order_id, pr.bar_code, ps.payment_sales_id, ps.date_sales_payment as last_payment_date, DATEDIFF(CURDATE(), is3.due_date) as aging, is4.total_due
// FROM invoice_sales as is1
// JOIN sales_order as so on is1.sales_order_id = so.sales_order_id
// JOIN (SELECT * FROM ( SELECT sales_order_id, payment_sales_id, date_sales_payment From payment_sales ORDER BY payment_sales_id DESC) AS sub GROUP BY payment_sales_id) as ps on ps.sales_order_id = is1.sales_order_id
// JOIN ( SELECT invoice_sales_id, sales_order_id, due_date FROM invoice_sales where payment_sales_id is null GROUP BY sales_order_id) as is3 on is1.sales_order_id = is3.sales_order_id
// LEFT JOIN (select sum(total_amount) as total_due, sales_order_id from invoice_sales where payment_sales_id is Null AND due_date < CURDATE() GROUP BY sales_order_id) AS is4 on is1.sales_order_id = is4.sales_order_id
// JOIN product as pr on pr.product_id = is1.product_id
// where exists (
// 	select * from invoice_sales as is2
//     WHERE is1.invoice_sales_id = is2.invoice_sales_id
//     AND is2.status = 1
//     AND is2.payment_sales_id IS Null
//     AND is2.product_id IS NOT Null
// )
// GROUP BY is1.product_id
		// $sub1 = DB::query()->fromSub(function ($query) {
  //   				$query->selectRaw('*')->fromSub(function ($query2) {
  //   					$query2->selectRaw('sales_order_id, payment_sales_id, date_sales_payment')
  //   							->from('payment_sales')
  //   							->orderBy('payment_sales_id', 'desc');
  //   				}, 'sub')->groupby('payment_sales_id');
		// 		}, 'ps');
		$data = [];
		if($request->type == "aging" || $request->type == "date" ){
			$data = $this->getQuery();

			if($request->type == 'aging'){
				$data = $data->having('aging', '=', $request->aging)->orderBy('upcoming_due.total_due', 'desc');
			}else if($request->type == 'date'){
				$data = $data->whereRaw("DATE_FORMAT(sales_order.date_sales_order, '%d') = ".date('d', strtotime($request->date)));
			}

			$data = $data->get();
		}

		if($request->type)
		{
			return view('followup.followup_detail', compact('data'));
		}
		return view('followup.followup', compact('data'));
	}

	public function downloadFollowupByAging(Request $request){
		$data = $this->getQuery()
				->with('so.followup')
				->having('aging', '=', $request->aging)->orderBy('upcoming_due.total_due', 'desc')
				->get();

		// return view('followup.followup_excel', ['data' => $data]);
		Excel::create('Report Follow Up By Aging '. $request->aging, function($excel) use($data){
			$excel->sheet('Report Follow Up By Aging', function($sheet) use ($data){
				$sheet->loadView('followup.followup_excel', ['data'=>$data]);
			});
		})->download('xls');
	}

	public function downloadFollowupByDate(Request $request){
		$data = $this->getQuery()
				->with('so.followup')
				->whereRaw("DATE_FORMAT(sales_order.date_sales_order, '%d') = ".date('d', strtotime($request->date)))
				->get();

		// return view('followup.followup_excel', compact('data'));
		Excel::create('Report Follow Up By Date '. $request->date, function($excel) use($data){
			$excel->sheet('Report Follow Up By Date', function($sheet) use ($data){
				$sheet->loadView('followup.followup_excel', ['data'=>$data]);
			});
		})->download('xls');
	}

	public function showWaContent(Request $request){
		$id = $request->id;
		$customer = customer::where('customer_id',$id)->first();
		return view('followup.wa_content', compact('customer'));
	}

	public function getFollowUpNotes(Request $request){
		$id = $request->id;
		$notes = followup::with(['so', 'invoice.product.category', 'invoice.product.brand' ])->where('sales_order_id', $id)->get();
		return view('followup.notes_content', compact('notes'));
	}

	public function getFollowUpDetail(Request $request){
		$id = $request->id;
		$notes = followup::find($id);
		return $notes;
	}

	public function createFollowUpNotes(Request $request){

		$followup = new followup();
		$followup->sales_order_id = $request->soId;
		$followup->invoice_sales_id = $request->invoiceId;
		$followup->notes = nl2br($request->notes);
		$followup->latest_aging = $request->latestAging;
		$followup->type = $request->type;
		$followup->status = 1;
		$followup->save();

		return $followup;
	}

	public function updateFollowUpNotes(Request $request){

		$followup = followup::find($request->id);
		$followup->notes = nl2br($request->notes);
		$followup->save();

		return $followup;
	}

	private function getQuery(){
		$sub1 = DB::query()->fromSub(function ($query) {
    				$query->selectRaw('sub.*, sum(total_amount) total_paid')->fromSub(function ($query2) {
    					$query2->selectRaw('invoice_sales.invoice_sales_id, invoice_sales.product_id, invoice_sales.sales_order_id, invoice_sales.total_amount,invoice_sales.payment_sales_id, term_number, payment_sales.date_sales_payment')
    							->from('invoice_sales')
    							->join('payment_sales','payment_sales.payment_sales_id','=','invoice_sales.payment_sales_id')
    							->whereNotNull('invoice_sales.payment_sales_id')
    							->where('invoice_sales.status', 1)
    							->orderBy('invoice_sales.invoice_sales_id', 'desc');
    				}, 'sub')->groupby('product_id');
				}, 'paid');

			$sub2 = DB::query()->fromSub(function ($query) {
	    				$query->selectRaw('invoice_sales_id, sales_order_id, due_date')
	    					->from('invoice_sales')
	    					->whereNull('payment_sales_id')
	    					->where('due_date', '<', DB::Raw('CURDATE()'))
	    					->groupby('sales_order_id');
					}, 'unpaid');

			$sub3 = DB::query()->fromSub(function ($query) {
	    				$query->selectRaw('sum(total_amount) as total_due, sales_order_id')
	    					->from('invoice_sales')
	    					->whereNull('payment_sales_id')
	    					->where('due_date', '<', Carbon::now()->toDateString())
	    					->groupby('sales_order_id');
					}, 'upcoming_due');

			$query = salesinvoice::with(['so.customer', 'product.brand.category'])
						->selectRaw('invoice_sales.*,
							CASE
							    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 0 THEN "0 Bulan"
							    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 30 THEN "1 Bulan"
							    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 60 THEN "1-2 Bulan"
							    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 90 THEN "2-3 Bulan"
							    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 180 THEN "3-6 Bulan"
							    WHEN DATEDIFF(CURDATE(), unpaid.due_date) <= 360 THEN "6-12 Bulan"
							    ELSE ">1 Tahun"
							END AS aging, upcoming_due.total_due')
						->join('sales_order', 'sales_order.sales_order_id', '=', 'invoice_sales.sales_order_id')
						->joinSub($sub1, 'paid', function($join){
							$join->on('paid.sales_order_id','=','invoice_sales.sales_order_id');
						})
						->joinSub($sub2, 'unpaid', function($join){
							$join->on('unpaid.sales_order_id','=','invoice_sales.sales_order_id');
						})
						->leftJoinSub($sub3, 'upcoming_due', function($join){
							$join->on('upcoming_due.sales_order_id','=','invoice_sales.sales_order_id');
						})
						->whereExists(function ($query) {
							$query->select(DB::raw(1))
								->from('invoice_sales as is_1')
								->where('status',1)
								->whereNull('payment_sales_id')
								->whereNotNull('product_id')
								->whereRaw('is_1.invoice_sales_id = invoice_sales.invoice_sales_id');
						})
						->groupby('invoice_sales.product_id');
		return $query;
	}
}