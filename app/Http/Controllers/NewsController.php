<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\news;

class NewsController extends Controller
{
    public function showNewsPage()
    {	
    	$news = news::paginate(10);
    	return view('news.news', compact('news'));
    }
    public function createNews(Request $request)
    {

    	$news = new news;
    	$news->title = $request->title;
    	$news->date = date('Y-m-d',strtotime($request->tanggal));
    	$news->description = $request->deskripsi;
    	$news->status = 1;
    	$news->save();
    }
    public function getNewsData(Request $request){
    	$id = $request->id;
    	$news = news::where('news_id',$id)->select('*')->first();
    	return $news;
    }

    public function modalNewsData(Request $request){
    	$id= $request->id;
    	$data= news::where('news_id',$id)->first();
    	return view('news.modal_news', compact('data'));
    }

    public function deleteNews(Request $request)
    {
    	$id = $request->id;
    	news::where('news_id',$id)->delete();
    }

    public function updateNews(Request $request)
    {
    	$id = $request->id;
    	news::where('news_id', $id)->update(
    		[
    		'title' => $request->title,
    		'date' => date('Y-m-d',strtotime($request->tanggal)),
    		'description' => $request->deskripsi,
    		]
    		);
    }
}