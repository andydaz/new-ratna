<?php

namespace App\Http\Controllers;

use App\customerstate;
use App\Http\Controllers\CustomerStateController;
use App\salesorder;
use App\detailsalesorder;
use App\salesinvoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\customer;
use App\customerlevel;
use App\userbank;
use App\bank;
use App\salespayment;
use Indonesia;
use Image;
use DB;
use DataTable;

class PelangganController extends CustomerStateController
{
	public function showPelangganPage()
	{
		$customer = customer::where('status',1)->get();
		$provinsi = Indonesia::allProvinces();
		$level = customerlevel::where('status','<>',2)->get();
		$bank = bank::all();
		$blocked = customer::where('status',3)->get();

		return view('pelanggan.pelanggan', compact('customer','level','provinsi','bank','blocked'));
	}

	public function getPelanggantable(Request $request)
	{
		$customer = customer::where('status',1);

		if($request->nama && $request->nama != "")
		{
			$customer = $customer->havingRaw('Concat(customer.first_name," ",customer.last_name) like "%'.$request->nama.'%"');
		}
		if($request->booknumber && $request->booknumber != "")
		{
			$customer = $customer->where('book_number','like', '%'.$request->booknumber.'%');
		}
		if($request->phone && $request->phone != "")
		{
			$customer = $customer->where('phone','like', '%'.$request->phone.'%');
		}
//		return $customer;

		return DataTable::of($customer)
			->setRowAttr([
				'value' => function($customer){
					return $customer->customer_id;
				},
			])
			->addColumn('action', function ($customer) {
				if (Session('roles')->name == 'master' && $customer->customer_id != 1) {
					return '<a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
					<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
					<a href="#" class="btn btn-danger block-pelanggan"><i class="material-icons">block</i></a>';
				}else if (Session('roles')->name == 'Admin Sales' && $customer->customer_id != 1) {
					return '<a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
					<a href="#" class="btn btn-danger block-pelanggan"><i class="material-icons">block</i></a>';
				}else if($customer->customer_id != 1)
				{
					return '<a href="#" class="btn btn-danger block-pelanggan"><i class="material-icons">block</i></a>';
				}
			})
			->addColumn('fullname', function ($customer) {
					return $customer->FullName;
			})
			->smart(false)
			->make(true);
	}

	public function modalPelangganData(Request $request){
		$id= $request->id;
		$data= customer::where('customer_id',$id)->first();

		return view('pelanggan.modal_pelanggan', compact('data'));
	}

	public function getPelangganData(Request $request){
		$id = $request->id;
		$customer = customer::with('level')
			->leftjoin('user_bank','user_bank.customer_id','=','customer.customer_id')
			->leftjoin('indonesia_cities as ic','ic.id','=','customer.city_id')
			->leftjoin('indonesia_provinces as ip','ip.id','=','ic.province_id')
			->where('customer.customer_id',$id)
			->select('customer.*','user_bank.user_bank_id', 'ip.id as provinsi')
			->first();

		$customer_state = customerstate::where('customer_id',$id)
			->orderBy('customer_state_id','desc')
			->first()
			->customer_state_id;

		$so = salesorder::where('customer_state_id',$customer_state)
			->where('status',1)
			->get();

		$total_this_state = $so->sum('grand_total_idr');




		$sub = DB::query()->fromSub(function ($query) {
	    				$query->selectRaw('invoice_sales_id, sales_order_id, due_date')
	    					->from('invoice_sales')
	    					->whereNull('payment_sales_id')
	    					->where('due_date', '<', DB::Raw('CURDATE()'))
	    					->groupby('sales_order_id');
					}, 'unpaid');

		return compact('customer','total_this_state');
	}

	public function getCashHistoryTransaction(Request $request){
		$id = $request->id;
		$history_cash =
			detailsalesorder::with(['so.payment_type', 'product.brand', 'product.category', 'product.type'])
				->leftjoin('sales_order', 'sales_order.sales_order_id', '=', 'sales_order_details.sales_order_id')
				->where('sales_order.status', 1)
				->where('sales_order.payment_type_id','<>',2)
				->where('sales_order.customer_id', $id)
				->get();

		$view = view('pelanggan.so_cash_content', compact('history_cash'))->render();
		return compact('view');
	}

	public function getCreditHistoryTransaction(Request $request){
		$id = $request->id;
		$history_credit = salesinvoice::selectRaw('invoice_sales.*,
							CASE
							    WHEN DATEDIFF((CASE WHEN invoice_sales.payment_sales_id IS NULL THEN CURDATE() ELSE payment_sales.date_sales_payment END), invoice_sales.due_date) <= 0 THEN "0 Bulan"
							    WHEN DATEDIFF((CASE WHEN invoice_sales.payment_sales_id IS NULL THEN CURDATE() ELSE payment_sales.date_sales_payment END), invoice_sales.due_date) <= 30 THEN "1 Bulan"
							    WHEN DATEDIFF((CASE WHEN invoice_sales.payment_sales_id IS NULL THEN CURDATE() ELSE payment_sales.date_sales_payment END), invoice_sales.due_date) <= 60 THEN "1-2 Bulan"
							    WHEN DATEDIFF((CASE WHEN invoice_sales.payment_sales_id IS NULL THEN CURDATE() ELSE payment_sales.date_sales_payment END), invoice_sales.due_date) <= 90 THEN "2-3 Bulan"
							    WHEN DATEDIFF((CASE WHEN invoice_sales.payment_sales_id IS NULL THEN CURDATE() ELSE payment_sales.date_sales_payment END), invoice_sales.due_date) <= 180 THEN "3-6 Bulan"
							    WHEN DATEDIFF((CASE WHEN invoice_sales.payment_sales_id IS NULL THEN CURDATE() ELSE payment_sales.date_sales_payment END), invoice_sales.due_date) <= 360 THEN "6-12 Bulan"
							    ELSE ">1 Tahun"
							END AS aging')
			->with(['so', 'payment', 'product.brand', 'product.category', 'product.type'])
			->leftjoin('payment_sales', 'payment_sales.payment_sales_id', '=', 'invoice_sales.payment_sales_id')
			->leftjoin('sales_order', 'sales_order.sales_order_id', '=', 'invoice_sales.sales_order_id')
			->where('sales_order.status', 1)
			->where('sales_order.payment_type_id', 2)
			->where('sales_order.customer_id', $id);

		if($request->aging && $request->aging != ""){
			$history_credit = $history_credit->having('aging', '=', $request->aging);
		}
		$history_credit = $history_credit->get();

		$view = view('pelanggan.so_cicil_content', compact('history_credit'))->render();
		return compact('view');
	}

	public function validPelangganPhone(Request $request){
		$customer = customer::where('status',1)
			->Where(function ($query) use ($request){
				$query->orWhere('phone',$request->phone)
					->orWhere('phone2',$request->phone);
			});
		if($request->customer != "")
		{
			$customer = $customer->where('customer_id','<>',$request->customer);
		}
		$customer= $customer->count();

		if($customer > 0 ){
			return 1;
		}else{
			return 0;
		}
	}

	public function validPelangganNik(Request $request){
		$customer = customer::where('status',1)
			->where('nik',$request->nik);

		if($request->customer != "")
		{
			$customer = $customer->where('customer_id','<>',$request->customer);
		}

		$customer = $customer->count();

		if($customer > 0 ){
			return 1;
		}else{
			return 0;
		}
	}

	public function getPelangganInfo(Request $request){
		$id = $request->id;

		$customer = customer::with('laststate.level')
			->with('unsettled_sales.payment')
			->where('customer.customer_id',$id)
			->first();
		$totalsounsettled = $customer->unsettled_sales->sum('grand_total_idr');
		$totalpaymentunsettled = 0;

		foreach ($customer->unsettled_sales as $key => $value)
		{
			$payment = $value->payment->sum('total_paid');
			$totalpaymentunsettled += $payment;
		}

		$totalremaining = $totalsounsettled - $totalpaymentunsettled;

		return compact('customer','totalremaining');
	}

	public function getCustomerBalance(Request $request)
	{
		return customer::where('customer_id',$request->id)->first()->total_balance;
	}

	public function deletePelanggan(Request $request)
	{
		$id = $request->id;
		customer::where('customer_id',$id)->update(['status'=>2]);
	}

	public function createPelanggan(Request $request)
	{
		$customer = new customer;
		$customer->account_id = Session::get('user')->account_id;
		$customer->city_id = $request->kota ;
		$customer->customer_level_id = $request->level;
		$customer->first_name = $request->namadepan;
		$customer->last_name = $request->namabelakang;
		$customer->book_number = $request->booknumber;
		$customer->email = $request->email;
		$customer->nik = $request->nik;
		$customer->address = $request->alamat;
		$customer->postal_code = $request->kodepos;
		$customer->phone = $request->telp1;
		$customer->phone2 = $request->telp2;
		$customer->fax = $request->fax;
		$customer->check_date = Carbon::now()->addYear();
		$customer->status = 1;
		$customer->save();

		if($request->hasFile('identity')){
			$identity = $request->file('identity');
//			$filename = 'identity-'.$customer->customer_id.'.jpg';
			$filename =  $name = rand(11111, 99999) . '.' . $identity->getClientOriginalExtension();
			$path = public_path().'/customer/'.$customer->customer_id;
			if (!file_exists($path)) {
				mkdir($path,0777,true);
			}
			Image::make($identity)->resize(360,270)->save($path.'/'.$filename);
			$customer->identity = 'customer/'.$customer->customer_id.'/'.$filename;
			$customer->save();
		}

		$this->createCustomerState($customer->customer_id, 1, 'initial', Carbon::now()->addYear()->toDateString());
	}

	public function updatePelanggan(Request $request)
	{
		$id = $request->id;
		customer::where('customer_id', $id)->update(
			[
				'city_id' => $request->kota,
				'email' => $request->email,
				'customer_level_id' => $request->level,
				'first_name' => $request->namadepan,
				'last_name' => $request->namabelakang,
				'book_number' => $request->booknumber,
				'address' => $request->alamat,
				'postal_code' => $request->kodepos,
				'phone' => $request->telp1,
				'phone2' => $request->telp2,
				'fax' => $request->fax,
			]
		);

		$customer = customer::find($id);

		if($request->hasFile('identity')){
			$identity = $request->file('identity');
			//			$filename = 'identity-'.$customer->customer_id.'.jpg';
			$filename =  $name = rand(11111, 99999) . '.' . $identity->getClientOriginalExtension();
			$path = public_path().'/customer/'.$customer->customer_id;
			if (!file_exists($path)) {
				mkdir($path,0777,true);
			}else{
				array_map('unlink', glob($path."/*"));
			}
			Image::make($identity)->resize(360,270)->save($path.'/'.$filename);
			$customer->identity = 'customer/'.$customer->customer_id.'/'.$filename;
			$customer->save();
		}
		if($request->hasFile('avatar')){
			$avatar = $request->file('avatar');
			//			$filename = 'identity-'.$customer->customer_id.'.jpg';
			$filename =  $name = rand(11111, 99999) . '.' . $avatar->getClientOriginalExtension();
			$path = public_path().'/avatar/'.$customer->customer_id;
			if (!file_exists($path)) {
				mkdir($path,0777,true);
			}else{
				array_map('unlink', glob($path."/*"));
			}
			Image::make($avatar)->resize(360,270)->save($path.'/'.$filename);
			$customer->avatar = 'avatar/'.$customer->customer_id.'/'.$filename;
			$customer->save();
		}
		return $customer->customer_id;
	}

	public function blockPelanggan(Request $request){
		$customer = customer::find($request->id);
		$customer->block_reason = $request->reason;
		$customer->status = 3;
		$customer->save();
	}

	public function unblockPelanggan(Request $request){
		$customer = customer::find($request->id);
		$customer->block_reason = Null;
		$customer->status = 1;
		$customer->save();
	}

	public function showBalancePage(Request $request)
	{
		$data['customer'] = customer::all();
		return view('pelanggan.balance', compact('data'));
	}

	public function showBalanceDetail(Request $request)
	{
		$invoice = customer::join('sales_order','sales_order.customer_id','=','customer.customer_id')->leftjoin('invoice_sales','invoice_sales.sales_order_id','=','sales_order.sales_order_id')->where('customer.customer_id',$request->id)->select('invoice_sales_number as description','invoice_sales.total_amount','invoice_sales.created_at as date', DB::raw("'invoice' as 'source'"));
		$balance = salespayment::where('customer_id',$request->id)->select('payment_sales.payment_sales_number as description','payment_sales.total_paid as total_amount','payment_sales.created_at as date' , DB::Raw("'payment' as 'source'"))->union($invoice)->orderby('date')->get();
		// return $balance;
		$view = view('pelanggan.detail-balance', compact('balance'))->render();
		$totalbalance = customer::where('customer.customer_id',$request->id)->first()->total_balance;
		return compact('view','totalbalance');
	}

	public function customerLevel($id){
		//Ambil Total SO 3 bulan Terakhir untuk update levelnya
		$totalLast3MonthSo = salesorder::Last3Month()->where('customer_id',$id)->sum('grand_total_idr');
		$totalLast2MonthSo = salesorder::Last2Month()->where('customer_id',$id)->sum('grand_total_idr');
		$totalLast1Monthso = salesorder::Last1Month()->where('customer_id',$id)->sum('grand_total_idr');

		$limit = customer::leftjoin('customer_level','customer.customer_level_id','=','customer_level.customer_level_id')
			->where('customer_id',$id)
			->first();

		$currentLevel = customer::where('customer_id',$id)->first()->customer_level_id;
		$currentLowestLimit = $limit->nominal_from;
		$currentHighestLimit = $limit->nominal_end;

		if($totalLast3MonthSo < $currentLowestLimit){
			$newLevel = max(1, ($currentLevel - 3));
			customer::where('customer_id',$id)->update([
				'customer_level_id' => $newLevel,
			]);
		}else{
			if($totalLast2MonthSo < $currentLowestLimit){
				$newLevel = max(1, ($currentLevel - 2));
				customer::where('customer_id',$id)->update([
					'customer_level_id' => $newLevel,
				]);
			}else{
				if($totalLast1Monthso < $currentLowestLimit){
					$newLevel = max(1, ($currentLevel - 1));
					customer::where('customer_id',$id)->update([
						'customer_level_id' => $newLevel,
					]);
				}else if($totalLast1Monthso > $currentHighestLimit){
					$newLevel = $currentLevel + 1;
					customer::where('customer_id',$id)->update([
						'customer_level_id' => $newLevel,
					]);
				}
			}
		}
	}

	public function checkLevelUpMember($id)
	{
		$minlevelup = customer::where('customer_id',$id)
			->first()
			->level()
			->level_up_limit;

			return $minlevelup;
	}
}
