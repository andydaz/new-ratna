<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\salesinvoice;
use App\customer;
use App\followup;
use DB;
use Carbon\Carbon;
use Excel;

class OutstandingReceiveableController extends Controller
{
	private function getQuery($year, $month){
		$sub = DB::query()->fromSub(function ($query) {
	    				$query->selectRaw('sales_order_id, due_date, SUM(total_amount) as unpaid_amount')
	    					->from('invoice_sales')
	    					->whereNull('payment_sales_id')
	    					->where('due_date', '<=', DB::Raw('CURDATE()'))
	    					->groupBy('sales_order_id');
					}, 'unpaid');

		$sub1 = DB::query()->fromSub(function ($query) {
	    				$query->selectRaw('sales_order_id, term_number, SUM(total_amount) as paid_amount')
	    					->from('invoice_sales')
	    					->whereNotNull('payment_sales_id')
	    					->groupBy('sales_order_id');
					}, 'paid');


		$query = salesinvoice::with(['so.customer'])
			->selectRaw('invoice_sales.*, SUM(invoice_sales.total_amount) as total_receiveable, COALESCE(unpaid.unpaid_amount, 0) as unpaid_amount, COALESCE(paid.paid_amount, 0) as paid_amount')
			->join('sales_order', 'invoice_sales.sales_order_id', '=', 'sales_order.sales_order_id')
			->leftjoinSub($sub, 'unpaid', function($join){
				$join->on('unpaid.sales_order_id','=','invoice_sales.sales_order_id');
			})
			->leftjoinSub($sub1, 'paid', function($join){
				$join->on('paid.sales_order_id','=','invoice_sales.sales_order_id');
			})
			->where('invoice_sales.status','=',1)
			->whereYear('sales_order.date_sales_order', '=', $year)
			->where('sales_order.payment_type_id', '=' ,'2')
			->groupBy('sales_order.sales_order_id');


			if ($month){
				$query = $query->whereMonth('sales_order.date_sales_order', '=', $month);
			}

		return $query;
	}

	public function showOutstandingReceiveable(Request $request){

		$year = $request->year ? $request->year : Carbon::now()->year;
		$query =
		$data = [];
		$data['january'] = $this->getQuery($year, "1")->get();
		$data['february'] = $this->getQuery($year, "2")->get();
		$data['march'] = $this->getQuery($year, "3")->get();
		$data['april'] = $this->getQuery($year, "4")->get();
		$data['may'] = $this->getQuery($year, "5")->get();
		$data['june'] = $this->getQuery($year, "6")->get();
		$data['july'] = $this->getQuery($year, "7")->get();
		$data['august'] = $this->getQuery($year, "8")->get();
		$data['september'] = $this->getQuery($year, "9")->get();
		$data['october'] = $this->getQuery($year, "10")->get();
		$data['november'] = $this->getQuery($year, "11")->get();
		$data['december'] = $this->getQuery($year, "12")->get();

		$total = $this->getQuery($year, null)->get();

		if($request->year){
			return view('outstanding_receiveable.outstanding_receiveable_content', compact('data', 'total', 'year'));
		}else{
			return view('outstanding_receiveable.outstanding_receiveable', compact('data', 'total', 'year'));
		}
	}

	function downloadOutstandingReceiveable(Request $request, $month, $year){
		$sub2 = DB::query()->fromSub(function ($query) {
	    				$query
	    					->selectRaw('invoice_sales.*, date_sales_payment as lastpaid')
	    					->from('invoice_sales')
	    					->join('payment_sales', 'invoice_sales.payment_sales_id', '=', 'payment_sales.payment_sales_id')
	    					->whereIn('invoice_sales_id', function($query2)
						    {
						        $query2->selectRaw('MAX(invoice_sales_id)')
						              ->from('invoice_sales')
						              ->whereNotNull('payment_sales_id')
						              ->groupBy('sales_order_id');
						    });
	    				}, 'sub');

		$report = $this->getQuery($year, $month)
					->with(['product.category', 'product.brand', 'product.type'])
					->selectRaw('invoice_sales.*, SUM(invoice_sales.total_amount) as total_receiveable, unpaid.due_date as upcoming_due, COALESCE(unpaid.unpaid_amount, 0) as unpaid_amount, COALESCE(paid.paid_amount, 0) as paid_amount, lastpaid.lastpaid, lastpaid.term_number as last_paid_term')
					->leftjoinSub($sub2, 'lastpaid', function($join){
						$join->on('lastpaid.sales_order_id','=','invoice_sales.sales_order_id');
					})
					->get();

		// return view('outstanding_receiveable.outstanding_receiveable_excel', compact('report'));
		Excel::create('Report Outstanding Receiveable '.$month.' / '.$year, function($excel) use($report){
			$excel->sheet('Report Outstanding Receiveable', function($sheet) use ($report){
				$sheet->loadView('outstanding_receiveable.outstanding_receiveable_excel', ['report'=>$report]);
			});
		})->download('xls');
	}
}