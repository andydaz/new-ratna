<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Indonesia;
use App\bank;
use App\supplier;
use App\target;
use App\userbank;

class SupplierController extends Controller
{
    public function showSupplierPage()
    {
    	$provinsi = Indonesia::allProvinces();
    	$bank = bank::where('status',1)
				->get();

    	$supplier = supplier::all();

    	return view('supplier.supplier', compact('provinsi','bank', 'supplier'));
    }

    public function createSupplier(Request $request)
    {
    	$supplier = new supplier;
    	$supplier->account_id = Session::get('user')->account_id;
    	$supplier->city_id = $request->kota;
    	$supplier->first_name = $request->firstname;
    	$supplier->last_name = $request->lastname;
    	$supplier->company_name = $request->perusahaan;
    	$supplier->address = $request->alamat;
    	$supplier->phone = $request->telp1;
    	$supplier->phone2 = $request->telp2;
    	$supplier->fax = $request->fax;
    	$supplier->bank = $request->bank;
    	$supplier->account_number = $request->norek;
    	$supplier->on_behalf_of = $request->atasnama;
    	$supplier->status = 1;
    	$supplier->save();
    }
    public function filterSupplier(Request $request){
    	$filter = $request->filter;
    	$supplier = supplier::where('company_name','like','%'.$filter.'%')->paginate(10);

    	return view('supplier.supplier_table', compact('supplier'));
    }

    public function getSupplierData(Request $request){
    	$id = $request->id;
    	$supplier = supplier::leftjoin('user_bank as ub','ub.customer_id','=','supplier.supplier_id')
    		->leftjoin('indonesia_cities as ic','ic.id','=','supplier.city_id')
    		->leftjoin('indonesia_provinces as ip','ip.id','=','ic.province_id')
    		->where('supplier.supplier_id',$id)
    		->select('supplier.*','ub.user_bank_id','ip.id as provinsi')
    		->first();

    	return compact('supplier');
    }

    public function modalSupplierData(Request $request){
        $id= $request->id;
        $data= supplier::where('supplier_id',$id)->first();
        return view('supplier.modal_supplier', compact('data'));
    }

    public function deleteSupplier(Request $request)
    {
        $id = $request->id;
        supplier::where('supplier_id',$id)->delete();
    }

    public function updateSupplier(Request $request)
    {
        $id = $request->id;
        supplier::where('supplier_id', $id)->update(
            [
            'account_id' => "1",
            'city_id' => $request->kota,
            'company_name' => $request->perusahaan,
            'address' => $request->alamat,
            'first_name' => $request->firstname,
            'last_name' => $request->lastname,
            'phone' => $request->telp1,
            'phone2' => $request->telp2,
            'fax' => $request->fax,
            'bank' => $request->bank,
            'account_number' => $request->norek,
            'on_behalf_of' => $request->atasnama,
            ]
        );
        // userbank::where('customer_id',$id)->where('category_id',2)->update(
        //     [
        //         'bank_id' => $request->bank,
        //         'bank_account' => $request->norek,
        //         'on_behalf_of' => $request->atasnama,
        //     ]
        //);
    }

    public function showTargetPage()
    {
        $target = supplier::with('target')->has('target')->get();

        $supplier = supplier::doesnthave('target')->get();
        return view('supplier.set_target', compact('target','supplier'));
    }

    public function createTarget(Request $request)
    {
        $target = new target;
        $target->supplier_id = $request->supplier;
        $target->target_nominal = $request->nominal;
        $target->date_start = date('Y-m-d', strtotime($request->tglawal));
        $target->date_end = date('Y-m-d', strtotime($request->tglakhir));
        $target->status = 1;
        $target->save();
    }
}