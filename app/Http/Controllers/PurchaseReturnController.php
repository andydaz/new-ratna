<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\purchaseorder;
use App\purchasereturn;
use App\purchaseinvoice;
use App\goodreceive;
use App\detailgoodreceive;
use App\warehouse;
use App\detailpurchasereturn;
use App\detailpurchaseorder;
use App\product;
use App\returtype;
use App\supplier;
use DB;
use DataTable;

class PurchaseReturnController extends Controller
{
	public function showPurchaseReturnPage()
	{
		//tampilkan semua good receive.
		$data['goodreceive'] = goodreceive::whereRaw("good_receive_date between DATE_ADD(NOW(), INTERVAL -2 MONTH) and Now()")
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
			->whereExists(function ($query) {
				$query->select(DB::raw(1))
					->from('good_receive_details')
					->where('is_retur',0)
					->whereRaw('good_receive_details.good_receive_id = good_receive.good_receive_id');
			})
			->get();

		$data['returtype'] = returtype::where('retur_for',0)->where('retur_type_id',4)->get();
		$data['warehouse'] = warehouse::where('status','<>',2)->get();

		$data['retur'] = purchasereturn::leftjoin('good_receive','good_receive.good_receive_id','=','purchase_retur.good_receive_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->where('purchase_retur.status','<>',2)
			->select('purchase_retur.*','supplier.company_name','purchase_order.purchase_order_number')
			->orderBy('purchase_retur_id','desc')
			->get();

		return view('purchase_return.purchase_return', compact('data'));
	}

	public function getPurchaseReturnTable(Request $request)
	{
		$return = purchasereturn::leftjoin('good_receive','good_receive.good_receive_id','=','purchase_retur.good_receive_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->where('purchase_retur.status','<>',2)
			->select('purchase_retur.*','supplier.company_name','purchase_order.purchase_order_number')
			->orderBy('purchase_retur_id','desc');

		if($request->number && $request->number != "")
		{
			$return = $return->where('purchase_retur.retur_number', 'like', '%'.$request->number.'%');
		}
		if($request->supplier && $request->supplier != "")
		{
			$return = $return->where('supplier.company_name', 'like', '%'.$request->supplier.'%');
		}
		if($request->po && $request->po != "")
		{
			$return = $return->where('purchase_order.purchase_order_number', 'like', '%'.$request->po.'%');
		}
		$return = $return->get();

		return DataTable::of($return)
			->setRowAttr([
				'value' => function($return){
					return $return->purchase_retur_id;
				},
			])
			->addColumn('action', function ($return){
				if(Session('roles')->name == 'master') {
					return
						'<a class="btn btn-sm btn-raised red delete-modal tooltipped" data-position="bottom" data-tooltip="delete"><i class="material-icons">delete</i></a>';
				}
			})
			->smart(false)
			->make(true);
	}

	public function getLastReturNumber(Request $request)
	{
		$lastreturntoday = purchasereturn::where('retur_number','like','PR/'.date('dmy').'/%')->orderby('purchase_retur_id','desc')->first();
		if(empty($lastreturntoday))
		{
			$newreturnnumber = "PR/".date('dmy')."/1";
			return $newreturnnumber;
		}
		else{
			$tmpreturn = explode('/',$lastreturntoday->retur_number);
			$lastnumber = $tmpreturn[2];
			$newreturnnumber = "PR/".date('dmy')."/".($lastnumber+1);
			return $newreturnnumber;
		}
	}

	public function getGoodReceiveData(Request $request)
	{
		if($request->id == 0)
		{
			return;
		}
		$data['goodreceive'] = goodreceive::leftjoin('good_receive_details','good_receive.good_receive_id','=','good_receive_details.good_receive_id')
			->join('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
			->join('supplier','purchase_order.supplier_id','=','supplier.supplier_id')
			->join('purchase_order_details','good_receive_details.purchase_order_details_id','=','purchase_order_details.purchase_order_details_id')
			->join('product_category','product_category.product_category_id','purchase_order_details.product_category_id')
			->join('product_brand','product_brand.product_brand_id','purchase_order_details.product_brand_id')
			->join('product_type','product_type.product_type_id','purchase_order_details.product_type_id')
			->where('good_receive.good_receive_id',$request->id)
			->where('purchase_order_details.is_received','<>',0)
			->where('good_receive_details.is_retur','<>',1)
			->select('good_receive.*','good_receive_details.*','purchase_order_details.*','product_category.description as c_name','product_brand.name as b_name','product_type.name as t_name','supplier.company_name', 'purchase_order.notes as po_notes')
			->get();

		//check apakah sudah ada invoice yang dikeluarkan untuk po itu, kalau ada retur type potong invoice tidak ditampilkan
		$poid =  goodreceive::find($request->id)->purchase_order_id;

		$invoice = purchaseinvoice::where('purchase_order_id', $poid)
			->where('status','<>',2)
			->count();

		$potype = purchaseorder::find($poid)->payment_type_id;

		if($invoice > 0 && $potype == 2)
		{
			// return semua retur type kecuali potong invoice
			$data['returtype'] = returtype::where('retur_for',0)->whereIn('retur_type_id', [4])->get();
		}else if($invoice > 0)
		{
			// return semua retur type kecuali potong invoice
			$data['returtype'] = returtype::where('retur_for',0)->whereIn('retur_type_id', [4,6])->get();
		}else{
			//return semua retur type
			$data['returtype'] = returtype::where('retur_for',0)->get();
		}
		return $data;
	}

	public function getPurchaseReturn(Request $request)
	{
		$retur = purchasereturn::with('details.detailpo.category')
			->with('goodreceive.po.supplier')
			->with('details.detailpo.brand')
			->with('details.detailpo.type')
			->with('details.detailgr')
			->where('status',1)
			->where('purchase_retur.purchase_retur_id',$request->id)
			->first();

		return $retur;
	}

	public function createReturn(Request $request)
	{
		$return = new purchasereturn;
		$return->good_receive_id = $request->nogr;
		$return->account_id = Session::get('user')->account_id;
		$return->status_id = 1;
		$return->retur_number = $request->noretur;
		$return->total_price = $request->returvalue;
		$return->retur_type_id = $request->returtype;
		$return->date_retur = date('Y-m-d', strtotime($request->tglretur));
		$return->status = 1;
		$return->save();

		for ($i=0; $i < sizeof($request->id); $i++) {
			$podetail = detailgoodreceive::find($request->id[$i])->purchase_order_details_id;
			$detailreturn = new detailpurchasereturn;
			$detailreturn->purchase_retur_id = $return->purchase_retur_id;
			$detailreturn->good_receive_details_id = $request->id[$i];
			$detailreturn->purchase_order_details_id = $podetail;
			$detailreturn->status = 1;
			$detailreturn->save();
		}

		//check retur type kalau dia potong invoice maka tambah kolom reduction di po sesuai dengan jumlah retur
		if ($request->returtype == 3){
			echo $request->nogr;
			$po = goodreceive::where('good_receive_id', $request->nogr)->first()->purchase_order_id;
			purchaseorder::where('purchase_order_id',$po)->update([
				'reduction' => DB::raw('reduction+'.$request->returvalue),
			]);

			for ($i=0; $i < sizeof($request->id); $i++) {
				detailgoodreceive::where('good_receive_details_id',$request->id[$i])->update([
					'is_retur' => 1,
				]);
			}
		}
		//check retur type kalau dia kembalikan barang maka hapus product yang terkait dengan good receive tersebut kembalikan status is received jadi 0
		else if($request->returtype == 4){
			for ($i=0; $i < sizeof($request->id); $i++) {
				$podetail = detailgoodreceive::find($request->id[$i])->purchase_order_details_id;

				//ubah status retur, biar kalo good receive yang uda retur gak ditampilkan lagi di dropdown no gr
				detailgoodreceive::where('good_receive_details_id',$request->id[$i])->update([
					'is_retur' => 1,
				]);


				product::where('good_receive_details_id',$request->id)->where('status',1)->update([
					'status'=>4,
				]);

				//ubah status jadi 0 biar bisa muncul lagi di halaman good receive
				detailpurchaseorder::where('purchase_order_details_id',$podetail)->update([
					'is_received'=>0,
					'is_retur'=>1,
				]);
			}
			//check retur type kalau dia deposit maka tambahkan total deposit supplier
		}else if($request->returtype == 6){
			$supplier = goodreceive::join('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
				->where('good_receive.good_receive_id',$request->nogr)
				->first()
				->supplier_id;

			for ($i=0; $i < sizeof($request->id); $i++) {
				detailgoodreceive::where('good_receive_details_id',$request->id[$i])->update([
					'is_retur' => 1,
				]);
			}

			supplier::where('supplier_id',$supplier)->update([
				'deposit' => DB::raw('deposit+'.$request->returvalue),
			]);
		}
	}

	public function deleteReturn(Request $request)
	{
		$return = purchasereturn::find($request->id) ;

//		for ($i=0; $i < sizeof($request->id); $i++) {
//			$podetail = detailgoodreceive::find($request->id[$i])->purchase_order_details_id;
//			$detailreturn = new detailpurchasereturn;
//			$detailreturn->purchase_retur_id = $return->purchase_retur_id;
//			$detailreturn->good_receive_details_id = $request->id[$i];
//			$detailreturn->purchase_order_details_id = $podetail;
//			$detailreturn->status = 1;
//			$detailreturn->save();
//		}


		detailpurchasereturn::where('purchase_retur_id',$request->id)->update([
			'status' => 2,
		]);

		//inverse dari apa yang dilakukan saat create
		if ($return->retur_type_id == 3){
			$po = goodreceive::where('good_receive_id', $return->good_receive_id)->first()->purchase_order_id;
			purchaseorder::where('purchase_order_id',$po)->update([
				'reduction' => DB::raw('reduction-'.$return->total_price),
			]);

			for ($i=0; $i < sizeof($return->details); $i++) {
				detailgoodreceive::where('good_receive_details_id',$return->details[$i]->good_receive_details_id)->update([
					'is_retur' => 0,
				]);
			}
		}
		//check retur type kalau dia kembalikan barang maka hapus product yang terkait dengan good receive tersebut kembalikan status is received jadi 0
		else if($return->retur_type_id == 4){
			for ($i=0; $i < sizeof($return->details); $i++) {
				$podetail = detailgoodreceive::find($return->details[$i]->good_receive_details_id)->purchase_order_details_id;

				//ubah status retur, biar kalo good receive yang uda retur gak ditampilkan lagi di dropdown no gr
				detailgoodreceive::where('good_receive_details_id',$return->details[$i]->good_receive_details_id)->update([
					'is_retur' => 0,
				]);

				product::where('good_receive_details_id',$return->details[$i]->good_receive_details_id)->where('status',4)->update([
					'status'=>1,
				]);

				//ubah status jadi 0 biar bisa muncul lagi di halaman good receive
				detailpurchaseorder::where('purchase_order_details_id',$podetail)->update([
					'is_received'=>1,
					'is_retur'=>0,
				]);
			}
			//check retur type kalau dia deposit maka tambahkan total deposit supplier
		}else if($return->retur_type_id == 6){
			$supplier = goodreceive::join('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
				->where('good_receive.good_receive_id',$return->good_receive_id)
				->first()
				->supplier_id;

			for ($i=0; $i < sizeof($return->details); $i++) {
				detailgoodreceive::where('good_receive_details_id',$return->details[$i]->good_receive_details_id)->update([
					'is_retur' => 0,
				]);
			}

			supplier::where('supplier_id',$supplier)->update([
				'deposit' => DB::raw('deposit-'.$return->total_price),
			]);
		}

		$return->status = 2;
		$return->save();
	}
}