<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\customer;
use App\balancetransaction;
use App\paymentmethod;
use App\log;
use DB;

class BalanceManagementController extends Controller
{
	public function showBalanceManagementPage()
	{
		$data['transaction'] = balancetransaction::with('customer')
			->where('status',1)
			->orderBy('balance_transaction_id','desc')
			->get();

		$data['customer'] = customer::where('status','<>',2)->get();
		$data['paymentmethod'] = paymentmethod::where('payment_method_id','<>','2')->where('status',1)->get();
		return view('balance_management.balance_management', compact('data'));
	}

	public function getLastTransactionNumber()
	{
		$lasttransactiontoday = balancetransaction::where('balance_transaction_number','like','BT/'.date('dmy').'/%')->orderby('balance_transaction_id','desc')->first();
		if(empty($lasttransactiontoday))
		{
			$newtransactionnumber = "BT/".date('dmy')."/1";
			return $newtransactionnumber;
		}
		else{
			$tmptransaction = explode('/',$lasttransactiontoday->balance_transaction_number);
			$lastnumber = $tmptransaction[2];
			$newtransactionnumber = "BT/".date('dmy')."/".($lastnumber+1);
			return $newtransactionnumber;
		}
	}

	public function getSalesPayment(Request $request)
	{
		// $payment = salespayment::leftjoin('customer','customer.customer_id','payment_sales.customer_id')->select('payment_sales.*','customer.company_name','customer.total_balance')->where('payment_sales.payment_sales_id', $request->id)->first();
		// return $payment;
	}

	public function getCustomerBalance(Request $request)
	{
		if($request->id == 0)
		{
			return;
		}
		$customer = customer::where('customer_id', $request->id)->first();
		return $customer;
	}

	public function createBalanceTransaction(Request $request)
	{
		// return $request->all();
		$balancetransaction = new balancetransaction;
		$balancetransaction->customer_id = $request->customer;
		$balancetransaction->payment_method_id = $request->paymentmethod;
		$balancetransaction->balance_transaction_number = $request->nopayment;
		$balancetransaction->amount = $request->pembayaran;
		$balancetransaction->date_balance_transaction = date('Y-m-d',strtotime($request->tglpayment));
		$balancetransaction->status = 1;
		$balancetransaction->save();

		customer::where('customer_id',$request->customer)->update([
			'total_balance' => DB::raw('total_balance-'.$request->pembayaran),
		]);

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "menambahkan";
		$log->object = "Transaksi Saldo";
		$log->object_details = $balancetransaction->balance_transaction_number;
		$log->status = 1;
		$log->save();
	}

	public function deleteBalanceTransaction(Request $request)
	{
		$balancetransaction = balancetransaction::find($request->id);

		customer::where('customer_id',$balancetransaction->customer_id)->update([
			'total_balance' => DB::raw('total_balance+'.$balancetransaction->amount),
		]);

		$balancetransaction->status = 2;
		$balancetransaction->update();
	}
}