<?php

namespace App\Http\Controllers;

use App\paymentmethod;
use Illuminate\Http\Request;
use App\supplier;
use App\depositopayment;
use App\log;
use DB;

class DepositoPaymentController extends Controller
{
	public function showDepositoPaymentPage()
	{
		$data['payment'] = depositopayment::leftjoin('supplier','supplier.supplier_id','deposito_payment.supplier_id')
			->where('deposito_payment.status',1)
			->orderBy('deposito_payment_id',1)
			->get();
		$data['supplier'] = supplier::all();
		$data['paymentmethod'] = paymentmethod::where('payment_method_id','<>','2')->where('status',1)->get();
		return view('deposito_clearance.deposito_clearance', compact('data'));
	}

	public function getLastPaymentNumber()
	{
		$lastpaymenttoday = depositopayment::where('deposito_payment_number','like','DP/'.date('dmy').'/%')->orderby('deposito_payment_id','desc')->first();
		if(empty($lastpaymenttoday))
		{
			$newpaymentnumber = "DP/".date('dmy')."/1";
			return $newpaymentnumber;
		}
		else{
			$tmppayment = explode('/',$lastpaymenttoday->deposito_payment_number);
			$lastnumber = $tmppayment[2];
			$newpaymentnumber = "DP/".date('dmy')."/".($lastnumber+1);
			return $newpaymentnumber;
		}
	}

	public function getSalesPayment(Request $request)
	{
		// $payment = salespayment::leftjoin('customer','customer.customer_id','payment_sales.customer_id')->select('payment_sales.*','customer.company_name','customer.total_balance')->where('payment_sales.payment_sales_id', $request->id)->first();
		// return $payment;
	}

	public function getSupplierDeposit(Request $request)
	{
		if($request->id == 0)
		{
			return;
		}
		$supplier = supplier::where('supplier_id', $request->id)->first();
		return $supplier;
	}

	public function createDepositoPayment(Request $request)
	{
//		return $request->all();

		$depositopayment = new depositopayment;
		$depositopayment->supplier_id = $request->supplier;
		$depositopayment->payment_method_id = $request->metode;
		$depositopayment->deposito_payment_number = $request->nopayment;
		$depositopayment->payment_amount = $request->pembayaran;
		$depositopayment->date_deposito_payment = date('Y-m-d',strtotime($request->tglpayment));
		$depositopayment->status = 1;
		$depositopayment->save();

		supplier::where('supplier_id',$request->supplier)->update([
			'deposit' => DB::raw('deposit-'.$request->pembayaran),
		]);

		$log = new log;
		$log->actor = "andy.daz";
		$log->activity = "menambahkan";
		$log->object = "Deposit Payment";
		$log->object_details = $depositopayment->deposito_payment_number;
		$log->status = 1;
		$log->save();
	}

	public function deleteDepositoPayment(Request $request)
	{
		$depositopayment = depositopayment::find($request->id);

		supplier::where('supplier_id',$depositopayment->supplier_id)->update([
			'deposit' => DB::raw('deposit+'.$depositopayment->payment_amount),
		]);

		$depositopayment->status = 2;
		$depositopayment->update();
	}
}