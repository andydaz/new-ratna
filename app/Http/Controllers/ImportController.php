<?php

namespace App\Http\Controllers;

use App\customer;
use App\product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\productcategory;
use App\brandbarang;
use App\tipebarang;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\CustomerStateTrait;

class ImportController extends Controller
{
	//
	use CustomerStateTrait;

	public function showImportPage(){
		return view('import');
	}

	public function importCategory(Request $request){
		if($request->hasFile('category')){
			$path = $request->file('category')->getRealPath();
			$sheet1 = Excel::selectSheetsByIndex(0)->load($path, function($reader){})->get(); // untuk ambil sheet pertama

			if(!empty($sheet1) && $sheet1->count()){
				foreach ($sheet1 as $key => $value) {
					$category = new productcategory();
					$category->description = $value->product_category_description;
					$category->code = $value->kode;
					$category->status = 1;
					$category->save();
				}
			}
		}
	}

	public function importBrand(Request $request){
		if($request->hasFile('brand')){
			$path = $request->file('brand')->getRealPath();
			$sheet1 = Excel::selectSheetsByIndex(0)->load($path, function($reader){})->get(); // untuk ambil sheet pertama

			if(!empty($sheet1) && $sheet1->count()){
				foreach ($sheet1 as $key => $value) {
					$brand = new brandbarang();
					$brand->product_category_id = $value->product_category_id;
					$brand->name = $value->product_brand_name;
					$brand->code = $value->kode;
					$brand->status = 1;
					$brand->save();
				}
			}
		}
	}

	public function importType(Request $request){
		if($request->hasFile('type')){
			$path = $request->file('type')->getRealPath();
			$sheet1 = Excel::selectSheetsByIndex(0)->load($path, function($reader){})->get(); // untuk ambil sheet pertama

			if(!empty($sheet1) && $sheet1->count()){
				foreach ($sheet1 as $key => $value) {
					$type = new tipebarang();
					$type->product_brand_id = $value->product_brand_id;
					$type->name = $value->product_type_name;
					$type->status = 1;
					$type->save();
				}
			}
		}
	}

	public function importProduct(Request $request){
		if($request->hasFile('product')){
			$path = $request->file('product')->getRealPath();
			$sheet1 = Excel::selectSheetsByIndex(0)->load($path, function($reader){})->get(); // untuk ambil sheet pertama

			if(!empty($sheet1) && $sheet1->count()){
				foreach ($sheet1 as $key => $value) {
					$product = new product();
					$product->warehouse_id = 3;
					$product->product_category_id = $value->product_category_id;
					$product->product_brand_id = $value->product_brand_id;
					$product->product_type_id = $value->product_type_id;
					$product->price_buy = $value->price_buy;
					$product->price_sale = $value->price_sale;
					$product->profit_rate = ($value->price_sale - $value->price_buy) / $value->price_buy  * 100;
					$product->bar_code = $value->barcode;
					$product->status = 1;
					$product->save();
				}
			}
		}
	}

	public function importCustomer(Request $request){
		if($request->hasFile('customer')){
			$path = $request->file('customer')->getRealPath();
			$sheet1 = Excel::selectSheetsByIndex(0)->load($path, function($reader){})->get(); // untuk ambil sheet pertama

			if(!empty($sheet1) && $sheet1->count()){
				$customer = new customer();
				$customer->account_id = 9; //master
				$customer->customer_level_id = 1; //blue
				$customer->city_id = 3175; //jakarta utara
				$customer->first_name = "Tanpa Nama";
				$customer->last_name = "-";
				$customer->book_number = null;
				$customer->email = "-";
				$customer->nik = "-";
				$customer->address = '-';
				$customer->phone = 0 ;
				$customer->phone2 = null ;
				$customer->identity = null;
				$customer->existing_receiveable = 0;
				$customer->check_date = null;
				$customer->status = 1;
				$customer->save();
				$this->createCustomerState($customer->customer_id, 1, 'initial', Carbon::now()->addYears(1000)->toDateString());

				foreach ($sheet1 as $key => $value) {
					$customer = new customer();
					$customer->account_id = 9; //master
					$customer->customer_level_id = 1; //blue
					$customer->city_id = 3175; //jakarta utara
					$customer->first_name = $value->nama == ""? $value->telp1 == $value->telp2 ? : $value->telp1 : $value->nama;
					$customer->last_name = "";
					$customer->book_number = $value->no_buku;
					$customer->email = "";
					$customer->nik = "";
					$customer->nik = "";
					$customer->address = $value->alamat == ""? '-' : $value->alamat;
					$customer->phone = $value->telp1 == ""? 0 : $value->telp1;
					$customer->phone2 = $value->telp2 == ""? null : $value->telp2;
					$customer->identity = null;
 					$customer->existing_receiveable = $value->outstanding ==""? 0 : $value->outstanding;
					$customer->check_date = Carbon::now()->addYear()->toDateString();
					$customer->status = 1;
					$customer->save();

					$this->createCustomerState($customer->customer_id, 1, 'initial', Carbon::now()->addYear()->toDateString());
				}
			}
		}
	}
	public function updateCustomer(Request $request){
		if($request->hasFile('update')){
			$path = $request->file('update')->getRealPath();
			$sheet1 = Excel::selectSheetsByIndex(0)->load($path, function($reader){})->get(); // untuk ambil sheet pertama

			if(!empty($sheet1) && $sheet1->count()){
				foreach ($sheet1 as $key => $value) {
						$id = (int)$value->id + 1;
						if(customer::where('customer_id', $id)->count() > 0)
						{
							$customer = customer::find($id);
							$customer->address = $value->alamat;
							$customer->phone = $value->telp1 == ""? 1 : $value->telp1;
							$customer->phone2 = $value->telp2 == ""? 0 : $value->telp2;
							$customer->update();
						}
				}
			}
		}
	}
}
