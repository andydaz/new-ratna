<?php
namespace App\Traits;
use App\customerstate;
use Carbon\Carbon;

trait CustomerStateTrait
{
	//
	public static function createCustomerState($id, $level, $type, $checkdate)
	{
		$state = new customerstate();
		$state->customer_id = $id;
		$state->customer_level_id = $level;
		$state->update_type = $type;
		$state->date_changed = Carbon::now()->toDateString();
		$state->next_check_date = $checkdate;
		$state->status = 1;
		$state->save();
	}
}