<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    //
    protected $table = 'role';
    protected $primaryKey='role_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}