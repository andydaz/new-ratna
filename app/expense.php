<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class expense extends Model
{
    //
    protected $table = 'expense';
    protected $primaryKey='expense_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function expense_category(){
    	return $this->hasOne('App\expensecategory','expense_category_id','expense_category_id');
		}

		public function payment_method(){
    	return $this->hasOne('App\paymentmethod','payment_method_id','payment_method_id');
		}
}