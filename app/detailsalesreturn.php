<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailsalesreturn extends Model
{
    //
    protected $table = 'sales_retur_details';
    protected $primaryKey='sales_retur_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function detaildelivery(){
    	return $this->hasOne('App\detaildeliveryorder','delivery_order_details','delivery_order_details')
				->leftjoin('sales_order_details','delivery_order_details.sales_order_details_id','=','sales_order_details.sales_order_details_id');;
		}
}