<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipebarang extends Model
{
    //
    protected $table = 'product_type';
    protected $primaryKey='product_type_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function brand(){
    	return $this->belongsTo('App\brandbarang','product_brand_id','product_brand_id');
    }
}