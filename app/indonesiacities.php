<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class indonesiacities extends Model
{
    //
    protected $table = 'indonesia_cities';
    protected $primaryKey='id'; 
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}