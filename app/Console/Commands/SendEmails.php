<?php

namespace App\Console\Commands;

use App\customer;
use App\DripEmailer;
use App\salesinvoice;
use Illuminate\Support\Facades\Mail;

use Illuminate\Console\Command;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email to a.darmawan@altechomega.com';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
			$salesinvoice = salesinvoice::with('so')
				->where('invoice_sales_id',1)
				->first();

			$email = 'f.linardi@altechomega.com';

			Mail::send('emails.invoice', ['salesinvoice'=> $salesinvoice], function($message) use ($email){
				$message->from('a.darmawan@altechomega.com','No Reply');
				$message->to($email);
				$message->subject('Jatuh Tempo Reminder');
			});

    }
}
