<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class followup extends Model
{
    //
    protected $table = 'follow_up_notes';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function so(){
    	return $this->hasOne('App\salesorder','sales_order_id','sales_order_id');
	}

	public function invoice(){
    	return $this->hasOne('App\salesinvoice','invoice_sales_id','invoice_sales_id');
	}
}