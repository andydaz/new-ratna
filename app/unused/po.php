<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class po extends Model
{
    //
    protected $table = 'po_h';
    public $incrementing = false;
    protected $primaryKey='Kode';
    public $timestamps = false;

    public static function insertdata($data, $po)
    {
    	$rowcount = DB::table('po_h')->where('Kode',$po)->count();
    	if($rowcount == 0)
    	{
    		DB::table('po_h')->insert($data);
    		return "ok";
    	}
    	else{
    		return "gagal";
    	}
    }
    public static function updatedata($data, $po)
    {
        DB::table('po_h')->where('Kode',$po)->update($data);
        return "ok";
    }
    public static function deletedata($po)
    {
        DB::table('po_h')->where('Kode',$po)->delete();
    }
}