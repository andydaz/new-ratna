<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class kontrak extends Model
{
    //
    protected $table = 'kontrak_h';
    public $incrementing = false;
    protected $primaryKey='Kode';
    public $timestamps = false;

    public static function insertdata($data, $po)
    {
    	$rowcount = DB::table('kontrak_h')->where('Kode',$po)->count();
    	if($rowcount == 0)
    	{
    		DB::table('kontrak_h')->insert($data);
    		return "ok";
    	}
    	else{
    		return "gagal";
    	}
    }
    public static function updatedata($data, $po)
    {
        DB::table('kontrak_h')->where('Kode',$po)->update($data);
        return "ok";
    }
    public static function deletedata($po)
    {
        DB::table('kontrak_h')->where('Kode',$po)->delete();
    }
}