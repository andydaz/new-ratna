<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barangrusak extends Model
{
    //
    protected $table = 'barangrusak';
    public $incrementing = false;
    protected $primaryKey='No';
    public $timestamps = false;
}