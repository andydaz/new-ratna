<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class get_id extends Model
{
    //
	public static function getkontrak(){
		$result = DB::table('kontrak_h')
		->select('Kode')
		->whereraw("Kode  like CONCAT('KS/',DATE_FORMAT(NOW(), '%y%m%d'),'/%')")
		->orderby('Kode','desc')
		->limit(1)
		->get();

		return $result;
	}
	/*"select Kode from invoice_h where Kode  like CONCAT('IN/',DATE_FORMAT(NOW(), '%y%m%d'),'/%')  order by Kode desc limit 1"*/
	public static function getinvoice(){
		$result = DB::table('invoice_h')
		->select('Kode')
		->whereraw("Kode  like CONCAT('IN/',DATE_FORMAT(NOW(), '%y%m%d'),'/%')")
		->orderby('Kode','desc')
		->limit(1)
		->get();

		return $result;
	}
	/*"select Kode from invoice_h where Kode  like CONCAT('IN/',DATE_FORMAT(NOW(), '%y%m%d'),'/%')  order by Kode desc limit 1"*/
	public static function getpemesanan(){
		/*"select Kode from po_h where Kode  like CONCAT('MS/',DATE_FORMAT(NOW(), '%y%m%d'),'/%')  order by Kode desc limit 1");*/
		$result = DB::table('po_h')
		->select('Kode')
		->whereraw("Kode  like CONCAT('MS/',DATE_FORMAT(NOW(), '%y%m%d'),'/%')")
		->orderby('Kode','desc')
		->limit(1)
		->get();

		return $result;
	}
	public static function getpenerimaanbarang(){
		/*"select Kode from po_h where Kode  like CONCAT('MS/',DATE_FORMAT(NOW(), '%y%m%d'),'/%')  order by Kode desc limit 1");*/
		/*"select * from bpb_h order by no_bpb desc limit 1"*/
		$result = DB::table('bpb_h')
		->select('*')
		->orderby('no_bpb','desc')
		->limit(1)
		->get();

		return $result;
	}
	public static function getso(){
		/*"select Kode from po_h where Kode  like CONCAT('MS/',DATE_FORMAT(NOW(), '%y%m%d'),'/%')  order by Kode desc limit 1");*/
		/*"select * from bpb_h order by no_bpb desc limit 1"*/
		/*"select substring(No_Do,1,6) as No_Do from do_h where no_do  like CONCAT(DATE_FORMAT(NOW(), '%y'),'%') AND substring(No_Do,1,6) REGEXP '^[0-9]+$' order by no_do desc limit 1"*/
		$result = DB::table('do_h')
		->selectraw('substring(No_Do,1,6) as No_Do')
		->whereraw("no_do like CONCAT(DATE_FORMAT(NOW(), '%y'),'%') AND substring(No_Do,1,6) REGEXP '^[0-9]+$'")
		->orderby('no_do','desc')
		->limit(1)
		->get();

		return $result;
	}
	public static function getretur(){
		/*"select Kode from po_h where Kode  like CONCAT('MS/',DATE_FORMAT(NOW(), '%y%m%d'),'/%')  order by Kode desc limit 1");*/
		/*"select * from bpb_h order by no_bpb desc limit 1"*/
		/*"select substring(No_Do,1,6) as No_Do from do_h where no_do  like CONCAT(DATE_FORMAT(NOW(), '%y'),'%') AND substring(No_Do,1,6) REGEXP '^[0-9]+$' order by no_do desc limit 1"*/
		/*"select No_Do from retur_h order by No_Do desc limit 1"*/
		$result = DB::table('retur_h')
		->select('No_Do')
		->orderby('no_Do','desc')
		->limit(1)
		->get();

		return $result;
	}
}