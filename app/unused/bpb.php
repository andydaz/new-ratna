<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class bpb extends Model
{
    //
    protected $table = 'bpb_h';
    public $incrementing = false;
    protected $primaryKey='Kode';
    public $timestamps = false; 

    public static function insertdata($data, $bpb)
    {
    	$rowcount = DB::table('bpb_h')->where('No_Bpb',$bpb)->count();
    	if($rowcount == 0)
    	{
    		DB::table('bpb_h')->insert($data);
    		return "ok";
    	}
    	else{
    		return "gagal";
    	}
    }
    public static function updatedata($data, $bpb)
    {
        DB::table('bpb_h')->where('No_Bpb',$bpb)->update($data);
        return "ok";
    }
    public static function deletedata($bpb)
    {
        DB::table('bpb_h')->where('No_Bpb',$bpb)->delete();
        return "ok";
    }
    public static function updatebarangqty($kdbrg,$qty,$harga)
    {
    	/*"select Qty1,Berat,Harga_Beli from barang where Kode = '$kdbrg'"*/
    	$result = DB::table('barang')->select('Qty1','Berat','Harga_Beli')->where('Kode',$kdbrg)->get();
    	foreach ($result as $row)
		{
			$qtystok = $row->Qty1;
			$beratstok = $row->Berat;
			$hargastok = $row->Harga_Beli;
			
			$totalberatstok = $qtystok * $beratstok;
			$nilaistok = $totalberatstok * $hargastok;
			
			$beratbaru = $qty * $beratstok;
			$nilaibaru = $beratbaru * $harga;
			$hpp = ($nilaistok + $nilaibaru) / ($totalberatstok + $beratbaru);
			
			$qtylama = DB::table('barang')->where('Kode',$kdbrg)->first()->Qty1;
			$beratlama = DB::table('barang')->where('Kode',$kdbrg)->first()->QtyBerat;
			$log = "Insert Penerimaan (".date("Y-m-d H:i:s").") OLD : ".$hargastok." > ".$totalberatstok. " ; NEW : ".$harga. " > ".$beratbaru;

			$qtyakhir = $qtylama+$qty;
			$beratakhir = $beratlama+$beratbaru;
			DB::table('barang')
				->where('Kode',$kdbrg)
				->update(["Qty1" => $qtyakhir, "QtyBerat" =>$beratakhir, "log" => $log, "Harga_Beli"=>$hpp]);
		}
    }
    public static function updatebaranginvoice($kdbrg,$qty,$po,$arrNoDetail)
    {
    	$qtytemplama = DB::Table('invoice_d')->whereraw("Kode_barang = '$kdbrg' AND Kode_po = '$po' AND No = $arrNoDetail")->first()->QtyTemp;
    	$qtytempakhir = $qtytemplama - $qty;

    	DB::table('invoice_d')
				->whereraw("Kode_barang = '$kdbrg' AND Kode_po = '$po' AND No = $arrNoDetail")
				->update(["Qtytemp" => $qtytempakhir]);
    }
    public static function updatebarangpo($kdbrg,$qty,$po)
    {
    	$qtytemplama = DB::Table('po_d')->whereraw("Kode_barang = '$kdbrg' AND Kode_po = '$po'")->first()->QtyTemp;
    	$qtytempakhir = $qtytemplama - $qty;

    	DB::table('po_d')
				->whereraw("Kode_barang = '$kdbrg' AND Kode_po = '$po'")
				->update(["Qtytemp" => $qtytempakhir]);
    }
    public static function cancelbarang($kdbrg,$qty,$harga)
    {
        /*"select Qty1,Berat,Harga_Beli from barang where Kode = '$kdbrg'"*/
        $result = DB::table('barang')->select('Qty1','Berat','Harga_Beli')->where('Kode',$kdbrg)->get();
        foreach ($result as $row)
        {
            $qtystok = $row->Qty1;
            $beratstok = $row->Berat;
            $hargastok = $row->Harga_Beli;
            
            $totalberatstok = $qtystok * $beratstok;
            $nilaistok = $totalberatstok * $hargastok;
            
            $beratbaru = $qty * $beratstok;
            $nilaibaru = $beratbaru * $harga;
            $hpp = 0;

            $nilaiakhir = $nilaistok - $nilaibaru;
            $beratakhir = $totalberatstok - $beratbaru;

            if($nilaiakhir > 0 && $beratakhir > 0){
                $hpp = ($nilaistok - $nilaibaru) / ($totalberatstok - $beratbaru);
            }else{
                $hpp = 0;
            }
            
            $qtylama = DB::table('barang')->where('Kode',$kdbrg)->first()->Qty1;
            $beratlama = DB::table('barang')->where('Kode',$kdbrg)->first()->QtyBerat;
            $log = "Delete Penerimaan (".date("Y-m-d H:i:s").") OLD : ".$hargastok." > ".$totalberatstok. " ; NEW : ".$harga. " > ".$beratbaru;

            $qtyakhir = $qtylama-$qty;
            $beratakhir = $beratlama-$beratbaru;

                DB::table('barang')
                ->where('Kode',$kdbrg)
                ->update(["Qty1" => $qtyakhir, "QtyBerat" =>$beratakhir, "log" => $log, "Harga_Beli"=>$hpp]);
        }
    }
    public static function cancelbaranginvoice($kdbrg,$qty,$po,$arrNoDetail)
    {
        $qtytemplama = DB::Table('invoice_d')->whereraw("Kode_barang = '$kdbrg' AND Kode_po = '$po' AND No = $arrNoDetail")->first()->QtyTemp;
        $qtytempakhir = $qtytemplama + $qty;

        DB::table('invoice_d')
                ->whereraw("Kode_barang = '$kdbrg' AND Kode_po = '$po' AND No = $arrNoDetail")
                ->update(["Qtytemp" => $qtytempakhir]);
    }
    public static function cancelbarangpo($kdbrg,$qty,$po)
    {
        $qtytemplama = DB::Table('po_d')->whereraw("Kode_barang = '$kdbrg' AND Kode_po = '$po'")->first()->QtyTemp;
        $qtytempakhir = $qtytemplama + $qty;

        DB::table('po_d')
                ->whereraw("Kode_barang = '$kdbrg' AND Kode_po = '$po'")
                ->update(["Qtytemp" => $qtytempakhir]);
    }

}