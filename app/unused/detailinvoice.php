<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class detailinvoice extends Model
{
    //
    protected $table = 'invoice_d';
    public $incrementing = false;
    protected $primaryKey='No';
    public $timestamps = false;

    public static function deletedata($kode)
    {
    	DB::table('invoice_d')->where('Kode_po',$kode)->delete();
    }
    public static function insertdata($datadet, $kode)
    {
/*    	$update=array
            (
                'Kode_barang' 	=>$datadet['Kode_barang'],
                'no_kontrak'    =>$datadet['no_kontrak'],
                'Jumlah'		=>$datadet['Jumlah'],
				'QtyTemp'		=>$datadet['Jumlah'],
				'QtySJ'			=>$datadet['Jumlah'],
				'QtyBerat'		=>$datadet['QtyBerat'],
                'Harga'			=>$datadet['Harga'],
                'Nilai'			=>$datadet['Nilai'],   
				'Harga_Jual'	=>$datadet['Harga_Jual']
            );
            $update['Kode_po'] = $kode;*/
            $datadet['Kode_po'] = $kode;
    	DB::table('invoice_d')->insert($datadet); 
    	return "updated";
    }
}