<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class retur extends Model
{
    //
    protected $table = 'retur_h';
    public $incrementing = false;
    protected $primaryKey='No_Do';
    public $timestamps = false;

    public static function insertdata($data, $kode)
    {
    	$rowcount = DB::table('retur_h')->where('No_Do',$kode)->count();
    	if($rowcount == 0)
    	{
    		DB::table('retur_h')->insert($data);
    		return "ok";
    	}
    	else{
    		return "gagal";
    	}
    	
    }
    public static function deletedata($kode)
    {
        DB::table('retur_h')->where('No_Do',$kode)->delete();
        return "ok";
    }

    public static function updatebarang($kdbrg,$qty,$berat,$hpp){
			/*$rr=$this->db->query("select Qty1,Berat,Harga_Beli from barang where Kode = '$kdbrg'");
			$totalberatstok = 0;
			$hargastok = 0;
           foreach ($rr->result() as $row)
			{
				$qtystok = $row->Qty1;
				$beratstok = $row->Berat;
				$hargastok = $row->Harga_Beli;
				
				$totalberatstok = $qtystok * $beratstok;

			}
			//$this->db->set('QtyOp', "QtyOp + '$qty'", FALSE);
			
			$this->db->set('Qty1', "Qty1 + '$qty'", FALSE);
			$this->db->set('QtyBerat', "QtyBerat + '$berat'", FALSE); // tambah berat total
			$this->db->set('Harga_Beli', "'$hpp'", FALSE);// set hpp
			
			$log = "Retur Insert (".date("Y-m-d H:i:s").") OLD : ".$hargastok." > ".$totalberatstok. " ; NEW : ".$hpp. " > ".$berat;
			$this->db->set('log', $log);
			$where = "Kode = '$kdbrg' ;";
			
			$this->db->where($where);
			$this->db->update('barang');*/

			$result = DB::table('barang')->select('Qty1','Berat','Harga_Beli')->where('Kode',$kdbrg)->get();
    	foreach ($result as $row)
		{
			$qtystok = $row->Qty1;
			$beratstok = $row->Berat;
			$hargastok = $row->Harga_Beli;
			
			$totalberatstok = $qtystok * $beratstok;
			/*$nilaistok = $totalberatstok * $hargastok;*/
			
			/*$beratbaru = $qty * $beratstok;
			$nilaibaru = $beratbaru * $harga;
			$hpp = ($nilaistok + $nilaibaru) / ($totalberatstok + $beratbaru);*/
			
			$qtylama = DB::table('barang')->where('Kode',$kdbrg)->first()->Qty1;
			$beratlama = DB::table('barang')->where('Kode',$kdbrg)->first()->QtyBerat;
			$log = "Retur Insert (".date("Y-m-d H:i:s").") OLD : ".$hargastok." > ".$totalberatstok. " ; NEW : ".$hpp. " > ".$berat;

			$qtyakhir = $qtylama+$qty;
			$beratakhir = $beratlama+$berat;
			DB::table('barang')
				->where('Kode',$kdbrg)
				->update(["Qty1" => $qtyakhir, "QtyBerat" =>$beratakhir, "log" => $log, "Harga_Beli"=>$hpp]);
		}
	}

	public static function updatebarangso($kdbrg,$qty,$so)
    {
    	/*$this->db->set('QtyTemp', "QtyTemp - '$qty'", FALSE);

			$where = "Kode_Brg = '$kdbrg' AND No_Do = '$po';";
			
			$this->db->where($where);
			$this->db->update('do_d');*/
    	$qtytemplama = DB::Table('do_d')->whereraw("Kode_Brg = '$kdbrg' AND No_Do = '$so'")->first()->QtyTemp;
    	$qtytempakhir = $qtytemplama - $qty;

    	DB::table('do_d')
				->whereraw("Kode_Brg = '$kdbrg' AND No_Do = '$so'")
				->update(["Qtytemp" => $qtytempakhir]);
    }
    public static function cancelbarang($kdbrg,$qty,$berat,$hpp)
    {
        /*"select Qty1,Berat,Harga_Beli from barang where Kode = '$kdbrg'"*/
        $result = DB::table('barang')->select('Qty1','Berat','Harga_Beli')->where('Kode',$kdbrg)->get();
        foreach ($result as $row)
        {
            $qtystok = $row->Qty1;
            $beratstok = $row->Berat;
            $hargastok = $row->Harga_Beli;
            
            $totalberatstok = $qtystok * $beratstok;
            /*$nilaistok = $totalberatstok * $hargastok;
            
            $beratbaru = $qty * $beratstok;
            $nilaibaru = $beratbaru * $harga;
            $hpp = 0;

            $nilaiakhir = $nilaistok - $nilaibaru;
            $beratakhir = $totalberatstok - $beratbaru;

            if($nilaiakhir > 0 && $beratakhir > 0){
                $hpp = ($nilaistok - $nilaibaru) / ($totalberatstok - $beratbaru);
            }else{
                $hpp = 0;
            }
            */
            
            $qtylama = DB::table('barang')->where('Kode',$kdbrg)->first()->Qty1;
            $beratlama = DB::table('barang')->where('Kode',$kdbrg)->first()->QtyBerat;
            $log = "Retur Delete (".date("Y-m-d H:i:s").") OLD : ".$hargastok." > ".$totalberatstok. " ; NEW : ".$hpp. " > ".$berat;

            $qtyakhir = $qtylama-$qty;
            $beratakhir = $beratlama-$berat;

                DB::table('barang')
                ->where('Kode',$kdbrg)
                ->update(["Qty1" => $qtyakhir, "QtyBerat" =>$beratakhir, "log" => $log, "Harga_Beli"=>$hpp]);
        }
    }
    public static function cancelbarangso($kdbrg,$qty,$so)
    {
        $qtytemplama = DB::Table('do_d')->whereraw("Kode_Brg = '$kdbrg' AND No_Do = '$so'")->first()->QtyTemp;
        $qtytempakhir = $qtytemplama + $qty;

        DB::table('do_d')
                ->whereraw("Kode_Brg = '$kdbrg' AND No_Do = '$so'")
                ->update(["Qtytemp" => $qtytempakhir]);
    }
}