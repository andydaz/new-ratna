<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class detailkontrak extends Model
{
    //
    protected $table = 'kontrak_d';
    public $incrementing = false;
    protected $primaryKey='No';
    public $timestamps = false;

    public static function deletedata($kode)
    {
    	DB::table('kontrak_d')->where('Kode_po',$kode)->delete();
    }
    public static function insertdata($datadet, $kode)
    {
    	$update=array
            (
                'Kode_barang' 	=>$datadet['Kode_barang'],
                'Jumlah'		=>$datadet['Jumlah'],
				'QtyTemp'		=>$datadet['Jumlah'],
				'QtySJ'			=>$datadet['Jumlah'],
				'QtyBerat'		=>$datadet['QtyBerat'],
                'Harga'			=>$datadet['Harga'],
                'Nilai'			=>$datadet['Nilai'],   
				'Harga_Jual'	=>$datadet['Harga_Jual']
            );
            $update['Kode_po'] = $kode;
    	DB::table('kontrak_d')->insert($update); 
    	return "updated";
    }
    /*public static function updatedata($datadet,$kode)
    {
        $detail = DB::table('kontrak_d')->select('No')->where('Kode_po',$kode)->where('Kode_barang',$datadet['Kode_barang'])->limit(1);
        $result= $detail->get();
        $count = $detail->count();

        $update=array
            (
                'Kode_barang' 	=>$datadet['Kode_barang'],
                'Jumlah'		=>$datadet['Jumlah'],
				'QtyTemp'		=>$datadet['Jumlah'],
				'QtySJ'			=>$datadet['Jumlah'],
				'QtyBerat'		=>$datadet['QtyBerat'],
                'Harga'			=>$datadet['Harga'],
                'Nilai'			=>$datadet['Nilai'],   
				'Harga_Jual'	=>$datadet['Harga_Jual']
            );

        if($count==1)
        {
        	foreach($result as $row)
        	{
        		$result["No"] = $row->No;
            	$temp = $result["No"];	
        	}

            DB::table('kontrak_d')->where('Kode_po',$kode)->where('No',$temp)->update($update);
        }
        else{
        	$update['Kode_po'] = $kode;
        	DB::table('kontrak_d')->insert($update);
        }
    }*/
}