<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class detailbpb extends Model
{
    //
    protected $table = 'bpb_d';
    public $incrementing = false;
    protected $primaryKey='No_Bpb';
    public $timestamps = false; 

    public static function deletedata($kode)
    {
    	DB::table('bpb_d')->where('No_Bpb',$kode)->delete();
    }
    public static function insertdata($datadet)
    {

    	DB::table('bpb_d')->insert($datadet); 
    	/*return "updated";*/
    }
}