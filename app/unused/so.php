<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class so extends Model
{
    //
    protected $table = 'do_h';
    public $incrementing = false;
    protected $primaryKey='No_Do';
    public $timestamps = false; 

    public static function insertdata($data,$so)
    {
        $rowcount = DB::table('do_h')->where('No_Do',$so)->count();
    	if($rowcount == 0)
    	{
    		DB::table('do_h')->insert($data);
    		return "ok";
    	}
    	else{
    		return "gagal";
    	}
    }
    public static function updatedata($data,$so)
    {
    	DB::table('do_h')->where('No_Do',$so)->update($data);
    	return "ok";
    }
    public static function deletedata($so)
    {
            DB::table('do_h')->where('No_Do',$so)->delete();
            return 'ok';
    }
    public static function updatedatabaru($data,$so,$so2)
    {
    	$rowcount = DB::table('do_h')->where('No_Do',$so2)->count();
    	if($rowcount == 0)
    	{
    		DB::Table('do_h')->where('No_Do',$so)
    		->update($data);
    		return "ok";
    	}
    	else{
    		return 'gagal';
    	}
    }

    public static function updateterm($term,$pl)
    {
    	DB::Table('pelanggan')->where('Kode',$pl)->update($term);
    }

    public static function updatebarang($kdbrg,$qty,$berat)
    {
    	$qtylama = DB::Table('Barang')->where('Kode',$kdbrg)->first()->Qty1;
    	$qtyakhir = $qtylama-$qty;

    	DB::Table('Barang')
    	->where('Kode',$kdbrg)
    	->update(['Qty1'=>$qtyakhir]);
    }
    public static function cancelso($so)
    {
    	$result = DB::Table('do_d')->where('No_Do',$so)->get();
    	foreach($result as $row)
    	{
    		$kode = $row->Kode_Brg;
			$qty = $row->Qty;
			$berat = $row->Berat;

			$qtylama = DB::table('barang')->where('Kode',$kode)->first()->Qty1;
			$beratlama = DB::table('barang')->where('Kode',$kode)->first()->QtyBerat;

			$qtyakhir = $qtylama + $qty;
			$beratakhir = $beratlama + $berat;

			DB::table('barang')
			->where('Kode',$kode)
			->update(['Qty1'=>$qtyakhir, 'QtyBerat'=>$beratakhir]);

    	}
    }
}