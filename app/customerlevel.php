<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customerlevel extends Model
{
    //
    protected $table = 'customer_level';
    protected $primaryKey='customer_level_id'; 
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}