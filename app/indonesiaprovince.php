<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class indonesiaprovince extends Model
{
    //
    protected $table = 'indonesia_province';
    protected $primaryKey='id'; 
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}