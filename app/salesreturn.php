<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salesreturn extends Model
{
    //
    protected $table = 'sales_retur';
    protected $primaryKey='sales_retur_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function delivery(){
    	return $this->hasOne('App\deliveryorder','delivery_order_id', 'delivery_order_id');
    }

    public function details(){
    	return $this->hasMany('App\detailsalesreturn','sales_retur_id','sales_retur_id');
		}

//    public function alldata(){
//    	return $this->hasOne('App\deliveryorder', 'delivery_order_id', 'delivery_order_id')
//    				->leftjoin('sales_order','sales_order.sales_order_id','=','delivery_order.sales_order_id')
//    				->leftjoin('customer','customer.customer_id','=','sales_order.customer_id');
//    }


}