<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;

class customer extends Model
{
    //
    protected $table = 'customer';
    protected $primaryKey = 'customer_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    //Section for mutators
    public function getFullNameAttribute()
		{
			return "{$this->first_name} {$this->last_name}";
		}

		//Section for Relations
    public function so()
    {
        return $this->hasMany('App\salesorder', 'customer_id', 'customer_id')
            ->where('status', 1);
    }

    public function level()
    {
        return $this->hasOne('App\customerlevel', 'customer_level_id', 'customer_level_id')
            ->where('status', 1);
    }

    public function city()
    {
        return $this->hasOne('App\indonesiacities', 'customer_level_id', 'id');
    }

    public function payment()
    {
        return $this->hasManyThrough(
            'App\salespayment',
            'App\salesorder',
            'customer_id', // Foreign key on users table...
            'sales_order_id', // Foreign key on posts table...
            'customer_id', // Local key on countries table...
            'sales_order_id' // Local key on users table...
        );
    }

    public function unsettled_sales(){
        return $this->hasMany('App\salesorder', 'customer_id', 'customer_id')
            ->whereExists(function ($query) {
                $query->select(DB::raw(1),  DB::raw('SUM(total_paid) as total_payment'))
                    ->from('payment_sales')
                    ->leftjoin('payment_sales_details','payment_sales_details.payment_sales_id','=','payment_sales.payment_sales_id')
                    ->whereRaw('payment_sales.sales_order_id = sales_order.sales_order_id')
                    ->groupby('payment_sales.payment_sales_id')
										->where('payment_sales.status',1)
										->havingRaw("SUM(total_paid) < sales_order.grand_total_idr");
            })
            ->where('status', 1);
    }

	public function laststate(){
		return $this->hasOne('App\customerstate','customer_id','customer_id')
			->orderBy('customer_state_id','desc');
	}

	public function dueinvoice(){
    	return $this->hasManyThrough(
    		'App\salesinvoice',
				'App\salesorder',
				'customer_id',
				'sales_order_id',
				'customer_id',
				'sales_order_id'
				)->whereDate('invoice_sales.due_date','<',Carbon::now())
				->whereNull('invoice_sales.payment_sales_id')
				->where('invoice_sales.status',1);
	}
}