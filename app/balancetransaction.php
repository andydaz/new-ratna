<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class balancetransaction extends Model
{
    //
    protected $table = 'balance_transaction';
    protected $primaryKey='balance_transaction_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function customer(){
    	return $this->hasOne('App\customer', 'customer_id', 'customer_id');
    }
}