<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    //
    protected $table = 'category';
    protected $primaryKey='category_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}