<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchasereturn extends Model
{
    //
    protected $table = 'purchase_retur';
    protected $primaryKey='purchase_retur_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function details(){
    	return $this->hasMany('App\detailpurchasereturn','purchase_retur_id','purchase_retur_id');
		}
		public function goodreceive(){
    	return $this->belongsTo('App\goodreceive','good_receive_id','good_receive_id');
		}
}