<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchaseorderstate extends Model
{
    //
    protected $table = 'purchase_order_state';
    protected $primaryKey='purchase_order_state';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}